#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import csv
import sys
import gdocDownloader.gdoc2latex as gdoc
from wordpress_xmlrpc import Client, WordPressPage
from wordpress_xmlrpc.methods import posts
import unidecode

class session(object):
    'base class for individual sessions'

    def __init__(self,name = '',numberOfFilms = 0,description = '',runtime = '',rating = '',bestfor = '',streamingLink = ''):

        self.name = 'name'
        self.numberOfFilms = 0
        self.description = 'description'
        self.runtime = 'runtime'
        self.rating = 'Censorship ratings to be advised.'
        self.bestfor = ''
        self.streamingLink = ''

class locationObject(object):
    'base class for locations'

#    def __init__(self,place = '',dates = '',venue = '',venueInfo = ['','','','',''],priceInfo = ['','','','',''],venueLink = '',numberOfSessions=0,coord=[0.,0.,0.],onlineBooking=False):
    def __init__(self,place = '',dates = '',venue = '',street='',city='',phone='',email='',priceInfo = ['','','','','',''],venueLink = '',numberOfSessions=0,coord=[0.,0.,0.],notes = ['','','','','',''],onlineBooking=False):

        self.place = 'place'
        self.dates = 'dates'
        self.venue = 'venue'
        self.street = 'street'
        self.city = 'city'
        self.phone = ''        
        self.email = ''        
#        self.venueInfo = ['','','','','']
        self.priceInfo = ['','','','','','']
        self.venueLink = ''
        self.numberOfSessions = 0
        self.coord = [0.,0.,0.]
        self.notes = ['','','','','','']
        self.onlineBooking = False


class film(object):
    'class for individual films within sessions'

    def __init__(self,title = '',info1 = '',info2 = '',summary = '',credits = ['', '', '', ''] ,keyImage = '',trailerLink = '',makerLink = ''):

        self.title = 'title'
        self.info1 = 'info1'
        self.info2 = 'info2'
        self.summary = 'summary'
        self.credits = ['', '', '', '']
        self.keyImage = 'keyImage'
        self.trailerLink = 'trailerLink'
        self.makerLink = 'makerLink'

class scheduleObject(object):
    'class for location sessions'

    def __init__(self,date = '',time = '',sessionTitle = '',sessionLink = '',note='',bookingLink=''):

        self.date = 'date'
        self.time = 'time'
        self.sessionTitle = 'sessiontitle'
        self.sessionLink = 'sessionLink'
        self.note = ''
        self.bookingLink = ''


def linkFormat(oldString):
    newString = oldString.lower()
    newString = newString.replace('&','and')
    newString = newString.replace("'","")
    newString = newString.replace("’","")
    newString = newString.replace(',','')
    newString = newString.replace(';','')
    newString = newString.replace(' ','-')
    return(newString);

def inplace_change(filename, old_string, new_string):
        s=open(filename).read()
        if old_string in s:
                print('Changing "{old_string}" to "{new_string}"'.format(**locals()))
                s=s.replace(old_string, new_string)
                f=open(filename, 'w')
                f.write(s)
                f.flush()
                f.close()
        else:
                print('No occurances of "{old_string}" found.'.format(**locals()))
        
def createSession(session,filmList,programmePagePath,outputFolder):
    
    sessionName =  session.name.lower()
    sessionFileName = sessionName.replace(' ','-') 
    sessionFileName = sessionFileName.strip()
    sessionFileName = sessionFileName.replace("'",'')
    sessionFileName = sessionFileName.replace("’",'')
    sessionFileName = sessionFileName.replace('&','and') + '.php'#'.html'
    sessionFileName = outputFolder + sessionFileName

    f = open(sessionFileName, "w")
    print("writing to ", sessionFileName, "\n\n")

    #Session description, runtime, and rating
    line = '&nbsp;\n'
    f.write(line)    
    line = '<p>'+ session.description + '</p>\n\n'
    f.write(line)
    line = '<p>'+session.runtime + '<br>\n'
    f.write(line)
    line = session.rating + '</p>\n<br>&nbsp;\n\n'
    f.write(line)
    
    for filmIndex in range(session.numberOfFilms):

        #Film Title
        line = '<h2>'
        line += filmList[filmIndex].title
        line += '</h2>'
        f.write(line)

        #Key Image (with trailer button overlay if applicable)
        if filmList[filmIndex].trailerLink != '':
            line = '<a href="'
            line += filmList[filmIndex].trailerLink + '" target="_blank">\n'
            line += '<div id="overlaybase" class="alignright">\n'
            line += '  <img title="watch trailer" src="'
            line += filmList[filmIndex].keyImage
            line += '" target="_blank"/>\n'
            line += '  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>\n'
            line += '</div>\n'
            line += '</a>\n'
            f.write(line)            

        else:
            line = '<img class="overlaybase alignright " src="'
            line += filmList[filmIndex].keyImage
            line += '" />'
            line += '\n'
            f.write(line)

        #Film info and summary
        line = '<p>'+filmList[filmIndex].info1 + '</p>\n\n'
        if ("NZ Premiere" in line) or ("New Zealand Premiere" in line):
            line = line.replace("(","")
            line = line.replace(")","")            
            line = line.replace("NZ Premiere","\n<p><strong>New Zealand Premiere</strong></p>")
            line = line.replace("New Zealand Premiere","\n<p><strong>New Zealand Premiere</strong></p>")
        elif "World Premiere" in line:
            line = line.replace("(","")
            line = line.replace(")","")            
            line = line.replace("World Premiere","\n<p><strong>World Premiere</strong></p>")
        f.write(line)
        if filmList[filmIndex].info2 != '' :
            line = '<p>' + filmList[filmIndex].info2 + '</p>\n'
            f.write(line)
        for i in range(len(filmList[filmIndex].credits)):
            if (len(filmList[filmIndex].credits[i]) > 0) :
                line = '<p>' + filmList[filmIndex].credits[i] + '</p>\n'
                f.write(line)
        #line = '<div class="cf"></div>'
        #f.write(line)
        line = '<p>' + filmList[filmIndex].summary + '</p>\n'
        f.write(line)
        line = '<br style="clear:both;" />'
        f.write(line)

        if filmIndex != session.numberOfFilms-1:
            'Horizontal rule'
            line = '\n'
            line += '<hr class="sessionBreak">'
            line += '\n\n'
            f.write(line)


    # # Begin session by cinema
    # line = '\n\n<h2>Screenings</h2><br>&nbsp;'
    # f.write(line)
    
    # line = '<table id="table--pink-stripes" summary="' + tableSummary +  '">\n<tbody>\n'
    # f.write(line)    
    
    
    line = '\n<br>\n<div class="sms button" style="text-align: center; padding-top: 150px;"><h3><a class="sms button" href="'+programmePagePath+'">Back to Programme</a></h3></div>'
    f.write(line)
    f.close()
    return;

def createSessionWithScreenings(session,filmList,screeningList,
                                locationList,noteList,locationLinks,
                                programmePagePath,outputFolder):

    sessionName =  session.name.lower()
    sessionFileName = sessionName.replace(' ','-') 
    sessionFileName = sessionFileName.strip()
    sessionFileName = sessionFileName.replace("'",'')
    sessionFileName = sessionFileName.replace("’",'')
    sessionFileName = sessionFileName.replace("é",'%c3%a9')    
    sessionFileName = sessionFileName.replace('&','and') + '.php'#'.html'
    sessionFileName = outputFolder + sessionFileName    
    f = open(sessionFileName, "w")
    print("writing to ", sessionFileName, "\n\n")

    #Session description, runtime, and rating
    #line = '&nbsp;\n'
    #f.write(line)    
    line = '<p>'+ session.description + '</p>\n\n'
    f.write(line)
    line = '<p>'+session.runtime + '<br>\n'
    f.write(line)
    #line = session.rating + '</p>\n<br>&nbsp;\n\n'
    if 'online' not in sessionFileName:
        line = session.rating
        if session.bestfor != '':
            line += '\n' + session.bestfor
            line += '</p>\n\n'
        f.write(line)

    dropdownKey = 'DROPDOWN\n'
    line = dropdownKey
    f.write(line)    
    
    for filmIndex in range(session.numberOfFilms):

        #Film Title
        title = filmList[filmIndex].title
        #title.replace("(","<br>(")
        line = '<h2>'
        line += title #filmList[filmIndex].title
        if len(title) > 5:
            line = line.replace("(","<br>(")
        line += '</h2>'
        f.write(line)

        #Key Image (with trailer button overlay if applicable)
        if filmList[filmIndex].trailerLink != '':
            line = '<a href="'
            line += filmList[filmIndex].trailerLink + '" target="_blank">\n'
            line += '<div id="overlaybase" class="alignright">\n'
            line += '  <img title="watch trailer" src="'
            line += filmList[filmIndex].keyImage
            line += '" target="_blank"/>\n'
            line += '  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>\n'
            line += '</div>\n'
            line += '</a>\n'
            f.write(line)            

        else:
            line = '<img class="overlaybase alignright " src="'
            line += filmList[filmIndex].keyImage
            line += '" />'
            line += '\n'
            f.write(line)

        #Film info and summary
        line = '<p>'+filmList[filmIndex].info1 + '</p>\n\n'
        if ("NZ Premiere" in line) or ("New Zealand Premiere" in line):
            line = line.replace("(","")
            line = line.replace(")","")            
            line = line.replace("NZ Premiere","\n<p><strong>New Zealand Premiere</strong></p>")
            line = line.replace("New Zealand Premiere","\n<p><strong>New Zealand Premiere</strong></p>")        
        # if "NZ Premiere" in line:
        #     line = line.replace("(","")
        #     line = line.replace(")","")            
        #     line = line.replace("NZ Premiere","\n<p><strong>NZ Premiere</strong></p>")
        elif "World Premiere" in line:
            line = line.replace("(","")
            line = line.replace(")","")            
            line = line.replace("World Premiere","\n<p><strong>World Premiere</strong></p>")
        f.write(line)
        if filmList[filmIndex].info2 != '' :
            line = '<p>' + filmList[filmIndex].info2 + '</p>\n'
            f.write(line)
        line = '<p>'
        for i in range(len(filmList[filmIndex].credits)):
            if (len(filmList[filmIndex].credits[i]) > 0):
                line += filmList[filmIndex].credits[i] + '\n'
        if filmList[filmIndex].makerLink != '':
            line += '<a href="' + filmList[filmIndex].makerLink + '" target="_blank">'
            line += 'Special message from the filmmaker(s)</a>\n'
        line += '</p>'
        f.write(line)
        line = '<div class="cf"></div>'
        f.write(line)
        line = '<p>' + filmList[filmIndex].summary + '</p>\n'
        f.write(line)
        #line = '<br style="clear:both;" />'
        #f.write(line)

        if filmIndex != session.numberOfFilms-1:
            'Horizontal rule'
            line = '\n'
            line += '<hr class="sessionBreak">'
            line += '\n\n'
            f.write(line)

    # Begin session by cinema
    line = '<hr class="sessionBreak">'
    f.write(line)
    if 'online' not in sessionFileName:
        line = '\n\n<h2>Screenings</h2>\n'
        f.write(line)
    #line = '<div class="pinktable">\n'
    #f.write(line)    

    screeningLocations = []
    anchorLinks = []
    print('\n DETECTING session named: ' + session.name + '\n')
    for loc in range(len(locationList)):
        foundScreening = False
        sessionTitles = []
        for s in range(len(screeningList[loc])):
            title = screeningList[loc][s].sessionTitle
            note = ''
            if '*' in title:
                noteAst = title.count('*')
                for i in range(len(noteList[loc])):
                    noteAst2 = noteList[loc][i].count('*')
                    if noteAst ==noteAst2:
                        note = noteList[loc][i]
                        note = note.replace('**','*')
                        note = note.replace('***','*')
                title = title.replace('*','')
            #sessionTitles.append(screeningList[loc][s].sessionTitle)
            sessionTitles.append(title)
        print(locationList[loc].place + ' : ' + str(sessionTitles))
        #if session.name in screeningList[loc].sessionTitle:
        if session.name in sessionTitles:
            #anchor links for dropdown
            screeningLocation = locationList[loc].place+' - '+locationList[loc].venue
            screeningLocations.append(screeningLocation)
            anchorLink = locationList[loc].place.replace(' ','')
            anchorLinks.append(anchorLink)
            line = '<a name="'+anchorLink+'"></a>\n\n'
            f.write(line)
            # Screening location
            line = '<a href="'+locationLinks[loc]+'"><h3>'+screeningLocation+'</h3></a>\n'            
            f.write(line)
            # booking info
            line = '<p>('
            if locationList[loc].onlineBooking:
                line += 'Online bookings available as indicated below'
                line += ')</p>'
            elif location.email:
                line += 'To reserve tickets, email '+ location.email
                if location.phone:
                    line += ' or call '+ location.phone
                    line += ')</p>'
                    line.replace('Ph:','')                    
                    #f.write(line)                    
            else:
                line += 'To reserve tickets, call '+ location.phone
                line += ')</p>'
                line.replace('Ph:','')
                #f.write(line)

            # Table
            locationName = locationList[loc].place.lower()
            locationName = locationName.strip()
            tableSummary=locationName.replace(' ','-') + '-2020'
            tableSummary= session.name.lower().replace(' ','-') + '_' + tableSummary
            line = '<table id="table--pink-stripes" summary="' + tableSummary +  '">\n<tbody>\n'
            f.write(line)

            for screening in  screeningList[loc]:
                title = screening.sessionTitle
                titleName = title.replace('*','')
                if (session.name == titleName):
                #if (session.name == screening.sessionTitle):
                    line = '<tr>\n'
                    f.write(line)
                    line = '<td width="50"><p>'+ screening.date + '</p></td>\n'
                    f.write(line)
                    if screening.bookingLink != '':
                        line = '<td width="50"><p>'+ screening.time + '\n<strong><a href="' + screening.bookingLink +'">Book tickets</a></strong></p></td>\n'                        
                    else:
                        line = '<td width="50"><p>'+ screening.time + '</p></td>\n'
                    f.write(line)
                    if '*' in title:
                        noteAst = title.count('*')
                        for i in range(len(noteList[loc])):
                            noteAst2 = noteList[loc][i].count('*')
                            if noteAst ==noteAst2:
                                note = noteList[loc][i]
                                note = note.replace('**','*')
                                note = note.replace('***','*')
                                line = '<td width="50"><p>'+ note + '</p></td>\n'
                                f.write(line)
                    else:
                        line = '<td width="50"></td>\n'
                        f.write(line)            
                    line = '</tr>\n'
                    f.write(line)

            line = '</div>\n'
            f.write(line)
            line = '</tbody>\n</table>'
            f.write(line)                    
                    
    #line = '<div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="'+programmePagePath+'">Back to Programme</a></h3></div>'
    #
    if 'online' in sessionFileName:
        line = '<div style="padding-top:5rem;"><p>This collection of short films is available for New Zealanders to view online between 3-25 October. Ticket holders will be emailed a link and instructions for how to watch the films on Vimeo.</p></div>\n'
        f.write(line)
        if session.streamingLink != '':
            line = '\n<p>[button link="' + session.streamingLink + '" size="large" text_size="beta"]Tickets via Eventbrite[/button]</p>\n\n'
            f.write(line)

    line = '<div class="sms button" style="text-align: center;"><h3><a class="sms button" href="'+programmePagePath+'">Back to Programme</a></h3></div>'
    f.write(line)
    f.close()

    # This is a bit hacky...
    # Add in a location dropdown
    locList = []
    locationAlphaIndex = sorted(range(len(screeningLocations)), key=lambda k: screeningLocations[k])
    line = '<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">\n'
    line += '<option value="">(Select cinema for times and tickets...)</option>\n'
    j = -1
    for i in locationAlphaIndex:
        j+=1
        if j > 0:
            loc = screeningLocations[i]
            if loc == prevLoc:
                continue
        line += '<option value="#' + anchorLinks[i] + '">'+ screeningLocations[i] +'</option>\n'
        prevLoc = screeningLocations[i]
    line += '</select>\n'
    # Exclude dropdown from online sessions
    if 'online' in sessionFileName:
        line = '<p>This collection of short films is available for New Zealanders to view online between 3-25 October. Ticket holders will be emailed a link and instructions for how to watch the films on Vimeo.</p>\n'
        if session.streamingLink != '':
            line += '<p>[button link="' + session.streamingLink + '" size="large" text_size="beta"]Tickets via Eventbrite[/button]</p>\n\n'

    inplace_change(sessionFileName, dropdownKey, line)

    return;


def createLocation(location,scheduleList,notes,navMap,programmePagePath,outputFolder):
    
    locationName =  location.place.lower()
    locationName = locationName.strip()
    tableSummary=locationName.replace(' ','-') + '-2017'
    locationFileName = locationName.replace(' ','-') + '.php'#'.html'
    locationFileName = outputFolder + locationFileName

    f = open(locationFileName, "w")
    print("writing to " + locationFileName + '\n')

    #Session description, runtime, and rating
    line = '<h3 class="programme">' + location.dates + '</h3>\n'
    f.write(line)
    if location.venueLink != '':
        line = '<h2 class="programme"><a href="' + location.venueLink + '">'+ location.venue + '</a></h2>\n'
    else:
        line = '<h2 class="programme">'+ location.venue + '</h2>\n'
    f.write(line)
    line = '<p>\n'
    f.write(line)
    line =  location.street + '<br>\n'
    f.write(line)
    line = location.city + '<br>\n'
    f.write(line)
    line = location.phone + '<br>\n'
    f.write(line)
    line = '</p>\n'
    f.write(line)
    line = '<h3>'
    if location.onlineBooking:
        line += 'Online bookings available as indicated below'
    elif location.email:
        line += 'To reserve tickets, email '+ location.email
        if location.phone:
            line += ' or call '+ location.phone
    else:
        line += 'To reserve tickets, call '+ location.phone
    line += '</h3>\n'
    #f.write(line)
    line = '<h3 class="programme">Ticket Prices:</h3>\n'
    f.write(line)
    line = '<p>\n'
    f.write(line)
    for i in range(6):
        if location.priceInfo[i] != '':
            line = location.priceInfo[i] + '<br>\n'
            f.write(line)
    line = '</p>\n'
    f.write(line)            

    #line = '<br>\n'
    #f.write(line)
    if location.place == 'Auckland Central':
        line = '<p><strong>NOTE</strong>: For our opening night at the ASB Waterfront Theatre on October 2, please see the <a href="http://www.showmeshorts.co.nz/events/auckland-opening-night/" title="Auckland Opening Night">Auckland Opening Night</a> page or <a href="https://nz.patronbase.com/_ATC2/Productions/20SS/Performances" title="Book tickets to the Auckland Opening">click here to book tickets</a>.<p>'
        #line = '<p>NOTE: For our opening night at The Civic on November 11, please see the <a href="http://www.showmeshorts.co.nz/events/auckland-opening-night/" title="Auckland Opening Night & Awards Ceremony">Auckland Opening Night & Awards Ceremony</a> page.<p>\n<br>'
        f.write(line)
    # if location.place == 'Wellington':
    #     line = '<p><strong>NOTE</strong>: For our opening night at Light House Cuba on October 8, please see the <a href="http://www.showmeshorts.co.nz/events/wellington-opening-night/" title="Wellington Opening Night">Wellington Opening Night</a> page or <a href="" title="Book tickets to the Wellington Opening">click here to book tickets</a>.<p>'
    #     f.write(line)
        
    line = '\n\n<h3 class="programme">Screenings</h3>'
    f.write(line)
    line = '<table id="table--pink-stripes" summary="' + tableSummary +  '">\n<tbody>\n'
    f.write(line)

    numberOfNotes = -1
    line = '<div>\n'
    f.write(line)
    for i in range(location.numberOfSessions):
        line = '<tr>\n'
        f.write(line)
        line = '<td width="25"><p>'+ scheduleList[i].date + '</p></td>\n'
        f.write(line)
        if scheduleList[i].bookingLink != '':
            line = '<td width="25"><p>'+ scheduleList[i].time + '\n<strong><a href="' + scheduleList[i].bookingLink +'">Book tickets</a></strong></p></td>\n'
        else:
            line = '<td width="25"><p>'+ scheduleList[i].time + '</p></td>\n'
        f.write(line)
#        if ('Audience' in scheduleList[i].sessionTitle or 'Estonian' in scheduleList[i].sessionTitle):
        if ('Audience' in scheduleList[i].sessionTitle):
            line = '<td width="25"><p><strong>' + scheduleList[i].sessionTitle + '</strong></p></td>\n'                
        else:
            line = '<td width="25"><p><a href="'+scheduleList[i].sessionLink + '"><strong>'
            line += scheduleList[i].sessionTitle + '</strong></a></p></td>\n'
        f.write(line)
        line = '</tr>\n'
        f.write(line)
        
    line = '</div>\n'
    f.write(line)
    line = '</tbody>\n</table>\n'
    f.write(line)

    for note in notes:
        if note != '':
            line = '<p>' + note + '</p>\n\n'
            f.write(line)

    #line = '<div style="text-align: center; padding-top: 50px;"><h3><a href="'+programmePagePath+'">Back to Programme</a></h3></div>'
    line = '<div style="text-align: center;"><h3><a href="'+programmePagePath+'">Back to Programme</a></h3></div>'        
    #line = '\n<br>\n<div class="sms button" style="text-align: center;"><br>&nbsp;<br><h3><a class="sms button" href="'+programmePagePath+'">Back to Programme</a></h3></div>'
    f.write(line)
    f.close()
    return;


def pushUpdatePage (credentials, page_id, new_content):
    'Update the content of an existing page on the website'
    rpc_url = 'http://www.showmeshorts.co.nz/xmlrpc.php'
    client = Client(rpc_url, credentials[0], credentials[1])
    page = client.call(posts.GetPost(page_id))
    old_content = page.content
    if new_content != old_content:
        page.content = new_content
        if client.call(posts.EditPost(page_id, page)):
            print("<---- Updated page: {} (id: {}). Link: {} ---->".format(page.title, page.id, page.link))
    else:
        print(">---- Skipped updating unchanged page: {} (id: {}). Link: {} ----<".format(page.title, page.id, page.link))
    return;

    
# WP page ids for auto-updater
page_ids = {'The Sampler' : 5640,
            'Whānau Friendly' : 7464,
            'Love Lines' : 7466,
            'Tangled Worlds' : 7468,
            'Food Fights' : 7470,
            "Let's Get Physical" : 7472,
            'Moments of Truth' : 7474,
            'UK Focus' : 7476,
            'Aotearoa Online' : 7478,
            'International Time Zone Online' : 7480,
            'UK Focus Online' : 7482,
            'Auckland Central' : 5605,
            'Ōrākei' : 7484,
            'Takapuna' : 7486,
            'Devonport' : 3242,
            'Great Barrier Island' : 4751,
            'Kingsland' : 6915,
            'Matakana' : 3257,
            'Titirangi' : 6933,
            'Waiheke Island' : 3247,
            'Christchurch' : 3249,
            'Colville' : 5609,
            'Picton' : 6364,
            'Katikati' : 7488,
            'Wellington' : 7614,
            'Ōpōtiki' : 7490,
            'Stewart Island' : 7492,
            'Arrowtown' : 6367,
            'Dunedin' : 3251,
            'Gisborne' : 4749,
            'Waikaia' : 6938,
            'Hamilton' : 6909,
            'Napier' : 6357,
            'Nelson' : 5090,
            'New Plymouth' : 3259,
            'Russell' : 6927,
            'Tākaka' : 7807,
            'Tauranga' : 3269,
            'Thames' : 5638,
            'Wānaka' : 5643,
            'Whangārei' : 5646,
            'Programme-test' : 6948
}

sessionNames = ['1. The Sampler',
                '2. Food Fights',
                "3. Let's Get Physical",
                '4. Love Lines',
                '5. Moments of Truth',
                '6. Tangled Worlds',
                '7. UK Focus',
                '8. Whānau Friendly',
                '9. Aotearoa Online',
                '10. International Time Zone Online',
                '11. UK Focus Online']
#sessionNames = ['1. The Sampler']
sessionNames2 = [s.split('. ')[1] for s in sessionNames]
print("Session names:\n")
print(sessionNames)
print("\n")

locationNames2 = ['Auckland Central',
                  'Ōrākei',
                  'Takapuna',
                  'Devonport',
                  'Great Barrier Island',
                  'Kingsland',
                  'Matakana',
                  'Titirangi',
                  'Waiheke Island',
                  'Christchurch',
                  'Colville',
                  'Picton',
                  'Katikati',
                  'Wellington',
                  'Ōpōtiki',
                  'Stewart Island',
                  'Arrowtown',
                  'Dunedin',
                  'Gisborne',
                  'Waikaia',
                  'Hamilton',
                  'Napier',
                  'Nelson',
                  'New Plymouth',
                  'Russell',
                  'Tākaka',
                  'Tauranga',
                  'Thames',
                  'Wānaka',
                  'Whangārei']
print("Location names:\n")
print(locationNames2)
print("\n\n")

#Control Panel
#================================================================
downloadBrochures = True
exitCondition = 20000
navMapDimensions = [672.0,873.0]
navMapLocations = './map/locationCoords.txt'
scaleMapX = 1.0
scaleMapY = 1.0
brochureURL = ['https://docs.google.com/document/d/1J4EAL-SwRhRnQMb1dtpBSlAoI6MKi1WVGQja-QPtcQ8/edit?usp=sharing']
downloadedBrochures = ['mainFestival.txt']
#pagePath = "http://localhost/sms2017/"#
pagePath = 'http://www.showmeshorts.co.nz/'
navMap = pagePath + 'images/webprogramme/smsMap2020.png'
keyImagePath = pagePath + "images/webprogramme/films/2020/"
#pdfPath = pagePath + "images/programmes/sms2017_programme.pdf"
#programmePagePath = pagePath + "2015-programme/"
programmePagePath = pagePath + "programme/"
tempProgrammePagePath = pagePath + "programme/"
#outputFolder='/media/dladd/seagate/SMS/website2017/wordpress/documents/webprogramme/2018/'#'./generated/'
outputFolder='./generated/'

debug = True
smartImageCrop = False


# Auto-updater details
try:
    file = open('robo.csv')
    credentials = file.read().strip('\r\n').split(',')
    file.close()
except:
    print('Could not get robo-editor credentials!')

#updatePages = ['Russell', 'The Sampler']

# UPDATE ALL
updatePages = []
#updatePages = ['Programme-test','The Sampler']
updatePages = ['Programme-test']
updatePages += sessionNames2 + locationNames2

# Test programme page


#updatePages = ['Wānaka','Whangārei']
#updatePages = ['International Time Zone Online', 'Aotearoa Online', 'UK Focus Online']
#updatePages = ['The Sampler', 'International Time Zone Online']

#================================================================

# Read in the location coordinates file
with open(navMapLocations) as f:
    reader = csv.reader(f)
    locationNames = []
    locationCoords = []
    i = 0
    for row in reader:
        locationNames.append(row[0].rstrip('\r\n'))
        locationCoords.append([])
        locationCoords.append([])
        locationCoords.append([])
        locationCoords[i].append(row[1].strip())
        locationCoords[i].append(row[2].strip())
        locationCoords[i].append(row[3].strip())
        i+=1

# make list of sessions
sessionList = []
locationList = []
screeningList = []
noteList = []

runtimeKeys = ["Estimated total run time", "Total length: "]

filmList = []
numSessionsFound = 0

#Download brochure files
if downloadBrochures:
    i = 0
    for url in brochureURL:
        gdoc.download_text(url,downloadedBrochures[i])
        i+=1

sessionIndex = 0
locationIndex = 0    
for programmeFileName in downloadedBrochures:
    print('Reading: ' + str(programmeFileName) + "\n\n")
    f = open(programmeFileName, "r")
    #f = open('top.txt', "r")
    totalLines = 0
    sessionFound = False
    locationFound = False

    while 1:

        totalLines += 1
        line=f.readline()
        line = line.strip()
        
        if 'REGION' in line:
            line = line.replace('AUCKLAND REGION -- ', '')
            line = line.replace('WELLINGTON REGION -- CENTRAL', 'WELLINGTON')
            line = line.replace('WELLINGTON REGION -- ', '')
            if 'Academy' in line:
                line = 'AUCKLAND CBD'

        # Construct session data
        #print(line.lower())
        if (line.lower() in (name.lower() for name in sessionNames)) and numSessionsFound < len(sessionNames) :
            #filmList = []
            filmList.append([])
            s = session()
            s.numberOfFilms = 0
            s.name = line
            if ('. ' in line):
                s.name = line.split('. ')[1]
            sessionFound = True
            print("\n\nReading Session: " + s.name + '\n')

            # Get session description
            s.description = ''
            s.bestfor = ''
            s.streamingLink = ''
            while 1:
                totalLines += 1
                line=f.readline()
                line = line.rstrip('\r\n')
                if line.strip():
                    if debug:
                        print('description: ' + line)
                    if s.name == 'He Tangata':
                        s.description += '<p>' + line + '</p>\n'
                        if line.endswith('united.'):
                            #s.description.replace('<p><p>', '<p>')
                            break
                    else:
                        s.description = line
                        break
                elif totalLines > exitCondition:
                    exit('ERROR: Exited on max line condition')

            #Read film data, session rating, and ratings
            while 1:
                totalLines += 1
                prevLine = f.tell()
                line=f.readline()
                line = line.rstrip('\r\n')
    #            print(line)
                if any (key in line for key in runtimeKeys):
                    numSessionsFound += 1
                    s.runtime = line
                    if debug:
                        print('  runtime: ' + line)
                elif "Screening details" in line:
                    continue
                elif "insert logo" in line:
                    continue
                elif ('arating' not in line and 'brating' not in line) and ("rating" in line or "rated" in line or "Rated" in line or "Rating" in line):
                    s.rating = line
                    if debug:
                        print('  rating: ' + line)
                    #break
                elif ('Best for:' in line):
                    s.bestfor = line
                    if debug:
                        print('  best for: ' + line)
                    #break
                elif ('eventbrite' in line):
                    s.streamingLink = line
                    s.streamingLink = s.streamingLink.replace('Book here:', '')
                    s.streamingLink = s.streamingLink.replace(' ', '')
                    if debug:
                        print('  streaming link: ' + line)
                    #break
                elif line in locationNames2:
                    break
                elif line in sessionNames:
                    line = f.seek(prevLine)
                    break
                elif line == "Auckland Region":
                    break
                elif line == "Opening Night":
                    break
                elif totalLines > exitCondition:
                    exit('ERROR: Exited on max line condition')
                elif line.strip():
                    #Found film: read in brochure data
                    tmp = film()                    
                    tmp.title = line
                    if debug:
                        print('\n  title: ' + line)
                    line=f.readline()
                    line = line.rstrip('\r\n')
                    tmp.info1 = line
                    if debug:
                        print('  info1: ' + line)
                    line=f.readline()
                    line = line.rstrip('\r\n')
                    tmp.info2 = line
                    if len(tmp.info2) > 60:
                        tmp.summary=tmp.info2
                        tmp.info2 = ''
                    else:
                        line=f.readline()
                        line = line.rstrip('\r\n')
                        tmp.summary = line
                    if debug:
                        print('  summary: ' + tmp.summary)
                    line=f.readline()
                    line = line.rstrip('\r\n')

                    for i in range(4):
                        if 'Dir:' in line or 'Writ:' in line or 'Prod:' in line :
                            tmp.credits[i] = line
                            line = f.readline()
                            line = line.rstrip('\r\n')
                        else:
                            break
                    if debug:
                        print('  credits: ' + str(tmp.credits))
                    #line=f.readline()
                    #line = line.rstrip('\r\n')
                    if 'remiere' in line:
                        orig = tmp.info1
                        tmp.info1 = line + orig
                        line=f.readline()
                        line = line.rstrip('\r\n')
                    if 'Trailer:' in line:
                        line = line.replace('Trailer:','')
                        line = line.replace(' ','')
                        line = line.replace('yo...v','youtube.com/watch?v')
                        tmp.trailerLink = line
                        line=f.readline()
                        line = line.rstrip('\r\n')
                        if 'Maker:' in line:
                            line = line.replace('Maker:','')
                            line = line.replace(' ','')
                            line = line.replace('yo...v','youtube.com/watch?v')
                            tmp.makerLink = line
                            if debug:
                                print('  maker: ' + tmp.makerLink)
                        if debug:
                            print('  trailer: ' + tmp.trailerLink)
                    else:
                        tmp.trailerLink = ''
                        tmp.makerLink = ''
                    #add key image
                    keyImageName = tmp.title.lower()
                    keyImageName = keyImageName.replace(' ','')
                    keyImageName = keyImageName.replace('%','')
                    keyImageName = keyImageName.replace('&','and')
                    keyImageName = keyImageName.replace(',','')
                    keyImageName = keyImageName.replace('è','e')
                    keyImageName = keyImageName.replace('.','')
                    keyImageName = keyImageName.replace(':','')
                    keyImageName = keyImageName.replace("'",'')
                    keyImageName = keyImageName.replace('–','-')
                    keyImageName = keyImageName.replace('--','-')
                    keyImageName = keyImageName.replace('?','')
                    keyImageName = keyImageName.replace('!','')
                    keyImageName = keyImageName.replace('#','')                    
                    keyImageName = keyImageName.replace(";",'')
                    keyImageName = keyImageName.replace('ö','')

                    keyImageName = keyImageName.replace('箒(thebroom)','thebroom')
                    keyImageName = keyImageName.replace('相撲人(littlemisssumo)','littlemisssumo')
                    keyImageName = keyImageName.replace('まれびとの住まう島(boze)','boze')
                    keyImageName = keyImageName.replace('ヒゲとレインコート(beardandraincoat)','beardandraincoat')
                    keyImageName = keyImageName.replace('マイリトルゴート(mylittlegoat)','mylittlegoat')
                    keyImageName = keyImageName.replace('','')

                    keyImageName = keyImageName.replace('rū','ru')
                    keyImageName = keyImageName.replace('安（lullaby）','lullaby')
                    keyImageName = keyImageName.replace('å','a')
                    keyImageName = keyImageName.replace('tewhakairo-ngākīotetaiao(thecarvingscarrythestoriesoftheworld)','tewhakairo-ngakiotetaiao(thecarvingscarrythestoriesoftheworld)')                                        
                    keyImageName = keyImageName.replace('妹妹(sister)', 'sister')
                    keyImageName = keyImageName.replace('단팥죽(theredbeansoup)', 'theredbeansoup')
                    keyImageName = keyImageName.replace('(oo)','oo')
                    keyImageName = keyImageName.replace('몸값(bargain)','bargain')
                    keyImageName = keyImageName.replace('통일전야-어느저녁식사(theeveofunification)','theeveofunification')
                    keyImageName = keyImageName.replace('변성기(almond:myvoiceisbreaking)','almond-myvoiceisbreaking')
                    keyImageName = keyImageName.replace('사슴꽃(deerflower)','deerflower')
                    keyImageName = keyImageName.replace('minbörda(theburden)', 'theburden')
                    keyImageName = keyImageName.replace('cerdita(piggy)', 'cerdita')
                    keyImageName = keyImageName.replace('牛奶(milk)', 'milk')
                    keyImageName = keyImageName.replace('9pasos(9steps)', '9steps')                    
                    keyImageName = keyImageName.replace('nekonohi(catdays)', 'nekonohi')
                    keyImageName = keyImageName.replace('odderetegg(oddisanegg)', 'odderetegg')
                    keyImageName = keyImageName.replace('laboite(thebox)', 'laboite')
                    keyImageName = keyImageName.replace('5ansaprèslaguerre(5yearsafterthewar)', '5yearsafterthewar')
                    keyImageName = keyImageName.replace('raymondeoulévasionverticale(raymondeortheverticalescape)', 'raymondeortheverticalescape')
                    keyImageName = keyImageName.replace('mondernierÉté(mylastsummer)', 'mondernierete(mylastsummer)')                    
                    keyImageName = keyImageName.replace('铁手(ironhands)', 'ironhands')
                    keyImageName = keyImageName.replace('동心(one-minded)', 'one-minded')
                    keyImageName = keyImageName.replace('parvaneha(butterflies)', 'butterflies')
                    keyImageName = keyImageName.replace('diebadewanne(thebathtub)', 'diebadewanne')
                    keyImageName = keyImageName.replace('oeilpouroeil(aneyeforaneye)','oeilpouroeil')
                    keyImageName = keyImageName.replace('solsticeduncoeurbrisé(abroken-heartedsolstice)','abroken-heartedsolstice')
                    keyImageName = keyImageName.replace('nýrdaguríeyjafirði(dovetail)','dovetail')
                    keyImageName = keyImageName.replace('fame(hunger)','fame')
                    keyImageName = keyImageName.replace('کلاف(tangle)','tangle')
                    keyImageName = keyImageName.replace('daddysgirl(kōtiro)','daddysgirl')
                    keyImageName = keyImageName.replace('tresveces(threetimes)','threetimes')
                    keyImageName = keyImageName.replace('mezzestagioni(transitionalseasons)','transitionalseasons')
                    tmp.keyImage = keyImagePath + keyImageName + '.jpg'
                    if debug:
                        print('  key image: ' + tmp.keyImage)
                    #filmList.append(tmp)
                    filmList[sessionIndex].append(tmp)
                    s.numberOfFilms += 1
    #                break

            sessionList.append(s)
            #createSession(sessionList[sessionIndex],filmList,programmePagePath,outputFolder)
            #createSession(sessionList[sessionIndex],filmList[sessionIndex],programmePagePath,outputFolder)
            sessionIndex += 1

        elif line in (name.upper() for name in locationNames):
            print(line)
            #if locationIndex == 0 and line.title() != 'Auckland Central':
            #    continue            
            lindex = locationNames.index(line.title().replace('Cbd', 'CBD'))
            location = locationObject()
            location.numberOfSessions = 0
            location.place = line.title().replace('Cbd', 'CBD')
            print("\n\nReading Location: " + location.place)
            print('    line number: ' + str(totalLines))
            print('    locationIndex: ' + str(locationIndex))
            # Get the map coordinate locations
            coord = locationCoords[lindex]
            location.coord = [float(coord[0]),float(coord[1]),float(coord[2])]
            if debug:
                print('    map coordinates: '+ str(location.coord))
            location.coord[0] = location.coord[0]/navMapDimensions[0]*100.0
            location.coord[1] = location.coord[1]/navMapDimensions[1]*100.0
            foundLocVen = False
            foundLocPrice = False
            foundLocProg = False
            # Read location info
            while 1:
                line=f.readline()
                line = line.strip()
    #            if line != '':
                if line.strip():
                    if foundLocVen == False:
                        foundLocVen = True
                        location.dates=line
                        if debug:
                            print('dates: ' + line)
                        line=f.readline()
                        line = line.strip()
                        location.venue=line
                        if debug:
                            print('venue: ' + line)
                        line=f.readline()
                        line = line.strip()
                        location.street=line
                        if debug:
                            print('street: ' + line)
                        line=f.readline()
                        line = line.strip()
                        location.city=line
                        if debug:
                            print('city: ' + line)
                        line=f.readline()
                        line = line.strip()
                        if 'ph' in line or 'Ph' in line or 'Door' in line:
                            phone = line.replace('ph','')
                            phone = phone.replace('Ph','')
                            phone = phone.strip
                            location.phone=line
                            if debug:
                                print('phone: ' + line)
                            line=f.readline()
                            line = line.strip()
                        else:
                            location.phone=''
                        if '@' in line:
                            location.email=line
                            if debug:
                                print('email: ' + line)
                            line=f.readline()
                            line = line.strip()
                        else:
                            location.email=''
                        if 'www' in line or '.co.nz' in line or 'http' in line:
                            location.venueLink=line
                            if debug:
                                print('venue link: ' + line)
                        else:
                            location.venueLink=''

                    elif foundLocPrice == False:
                        if line == 'Ticket Prices' or line == 'Tickets' or line == 'Ticket prices' or line == 'Ticket Price' or line == 'Ticket price':
                            pass
                        else:
                            foundLocPrice = True
                            for i in range(10):
                                if line:
                                    line = line.replace('see page xx for more details', 'purchase from cinema box office')
                                    location.priceInfo[i] = line
                                    if debug:
                                        print('price ' + str(i) + ' : ' + line)
                                    line=f.readline()
                                    line = line.strip()
                                else:
                                    break
                    elif foundLocProg == False:
                        if line == 'Programme':
                            pass
                        else:
                            foundLocProg = True
                            print('\nProgramme: ')
                            #item = scheduleObject()
                            itemList = []
                            notes = []
                            itemIndex=0
                            numberOfNotes = -1
                            for i in range(100):
                                if i > 0:
                                    #line=f.readline()
                                    line = line.strip()
                                if line == '.':
                                    line = f.readline()
                                    line = line.strip()
                                    continue
                                if line:
                                    if line.startswith('*'):
                                        numberOfNotes+=1
                                        #itemList[itemIndex].note[numberOfNotes] = line
                                        notes.append(line)
                                        print('    Note: ' + line)
                                        line = f.readline()
                                        line = line.strip()
                                        # for j in range(len(itemList)):
                                        #     if '*' in itemList[j].sessionTitle:
                                        #         itemList[j].note[numberOfNotes] = line
                                        #         print('    Note: ' + line)
                                        # line = f.readline()
                                        # line = line.strip()
                                        # #         #astCountItem = itemList[j].sessionTitle.count('*')
                                    else:
                                        if 'Nov' in line:
                                            month = 'Nov'
                                        elif 'Dec' in line:
                                            month = 'Dec'
                                        elif 'Oct' in line:
                                            month = 'Oct'
                                        elif 'Jan' in line:
                                            month = 'Jan'
                                        items =re.split('Oct|Nov|Dec|Jan|pm|am|http', line)
                                        #print(items)
                                        item = scheduleObject()
                                        item.date = items[0] + month
                                        line = f.readline()
                                        line = line.strip()
                                        item.time = line
                                        # if items[1].strip() == '11.00' or items[1].strip() == '10.00':
                                        #     item.time = items[1] + 'am'
                                        # else:
                                        #     item.time = items[1] + 'pm'
                                        if programmeFileName != 'regionalFestival.txt':
                                            line = f.readline()
                                            line = line.strip()
                                        item.sessionTitle = line
                                        #if ('. ' in line):
                                        #    item.sessionTitle = line.split('. ')[1]
                                        if 'Wellington Opening Night' in line:
                                            print("found welly opening")
                                            item.sessionLink = pagePath + "events/wellington-opening-night"
                                        elif 'Awards Ceremony' in line:
                                            print("found Awards night")
                                            item.sessionLink = pagePath + "events/awards-night"
                                        elif 'Auckland Short Film Talk' in line:
                                            print("found AKL SFT")
                                            item.sessionLink = pagePath + "events/short-film-talks"
                                        elif 'Masterclass' in line:
                                            item.sessionLink = pagePath + "events/short-film-distribution-masterclass"
                                        elif 'Estonian' in line:
                                            print("found welly Estonian night")
                                            item.sessionLink = pagePath + "events/estonian-short-film-night"
                                            notes.append('* Special event')
                                            notes.append('** Tickets include a glass of wine before screening and a Q&A with filmmakers afterwards')
                                        elif programmeFileName == 'regionalFestival.txt':
                                            item.sessionTitle = 'The Sampler'
                                            item.sessionLink = tempProgrammePagePath + 'the-sampler'
                                        else:
                                            pageName = item.sessionTitle.lower()
                                            pageName = pageName.strip()
                                            pageName = pageName.replace("’",'')
                                            pageName = pageName.replace("'",'')                                                                                
                                            pageName = pageName.replace(' ','-')
                                            pageName = pageName.replace('&','and')
                                            pageName = pageName.replace('*','')
                                            item.sessionLink = tempProgrammePagePath + pageName
                                            item.sessionLink = unidecode.unidecode(item.sessionLink)
                                        line = f.readline()
                                        line = line.strip()
                                        if 'http' in line:
                                            item.bookingLink = line
                                            location.onlineBooking = True
                                            line = f.readline()
                                            line = line.strip()
                                        else:
                                            item.bookingLink = ''
                                            #line = f.readline()
                                            #line = line.strip()
                                        if debug:
                                            print('    Date: ' + item.date + '    Time: ' + item.time + '    Session: ' + item.sessionTitle + '    Session Link: ' + item.sessionLink)
                                            if item.bookingLink:
                                                print('    Booking Link: ' + item.bookingLink)
                                        itemList.append(item)
                                        itemIndex += 1
                                        location.numberOfSessions += 1
                                else:
                                    break

                            locationList.append(location)
                            screeningList.append(itemList)
                            noteList.append(notes)
                            createLocation(locationList[locationIndex],itemList,notes,navMap,programmePagePath,outputFolder)
                            print('creating location: ' + locationList[locationIndex].place)
                            locationIndex += 1
                    else:
                        break
                elif foundLocProg:
                    break
        elif line == 'Ambassadors':
            break
        elif line == 'Larger logos:':
            break
        elif totalLines > exitCondition:
            exit('ERROR: Exited on max line condition')

    f.close()

numberOfLocations=locationIndex
numberOfSessions=sessionIndex

directorLink= tempProgrammePagePath + "welcome"
navigationFileName = outputFolder+'programme-test.php'

f = open(navigationFileName, "w")
print("writing to ", navigationFileName)

#Programme cover image
#line = '&nbsp;<br>\n'
#f.write(line)
imagePath = pagePath + 'images/webprogramme/events/2017/programmeCover2017.jpg'
line = '[caption align="alignright" width="150"]<a href="http://www.showmeshorts.co.nz/images/programmes/sms2017_programme.pdf"><img src="'+ imagePath +'" alt="Click to download the print programme" width="150" class="size-full wp-image-3116" /></a> Click to download the print programme[/caption]'
f.write(line)

# Description
text = """<p>Show Me Shorts Film Festival is proud to present our 2020 programme. Browse by our fantastic locations throughout New Zealand and our seven themed sessions below.</p>

<p>Also be sure to check out our events- we kick off the festival in style with the <a href="http://www.showmeshorts.co.nz/events/auckland-opening-night/" title="Auckland Opening Night">Auckland Opening Night & Awards Ceremony</a> at The Civic in Auckland and the <a href="http://www.showmeshorts.co.nz/events/wellington-opening-night/" title="Wellington Opening Night">Wellington Opening Night</a> at The Embassy in Wellington. The <a href="http://www.showmeshorts.co.nz/events/short-film-talks/" title="Short Film Talks">Short Film Talks</a> in Auckland and Wellington are fantastic ways to learn more about shorts from some of NZ's top short film makers.</p>

<p>You can also find more about this year's festival from the <a href="http://www.showmeshorts.co.nz/directors-welcome-2017/">Director's Welcome</a> or download a <a href="http://www.showmeshorts.co.nz/images/programmes/sms2017_programme.pdf" title="PDF of the 2017 Show Me Shorts Film Festival">PDF of our print programme</a>.</p>
<br>
<hr class="sessionBreak">

<h2>Trailer</h2>
<iframe width="853" height="480" src="https://www.youtube.com/embed/dgpMyj6D0Gk" frameborder="0" allowfullscreen></iframe>
"""
line = text
f.write(line)

#line = '\n\n<br>\n\n'
#f.write(line)

line = '\n\n[raw]\n<hr class="sessionBreak">\n'
f.write(line)

#line = '<hr class="sessionBreak"><br style="clear:both;" />\n\n'
#line = '<hr class="sessionBreak">\n\n'
#f.write(line)
line = '<a name="locations"></a>'
f.write(line)
line = '\n\n<h2>Locations</h2>\n\n'
f.write(line)

line = '<div id="navMapContainer">\n'
f.write(line)
line = '<img id="smsMapImage" src="' + navMap + '" />\n\n'
f.write(line)

locationLinks = []
locationPlaces = []
screeningLocations = []
for i in range(numberOfLocations):

    # Get the location/date info
    locationName = locationList[i].place
    locationName = locationName.title().replace('Cbd', 'CBD')
    locationPlaces.append(locationName)
    screeningLocation = locationList[i].place+' - '+locationList[i].venue
    screeningLocations.append(screeningLocation)    

    locationDates = locationList[i].dates
    locationDates = locationDates.replace('October', 'Oct')
    locationDates = locationDates.replace('November', 'Nov')
    locationDates = locationDates.replace('January','Jan')
    locationDates = locationDates.replace('December','Dec')

    locationLink = locationName.lower()
    locationLink = locationLink.replace(' ','-')
    locationLink = unidecode.unidecode(locationLink)
    locationLink = tempProgrammePagePath + locationLink
    locationLinks.append(locationLink)

    # Write to programme file
    widthPercent = str(int(locationList[i].coord[0]))
    heightPercent = str(int(locationList[i].coord[1]))
    zIndex = str(int(locationList[i].coord[2]))
    line = '<div style="position: absolute; left: ' + widthPercent + '%; top: ' + heightPercent + '%; z-index: ' + zIndex + ';">\n'
    f.write(line)

    line = '<a  href ="' + locationLink + '">'
    f.write(line)
    line = '<location><div>\n' + locationName + "<span class='spacer'></span>\n"
    f.write(line)
    line = "<br /><div class='date'>" + locationDates + "</div>\n</div>\n</location>\n</a>\n</div>\n\n"
    f.write(line)

line = '</div>' # close map container
f.write(line)
#line = '<br style="clear:both;" />\n'
#f.write(line)

# Add in a location by alpha dropdown menu option
locList = []
#locationAlphaIndex = sorted(range(len(locationPlaces)), key=lambda k: locationPlaces[k])
locationAlphaIndex = sorted(range(len(screeningLocations)), key=lambda k: screeningLocations[k])
line = '\n<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">\n'
f.write(line)
line = '<option value=""><em>(Or select a location and cinema...)</em></option>\n'
f.write(line)
for i in locationAlphaIndex:
    line = '<option value="' + locationLinks[i] + '">'+ screeningLocations[i] +'</option>\n'
#    line = '<option value="' + locationLinks[i] + '">'+ locationPlaces[i] +'</option>\n'
    f.write(line)
line = '</select>\n'
f.write(line)
#line = '<br style="clear:both;" />\n'
#f.write(line)
    
#line = '<hr class="sessionBreak"><br style="clear:both;" />\n'
#line = '<hr class="sessionBreak">\n'
line = '<br style="clear:both;" />\n'
f.write(line)
#line = '\n\n<br>\n\n'
#f.write(line)

line = '\n<hr class="sessionBreak">\n<h2>Events</h2>\n\n'
f.write(line)

space = "<span class='spacer'></span><br /><span class='spacer'></span>"

for i in range(numberOfSessions+ 5):
    print(i)

    if i == 0:
        #sessionName = "Auckland" + space + "Opening Night &" + space + "Awards Ceremony"
        sessionName = "Auckland Opening Night"
        sessionLink = pagePath + 'events/auckland-opening-night'        
        imageName = sessionLink.replace(pagePath,'')
        imageName = pagePath + 'images/webprogramme/events/2020/aucklandopening.jpg'
        print(sessionLink)
    elif i == 1:
        #sessionName = "Auckland" + space + "Opening Night &" + space + "Awards Ceremony"
        sessionName = "Auckland Awards Ceremony"
        sessionLink = pagePath + 'events/awards-night'
        imageName = sessionLink.replace(pagePath,'')
        imageName = pagePath + 'images/webprogramme/events/2020/aucklandawards.jpg'
    elif i == 2:
        sessionName = "Wellington" + space + "Opening Night"
        sessionLink = pagePath + 'events/wellington-opening-night'        
        imageName = sessionLink.replace(pagePath,'')
        imageName = pagePath + 'images/webprogramme/events/2020/wellingtonopening.jpg'
    elif i == 3:
        sessionName = 'Short Film Talks'
        sessionLink = pagePath + 'events/short-film-talks'
        imageName = pagePath + 'images/webprogramme/events/2020/shortfilmtalks.jpg'
    elif i == 4:
        sessionName = 'Meet the Festivals'
        sessionLink = pagePath + 'events/meet-the-festivals'
        imageName = pagePath + 'images/webprogramme/events/2020/meetthefestivals.jpg'
    else:
        sessionName = sessionList[i-5].name
        sessionName = sessionName.title()
        sessionName = sessionName.replace('Of','of')
        sessionName = sessionName.replace('Uk','UK')
        if 'Physical' in sessionName.title():
            sessionName = "Let's Get Physical"

        sessionLink = linkFormat(sessionName)
        sessionLink = tempProgrammePagePath + sessionLink
        sessionLink = sessionLink.replace('é','e')
        sessionLink = unidecode.unidecode(sessionLink)
        imageName = sessionLink.replace(tempProgrammePagePath,'')
        imageName = pagePath + 'images/webprogramme/sessions/2020/'+imageName+'.jpg'
        print(sessionName)
        sessionName = [i for i in sessionNames if sessionName in i][0]
        print(sessionName)

    print(sessionLink)

    imageName = imageName.replace('é','e')
    imageName = imageName.replace('-','')
    imageName = imageName.replace("'",'')
    imageName = imageName.replace("’",'')
    imageName = unidecode.unidecode(imageName)

    if i == 5:
        line = '<br style="clear:both;" />\n'
        line = '<hr class="sessionBreak"><br style="clear:both;" />\n'
        f.write(line)
        line = '<a name="screening-sessions"></a>'
        f.write(line)        
        line = '<h2>Screening sessions</h2>\n\n'
        f.write(line)
    if i == 13:
        line = '<br style="clear:both;" />\n'
        line = '<hr class="sessionBreak"><br style="clear:both;" />\n'
        f.write(line)
        line = '<a name="online-sessions"></a>'
        f.write(line)
        line = '<h2>Online sessions</h2>\n\n'
        f.write(line)

    print(sessionLink)
    line = '<a href="' + sessionLink + '">'
    f.write(line)
    line='<div id="imagelink" style="background-image: url(' + imageName + ');"><overlayHead><span>'
    line += sessionName
    line += '</span></overlayHead></div></a>'
    f.write(line)

line = '<hr class="sessionBreak"><br style="clear:both;" />\n\n'
#line = '<hr class="sessionBreak">\n\n'
#line = '<br style="clear:both;" />\n\n'
f.write(line)

line = "[/raw]"
f.write(line)

f.close()


# Create session and files
for sessionIndex in range(numberOfSessions):
    createSessionWithScreenings(sessionList[sessionIndex],filmList[sessionIndex],
                                screeningList,locationList,noteList,locationLinks,
                                programmePagePath,outputFolder)        

print("Show Me Shorts 2020 programme successfully created! \n")


for pageName in updatePages:
    fname = outputFolder + pageName.lower().replace(' ','-').replace("'",'') + '.php'
    with open(fname) as f:
        newContent = f.read()
        pushUpdatePage(credentials, page_ids[pageName], newContent)
        f.close()
