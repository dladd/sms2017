<h3 class="programme">15 Oct</h3>
<h2 class="programme"><a href="https://whangareifilmsociety.org/">Whangārei Film Society</a></h2>
<p>
14 Hassard Street<br>
Whangārei 0112<br>
Ph: 09 437 3282<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$10 General Admission<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="whangarei-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Thu 15 Oct</p></td>
<td width="25"><p>6:00 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>