[caption align="alignright" width="150"]<a href="http://www.showmeshorts.co.nz/images/programmes/sms2017_programme.pdf"><img src="http://www.showmeshorts.co.nz/images/webprogramme/events/2017/programmeCover2017.jpg" alt="Click to download the print programme" width="150" class="size-full wp-image-3116" /></a> Click to download the print programme[/caption]<p>Show Me Shorts Film Festival is proud to present our 2020 programme. Browse by our fantastic locations throughout New Zealand and our seven themed sessions below.</p>

<p>Also be sure to check out our events- we kick off the festival in style with the <a href="http://www.showmeshorts.co.nz/events/auckland-opening-night/" title="Auckland Opening Night">Auckland Opening Night & Awards Ceremony</a> at The Civic in Auckland and the <a href="http://www.showmeshorts.co.nz/events/wellington-opening-night/" title="Wellington Opening Night">Wellington Opening Night</a> at The Embassy in Wellington. The <a href="http://www.showmeshorts.co.nz/events/short-film-talks/" title="Short Film Talks">Short Film Talks</a> in Auckland and Wellington are fantastic ways to learn more about shorts from some of NZ's top short film makers.</p>

<p>You can also find more about this year's festival from the <a href="http://www.showmeshorts.co.nz/directors-welcome-2017/">Director's Welcome</a> or download a <a href="http://www.showmeshorts.co.nz/images/programmes/sms2017_programme.pdf" title="PDF of the 2017 Show Me Shorts Film Festival">PDF of our print programme</a>.</p>
<br>
<hr class="sessionBreak">

<h2>Trailer</h2>
<iframe width="853" height="480" src="https://www.youtube.com/embed/dgpMyj6D0Gk" frameborder="0" allowfullscreen></iframe>


[raw]
<hr class="sessionBreak">
<a name="locations"></a>

<h2>Locations</h2>

<div id="navMapContainer">
<img id="smsMapImage" src="http://www.showmeshorts.co.nz/images/webprogramme/smsMap2020.png" />

<div style="position: absolute; left: 15%; top: 21%; z-index: 30;">
<a  href ="http://www.showmeshorts.co.nz/programme/auckland-central"><location><div>
Auckland Central<span class='spacer'></span>
<br /><div class='date'>3--14 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 14%; top: 16%; z-index: 20;">
<a  href ="http://www.showmeshorts.co.nz/programme/devonport"><location><div>
Devonport<span class='spacer'></span>
<br /><div class='date'>16 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 77%; top: 13%; z-index: 20;">
<a  href ="http://www.showmeshorts.co.nz/programme/great-barrier-island"><location><div>
Great Barrier Island<span class='spacer'></span>
<br /><div class='date'>19 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 12%; top: 26%; z-index: 20;">
<a  href ="http://www.showmeshorts.co.nz/programme/kingsland"><location><div>
Kingsland<span class='spacer'></span>
<br /><div class='date'>6 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 11%; top: 9%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/matakana"><location><div>
Matakana<span class='spacer'></span>
<br /><div class='date'>19--23 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 3%; top: 21%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/titirangi"><location><div>
Titirangi<span class='spacer'></span>
<br /><div class='date'>7 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 29%; top: 15%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/waiheke-island"><location><div>
Waiheke Island<span class='spacer'></span>
<br /><div class='date'>4 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 25%; top: 26%; z-index: 20;">
<a  href ="http://www.showmeshorts.co.nz/programme/orakei"><location><div>
Ōrākei<span class='spacer'></span>
<br /><div class='date'>18 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 4%; top: 15%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/takapuna"><location><div>
Takapuna<span class='spacer'></span>
<br /><div class='date'>18 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 56%; top: 64%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/christchurch"><location><div>
Christchurch<span class='spacer'></span>
<br /><div class='date'>15--21 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 35%; top: 8%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/colville"><location><div>
Colville<span class='spacer'></span>
<br /><div class='date'>30 Oct--6 Nov</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 63%; top: 55%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/picton"><location><div>
Picton<span class='spacer'></span>
<br /><div class='date'>23-24 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 70%; top: 48%; z-index: 20;">
<a  href ="http://www.showmeshorts.co.nz/programme/wellington"><location><div>
Wellington<span class='spacer'></span>
<br /><div class='date'>8--21 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 20%; top: 75%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/arrowtown"><location><div>
Arrowtown<span class='spacer'></span>
<br /><div class='date'>18 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 41%; top: 84%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/dunedin"><location><div>
Dunedin<span class='spacer'></span>
<br /><div class='date'>18 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 89%; top: 33%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/gisborne"><location><div>
Gisborne<span class='spacer'></span>
<br /><div class='date'>10-17 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 68%; top: 25%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/hamilton"><location><div>
Hamilton<span class='spacer'></span>
<br /><div class='date'>28 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 83%; top: 38%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/napier"><location><div>
Napier<span class='spacer'></span>
<br /><div class='date'>10 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 55%; top: 50%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/nelson"><location><div>
Nelson<span class='spacer'></span>
<br /><div class='date'>10-11 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 55%; top: 36%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/new-plymouth"><location><div>
New Plymouth<span class='spacer'></span>
<br /><div class='date'>8 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 65%; top: 4%; z-index: 100;">
<a  href ="http://www.showmeshorts.co.nz/programme/russell"><location><div>
Russell<span class='spacer'></span>
<br /><div class='date'>18 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 83%; top: 23%; z-index: 20;">
<a  href ="http://www.showmeshorts.co.nz/programme/tauranga"><location><div>
Tauranga<span class='spacer'></span>
<br /><div class='date'>18 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 39%; top: 25%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/thames"><location><div>
Thames<span class='spacer'></span>
<br /><div class='date'>27 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 26%; top: 82%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/waikaia"><location><div>
Waikaia<span class='spacer'></span>
<br /><div class='date'>14 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 32%; top: 70%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/wanaka"><location><div>
Wānaka<span class='spacer'></span>
<br /><div class='date'>18 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 69%; top: 9%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/whangarei"><location><div>
Whangārei<span class='spacer'></span>
<br /><div class='date'>15 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 77%; top: 20%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/katikati"><location><div>
Katikati<span class='spacer'></span>
<br /><div class='date'>4 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 91%; top: 26%; z-index: 30;">
<a  href ="http://www.showmeshorts.co.nz/programme/opotiki"><location><div>
Ōpōtiki<span class='spacer'></span>
<br /><div class='date'>10 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 51%; top: 44%; z-index: 5;">
<a  href ="http://www.showmeshorts.co.nz/programme/takaka"><location><div>
Tākaka<span class='spacer'></span>
<br /><div class='date'>9-10 Oct</div>
</div>
</location>
</a>
</div>

<div style="position: absolute; left: 25%; top: 94%; z-index: 10;">
<a  href ="http://www.showmeshorts.co.nz/programme/stewart-island"><location><div>
Stewart Island<span class='spacer'></span>
<br /><div class='date'>2-27 Jan</div>
</div>
</location>
</a>
</div>

</div>
<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value=""><em>(Or select a location and cinema...)</em></option>
<option value="http://www.showmeshorts.co.nz/programme/arrowtown">Arrowtown - Dorothy Browns</option>
<option value="http://www.showmeshorts.co.nz/programme/auckland-central">Auckland Central - Rialto Cinemas Newmarket</option>
<option value="http://www.showmeshorts.co.nz/programme/christchurch">Christchurch - Alice</option>
<option value="http://www.showmeshorts.co.nz/programme/colville">Colville - Colville Hall, Colville Rd</option>
<option value="http://www.showmeshorts.co.nz/programme/devonport">Devonport - The Vic Devonport</option>
<option value="http://www.showmeshorts.co.nz/programme/dunedin">Dunedin - Rialto Cinemas</option>
<option value="http://www.showmeshorts.co.nz/programme/gisborne">Gisborne - Dome Cinema, Poverty Bay Club</option>
<option value="http://www.showmeshorts.co.nz/programme/great-barrier-island">Great Barrier Island - Barrier Social Club</option>
<option value="http://www.showmeshorts.co.nz/programme/hamilton">Hamilton - The Lido</option>
<option value="http://www.showmeshorts.co.nz/programme/katikati">Katikati - The Junction Theatre</option>
<option value="http://www.showmeshorts.co.nz/programme/kingsland">Kingsland - The Kingslander</option>
<option value="http://www.showmeshorts.co.nz/programme/matakana">Matakana - Matakana Cinemas</option>
<option value="http://www.showmeshorts.co.nz/programme/napier">Napier - MTG Century Theatre</option>
<option value="http://www.showmeshorts.co.nz/programme/nelson">Nelson - Suter Theatre, Suter Art Gallery</option>
<option value="http://www.showmeshorts.co.nz/programme/new-plymouth">New Plymouth - 4th Wall Theatre</option>
<option value="http://www.showmeshorts.co.nz/programme/picton">Picton - Endeavour Park</option>
<option value="http://www.showmeshorts.co.nz/programme/russell">Russell - Cinema Kororareka</option>
<option value="http://www.showmeshorts.co.nz/programme/stewart-island">Stewart Island - Bunkhouse Theatre</option>
<option value="http://www.showmeshorts.co.nz/programme/takapuna">Takapuna - Monterey</option>
<option value="http://www.showmeshorts.co.nz/programme/tauranga">Tauranga - Rialto Cinemas</option>
<option value="http://www.showmeshorts.co.nz/programme/thames">Thames - Embassy Cinemas</option>
<option value="http://www.showmeshorts.co.nz/programme/titirangi">Titirangi - Lopdell House</option>
<option value="http://www.showmeshorts.co.nz/programme/takaka">Tākaka - The Village Theatre</option>
<option value="http://www.showmeshorts.co.nz/programme/waiheke-island">Waiheke Island - Waiheke Island Community Cinema</option>
<option value="http://www.showmeshorts.co.nz/programme/waikaia">Waikaia - The Lodge 223</option>
<option value="http://www.showmeshorts.co.nz/programme/wellington">Wellington - Light House Cuba</option>
<option value="http://www.showmeshorts.co.nz/programme/whangarei">Whangārei - Whangārei Film Society</option>
<option value="http://www.showmeshorts.co.nz/programme/wanaka">Wānaka - Rubys Cinemas</option>
<option value="http://www.showmeshorts.co.nz/programme/opotiki">Ōpōtiki - Ōpōtiki De Luxe Theatre</option>
<option value="http://www.showmeshorts.co.nz/programme/orakei">Ōrākei - Silky Otter Cinemas</option>
</select>
<br style="clear:both;" />

<hr class="sessionBreak">
<h2>Events</h2>

<a href="http://www.showmeshorts.co.nz/events/auckland-opening-night"><div id="imagelink" style="background-image: url(http://www.showmeshorts.co.nz/images/webprogramme/events/2020/aucklandopening.jpg);"><overlayHead><span>Auckland Opening Night</span></overlayHead></div></a><a href="http://www.showmeshorts.co.nz/events/awards-night"><div id="imagelink" style="background-image: url(http://www.showmeshorts.co.nz/images/webprogramme/events/2020/aucklandawards.jpg);"><overlayHead><span>Auckland Awards Ceremony</span></overlayHead></div></a><a href="http://www.showmeshorts.co.nz/events/wellington-opening-night"><div id="imagelink" style="background-image: url(http://www.showmeshorts.co.nz/images/webprogramme/events/2020/wellingtonopening.jpg);"><overlayHead><span>Wellington<span class='spacer'></span><br /><span class='spacer'></span>Opening Night</span></overlayHead></div></a><a href="http://www.showmeshorts.co.nz/events/short-film-talks"><div id="imagelink" style="background-image: url(http://www.showmeshorts.co.nz/images/webprogramme/events/2020/shortfilmtalks.jpg);"><overlayHead><span>Short Film Talks</span></overlayHead></div></a><a href="http://www.showmeshorts.co.nz/events/meet-the-festivals"><div id="imagelink" style="background-image: url(http://www.showmeshorts.co.nz/images/webprogramme/events/2020/meetthefestivals.jpg);"><overlayHead><span>Meet the Festivals</span></overlayHead></div></a><hr class="sessionBreak"><br style="clear:both;" />
<a name="screening-sessions"></a><h2>Screening sessions</h2>

<a href="http://www.showmeshorts.co.nz/programme/the-sampler"><div id="imagelink" style="background-image: url(http://www.showmeshorts.co.nz/images/webprogramme/sessions/2020/thesampler.jpg);"><overlayHead><span>1. The Sampler</span></overlayHead></div></a><a href="http://www.showmeshorts.co.nz/programme/whanau-friendly"><div id="imagelink" style="background-image: url(http://www.showmeshorts.co.nz/images/webprogramme/sessions/2020/whanaufriendly.jpg);"><overlayHead><span>8. Whānau Friendly</span></overlayHead></div></a><a href="http://www.showmeshorts.co.nz/programme/love-lines"><div id="imagelink" style="background-image: url(http://www.showmeshorts.co.nz/images/webprogramme/sessions/2020/lovelines.jpg);"><overlayHead><span>4. Love Lines</span></overlayHead></div></a><a href="http://www.showmeshorts.co.nz/programme/tangled-worlds"><div id="imagelink" style="background-image: url(http://www.showmeshorts.co.nz/images/webprogramme/sessions/2020/tangledworlds.jpg);"><overlayHead><span>6. Tangled Worlds</span></overlayHead></div></a><a href="http://www.showmeshorts.co.nz/programme/food-fights"><div id="imagelink" style="background-image: url(http://www.showmeshorts.co.nz/images/webprogramme/sessions/2020/foodfights.jpg);"><overlayHead><span>2. Food Fights</span></overlayHead></div></a><a href="http://www.showmeshorts.co.nz/programme/lets-get-physical"><div id="imagelink" style="background-image: url(http://www.showmeshorts.co.nz/images/webprogramme/sessions/2020/letsgetphysical.jpg);"><overlayHead><span>3. Let's Get Physical</span></overlayHead></div></a><a href="http://www.showmeshorts.co.nz/programme/moments-of-truth"><div id="imagelink" style="background-image: url(http://www.showmeshorts.co.nz/images/webprogramme/sessions/2020/momentsoftruth.jpg);"><overlayHead><span>5. Moments of Truth</span></overlayHead></div></a><a href="http://www.showmeshorts.co.nz/programme/uk-focus"><div id="imagelink" style="background-image: url(http://www.showmeshorts.co.nz/images/webprogramme/sessions/2020/ukfocus.jpg);"><overlayHead><span>7. UK Focus</span></overlayHead></div></a><hr class="sessionBreak"><br style="clear:both;" />
<a name="online-sessions"></a><h2>Online sessions</h2>

<a href="http://www.showmeshorts.co.nz/programme/aotearoa-online"><div id="imagelink" style="background-image: url(http://www.showmeshorts.co.nz/images/webprogramme/sessions/2020/aotearoaonline.jpg);"><overlayHead><span>9. Aotearoa Online</span></overlayHead></div></a><a href="http://www.showmeshorts.co.nz/programme/international-time-zone-online"><div id="imagelink" style="background-image: url(http://www.showmeshorts.co.nz/images/webprogramme/sessions/2020/internationaltimezoneonline.jpg);"><overlayHead><span>10. International Time Zone Online</span></overlayHead></div></a><a href="http://www.showmeshorts.co.nz/programme/uk-focus-online"><div id="imagelink" style="background-image: url(http://www.showmeshorts.co.nz/images/webprogramme/sessions/2020/ukfocusonline.jpg);"><overlayHead><span>11. UK Focus Online</span></overlayHead></div></a><hr class="sessionBreak"><br style="clear:both;" />

[/raw]