<p>Increasingly we are all part of a global family. We can't visit our brothers and sisters abroad right now, so it's the perfect time for some armchair travel. Let this collection of short films take you around the globe to meet families in Finland, Inner Mongolia, Spain and the USA. Time is of the essence in these stories. The characters race against time, take us back in time, work in the time-telling industry, and one even lives inside a cuckoo clock. Don't forget to set your watch!</p>

<p>Total length: 70 minutes<br>
<p>This collection of short films is available for New Zealanders to view online between 3-25 October. Ticket holders will be emailed a link and instructions for how to watch the films on Vimeo.</p>
<p>[button link="https://www.eventbrite.co.nz/e/show-me-shorts-international-time-zone-online-tickets-118797666017" size="large" text_size="beta"]Tickets via Eventbrite[/button]</p>

<h2>The Explosion of a Swimming Ring </h2><a href="https://vimeo.com/379248082" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/theexplosionofaswimmingring.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 10 mins, Finland </p>

<p>Dir/Writ: Tommi Seitajoki
Prod: John Lundsten, Melli Maikkula
</p><div class="cf"></div><p>A family of three are spending their holiday at a waterpark. Soon, tensions arise between the father and the mother. As the argument grows more heated, it becomes clear that things will never be the same again. </p>

<hr class="sessionBreak">

<h2>Grab My Hand: A Letter to My Dad</h2><a href="https://filmfreeway.com/submissions/15174114" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/grabmyhandalettertomydad.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 5 mins, United States </p>

<p>Dir/Writ/Prod: Camrus Johnson
Dir: Pedro Piccinini
<a href="https://www.youtube.com/watch?v=C69oAT0PFd0" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>In telling the story of his dad's relationship with his recently deceased best friend, director Camrus Johnson connects with his grieving father and reminds us all to cherish every moment you have with loved ones. </p>

<hr class="sessionBreak">

<h2>Tres veces <br>(Three Times) </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/threetimes.jpg" />
<p>Drama, 20 mins, Spain </p>

<p>Dir/Writ: Paco Ruiz
Prod: Roberto Butragueño
<a href="https://www.youtube.com/watch?v=qjs7ObSKzes" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>Mario is at home alone. His parents are gone and it will take them a while to return. Enough time to arrange a sex date on the internet with a total stranger. </p>

<hr class="sessionBreak">

<h2>Heading South </h2><a href="https://vimeo.com/409621627" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/headingsouth.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 12 mins, United States/China </p>

<p>Dir/Writ/Prod: Yuan Yuan
Prod: Fei Meng
<a href="https://www.youtube.com/watch?v=blfE5PKRs8Y" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>Eight-year-old Chasuna travels from her home in the Inner Mongolian grassland to visit her father in the city, where she is forced to grapple with his remarriage to a Chinese woman. The film explores family fragmentation as a result of urbanisation in Inner Mongolia. </p>

<hr class="sessionBreak">

<h2>The Watchmaker </h2><a href="https://vimeo.com/390666587" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/thewatchmaker.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Mockumentary, 5 mins, United States </p>

<p>Dir/Writ/Prod: Charlie Fonville
</p><div class="cf"></div><p>A profile of master Japanese watchmaker Takumi Oshiro, who is on the verge of completing one of his greatest masterpieces. </p>

<hr class="sessionBreak">

<h2>Cuckoo! </h2><a href="https://vimeo.com/402563239" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/cuckoo.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Comedy, 7 mins, Netherlands </p>

<p>Dir/Writ/Prod: Jörgen Scholtens 
Writ: Pepijn van Weeren 
Prod: Jimmy Groeneveld
<a href="https://www.youtube.com/watch?v=Pja_5gJV0Hs" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>Frank is a lonely man who lives inside a cuckoo clock. Every hour he straps himself into his ejection seat and shoots through the doors of the clock yelling "Cuckoo!" His work is of vital importance, as the old lady who sits under the clock needs to take her medication exactly at the top of every hour.</p>

<hr class="sessionBreak">

<h2>Kapaemahu </h2><a href="https://vimeo.com/392887414" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/kapaemahu.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 9 mins, United States </p>

<p>Dir/Writ/Prod: Hinaleimoana Wong-Kalu, Dean Hamer, Joe Wilson
</p><div class="cf"></div><p>Four mysterious boulders on Waikiki Beach have a hidden history; within them, there are legendary transgender healing spirits. </p>
<hr class="sessionBreak"><div style="padding-top:5rem;"><p>This collection of short films is available for New Zealanders to view online between 3-25 October. Ticket holders will be emailed a link and instructions for how to watch the films on Vimeo.</p></div>

<p>[button link="https://www.eventbrite.co.nz/e/show-me-shorts-international-time-zone-online-tickets-118797666017" size="large" text_size="beta"]Tickets via Eventbrite[/button]</p>

<div class="sms button" style="text-align: center;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>