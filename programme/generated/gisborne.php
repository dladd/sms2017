<h3 class="programme">10-17 Oct</h3>
<h2 class="programme"><a href="http://www.domecinema.co.nz/">Dome Cinema, Poverty Bay Club</a></h2>
<p>
Childers Rd and Customhouse St<br>
Gisborne 4010<br>
Ph: 027 5902117<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="gisborne-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 10 Oct</p></td>
<td width="25"><p>7:00 PM
<strong><a href="http://www.domecinema.co.nz/current-films/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler*</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 17 Oct</p></td>
<td width="25"><p>7:00 PM
<strong><a href="http://www.domecinema.co.nz/current-films/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler**</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<p>*This screening will include all the films from The Sampler session, plus local film Daddy's Girl (Kōtiro).</p>

<p>**This screening will include all the films from The Sampler session, plus local film Hāngī Pants.</p>

<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>