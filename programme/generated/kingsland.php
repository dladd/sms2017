<h3 class="programme">6 Oct</h3>
<h2 class="programme"><a href="https://www.thekingslander.co.nz/">The Kingslander</a></h2>
<p>
470 New North Rd<br>
Kingsland<br>
<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="kingsland-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Tue 6 Oct</p></td>
<td width="25"><p>7:00 PM
<strong><a href="https://www.eventbrite.co.nz/e/show-me-shorts-auckland-short-film-talk-tickets-118795272859">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/events/short-film-talks"><strong>Auckland Short Film Talk</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>