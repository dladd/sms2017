<h3 class="programme">10-11 Oct</h3>
<h2 class="programme"><a href="https://suter.statecinemas.co.nz">Suter Theatre, Suter Art Gallery</a></h2>
<p>
208 Bridge St<br>
Nelson<br>
Ph: 03 548 3885<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$17 General Admission<br>
$12 Children / Seniors<br>
$14 Students/Film Industry Guild<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="nelson-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 10 Oct</p></td>
<td width="25"><p>7:30 PM
<strong><a href="https://suter.statecinemas.co.nz/movie/show-me-shorts-2020-the-sampler">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 11 Oct</p></td>
<td width="25"><p>7:30 PM
<strong><a href="https://suter.statecinemas.co.nz/movie/show-me-shorts-2020-the-sampler">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>