<p>Children love short films! Introduce the small people in your life to the magic of cinema. This collection of films is suitable for the whole family. Take a wild ride on a tram that is off its tracks without brakes! Discover a meteorite that has crash-landed into Earth. Meet a magician who will teach us his magic tricks. Hop on a pirate ship in search of treasure. Learn the important art of making new friends.</p>

<p>Total length: 56 minutes<br>
Rating: PG Violence & scary scenes.
Best for: 7--12 years and above</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Rialto Cinemas Newmarket</option>
<option value="#Christchurch">Christchurch - Alice</option>
<option value="#Katikati">Katikati - The Junction Theatre</option>
<option value="#Matakana">Matakana - Matakana Cinemas</option>
<option value="#Napier">Napier - MTG Century Theatre</option>
<option value="#Titirangi">Titirangi - Lopdell House</option>
<option value="#Tākaka">Tākaka - The Village Theatre</option>
<option value="#Wellington">Wellington - Light House Cuba</option>
<option value="#Ōpōtiki">Ōpōtiki - Ōpōtiki De Luxe Theatre</option>
</select>
<h2>Spring</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/spring.jpg" />
<p>Animation, 8 mins, Netherlands</p>

<p>Dir: Andy Goralczyck 
Prod: Francesco Siddi, Ton Roosendaal
</p><div class="cf"></div><p>Spring is late this year. Covered by a sea of clouds, a valley stands still in the icy grip of winter. A shepherd girl and her loyal dog have to face ancient spirits in order to continue the cycle of life.</p>

<hr class="sessionBreak">

<h2>O28</h2><a href="https://vimeo.com/344326500" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/o28.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 5 mins, France</p>

<p>Dir/Writ: Otalia Caussé
Dir: Geoffroy Collin, Louise Grardel, Antoine Marchand, Robin Merle, Fabien Meyran 
Prod: Philippe Meis
</p><div class="cf"></div><p>In Lisbon, two married German tourists are riding the legendary Number 28 Tram. But what should they do when the brakes fail and they're sent hurtling downhill... with a baby on board?</p>

<hr class="sessionBreak">

<h2>Down To Earth</h2><a href="https://www.youtube.com/watch?time_continue=40&v=3x1wBH-H4Uk&feature=emb_logo" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/downtoearth.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Sci-fi, 10 mins, Australia</p>

<p>Dir: Nick Crowhurst
Writ: Stephanie Jaclyn
Prod: Jodie Kirkbride
<a href="https://youtu.be/JS_YFAqIw0o" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>Three young misfits venture into the Australian outback to find what they believe is a meteorite that has crash-landed near their country town, only to discover it's something far more mysterious.</p>

<hr class="sessionBreak">

<h2>To Gerard</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/togerard.jpg" />
<p>Animation, 8 mins, United States</p>

<p>Dir/Writ: Taylor Meacham
Prod: Jeff Hermann
<a href="https://www.youtube.com/watch?v=u-4vE6w9Cwo" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>A sprightly elderly man inspires a little girl to follow her dreams through magic.</p>

<hr class="sessionBreak">

<h2>Kindled</h2><a href="https://www.youtube.com/watch?v=RzQY-WzB8Bo&feature=youtu.be" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/kindled.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>
<p><strong>New Zealand Premiere</strong></p>Animation, 4 mins, New Zealand</p>

<p>Dir: Natasha Bishop 
Prod: Antony Thomas
</p><div class="cf"></div><p>A flame sprite and a wood sprite show that friendship can prevail, no matter the obstacles.</p>

<hr class="sessionBreak">

<h2>Tobi and the Turbobus</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/tobiandtheturbobus.jpg" />
<p>Animation, 8 mins, Germany</p>

<p>Dir/Writ/Prod: Verena Fels
Dir: Marc Angele
</p><div class="cf"></div><p>When Tobi's friend on the school bus abandons him, he must learn to make new friends and figure out who his tribe should be. A heartwarming story about growing up and finding your place in the wolf pack.</p>

<hr class="sessionBreak">

<h2>Prickly Jam</h2><a href="https://vimeo.com/359442419/61d1717423" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/pricklyjam.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 6 mins, New Zealand</p>

<p>Dir/Writ/Prod: James Cunningham
Writ: Kathryn Burnett
</p><div class="cf"></div><p>An end-of-days comedy animation about an amorous cricket and a starving hedgehog who wants to eat the last cricket left in the world.</p>

<hr class="sessionBreak">

<h2>Oeil Pour Oeil <br>(An Eye for an Eye)</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/oeilpouroeil.jpg" />
<p>Animation, 6 mins, France</p>

<p>Dir/Writ: Thomas Boileau, Alan Guimont, Robin Courtoise, Mathieu Lecroq, Malcolm Hunt, François Briantais 
Prod: ESMA
</p><div class="cf"></div><p>A comedic animation about an unlucky one-eyed pirate captain who keeps failing to find the treasure -- each time he tries with a new crew of pirates as one-eyed as he is.</p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Rialto Cinemas Newmarket</h3></a>
<table id="table--pink-stripes" summary="whānau-friendly_auckland-central-2020">
<tbody>
<tr>
<td width="50"><p>Mon 5 Oct</p></td>
<td width="50"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Whanau-Friendly#cinemas=751&date=2020-10-05">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Thu 8 Oct</p></td>
<td width="50"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Whanau-Friendly#cinemas=751&date=2020-10-08">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sat 10 Oct</p></td>
<td width="50"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Whanau-Friendly#cinemas=751&date=2020-10-10">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Matakana"></a>

<a href="http://www.showmeshorts.co.nz/programme/matakana"><h3>Matakana - Matakana Cinemas</h3></a>
<table id="table--pink-stripes" summary="whānau-friendly_matakana-2020">
<tbody>
<tr>
<td width="50"><p>Sat 17 Oct</p></td>
<td width="50"><p>1:00 PM
<strong><a href="https://www.matakanacinemas.co.nz/movie/show-me-shorts-2020-whanau-friendly">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Titirangi"></a>

<a href="http://www.showmeshorts.co.nz/programme/titirangi"><h3>Titirangi - Lopdell House</h3></a>
<table id="table--pink-stripes" summary="whānau-friendly_titirangi-2020">
<tbody>
<tr>
<td width="50"><p>Wed 7 Oct</p></td>
<td width="50"><p>10:30 AM
<strong><a href="https://www.eventbrite.com/e/show-me-shorts-whanau-friendly-in-titirangi-tickets-119830071971">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice</h3></a>
<table id="table--pink-stripes" summary="whānau-friendly_christchurch-2020">
<tbody>
<tr>
<td width="50"><p>Sat 17 Oct</p></td>
<td width="50"><p>1:00 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-whanau-friendly/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sun 18 Oct</p></td>
<td width="50"><p>1:00 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-whanau-friendly/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Wellington"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington"><h3>Wellington - Light House Cuba</h3></a>
<table id="table--pink-stripes" summary="whānau-friendly_wellington-2020">
<tbody>
<tr>
<td width="50"><p>Fri 9 Oct</p></td>
<td width="50"><p>4:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-whanau-friendly">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sun 11 Oct</p></td>
<td width="50"><p>2:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-whanau-friendly">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sat 17 Oct</p></td>
<td width="50"><p>4:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-whanau-friendly">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Napier"></a>

<a href="http://www.showmeshorts.co.nz/programme/napier"><h3>Napier - MTG Century Theatre</h3></a>
<table id="table--pink-stripes" summary="whānau-friendly_napier-2020">
<tbody>
<tr>
<td width="50"><p>Sat 10 Oct</p></td>
<td width="50"><p>2:00 PM
<strong><a href="https://www.mtghawkesbay.com/whats-on/show/653735/show-me-shorts-film-festival-wh-nau-friendly&refID=6#eventid">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Katikati"></a>

<a href="http://www.showmeshorts.co.nz/programme/katikati"><h3>Katikati - The Junction Theatre</h3></a>
<table id="table--pink-stripes" summary="whānau-friendly_katikati-2020">
<tbody>
<tr>
<td width="50"><p>Sun 4 Oct</p></td>
<td width="50"><p>2:00 PM
<strong><a href="https://www.theartsjunction.org.nz/what-s-on">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Ōpōtiki"></a>

<a href="http://www.showmeshorts.co.nz/programme/opotiki"><h3>Ōpōtiki - Ōpōtiki De Luxe Theatre</h3></a>
<table id="table--pink-stripes" summary="whānau-friendly_ōpōtiki-2020">
<tbody>
<tr>
<td width="50"><p>Sat 10 Oct</p></td>
<td width="50"><p>2:30 PM
<strong><a href="https://ticketing.oz.veezi.com/purchase/3587?siteToken=p1r5j984amrg0qeaey5brspj7r">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Tākaka"></a>

<a href="http://www.showmeshorts.co.nz/programme/takaka"><h3>Tākaka - The Village Theatre</h3></a>
<table id="table--pink-stripes" summary="whānau-friendly_tākaka-2020">
<tbody>
<tr>
<td width="50"><p>Fri 9 Oct</p></td>
<td width="50"><p>2:00 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>