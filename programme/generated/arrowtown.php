<h3 class="programme">18 Oct</h3>
<h2 class="programme"><a href="www.dorothybrowns.com">Dorothy Browns</a></h2>
<p>
18 Buckingham Street,<br>
Arrowtown<br>
Ph: 03 442 1964<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$18.50 Adult<br>
$15 Senior / Student / Film Industry Guild<br>
$12 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="arrowtown-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 18 Oct</p></td>
<td width="25"><p>5:00 PM
<strong><a href="http://www.dorothybrowns.com/film/show-me-shorts-the-sampler-2020">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>