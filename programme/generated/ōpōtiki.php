<h3 class="programme">10 Oct</h3>
<h2 class="programme"><a href="https://deluxetheatre.co.nz/">Ōpōtiki De Luxe Theatre</a></h2>
<p>
127 Church Street<br>
Ōpōtiki<br>
Ph: 07-315 6110<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$14 Adults<br>
$10 Students / Seniors / Film Industry Guilds<br>
$7 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="ōpōtiki-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 10 Oct</p></td>
<td width="25"><p>2:30 PM
<strong><a href="https://ticketing.oz.veezi.com/purchase/3587?siteToken=p1r5j984amrg0qeaey5brspj7r">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/whanau-friendly"><strong>Whānau Friendly</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 10 Oct</p></td>
<td width="25"><p>7:30 PM
<strong><a href="https://ticketing.oz.veezi.com/purchase/3588?siteToken=p1r5j984amrg0qeaey5brspj7r">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler*</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<p>*This screening will include all the films from The Sampler session, plus local film Hāngī Pants.</p>

<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>