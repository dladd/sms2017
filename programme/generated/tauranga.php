<h3 class="programme">18 Oct</h3>
<h2 class="programme"><a href="www.rialtotauranga.co.nz/">Rialto Cinemas</a></h2>
<p>
Goddard Centre, 21 Devonport Rd<br>
Tauranga, 3110<br>
Ph: 07 577 0445<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$12 General Admission<br>
$10 Children/Seniors/Students/Film Industry Guilds<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="tauranga-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 18 Oct</p></td>
<td width="25"><p>3:30 PM
<strong><a href="https://www.rialtotauranga.co.nz/movie/show-me-shorts-2020-the-sampler-plus-hng-pants">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler*</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<p>*This screening will include all the films from The Sampler session, plus local film Hāngī Pants.</p>

<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>