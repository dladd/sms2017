<h3 class="programme">30 Oct--6 Nov</h3>
<h2 class="programme"><a href="www.cssc.net.nz">Colville Hall, Colville Rd</a></h2>
<p>
Colville<br>
Ph: 07 866 6920<br>
<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$12.50 General Admission<br>
$20 Combo<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="colville-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Fri 30 Oct</p></td>
<td width="25"><p>7:30 PM
<strong><a href="http://www.cssc.net.nz/show-me-shorts.html">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/lets-get-physical"><strong>Let's Get Physical</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 6 Nov</p></td>
<td width="25"><p>7:30 PM
<strong><a href="http://www.cssc.net.nz/show-me-shorts.html">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>