<h3 class="programme">3--14 Oct</h3>
<h2 class="programme"><a href="www.rialto.co.nz">Rialto Cinemas Newmarket</a></h2>
<p>
167-169 Broadway<br>
Newmarket, Auckland<br>
Ph: 09 369 2417<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$27 Awards Ceremony (includes glass of wine)<br>
$18 General Admission<br>
$15 Students / Film Industry Guilds<br>
$13 Seniors / Children<br>
</p>
<p><strong>NOTE</strong>: For our opening night at the ASB Waterfront Theatre on October 2, please see the <a href="http://www.showmeshorts.co.nz/events/auckland-opening-night/" title="Auckland Opening Night">Auckland Opening Night</a> page or <a href="https://nz.patronbase.com/_ATC2/Productions/20SS/Performances" title="Book tickets to the Auckland Opening">click here to book tickets</a>.<p>

<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="auckland-central-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 3 Oct</p></td>
<td width="25"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Food-Fight#cinemas=751&date=2020-10-03">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/food-fights"><strong>Food Fights*</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 3 Oct</p></td>
<td width="25"><p>8:15 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Love-Lines#cinemas=751&date=2020-10-03">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/love-lines"><strong>Love Lines</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 4 Oct</p></td>
<td width="25"><p>3:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Moments-Of-Truth#cinemas=751&date=2020-10-04">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/moments-of-truth"><strong>Moments of Truth*</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 4 Oct</p></td>
<td width="25"><p>5:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Awards-Night#cinemas=751&date=2020-10-04">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/events/awards-night"><strong>Awards Ceremony</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 5 Oct</p></td>
<td width="25"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Whanau-Friendly#cinemas=751&date=2020-10-05">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/whanau-friendly"><strong>Whānau Friendly</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 5 Oct</p></td>
<td width="25"><p>6:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Lets-Get-Physical#cinemas=751&date=2020-10-05">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/lets-get-physical"><strong>Let's Get Physical</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 6 Oct</p></td>
<td width="25"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-The-Sampler#cinemas=751&date=2020-10-06">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 6 Oct</p></td>
<td width="25"><p>6:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Tangled-Worlds#cinemas=751&date=2020-10-06">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/tangled-worlds"><strong>Tangled Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 7 Oct</p></td>
<td width="25"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Love-Lines#cinemas=751&date=2020-10-07">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/love-lines"><strong>Love Lines</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 7 Oct</p></td>
<td width="25"><p>6:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Uk-Focus#cinemas=751&date=2020-10-07">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/uk-focus"><strong>UK Focus</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 8 Oct</p></td>
<td width="25"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Whanau-Friendly#cinemas=751&date=2020-10-08">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/whanau-friendly"><strong>Whānau Friendly</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 8 Oct</p></td>
<td width="25"><p>6:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Food-Fight#cinemas=751&date=2020-10-08">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/food-fights"><strong>Food Fights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 9 Oct</p></td>
<td width="25"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Moments-Of-Truth#cinemas=751&date=2020-10-09">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/moments-of-truth"><strong>Moments of Truth</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 9 Oct</p></td>
<td width="25"><p>8:15 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Lets-Get-Physical#cinemas=751&date=2020-10-09">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/lets-get-physical"><strong>Let's Get Physical</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 10 Oct</p></td>
<td width="25"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Whanau-Friendly#cinemas=751&date=2020-10-10">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/whanau-friendly"><strong>Whānau Friendly</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 10 Oct</p></td>
<td width="25"><p>8:15 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Uk-Focus#cinemas=751&date=2020-10-10">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/uk-focus"><strong>UK Focus</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 11 Oct</p></td>
<td width="25"><p>3:30 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-The-Sampler#cinemas=751&date=2020-10-11">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 11 Oct</p></td>
<td width="25"><p>5:30 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Tangled-Worlds#cinemas=751&date=2020-10-11">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/tangled-worlds"><strong>Tangled Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 12 Oct</p></td>
<td width="25"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Food-Fight#cinemas=751&date=2020-10-12">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/food-fights"><strong>Food Fights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 12 Oct</p></td>
<td width="25"><p>6:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Moments-Of-Truth#cinemas=751&date=2020-10-12">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/moments-of-truth"><strong>Moments of Truth</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 13 Oct</p></td>
<td width="25"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Uk-Focus#cinemas=751&date=2020-10-13">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/uk-focus"><strong>UK Focus</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 13 Oct</p></td>
<td width="25"><p>6:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Love-Lines#cinemas=751&date=2020-10-13">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/love-lines"><strong>Love Lines</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 14 Oct</p></td>
<td width="25"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Lets-Get-Physical#cinemas=751&date=2020-10-14">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/lets-get-physical"><strong>Let's Get Physical</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 14 Oct</p></td>
<td width="25"><p>6:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-The-Sampler#cinemas=751&date=2020-10-14">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<p>*World premiere screening with filmmaker intro</p>

<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>