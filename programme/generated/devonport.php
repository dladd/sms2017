<h3 class="programme">16 Oct</h3>
<h2 class="programme"><a href="www.thevic.co.nz">The Vic Devonport</a></h2>
<p>
48-56 Victoria Rd<br>
Devonport, Auckland<br>
Ph: 09 446 0100<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$16 General Admission<br>
$12.50 Students / Film Industry Guilds<br>
$11.50 Seniors<br>
$10.50 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="devonport-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Fri 16 Oct</p></td>
<td width="25"><p>8:00 PM
<strong><a href="https://www.thevic.co.nz/movie/show-me-shorts-2020-the-sampler">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>