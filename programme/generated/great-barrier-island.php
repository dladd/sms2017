<h3 class="programme">19 Oct</h3>
<h2 class="programme">Barrier Social Club</h2>
<p>
21 Medland Rd<br>
Tryphena, Great Barrier Island<br>
Ph: 09 429 0421<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$10 General Admission<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="great-barrier-island-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Mon 19 Oct</p></td>
<td width="25"><p>7:15 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>