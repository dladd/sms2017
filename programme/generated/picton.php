<h3 class="programme">23-24 Oct</h3>
<h2 class="programme"><a href="www.endeavourparkpicton.co.nz">Endeavour Park</a></h2>
<p>
181 Waikawa Road<br>
Picton<br>
Ph: 03 573 5400<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 Adult<br>
$12.00 Senior / Student / Film Industry Guild<br>
$10 Child<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="picton-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Fri 23 Oct</p></td>
<td width="25"><p>7:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/love-lines"><strong>Love Lines</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 24 Oct</p></td>
<td width="25"><p>7:00 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>