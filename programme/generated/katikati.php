<h3 class="programme">4 Oct</h3>
<h2 class="programme"><a href="https://www.theartsjunction.org.nz/">The Junction Theatre</a></h2>
<p>
The Arts Junction, 36 Main Road<br>
Katikati, Western Bay of Plenty<br>
Ph: 07 549 5250<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$10 General Admission<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="katikati-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 4 Oct</p></td>
<td width="25"><p>2:00 PM
<strong><a href="https://www.theartsjunction.org.nz/what-s-on">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/whanau-friendly"><strong>Whānau Friendly</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 4 Oct</p></td>
<td width="25"><p>6:30 PM
<strong><a href="https://www.theartsjunction.org.nz/what-s-on">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>