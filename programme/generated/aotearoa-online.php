<p>This collection of short films includes the work of rising stars of our film industry in Aotearoa New Zealand. Kiwis have built a reputation as some of the best short film makers in the world. Our aptitude for shorts could be owing to the `number-8 wire' attitude we share, and the strong oratory storytelling tradition of our Pacific heritage. Naturally there are some classic coming-of-age stories here, but you will also find comedy, sci-fi, fantasy and rock'n'roll. Enjoy the magic of cinema from perspectives of the Land of the Long White Cloud.</p>

<p>Total length: 90 minutes<br>
<p>This collection of short films is available for New Zealanders to view online between 3-25 October. Ticket holders will be emailed a link and instructions for how to watch the films on Vimeo.</p>
<p>[button link="https://www.eventbrite.co.nz/e/show-me-shorts-aotearoa-online-tickets-118797890689" size="large" text_size="beta"]Tickets via Eventbrite[/button]</p>

<h2>Oranges and Lemons </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/orangesandlemons.jpg" />
<p>Drama, 13 mins, New Zealand </p>

<p>Dir/Writ/Prod: Robyn Grace
Prod: Rory MacGillycuddy, Jarl Devine
<a href="https://www.youtube.com/watch?v=7UtF-OuOZQA" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>Oranges and Lemons is about summer heat and childhood bravery in a time gone by, when life was simpler but bullying was just as complicated. </p>

<hr class="sessionBreak">

<h2>Democracy </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/democracy.jpg" />
<p>Comedy, 14 mins, New Zealand </p>

<p>Dir/Writ: Finnius Teppett 
Prod: Bevin Linkhorn, Sadie Wilson
<a href="https://youtu.be/QqkX98z9AxY" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>Over breakfast one morning, Len's family votes him out from his position as Dad. Dejected, he packs his bags and heads to the office. When he launches his re-election campaign, Len discovers he's got unexpected competition. </p>

<hr class="sessionBreak">

<h2>We Rock! </h2><a href="https://vimeo.com/455626456" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/werock.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Documentary, 13 mins, New Zealand </p>

<p>Dir: Morgan Leary
Prod: Morgan Leigh Stewart
</p><div class="cf"></div><p>Girls Rock Camp Aotearoa is an empowering annual rock-and-roll camp for Kiwi girls, trans and non-binary youth. In this documentary we follow the campers throughout the week, witnessing their shyness transform into triumph. </p>

<hr class="sessionBreak">

<h2>Daddy's Girl <br>(Kōtiro)</h2><a href="https://vimeo.com/451457379" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/daddysgirl.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 13 mins, New Zealand </p>

<p>Dir/Writ: Cian Elyse White 
Prod: Tweedie Waititi
</p><div class="cf"></div><p>Te Puhi prepares herself for the toughest day of her life as the harsh reality of her father's dementia forces her to face the painful truth that she must let go of her daddy. Daddy's Girl is a story about the powerful love between a father and his daughter. </p>

<hr class="sessionBreak">

<h2>Aitu </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/aitu.jpg" />
<p>Fantasy, 7 mins, New Zealand </p>

<p>Dir/Writ: Piripi Curtis, Gabrielle Faaiuaso
Writ: Tim Worrall
Prod: Libby Hakaraia, Pauline Clague
</p><div class="cf"></div><p>While travelling at night, Niwa and her father are pulled over by a cop. They stop in a tapu (forbidden) area that is home to an aitu (malevolent spirit), who sees his chance for revenge. Luckily Niwa is descended from a long line of women with supernatural protective powers.</p>

<hr class="sessionBreak">

<h2>Biggie and Shrimp </h2><a href="https://vimeo.com/333670385" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/biggieandshrimp.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 14 mins, New Zealand </p>

<p>Dir/Writ: Harvey Hayes 
Prod: Lissandra Leite
</p><div class="cf"></div><p>On a downbeat housing estate, a teenager confronts his self-identity and masculinity, putting his only true friendship at risk. </p>

<hr class="sessionBreak">

<h2>Zealandia </h2><a href="https://vimeo.com/453163883" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/zealandia.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Sci-fi, 15 mins, New Zealand </p>

<p>Dir: Bruno du Bois 
Writ: Aline Tran 
Prod: Carlos Ramirez Laloli
<a href="https://www.youtube.com/watch?v=Gy0rGxPQnWE" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>In 2032, Zealandia is the only virus-free country left on earth, but at a high cost to its population due to strict medical regulations and border controls. Into this environment comes a woman carrying a bag... could this be the downfall of an oppressive state? </p>
<hr class="sessionBreak"><div style="padding-top:5rem;"><p>This collection of short films is available for New Zealanders to view online between 3-25 October. Ticket holders will be emailed a link and instructions for how to watch the films on Vimeo.</p></div>

<p>[button link="https://www.eventbrite.co.nz/e/show-me-shorts-aotearoa-online-tickets-118797890689" size="large" text_size="beta"]Tickets via Eventbrite[/button]</p>

<div class="sms button" style="text-align: center;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>