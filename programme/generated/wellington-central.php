<h3 class="programme">8--21 Oct</h3>
<h2 class="programme"><a href="www.lighthousecinema.co.nz">Light House Cuba</a></h2>
<p>
29 Wigan St, Te Aro<br>
Wellington<br>
Ph: 04 385 3337<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$17.50 General Admission<br>
$13.50 Seniors / Students / Film Industry Guilds<br>
$12.50 Children<br>
</p>
<p><strong>NOTE</strong>: For our opening night at the Embassy on October 11, please see the <a href="http://www.showmeshorts.co.nz/events/wellington-opening-night/" title="Wellington Opening Night">Wellington Opening Night</a> page or <a href="https://www.eventbrite.co.nz/e/show-me-shorts-wellington-opening-night-tickets-67945854881" title="Book tickets to the Wellington Opening">click here to book tickets</a>.<p>

<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="wellington-central-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Thu 8 Oct</p></td>
<td width="25"><p>6:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/events/wellington-opening-night"><strong>Wellington Opening Night</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 9 Oct</p></td>
<td width="25"><p>4:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/whānau-friendly"><strong>Whānau Friendly</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 9 Oct</p></td>
<td width="25"><p>8:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/love-lines"><strong>Love Lines</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 10 Oct</p></td>
<td width="25"><p>4:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/food-fights"><strong>Food Fights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 10 Oct</p></td>
<td width="25"><p>8:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/lets-get-physical"><strong>Let's Get Physical</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 11 Oct</p></td>
<td width="25"><p>2:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/whānau-friendly"><strong>Whānau Friendly</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 11 Oct</p></td>
<td width="25"><p>6:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 12 Oct</p></td>
<td width="25"><p>4:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/moments-of-truth"><strong>Moments of Truth</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 12 Oct</p></td>
<td width="25"><p>6:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/lets-get-physical"><strong>Let's Get Physical</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 13 Oct</p></td>
<td width="25"><p>4:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/tangled-worlds"><strong>Tangled Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 13 Oct</p></td>
<td width="25"><p>6:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/short-film-talk"><strong>Short Film Talk</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 14 Oct</p></td>
<td width="25"><p>4:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/love-lines"><strong>Love Lines</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 14 Oct</p></td>
<td width="25"><p>6:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/uk-focus"><strong>UK Focus</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 15 Oct</p></td>
<td width="25"><p>4:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 15 Oct</p></td>
<td width="25"><p>6:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/food-fights"><strong>Food Fights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 16 Oct</p></td>
<td width="25"><p>4:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/lets-get-physical"><strong>Let's Get Physical</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 16 Oct</p></td>
<td width="25"><p>8:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/moments-of-truth"><strong>Moments of Truth</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 17 Oct</p></td>
<td width="25"><p>4:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/whānau-friendly"><strong>Whānau Friendly</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 17 Oct</p></td>
<td width="25"><p>8:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/uk-focus"><strong>UK Focus</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 18 Oct</p></td>
<td width="25"><p>4:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 18 Oct</p></td>
<td width="25"><p>6:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/tangled-worlds"><strong>Tangled Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 19 Oct</p></td>
<td width="25"><p>2:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/food-fights"><strong>Food Fights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 19 Oct</p></td>
<td width="25"><p>6:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/moments-of-truth"><strong>Moments of Truth</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 20 Oct</p></td>
<td width="25"><p>4:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/uk-focus"><strong>UK Focus</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 20 Oct</p></td>
<td width="25"><p>6:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/love-lines"><strong>Love Lines</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 21 Oct</p></td>
<td width="25"><p>4:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/lets-get-physical"><strong>Let's Get Physical</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 21 Oct</p></td>
<td width="25"><p>6:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>