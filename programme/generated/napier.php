<h3 class="programme">10 Oct</h3>
<h2 class="programme"><a href="https://www.mtghawkesbay.com/whats-on/theatre">MTG Century Theatre</a></h2>
<p>
MTG Hawke's Bay<br>
Napier 4110<br>
Ph: (06) 835 7781<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$17 General Admission<br>
$13 Students / Seniors / Friends of the Museum / Film Industry Guilds<br>
$12 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="napier-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 10 Oct</p></td>
<td width="25"><p>2:00 PM
<strong><a href="https://www.mtghawkesbay.com/whats-on/show/653735/show-me-shorts-film-festival-wh-nau-friendly&refID=6#eventid">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/whanau-friendly"><strong>Whānau Friendly</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 10 Oct</p></td>
<td width="25"><p>6:00 PM
<strong><a href="https://www.mtghawkesbay.com/whats-on/show/653734/show-me-shorts-film-festival-the-sampler&refID=6#eventid">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>