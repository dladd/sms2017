<p>This vibrant selection of short films from the United Kingdom has been curated in collaboration with the London Short Film Festival and offers an eclectic mix of genres and styles. We meet a gay mouse working as a tailor and a little boy who believes his brother is a mermaid. We will dance our feelings, eavesdrop on disagreeing documentarians, and disrupt a seemingly happy marriage with a radio show. The movies express a variety of views and perspectives from some of the UK's brightest talent.</p>

<p>Total length: 76 minutes<br>
<p>This collection of short films is available for New Zealanders to view online between 3-25 October. Ticket holders will be emailed a link and instructions for how to watch the films on Vimeo.</p>
<p>[button link="https://www.eventbrite.co.nz/e/show-me-shorts-uk-focus-online-tickets-118797148469" size="large" text_size="beta"]Tickets via Eventbrite[/button]</p>

<h2>My Brother is a Mermaid</h2><a href="https://vimeo.com/manage/292204358/general" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/mybrotherisamermaid.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Fantasy, 20 mins, United Kingdom</p>

<p>Dir/Writ/Prod: Alfie Dale
</p><div class="cf"></div><p>This social realist fairytale is about a trans-feminine teenager, as seen through the eyes of their seven-year-old brother. Set in a desolate and prejudiced coastal town, the film examines how a child's unconditional love can be a powerful and disruptive force for good.</p>

<hr class="sessionBreak">

<h2>Just Agree Then</h2><a href="https://vimeo.com/292204358" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/justagreethen.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Documentary, 9 mins, United Kingdom</p>

<p>Dir: Duncan Cowles, Ross Hogg
<a href="https://www.youtube.com/watch?v=oPnVCaeFYHU" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>In August 2018, seven months before the UK is due to leave the EU, two stubborn Scottish filmmakers struggle to make a short film together in the Austrian Alps. Contrasting artistic visions, misplaced confidence and blatant ignorance collide against the backdrop of an increasingly fragile and divided world.</p>

<hr class="sessionBreak">

<h2>Dungarees</h2><a href="https://www.youtube.com/watch?v=n8IVUpebWIU&feature=emb_title" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/dungarees.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 6 mins, United Kingdom</p>

<p>Dir/Writ: Abel Rubinstein
Prod: Tara Trangmar, Ludwig Meslet
<a href="https://www.youtube.com/watch?v=OxmS_jX2NnE" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>What even is a male? Why can't I wear a dress? Fuck it. Let's have sex and worry about it later. Transgender Blake and cis-gender Cane hang out, play video games and grapple with their insecurities. This is their love story.</p>

<hr class="sessionBreak">

<h2>The Fabric of You</h2><a href="https://vimeo.com/355325319" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/thefabricofyou.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 11 mins, United Kingdom</p>

<p>Dir/Writ: Josephine Lohoar Self
Prod: Calum Hart, Reetta Tihinen
<a href="https://www.youtube.com/watch?v=aQuhtM-oie0" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>In this clever stop-motion animation, a gay mouse who works as a tailor struggles to come to terms with the loss of his lover, in an oppressive and unforgiving world where everybody wants to look the same.</p>

<hr class="sessionBreak">

<h2>The Circle</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/thecircle.jpg" />
<p>Dance, 16 mins, United Kingdom</p>

<p>Dir: Lanre Malaolu 
Prod: Elizabeth Benjamin
</p><div class="cf"></div><p>The Circle is a bold and lyrical portrayal of two brothers from inner-city London and the challenges they face daily, interpreted through dance.</p>

<hr class="sessionBreak">

<h2>Talk Radio</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/talkradio.jpg" />
<p>Comedy, 9 mins, United Kingdom</p>

<p>Dir/Writ: Ben S. Hyland
Prod: Adam Gregory Smith
<a href="https://www.youtube.com/watch?v=98W575GEQdI" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>Pauline and Barry are a seemingly happily married middle-aged couple. That is, until Pauline tunes in to a live talk-radio show and thinks she recognises the nameless voice relaying a lifetime of regrets.</p>

<hr class="sessionBreak">

<h2>Grandad Was A Romantic</h2><a href="https://vimeo.com/317002105" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/grandadwasaromantic.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 5 mins, United Kingdom</p>

<p>Dir/Writ/Prod: Maryam Mohajer
<a href="https://www.youtube.com/watch?v=xMQ2NPkznYg" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>My grandad was a romantic. He once saw a picture of my granny and realised that she was the love of his life. One day he decided to go meet my granny.</p>
<hr class="sessionBreak"><div style="padding-top:5rem;"><p>This collection of short films is available for New Zealanders to view online between 3-25 October. Ticket holders will be emailed a link and instructions for how to watch the films on Vimeo.</p></div>

<p>[button link="https://www.eventbrite.co.nz/e/show-me-shorts-uk-focus-online-tickets-118797148469" size="large" text_size="beta"]Tickets via Eventbrite[/button]</p>

<div class="sms button" style="text-align: center;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>