<h3 class="programme">15--21 Oct</h3>
<h2 class="programme"><a href="https://alice.co.nz">Alice</a></h2>
<p>
209 Tuam St.<br>
Christchurch<br>
Ph: 03 365 0615<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$17 General Admission<br>
$12 Seniors / Children / Students / Film Industry Guilds<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="christchurch-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Thu 15 Oct</p></td>
<td width="25"><p>6:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-the-sampler-3/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 16 Oct</p></td>
<td width="25"><p>6:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-love-lines/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/love-lines"><strong>Love Lines</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 16 Oct</p></td>
<td width="25"><p>8:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-lets-get-physical/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/lets-get-physical"><strong>Let's Get Physical</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 17 Oct</p></td>
<td width="25"><p>1:00 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-whanau-friendly/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/whanau-friendly"><strong>Whānau Friendly</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 17 Oct</p></td>
<td width="25"><p>6:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-food-fights/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/food-fights"><strong>Food Fights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 17 Oct</p></td>
<td width="25"><p>8:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-moments-of-truth/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/moments-of-truth"><strong>Moments of Truth</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 18 Oct</p></td>
<td width="25"><p>1:00 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-whanau-friendly/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/whanau-friendly"><strong>Whānau Friendly</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 18 Oct</p></td>
<td width="25"><p>4:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-the-sampler-3/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 18 Oct</p></td>
<td width="25"><p>6:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-tangled-worlds/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/tangled-worlds"><strong>Tangled Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 19 Oct</p></td>
<td width="25"><p>6:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-love-lines/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/love-lines"><strong>Love Lines</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 20 Oct</p></td>
<td width="25"><p>6:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-uk-focus/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/uk-focus"><strong>UK Focus</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 21 Oct</p></td>
<td width="25"><p>6:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-lets-get-physical/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/lets-get-physical"><strong>Let's Get Physical</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>