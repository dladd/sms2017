<h3 class="programme">28 Oct</h3>
<h2 class="programme"><a href="https://www.lidocinema.co.nz/">The Lido</a></h2>
<p>
Level 1, Centre Place<br>
501 Victoria St, Hamilton<br>
Ph: 07-838 9010<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$16 General Admission<br>
$13 Students/Film Industry Guilds<br>
$10 Seniors/Children<br>
Free - Hamilton Film Society Members<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="hamilton-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Wed 28 Oct</p></td>
<td width="25"><p>7:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>