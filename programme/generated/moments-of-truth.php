<p>Short films invoke delicious moments of intensity. They have a lot to say within a small time frame. The stories in this collection are about characters who are at a crossroads in life. They have to make a tough decision about where to go from here. We stand with them on the precipice, facing our future and taking a bold step forward into unknown territory.</p>

<p>Total length: 92 minutes<br>
Rating: M Suicide, offensive language & content that may disturb.<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Rialto Cinemas Newmarket</option>
<option value="#Christchurch">Christchurch - Alice</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#Wellington">Wellington - Light House Cuba</option>
</select>
<h2>Rebel</h2><a href="https://www.youtube.com/watch?v=6AOuP_9wTIg" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/rebel.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 15 mins, Canada</p>

<p>Dir/Writ: Pier-Philippe Chevigny
Prod: Geneviève Gosselin-G.
<a href="https://www.youtube.com/watch?v=rUt8FsHm1hg" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>To Alex, a naive six-year-old boy, the mysterious patrols led by his father's right-wing militia are nothing but opportunities to play hide-and-seek out in the woods. Until he ventures too far.</p>

<hr class="sessionBreak">

<h2>The Affected</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/theaffected.jpg" />
<p>Drama, 13 mins, Norway</p>

<p>Dir/Writ: Rikke Gregersen
Writ: Trond Arntzen
Prod: Stine Blichfeldt
</p><div class="cf"></div><p>Minutes before take-off, an airplane is prevented from leaving when a passenger refuses to sit down in order to stop the deportation of a man inside the plane. The pilot suddenly finds himself having to take a political stand that he has no interest in taking.</p>

<hr class="sessionBreak">

<h2>Hot Chocolate</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/hotchocolate.jpg" />
<p>
<p><strong>New Zealand Premiere</strong></p>Drama, 17 mins, New Zealand</p>

<p>Dir/Writ/Prod: David Hay
Prod: Tess Whelan
</p><div class="cf"></div><p>Maddie and her eight-year-old sister Nina stop by her student flat on their way to the family home. When Maddie discovers the body of one of her flatmates who has committed suicide, she tries to hold herself together and deal with the situation without alerting Nina to the situation.</p>

<hr class="sessionBreak">

<h2>Being There</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/beingthere.jpg" />
<p>
<p><strong>World Premiere</strong></p>Drama, 9 mins, Norway</p>

<p>Dir/Writ: Helen Komini Knudsen
Prod: Bente Maalen
</p><div class="cf"></div><p>A tragic scene takes place between a mother and daughter outside an animal clinic. Many observe the scene and pass by, but only one dares to really be there. This movie honors that one.</p>

<hr class="sessionBreak">

<h2>The Test</h2><a href="https://vimeo.com/437744829" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/thetest.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Comedy, 8 mins, Australia</p>

<p>Dir/Writ/Prod: Jessica Smith
Prod: January Jones
<a href="https://www.youtube.com/watch?v=gcsdrY7ZJsE" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>Two women anxiously await the results of a test that could allow them to take the next step in their relationship -- or put an end to their plans, and perhaps even their relationship.</p>

<hr class="sessionBreak">

<h2>Fire Me If You Can</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/firemeifyoucan.jpg" />
<p>Comedy, 13 mins, France</p>

<p>Dir/Writ: Camille Delamarre
Writ/Prod: Stéphane Landowski
Prod: Boris Mendza, Gaël Cabouat, David Atrakchi
</p><div class="cf"></div><p>When a company starts its redundancy plan, the cold-hearted Michel (aka The Sniper) is ready to fire. However, Pierrot is the company's go-to guy and is loved by everyone. Nobody has ever been able to resist his charm before.</p>

<hr class="sessionBreak">

<h2>White-Eye</h2><a href="https://vimeo.com/397624703" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/white-eye.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 20 mins, Israel</p>

<p>Dir/Writ: Tomer Shushan
Prod: Shira Hochman, Kobi Mizrahi
<a href="https://www.youtube.com/watch?v=mfd8YUxp3gE" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>A man finds his stolen bicycle, but it now belongs to a stranger. In his attempts to retrieve the bicycle, he struggles to remain human.</p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Rialto Cinemas Newmarket</h3></a>
<table id="table--pink-stripes" summary="moments-of-truth_auckland-central-2020">
<tbody>
<tr>
<td width="50"><p>Sun 4 Oct</p></td>
<td width="50"><p>3:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Moments-Of-Truth#cinemas=751&date=2020-10-04">Book tickets</a></strong></p></td>
<td width="50"><p>*World premiere screening with filmmaker intro</p></td>
</tr>
<tr>
<td width="50"><p>Fri 9 Oct</p></td>
<td width="50"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Moments-Of-Truth#cinemas=751&date=2020-10-09">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Mon 12 Oct</p></td>
<td width="50"><p>6:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Moments-Of-Truth#cinemas=751&date=2020-10-12">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice</h3></a>
<table id="table--pink-stripes" summary="moments-of-truth_christchurch-2020">
<tbody>
<tr>
<td width="50"><p>Sat 17 Oct</p></td>
<td width="50"><p>8:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-moments-of-truth/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Wellington"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington"><h3>Wellington - Light House Cuba</h3></a>
<table id="table--pink-stripes" summary="moments-of-truth_wellington-2020">
<tbody>
<tr>
<td width="50"><p>Mon 12 Oct</p></td>
<td width="50"><p>4:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-moments-of-truth">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Fri 16 Oct</p></td>
<td width="50"><p>8:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-moments-of-truth">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Mon 19 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-moments-of-truth">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="moments-of-truth_stewart-island-2020">
<tbody>
<tr>
<td width="50"><p>Sat 16 Jan</p></td>
<td width="50"><p>7:30 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>