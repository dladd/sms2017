<p>As in many cultures, mealtimes bring us together in Aotearoa. They are a time for sharing with our loved ones, but they can also be a source of tension, as these short films demonstrate. A fiery argument can start from something as simple as deciding where to eat out, or whose turn it is to buy cake decorations for the party. </p>

<p>Total length: 92 minutes<br>
Rating: M Offensive language<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Rialto Cinemas Newmarket</option>
<option value="#Christchurch">Christchurch - Alice</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#Wellington">Wellington - Light House Cuba</option>
</select>
<h2>Fun Factory </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/funfactory.jpg" />
<p>Drama, 12 mins, Norway </p>

<p>Dir/Writ: Lisa Brooke Hansen, Even Hafnor
Prod: Verona Meier
</p><div class="cf"></div><p>During a Saturday afternoon at a Norwegian play-land, a couple witnesses an argument between two mothers and the manager. The couple wonder if they should intervene, but the situation soon triggers underlying issues of their own. </p>

<hr class="sessionBreak">

<h2>Camera Obscura </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/cameraobscura.jpg" />
<p>Drama, 19 mins, France </p>

<p>Dir/Writ: Mary Noelle Dana, Sonia Sieff 
Prod: Christophe Starkman
<a href="https://www.youtube.com/watch?v=z4IFO9QHepU" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>A feminist college student working in catering on a high-end fashion shoot is pressured into replacing the unsuitable model. The film captures a slice of this world of false glamour, and her distaste for it. </p>

<hr class="sessionBreak">

<h2>Canelones </h2><a href="https://vimeo.com/445576125" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/canelones.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 13 mins, Argentina </p>

<p>Dir/Writ/Prod: Nicolas Mayer 
Writ/Prod: Diego Robaldo
Writ: Rocio Bilota
Prod: Manuel Tobar
</p><div class="cf"></div><p>Hernan and Chiri are teenagers making prank calls, and competing to see who can remain on the phone the longest. One call will lead them on an inner journey that will construct a bridge between childhood innocence and adulthood. </p>

<hr class="sessionBreak">

<h2>Foods For Coping </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/foodsforcoping.jpg" />
<p>
<p><strong>World Premiere</strong></p>Drama, 12 mins, New Zealand </p>

<p>Dir/Writ/Prod: Zoey Zhao 
Writ: Malinna Liang 
<a href="https://youtu.be/U8GZsqEyj2Q" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>Single mother Hui grapples with the fallout of discovering her 16-year-old daughter Yilin is pregnant. When Yilin refuses to terminate her pregnancy, Hui calls on help from the mother of Yilin's boyfriend. </p>

<hr class="sessionBreak">

<h2>As You Like </h2><a href="https://vimeo.com/443308725" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/asyoulike.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>
<p><strong>World Premiere</strong></p>Comedy, 11 mins, Israel </p>

<p>Dir/Writ/Prod: Maya Yadlin
<a href="https://www.youtube.com/watch?v=7mqUifwxb-U" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>A father, mother and their two daughters are at home doing their own thing when Dad makes a fun, spontaneous suggestion: to go to a restaurant together. Rather than bringing the family together, each step toward implementing the plan creates more and more complications and conflicts. </p>

<hr class="sessionBreak">

<h2>Hāngī Pants </h2><a href="https://vimeo.com/446176539" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/hāngīpants.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>
<p><strong>World Premiere</strong></p>Comedy, 13 mins, New Zealand </p>

<p>Dir/Writ: Jake Mokomoko 
Writ/Prod: Claire Varley
</p><div class="cf"></div><p>A flamboyant display of karanga (Māori welcome) takes place during the tangi (funeral) of a man beloved by many, when a succession of ex-wives show up and seek to out-do each other. </p>

<hr class="sessionBreak">

<h2>A Piece of Cake </h2><a href="https://www.youtube.com/watch?v=Omjny0Hhhts&feature=emb_logo" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/apieceofcake.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 12 mins, United States </p>

<p>Dir/Writ: The Bragg Brothers 
Prod: Lana Link, RD Delgado, Rob Pfaltzgraff
<a href="https://youtu.be/frGmKPKaJeo" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>When a desperate father discovers his daughter's favorite cake decoration is illegal, he descends into a confectionery black market. Now he must make the ultimate parenting choice: break a birthday promise or break the law? </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Rialto Cinemas Newmarket</h3></a>
<table id="table--pink-stripes" summary="food-fights_auckland-central-2020">
<tbody>
<tr>
<td width="50"><p>Sat 3 Oct</p></td>
<td width="50"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Food-Fight#cinemas=751&date=2020-10-03">Book tickets</a></strong></p></td>
<td width="50"><p>*World premiere screening with filmmaker intro</p></td>
</tr>
<tr>
<td width="50"><p>Thu 8 Oct</p></td>
<td width="50"><p>6:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Food-Fight#cinemas=751&date=2020-10-08">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Mon 12 Oct</p></td>
<td width="50"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Food-Fight#cinemas=751&date=2020-10-12">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice</h3></a>
<table id="table--pink-stripes" summary="food-fights_christchurch-2020">
<tbody>
<tr>
<td width="50"><p>Sat 17 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-food-fights/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Wellington"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington"><h3>Wellington - Light House Cuba</h3></a>
<table id="table--pink-stripes" summary="food-fights_wellington-2020">
<tbody>
<tr>
<td width="50"><p>Sat 10 Oct</p></td>
<td width="50"><p>4:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-food-fights#single-schedule">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Thu 15 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-food-fights#single-schedule">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Mon 19 Oct</p></td>
<td width="50"><p>2:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-food-fights#single-schedule">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="food-fights_stewart-island-2020">
<tbody>
<tr>
<td width="50"><p>Wed 13 Jan</p></td>
<td width="50"><p>7:30 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>