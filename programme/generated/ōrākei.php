<h3 class="programme">18 Oct</h3>
<h2 class="programme"><a href="https://www.silkyotter.co.nz/">Silky Otter Cinemas</a></h2>
<p>
228 Orakei Rd<br>
Orakei Bay Village<br>
Ph: 09 523 0400<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$21 General Admission<br>
$18 Students / Seniors / Film Industry Guilds<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="ōrākei-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 18 Oct</p></td>
<td width="25"><p>5:30 PM
<strong><a href="https://www.silkyotter.co.nz/movies/57340/show-me-shorts-the-sampler">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>