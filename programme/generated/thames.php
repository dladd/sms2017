<h3 class="programme">27 Oct</h3>
<h2 class="programme"><a href="http://www.cinemathames.co.nz/">Embassy Cinemas</a></h2>
<p>
708 Pollen Street, Thames<br>
07 868 6602<br>
<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
$12 Concession<br>
$10 Child<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="thames-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Tue 27 Oct</p></td>
<td width="25"><p>7:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>