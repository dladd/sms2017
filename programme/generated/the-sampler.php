<p>Sit back and enjoy some of the best and most vibrant short films from New Zealand and around the world. This collection provides a great way to sample what Show Me Shorts is all about. The films feature a stroll through the streets of Naples, comedian Will Ferrell as a psychologist, paragliders in Nepal, a living statue, a couple of young hustlers, a rugby-mad little girl, and a boy who loves to dance.</p>

<p>Total length: 89 minutes<br>
Rating: M Offensive language & suicide references.<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#Arrowtown">Arrowtown - Dorothy Browns</option>
<option value="#AucklandCentral">Auckland Central - Rialto Cinemas Newmarket</option>
<option value="#Christchurch">Christchurch - Alice</option>
<option value="#Colville">Colville - Colville Hall, Colville Rd</option>
<option value="#Devonport">Devonport - The Vic Devonport</option>
<option value="#Dunedin">Dunedin - Rialto Cinemas</option>
<option value="#Gisborne">Gisborne - Dome Cinema, Poverty Bay Club</option>
<option value="#GreatBarrierIsland">Great Barrier Island - Barrier Social Club</option>
<option value="#Hamilton">Hamilton - The Lido</option>
<option value="#Katikati">Katikati - The Junction Theatre</option>
<option value="#Matakana">Matakana - Matakana Cinemas</option>
<option value="#Napier">Napier - MTG Century Theatre</option>
<option value="#Nelson">Nelson - Suter Theatre, Suter Art Gallery</option>
<option value="#NewPlymouth">New Plymouth - 4th Wall Theatre</option>
<option value="#Picton">Picton - Endeavour Park</option>
<option value="#Russell">Russell - Cinema Kororareka</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#Takapuna">Takapuna - Monterey</option>
<option value="#Tauranga">Tauranga - Rialto Cinemas</option>
<option value="#Thames">Thames - Embassy Cinemas</option>
<option value="#Tākaka">Tākaka - The Village Theatre</option>
<option value="#WaihekeIsland">Waiheke Island - Waiheke Island Community Cinema</option>
<option value="#Waikaia">Waikaia - The Lodge 223</option>
<option value="#Wellington">Wellington - Light House Cuba</option>
<option value="#Whangārei">Whangārei - Whangārei Film Society</option>
<option value="#Wānaka">Wānaka - Rubys Cinemas</option>
<option value="#Ōpōtiki">Ōpōtiki - Ōpōtiki De Luxe Theatre</option>
<option value="#Ōrākei">Ōrākei - Silky Otter Cinemas</option>
</select>
<h2>Ashmina</h2><a href="https://www.youtube.com/watch?v=qSsARv3NUM4" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/ashmina.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 15 mins, Nepal/United Kingdom</p>

<p>Dir/Writ/Prod: Dekel Berenson
Prod: Merlin Merton, Dominic Davey
</p><div class="cf"></div><p>Ashmina is a 13-year-old girl living in Pokhara, Nepal, the paragliding capital of the world. The remote town is a busy tourist destination where the locals are profoundly affected by the swarms of tourists. Ashmina helps her family make ends meet by working at the landing field, packing parachutes in return for small change.</p>

<hr class="sessionBreak">

<h2>Money Honey</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/moneyhoney.jpg" />
<p>Drama, 10 mins, New Zealand</p>

<p>Dir/Writ: Isaac Knights-Washbourn
Prod: Lissandra Leite, Robby Peters
<a href="https://www.youtube.com/watch?v=MsrU0xggGJk" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>Stuck in the middle of Auckland's housing crisis, two young hustlers discover some money and try to double it in the hope of buying an epic sandwich.</p>

<hr class="sessionBreak">

<h2>Working Class</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/workingclass.jpg" />
<p>
<p><strong>World Premiere</strong></p>Comedy, 10 mins, New Zealand</p>

<p>Dir/Prod: Kyan Krumdieck
Writ: Gregory Cooper
Prod: Sophia Seaton
<a href="https://www.youtube.com/watch?v=hw6uFBxULeE" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>Gabby is a failed actor working as a living statue in Cathedral Square, Christchurch, and cleaning wealthy and entitled people's homes. One day she tries on the owner's clothes while cleaning, but when a fellow tradesperson arrives at the house she's forced to continue the act for this audience of one.</p>

<hr class="sessionBreak">

<h2>David</h2><a href="https://youtu.be/ZZksQfNH6os" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/david.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Comedy, 12 mins, United States</p>

<p>Dir/Writ/Prod: Zach Woods
Writ: Brandon Gardner
Prod: Andrew Porter, Kevin Chinoy & Francesca Silvestri
</p><div class="cf"></div><p>A severely depressed man reaches out for an emergency therapy session. He's not the only one who needs help.</p>

<hr class="sessionBreak">

<h2>Super Comfort</h2><a href="https://vimeo.com/284886838" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/supercomfort.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 15 mins, Finland</p>

<p>Dir/Writ: Kirsikka Saari
Prod: Sanna Kultanen
</p><div class="cf"></div><p>Taina is a middle-aged woman preparing for a much-anticipated weekend visit from her adult son and his girlfriend. But the more Taina tries to make everything perfect, the less comfortable they all feel.</p>

<hr class="sessionBreak">

<h2>Mezze Stagioni <br>(Transitional Seasons)</h2><a href="https://vimeo.com/339554641" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/transitionalseasons.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 10 mins, Italy</p>

<p>Dir/Writ/Prod: Riccardo Menicatti, Bruno Ugioli
Prod: Stefano D'Antuono
<a href="https://www.youtube.com/watch?v=gjM_BrIViAk" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>Pietro and Emilia are a couple chatting while getting ready for a ceremony. As they pass through each room of their house, time travels, revealing important moments of their life together.</p>

<hr class="sessionBreak">

<h2>Frankie Jean and the Morning Star</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/frankiejeanandthemorningstar.jpg" />
<p>
<p><strong>World Premiere</strong></p>Drama, 17 mins, New Zealand</p>

<p>Dir/Writ: Hannah Marshall
Prod: Tara Riddell, Gareth Williams
</p><div class="cf"></div><p>Frankie Jean is a little girl obsessed with rugby. Her dad is teaching her kicking skills and about not giving up. Early one morning she meets a depressed teenager while kicking a ball around, and passes on a vital lesson that might just save his life.</p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Rialto Cinemas Newmarket</h3></a>
<table id="table--pink-stripes" summary="the-sampler_auckland-central-2020">
<tbody>
<tr>
<td width="50"><p>Tue 6 Oct</p></td>
<td width="50"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-The-Sampler#cinemas=751&date=2020-10-06">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sun 11 Oct</p></td>
<td width="50"><p>3:30 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-The-Sampler#cinemas=751&date=2020-10-11">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Wed 14 Oct</p></td>
<td width="50"><p>6:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-The-Sampler#cinemas=751&date=2020-10-14">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Devonport"></a>

<a href="http://www.showmeshorts.co.nz/programme/devonport"><h3>Devonport - The Vic Devonport</h3></a>
<table id="table--pink-stripes" summary="the-sampler_devonport-2020">
<tbody>
<tr>
<td width="50"><p>Fri 16 Oct</p></td>
<td width="50"><p>8:00 PM
<strong><a href="https://www.thevic.co.nz/movie/show-me-shorts-2020-the-sampler">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="GreatBarrierIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/great-barrier-island"><h3>Great Barrier Island - Barrier Social Club</h3></a>
<table id="table--pink-stripes" summary="the-sampler_great-barrier-island-2020">
<tbody>
<tr>
<td width="50"><p>Mon 19 Oct</p></td>
<td width="50"><p>7:15 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Matakana"></a>

<a href="http://www.showmeshorts.co.nz/programme/matakana"><h3>Matakana - Matakana Cinemas</h3></a>
<table id="table--pink-stripes" summary="the-sampler_matakana-2020">
<tbody>
<tr>
<td width="50"><p>Sun 18 Oct</p></td>
<td width="50"><p>6:45 PM
<strong><a href="https://www.matakanacinemas.co.nz/movie/show-me-shorts-2020-the-sampler">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="WaihekeIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/waiheke-island"><h3>Waiheke Island - Waiheke Island Community Cinema</h3></a>
<table id="table--pink-stripes" summary="the-sampler_waiheke-island-2020">
<tbody>
<tr>
<td width="50"><p>Sun 4 Oct</p></td>
<td width="50"><p>7:30 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Ōrākei"></a>

<a href="http://www.showmeshorts.co.nz/programme/orakei"><h3>Ōrākei - Silky Otter Cinemas</h3></a>
<table id="table--pink-stripes" summary="the-sampler_ōrākei-2020">
<tbody>
<tr>
<td width="50"><p>Sun 18 Oct</p></td>
<td width="50"><p>5:30 PM
<strong><a href="https://www.silkyotter.co.nz/movies/57340/show-me-shorts-the-sampler">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Takapuna"></a>

<a href="http://www.showmeshorts.co.nz/programme/takapuna"><h3>Takapuna - Monterey</h3></a>
<table id="table--pink-stripes" summary="the-sampler_takapuna-2020">
<tbody>
<tr>
<td width="50"><p>Sun 18 Oct</p></td>
<td width="50"><p>5:30 PM
<strong><a href="https://www.montereytakapuna.co.nz/movie/show-me-shorts-2020-the-sampler">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice</h3></a>
<table id="table--pink-stripes" summary="the-sampler_christchurch-2020">
<tbody>
<tr>
<td width="50"><p>Thu 15 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-the-sampler-3/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sun 18 Oct</p></td>
<td width="50"><p>4:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-the-sampler-3/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Colville"></a>

<a href="http://www.showmeshorts.co.nz/programme/colville"><h3>Colville - Colville Hall, Colville Rd</h3></a>
<table id="table--pink-stripes" summary="the-sampler_colville-2020">
<tbody>
<tr>
<td width="50"><p>Fri 6 Nov</p></td>
<td width="50"><p>7:30 PM
<strong><a href="http://www.cssc.net.nz/show-me-shorts.html">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Picton"></a>

<a href="http://www.showmeshorts.co.nz/programme/picton"><h3>Picton - Endeavour Park</h3></a>
<table id="table--pink-stripes" summary="the-sampler_picton-2020">
<tbody>
<tr>
<td width="50"><p>Sat 24 Oct</p></td>
<td width="50"><p>7:00 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Wellington"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington"><h3>Wellington - Light House Cuba</h3></a>
<table id="table--pink-stripes" summary="the-sampler_wellington-2020">
<tbody>
<tr>
<td width="50"><p>Sun 11 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-the-sampler#single-schedule">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Thu 15 Oct</p></td>
<td width="50"><p>4:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-the-sampler#single-schedule">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sun 18 Oct</p></td>
<td width="50"><p>4:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-the-sampler#single-schedule">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Wed 21 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-the-sampler#single-schedule">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Arrowtown"></a>

<a href="http://www.showmeshorts.co.nz/programme/arrowtown"><h3>Arrowtown - Dorothy Browns</h3></a>
<table id="table--pink-stripes" summary="the-sampler_arrowtown-2020">
<tbody>
<tr>
<td width="50"><p>Sun 18 Oct</p></td>
<td width="50"><p>5:00 PM
<strong><a href="http://www.dorothybrowns.com/film/show-me-shorts-the-sampler-2020">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Dunedin"></a>

<a href="http://www.showmeshorts.co.nz/programme/dunedin"><h3>Dunedin - Rialto Cinemas</h3></a>
<table id="table--pink-stripes" summary="the-sampler_dunedin-2020">
<tbody>
<tr>
<td width="50"><p>Sun 18 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-The-Sampler---Dunedin#date=2020-10-18">Book tickets</a></strong></p></td>
<td width="50"><p>*This screening will include all the films from The Sampler session, plus local film Hot Chocolate.</p></td>
</tr>
</div>
</tbody>
</table><a name="Gisborne"></a>

<a href="http://www.showmeshorts.co.nz/programme/gisborne"><h3>Gisborne - Dome Cinema, Poverty Bay Club</h3></a>
<table id="table--pink-stripes" summary="the-sampler_gisborne-2020">
<tbody>
<tr>
<td width="50"><p>Sat 10 Oct</p></td>
<td width="50"><p>7:00 PM
<strong><a href="http://www.domecinema.co.nz/current-films/">Book tickets</a></strong></p></td>
<td width="50"><p>*This screening will include all the films from The Sampler session, plus local film Daddy's Girl (Kōtiro).</p></td>
</tr>
<tr>
<td width="50"><p>Sat 17 Oct</p></td>
<td width="50"><p>7:00 PM
<strong><a href="http://www.domecinema.co.nz/current-films/">Book tickets</a></strong></p></td>
<td width="50"><p>*This screening will include all the films from The Sampler session, plus local film Hāngī Pants.</p></td>
</tr>
</div>
</tbody>
</table><a name="Hamilton"></a>

<a href="http://www.showmeshorts.co.nz/programme/hamilton"><h3>Hamilton - The Lido</h3></a>
<table id="table--pink-stripes" summary="the-sampler_hamilton-2020">
<tbody>
<tr>
<td width="50"><p>Wed 28 Oct</p></td>
<td width="50"><p>7:30 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Napier"></a>

<a href="http://www.showmeshorts.co.nz/programme/napier"><h3>Napier - MTG Century Theatre</h3></a>
<table id="table--pink-stripes" summary="the-sampler_napier-2020">
<tbody>
<tr>
<td width="50"><p>Sat 10 Oct</p></td>
<td width="50"><p>6:00 PM
<strong><a href="https://www.mtghawkesbay.com/whats-on/show/653734/show-me-shorts-film-festival-the-sampler&refID=6#eventid">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Nelson"></a>

<a href="http://www.showmeshorts.co.nz/programme/nelson"><h3>Nelson - Suter Theatre, Suter Art Gallery</h3></a>
<table id="table--pink-stripes" summary="the-sampler_nelson-2020">
<tbody>
<tr>
<td width="50"><p>Sat 10 Oct</p></td>
<td width="50"><p>7:30 PM
<strong><a href="https://suter.statecinemas.co.nz/movie/show-me-shorts-2020-the-sampler">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sun 11 Oct</p></td>
<td width="50"><p>7:30 PM
<strong><a href="https://suter.statecinemas.co.nz/movie/show-me-shorts-2020-the-sampler">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="NewPlymouth"></a>

<a href="http://www.showmeshorts.co.nz/programme/new-plymouth"><h3>New Plymouth - 4th Wall Theatre</h3></a>
<table id="table--pink-stripes" summary="the-sampler_new-plymouth-2020">
<tbody>
<tr>
<td width="50"><p>Thu 8 Oct</p></td>
<td width="50"><p>7:30 PM
<strong><a href="http://www.4thwalltheatre.co.nz/onat4thwall/coming-soon/show-me-shorts-film-festival.html">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Russell"></a>

<a href="http://www.showmeshorts.co.nz/programme/russell"><h3>Russell - Cinema Kororareka</h3></a>
<table id="table--pink-stripes" summary="the-sampler_russell-2020">
<tbody>
<tr>
<td width="50"><p>Sun 18 Oct</p></td>
<td width="50"><p>7:00 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Tauranga"></a>

<a href="http://www.showmeshorts.co.nz/programme/tauranga"><h3>Tauranga - Rialto Cinemas</h3></a>
<table id="table--pink-stripes" summary="the-sampler_tauranga-2020">
<tbody>
<tr>
<td width="50"><p>Sun 18 Oct</p></td>
<td width="50"><p>3:30 PM
<strong><a href="https://www.rialtotauranga.co.nz/movie/show-me-shorts-2020-the-sampler-plus-hng-pants">Book tickets</a></strong></p></td>
<td width="50"><p>*This screening will include all the films from The Sampler session, plus local film Hāngī Pants.</p></td>
</tr>
</div>
</tbody>
</table><a name="Thames"></a>

<a href="http://www.showmeshorts.co.nz/programme/thames"><h3>Thames - Embassy Cinemas</h3></a>
<table id="table--pink-stripes" summary="the-sampler_thames-2020">
<tbody>
<tr>
<td width="50"><p>Tue 27 Oct</p></td>
<td width="50"><p>7:30 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Waikaia"></a>

<a href="http://www.showmeshorts.co.nz/programme/waikaia"><h3>Waikaia - The Lodge 223</h3></a>
<table id="table--pink-stripes" summary="the-sampler_waikaia-2020">
<tbody>
<tr>
<td width="50"><p>Wed 14 Oct</p></td>
<td width="50"><p>7:30 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Wānaka"></a>

<a href="http://www.showmeshorts.co.nz/programme/wanaka"><h3>Wānaka - Rubys Cinemas</h3></a>
<table id="table--pink-stripes" summary="the-sampler_wānaka-2020">
<tbody>
<tr>
<td width="50"><p>Sun 18 Oct</p></td>
<td width="50"><p>5:30 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Whangārei"></a>

<a href="http://www.showmeshorts.co.nz/programme/whangarei"><h3>Whangārei - Whangārei Film Society</h3></a>
<table id="table--pink-stripes" summary="the-sampler_whangārei-2020">
<tbody>
<tr>
<td width="50"><p>Thu 15 Oct</p></td>
<td width="50"><p>6:00 PM</p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Thu 15 Oct</p></td>
<td width="50"><p>8:00 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Katikati"></a>

<a href="http://www.showmeshorts.co.nz/programme/katikati"><h3>Katikati - The Junction Theatre</h3></a>
<table id="table--pink-stripes" summary="the-sampler_katikati-2020">
<tbody>
<tr>
<td width="50"><p>Sun 4 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://www.theartsjunction.org.nz/what-s-on">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Ōpōtiki"></a>

<a href="http://www.showmeshorts.co.nz/programme/opotiki"><h3>Ōpōtiki - Ōpōtiki De Luxe Theatre</h3></a>
<table id="table--pink-stripes" summary="the-sampler_ōpōtiki-2020">
<tbody>
<tr>
<td width="50"><p>Sat 10 Oct</p></td>
<td width="50"><p>7:30 PM
<strong><a href="https://ticketing.oz.veezi.com/purchase/3588?siteToken=p1r5j984amrg0qeaey5brspj7r">Book tickets</a></strong></p></td>
<td width="50"><p>*This screening will include all the films from The Sampler session, plus local film Hāngī Pants.</p></td>
</tr>
</div>
</tbody>
</table><a name="Tākaka"></a>

<a href="http://www.showmeshorts.co.nz/programme/takaka"><h3>Tākaka - The Village Theatre</h3></a>
<table id="table--pink-stripes" summary="the-sampler_tākaka-2020">
<tbody>
<tr>
<td width="50"><p>Sat 10 Oct</p></td>
<td width="50"><p>7:30 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="the-sampler_stewart-island-2020">
<tbody>
<tr>
<td width="50"><p>Sat 2 Jan</p></td>
<td width="50"><p>7:30 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>