<h3 class="programme">4 Oct</h3>
<h2 class="programme"><a href="www.waihekecinema.co.nz">Waiheke Island Community Cinema</a></h2>
<p>
Artworks, Oneroa<br>
Waiheke Island<br>
Ph: 09 372 4240<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$16 General Admission<br>
$12 Students / Seniors / Film Industry Guilds<br>
$9 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="waiheke-island-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 4 Oct</p></td>
<td width="25"><p>7:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>