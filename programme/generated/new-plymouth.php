<h3 class="programme">8 Oct</h3>
<h2 class="programme"><a href="www.4thwalltheatre.co.nz">4th Wall Theatre</a></h2>
<p>
11 Baring Terrace<br>
New Plymouth<br>
Ph: 0800 484 925<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
$12 Children / Students / Seniors / Film Industry Guilds<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="new-plymouth-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Thu 8 Oct</p></td>
<td width="25"><p>7:30 PM
<strong><a href="http://www.4thwalltheatre.co.nz/onat4thwall/coming-soon/show-me-shorts-film-festival.html">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>