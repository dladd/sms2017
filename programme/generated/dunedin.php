<h3 class="programme">18 Oct</h3>
<h2 class="programme"><a href="www.rialto.co.nz/cinema/dunedin">Rialto Cinemas</a></h2>
<p>
11 Moray Place<br>
Dunedin<br>
Ph: 03 474 2200<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$16 General Admission<br>
$13 Student / Film Industry Guilds<br>
$10 Senior/Child<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="dunedin-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 18 Oct</p></td>
<td width="25"><p>6:30 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-The-Sampler---Dunedin#date=2020-10-18">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler*</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<p>*This screening will include all the films from The Sampler session, plus local film Hot Chocolate.</p>

<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>