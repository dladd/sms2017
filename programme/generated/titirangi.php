<h3 class="programme">7 Oct</h3>
<h2 class="programme">Lopdell House</h2>
<p>
418 Titirangi Rd<br>
Auckland<br>
<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$14 General Admission<br>
$12 Students / Seniors / Film Industry Guilds<br>
$8 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="titirangi-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Wed 7 Oct</p></td>
<td width="25"><p>10:30 AM
<strong><a href="https://www.eventbrite.com/e/show-me-shorts-whanau-friendly-in-titirangi-tickets-119830071971">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/whanau-friendly"><strong>Whānau Friendly</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>