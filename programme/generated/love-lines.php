<p>We are all powerfully connected through a web of heartstrings. The short films collected here reflect the deep desire for human connection. The stories unspool the threads that bind us to one another -- sometimes stitched tightly, and other times they've come loose like the thread on a favourite jersey. These characters are seeking love, whether they know it or not.</p>

<p>Total length: 92 minutes<br>
Rating: R16 Rape, nudity, offensive language & sexual content.<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Rialto Cinemas Newmarket</option>
<option value="#Christchurch">Christchurch - Alice</option>
<option value="#Matakana">Matakana - Matakana Cinemas</option>
<option value="#Picton">Picton - Endeavour Park</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#Wellington">Wellington - Light House Cuba</option>
</select>
<h2>Daniel </h2><a href="https://vimeo.com/346833397/a209aafa2d" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/daniel.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 19 mins, New Zealand </p>

<p>Dir/Writ: Claire van Beek 
Prod: Alix Whittaker
<a href="https://youtu.be/edCgyoDKK0k" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>At an isolated convent in rural New Zealand, a young novice is confronted with the sudden desire to explore her sexual self in the wake of meeting a blue-tongued lizard. </p>

<hr class="sessionBreak">

<h2>Solstice d'un Coeur Brisé <br>(A Broken-Hearted Solstice) </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/abroken-heartedsolstice.jpg" />
<p>Comedy, 13 mins, Canada </p>

<p>Dir/Writ: Fanny Lefort 
Prod: Olivier Normandin
</p><div class="cf"></div><p>A professional mascot gets dumped on the longest day of the year. The icy mood she's plunged into doesn't fit with the heatwave she is experiencing. </p>

<hr class="sessionBreak">

<h2>The Trick </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/thetrick.jpg" />
<p>
<p><strong>World Premiere</strong></p>Drama, 13 mins, New Zealand </p>

<p>Dir/Writ/Prod: Harry McNaughton 
Dir: Natalie Medlock
Prod: Koro Dickinson
</p><div class="cf"></div><p>A lonely young toyboy hires a sex worker on his birthday, but he's looking for much more than a simple orgasm. Can true intimacy be reduced to a transaction? </p>

<hr class="sessionBreak">

<h2>Diagonal </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/diagonal.jpg" />
<p>Drama, 6 mins, Switzerland</p>

<p>Dir/Writ/Prod: Anne Thorens 
</p><div class="cf"></div><p>A young couple is already in the heat of the action when she asks him for a condom. He doesn't have one, but that doesn't seem to curb his enthusiasm. </p>

<hr class="sessionBreak">

<h2>Anna </h2><a href="https://vimeo.com/332215518" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/anna.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 15 mins, Ukraine/Israel/United Kingdom </p>

<p>Dir/Writ/Prod: Dekel Berenson
Prod: Merlin Merton, Olga Beskhmelnytsina
</p><div class="cf"></div><p>Anna, an ageing single mother in war-torn Eastern Ukraine, goes to a party to meet American men in search of love. Awkward, funny and touching, this award-winning film has screened at Cannes 2019 and more than 200 festivals globally. </p>

<hr class="sessionBreak">

<h2>Summer Thing by Alae </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/summerthingbyalae.jpg" />
<p>Music video, 4 mins, New Zealand </p>

<p>Dir/Writ: Vision Thing 
Prod: Blaine Stevenson
</p><div class="cf"></div><p>This music video is a tribute to classic mid-century musicals, and a love story between two creatures who naturally avoid sun and smiles. Proof that music and love conquer all stereotypes. </p>

<hr class="sessionBreak">

<h2>Nýr dagur í Eyjafirði <br>(Dovetail) </h2><a href="https://vimeo.com/345925559" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/dovetail.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 15 mins, Iceland </p>

<p>Dir/Writ: Magnús Leifsson 
Prod: Lalli Jonsson 
<a href="https://www.youtube.com/watch?v=THALMAimabk" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>Dovetail follows a man in his forties who has become trapped in a life that no longer fits him. In search of serenity, he imitates male stereotypes and dives into all the clichés of the material world. This short film is a lyrical presentation of beauty, grief and sorrow. </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Rialto Cinemas Newmarket</h3></a>
<table id="table--pink-stripes" summary="love-lines_auckland-central-2020">
<tbody>
<tr>
<td width="50"><p>Sat 3 Oct</p></td>
<td width="50"><p>8:15 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Love-Lines#cinemas=751&date=2020-10-03">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Wed 7 Oct</p></td>
<td width="50"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Love-Lines#cinemas=751&date=2020-10-07">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Tue 13 Oct</p></td>
<td width="50"><p>6:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Love-Lines#cinemas=751&date=2020-10-13">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Matakana"></a>

<a href="http://www.showmeshorts.co.nz/programme/matakana"><h3>Matakana - Matakana Cinemas</h3></a>
<table id="table--pink-stripes" summary="love-lines_matakana-2020">
<tbody>
<tr>
<td width="50"><p>Thu 15 Oct</p></td>
<td width="50"><p>6:45 PM
<strong><a href="https://www.matakanacinemas.co.nz/movie/show-me-shorts-2020-love-lines">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice</h3></a>
<table id="table--pink-stripes" summary="love-lines_christchurch-2020">
<tbody>
<tr>
<td width="50"><p>Fri 16 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-love-lines/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Mon 19 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-love-lines/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Picton"></a>

<a href="http://www.showmeshorts.co.nz/programme/picton"><h3>Picton - Endeavour Park</h3></a>
<table id="table--pink-stripes" summary="love-lines_picton-2020">
<tbody>
<tr>
<td width="50"><p>Fri 23 Oct</p></td>
<td width="50"><p>7:30 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Wellington"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington"><h3>Wellington - Light House Cuba</h3></a>
<table id="table--pink-stripes" summary="love-lines_wellington-2020">
<tbody>
<tr>
<td width="50"><p>Fri 9 Oct</p></td>
<td width="50"><p>8:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-love-lines">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Wed 14 Oct</p></td>
<td width="50"><p>4:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-love-lines">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Tue 20 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-love-lines">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="love-lines_stewart-island-2020">
<tbody>
<tr>
<td width="50"><p>Sat 23 Jan</p></td>
<td width="50"><p>7:30 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>