<p>We are made to move. The short films in this collection are vibrant -- pulsating with the energy of music, dance and sporting prowess. The stories are populated with reluctant boxers, overly competitive tennis players, a much-loved dance teacher, several fabulous drag queens and some super-ripped Ukrainians pumping iron. Something for every taste.</p>

<p>Total length: 85 minutes<br>
Rating: M Violence, offensive language & cruelty<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Rialto Cinemas Newmarket</option>
<option value="#Christchurch">Christchurch - Alice</option>
<option value="#Colville">Colville - Colville Hall, Colville Rd</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#Wellington">Wellington - Light House Cuba</option>
</select>
<h2>My Father The Mover </h2><a href="http://juliajansch.com/films/my-father-the-mover/" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/myfatherthemover.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Documentary, 13 mins, United States </p>

<p>Dir/Writ: Julia Jansch 
Prod: Mandilakhe Yengo, Sheila Nevins
<a href="https://www.youtube.com/watch?v=T8Ac23APecA" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>Alatha's father is 'The Mover' -- a dance teacher who helps kids in the South African township of Khayelitsha to transcend their hardship and ``find their superpowers''. Despite helping many kids, The Mover still has difficulty getting his own daughter to find her powers, but in a tender moment together this is about to change. </p>

<hr class="sessionBreak">

<h2>The Match </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/thematch.jpg" />
<p>Drama, 16 mins, Finland </p>

<p>Dir/Writ/Prod: Pia Andell
</p><div class="cf"></div><p>Two middle-aged women face off in a tense and riveting game of tennis. Their competitive attitudes transform the match into an extreme sport.</p>

<hr class="sessionBreak">

<h2>Pain </h2><a href="https://vimeo.com/390114028/14fa5ee4c7" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/pain.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 9 mins, New Zealand </p>

<p>Dir/Writ: Anna Rose Duckworth 
Prod: Ilai Amar
</p><div class="cf"></div><p>The story of a young girl's earth-shattering realisation that her father is not invincible after a cricket accident exposes his vulnerability. This leads her to question who her father is while struggling to grasp the concept of pain, both inside and out.</p>

<hr class="sessionBreak">

<h2>The Van </h2><a href="https://vimeo.com/ondemand/thevan/332732612" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/thevan.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 15 mins, France/Albania </p>

<p>Dir/Writ: Erenik Beqiri
Prod: Olivier Berlemont, Émilie Dubois, Ermir Keta, Amantia Peza
</p><div class="cf"></div><p>The van finally stops, the doors open and Ben comes out alive. A few more fights and he will be able to pay his way out of Albania, and hopefully take his father with him. </p>

<hr class="sessionBreak">

<h2>Hit Me Where It Hurts by Alae </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/hitmewhereithurtsbyalae.jpg" />
<p>Music video, 5 mins, New Zealand </p>

<p>Dir: Charlotte Evans, Alexander Gander, Chris Graham, Greg Page, Petra Cibilich
Prod: Mikee Carpinter
<a href="https://www.youtube.com/watch?v=_Hgurgm4Z5c" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>Heartbreak is tragic and nuanced. It can catch you in times of solitude and in public. It can make you want to scream, it can make you want to cry, and it can make you want to sing. This beautifully crafted music video brings together five of New Zealand's best music video directors. </p>

<hr class="sessionBreak">

<h2>Her Beneath Her </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/herbeneathher.jpg" />
<p>Documentary, 15 mins, New Zealand </p>

<p>Dir/Writ/Prod: Connor Slattery, Samantha Crews 
<a href="https://youtu.be/y1x4avdJ5j8" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>In the world of drag, an art form dedicated to celebrating femininity, female-identifying queens are not regarded as equals. This film documents the experiences of three 'hyper queens' and the challenges they face finding acceptance as women in a male-dominated industry. </p>

<hr class="sessionBreak">

<h2>Kachalka </h2><a href="https://vimeo.com/389741945" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/kachalka.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Documentary, 10 mins, Ireland </p>

<p>Dir: Gar O'Rourke 
Prod: Ken Wardrop 
<a href="https://www.youtube.com/watch?v=-ns3Gl8zb8I" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>A journey into the heart of the world's most hardcore gym -- Kiev's enormous open-air 'Kachalka' gym. This observational documentary takes us through the enormous scrap-metal site, allowing a glimpse of the workouts of various local gym-goers along the way. </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Rialto Cinemas Newmarket</h3></a>
<table id="table--pink-stripes" summary="let's-get-physical_auckland-central-2020">
<tbody>
<tr>
<td width="50"><p>Mon 5 Oct</p></td>
<td width="50"><p>6:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Lets-Get-Physical#cinemas=751&date=2020-10-05">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Fri 9 Oct</p></td>
<td width="50"><p>8:15 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Lets-Get-Physical#cinemas=751&date=2020-10-09">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Wed 14 Oct</p></td>
<td width="50"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Lets-Get-Physical#cinemas=751&date=2020-10-14">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice</h3></a>
<table id="table--pink-stripes" summary="let's-get-physical_christchurch-2020">
<tbody>
<tr>
<td width="50"><p>Fri 16 Oct</p></td>
<td width="50"><p>8:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-lets-get-physical/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Wed 21 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-lets-get-physical/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Colville"></a>

<a href="http://www.showmeshorts.co.nz/programme/colville"><h3>Colville - Colville Hall, Colville Rd</h3></a>
<table id="table--pink-stripes" summary="let's-get-physical_colville-2020">
<tbody>
<tr>
<td width="50"><p>Fri 30 Oct</p></td>
<td width="50"><p>7:30 PM
<strong><a href="http://www.cssc.net.nz/show-me-shorts.html">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Wellington"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington"><h3>Wellington - Light House Cuba</h3></a>
<table id="table--pink-stripes" summary="let's-get-physical_wellington-2020">
<tbody>
<tr>
<td width="50"><p>Sat 10 Oct</p></td>
<td width="50"><p>8:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-lets-get-physical#single-schedule">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Mon 12 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-lets-get-physical#single-schedule">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Fri 16 Oct</p></td>
<td width="50"><p>4:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-lets-get-physical#single-schedule">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Wed 21 Oct</p></td>
<td width="50"><p>4:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-lets-get-physical#single-schedule">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="let's-get-physical_stewart-island-2020">
<tbody>
<tr>
<td width="50"><p>Sat 9 Jan</p></td>
<td width="50"><p>7:30 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>