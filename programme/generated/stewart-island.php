<h3 class="programme">2-27 Jan</h3>
<h2 class="programme"><a href="http://www.bunkhousetheatre.co.nz/">Bunkhouse Theatre</a></h2>
<p>
Stewart Island<br>
027 867 9381<br>
<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
$13 Seniors / Students / Film Industry Guilds<br>
$10 Children<br>
$80 combo ticket / $70 concession<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="stewart-island-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 2 Jan</p></td>
<td width="25"><p>7:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 6 Jan</p></td>
<td width="25"><p>7:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/whanau-friendly"><strong>Whanāu Friendly</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 9 Jan</p></td>
<td width="25"><p>7:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/lets-get-physical"><strong>Let's Get Physical</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 13 Jan</p></td>
<td width="25"><p>7:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/food-fights"><strong>Food Fights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 16 Jan</p></td>
<td width="25"><p>7:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/moments-of-truth"><strong>Moments of Truth</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 20 Jan</p></td>
<td width="25"><p>7:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/tangled-worlds"><strong>Tangled Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 23 Jan</p></td>
<td width="25"><p>7:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/love-lines"><strong>Love Lines</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 27 Jan</p></td>
<td width="25"><p>7:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/uk-focus"><strong>UK Focus</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>