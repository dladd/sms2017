<p>We all exist in our own universe, but sometimes we are thrust into the worlds of others. We can become tangled in the lives and moralities of the people we meet along our path. The short films collected here take our characters into unexpected situations where they don't know what to expect, what is real, or where home is any more.</p>

<p>Total length: 82 minutes<br>
Rating: M Violence<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Rialto Cinemas Newmarket</option>
<option value="#Christchurch">Christchurch - Alice</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#Wellington">Wellington - Light House Cuba</option>
</select>
<h2>Figurant</h2><a href="https://vimeo.com/ondemand/figurant/377036458" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/figurant.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 14 mins, Czech Republic/France</p>

<p>Dir/Writ: Jan Vejnar
Prod: Kamila Dohnalová, Olivier Berlemont, Emilie Dubois
<a href="https://youtu.be/Qowe2wyMEvQ" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>A dishevelled man follows a group of workers looking for day-work in an industrial area. Soon, he's stripped of his clothes and identity, dressed in a military uniform and armed. His determination not to fall behind the others is tested by a series of increasingly unsettling events.</p>

<hr class="sessionBreak">

<h2>Nimic</h2><a href="https://www.youtube.com/watch?v=BnIc2cIe95A" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/nimic.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 12 mins, United States</p>

<p>Dir/Writ: Yorgos Lanthimos
Writ: Efthimis Filippou
Prod: Rekorder, Superprime, Merman, Droga5
</p><div class="cf"></div><p>A professional cellist has an encounter with a stranger on the subway that has unexpected, far-reaching ramifications for his life.</p>

<hr class="sessionBreak">

<h2>Fame <br>(Hunger)</h2><a href="https://vimeo.com/424841645" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/fame.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Art, 12 mins, Italy</p>

<p>Dir/Writ/Prod: Giuseppe Alessio Nuzzo
Prod: Eduardo Angeloni, Rai Cinema
</p><div class="cf"></div><p>On the waterfront and in the ancient centre of Naples, there is a crossroads of culture and colour. Here many social and economic groups collide in hope and despair, and a child's eyes are witnesses to her father's fate.</p>

<hr class="sessionBreak">

<h2>Career by Wax Chattels</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/careerbywaxchattels.jpg" />
<p>Music video, 4 mins, New Zealand</p>

<p>Dir/Writ: Dylan Pharazyn
Prod: Emma Mortimer
</p><div class="cf"></div><p>In this vivid music video, a sound recordist ventures into a haunted swamp to make field recordings, disturbing dark forces in her wake.</p>

<hr class="sessionBreak">

<h2>کلاف <br>(Tangle)</h2><a href="https://vimeo.com/454711417" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/tangle.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 8 mins, Iran, Islamic Republic </p>

<p>Dir/Writ: Maliheh Gholamzadeh
Prod: Seyed Javad Hosseini Nezhad
<a href="https://www.youtube.com/watch?v=pLIcMxhvxMA" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>This moving animation visually illustrates the way people who have left their homes and loved ones behind, through war and displacement, carry with them a quilt stitched of threads connecting them to their past.</p>

<hr class="sessionBreak">

<h2>Bambirak</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/bambirak.jpg" />
<p>
<p><strong>World Premiere</strong></p>Drama, 14 mins, Germany</p>

<p>Dir/Writ: Zamarin Wahdat
Prod: Joy Jorgensen, Rafael Langenscheidt
<a href="https://www.youtube.com/watch?v=vP98Rf-GCUw" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>A young girl is hiding in her father's truck. When he discovers her, he is forced to continue his workday with her along for the ride. This touching story of father--daughter bonding explores the plight of Afghani migrants who have settled in Germany, where they face mistrust and integration difficulties.</p>

<hr class="sessionBreak">

<h2>Trumpet</h2><a href="https://vimeo.com/423588266" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/trumpet.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Comedy, 17 mins, Switzerland</p>

<p>Dir/Writ: Kevin Haefelin
Prod: Youmi Haefelin-Roch
<a href="https://www.youtube.com/watch?v=XRwcSrFXC9M" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>A Japanese trumpet player on a cultural pilgrimage to discover New York City jazz experiences a hell of a night after getting lost in Brooklyn.</p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Rialto Cinemas Newmarket</h3></a>
<table id="table--pink-stripes" summary="tangled-worlds_auckland-central-2020">
<tbody>
<tr>
<td width="50"><p>Tue 6 Oct</p></td>
<td width="50"><p>6:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Tangled-Worlds#cinemas=751&date=2020-10-06">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sun 11 Oct</p></td>
<td width="50"><p>5:30 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Tangled-Worlds#cinemas=751&date=2020-10-11">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice</h3></a>
<table id="table--pink-stripes" summary="tangled-worlds_christchurch-2020">
<tbody>
<tr>
<td width="50"><p>Sun 18 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-tangled-worlds/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Wellington"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington"><h3>Wellington - Light House Cuba</h3></a>
<table id="table--pink-stripes" summary="tangled-worlds_wellington-2020">
<tbody>
<tr>
<td width="50"><p>Tue 13 Oct</p></td>
<td width="50"><p>4:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-tangled-worlds">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sun 18 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-tangled-worlds">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="tangled-worlds_stewart-island-2020">
<tbody>
<tr>
<td width="50"><p>Wed 20 Jan</p></td>
<td width="50"><p>7:30 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>