<h3 class="programme">18 Oct</h3>
<h2 class="programme"><a href="https://www.montereytakapuna.co.nz/">Monterey</a></h2>
<p>
32-34 Anzac Street<br>
Takapuna<br>
Ph: 09-666 0714<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$12 General Admission<br>
$10 Children/Seniors/Students/Film Industry Guilds<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="takapuna-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 18 Oct</p></td>
<td width="25"><p>5:30 PM
<strong><a href="https://www.montereytakapuna.co.nz/movie/show-me-shorts-2020-the-sampler">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>