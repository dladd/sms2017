<p>This bold collection of top new short films from the United Kingdom has been curated in collaboration with the London Short Film Festival. The seven short films explore the lives of Brits in the 21st century -- their concerns, fears, hopes and dreams. We delve into a variety of topics such as sex, love, family, displacement, drugs, dating and nights out on the town. The movies demonstrate a wide breadth of new filmmaking talent in the UK creative sector.</p>

<p>Total length: 73 minutes<br>
Rating: R16 Sexual abuse themes, violence, offensive language, sex scenes & drug references.<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Rialto Cinemas Newmarket</option>
<option value="#Christchurch">Christchurch - Alice</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#Wellington">Wellington - Light House Cuba</option>
</select>
<h2>End-O</h2><a href="https://vimeo.com/357587211" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/end-o.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Comedy, 15 mins, United Kingdom</p>

<p>Dir: Alice Seabright
Writ: Elaine Grace
Prod: Alexandra Blue, Kate Phibbs
</p><div class="cf"></div><p>Jaq is angry. But she also wants to have sex. This should be straightforward -- angry sex is an accepted form of copulation. But her body is out to sabotage her coitus in the worst way... endometriosis.</p>

<hr class="sessionBreak">

<h2>Mandem</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/mandem.jpg" />
<p>Drama, 10 mins, United Kingdom</p>

<p>Dir/Writ: John Ogunmuyiwa
Prod: Emily Everdee
<a href="https://www.youtube.com/watch?v=WlQ8RDXfLdw" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>A job is just a job, but as with anything, time flies when you're doing it with your best friend. And today's no different. We follow Ty and Malcolm as they go about their daily routine. Just another day for two really good friends.</p>

<hr class="sessionBreak">

<h2>Kingdom Come</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/kingdomcome.jpg" />
<p>Drama, 17 mins, United Kingdom</p>

<p>Dir: Sean Dunn 
Prod: Alex Polunin
</p><div class="cf"></div><p>A middle-class family spends the day together purchasing items in anticipation of the arrival of a new baby. We follow them into a consumerist dreamscape that soon dissolves into something nightmarish.</p>

<hr class="sessionBreak">

<h2>Drum</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/drum.jpg" />
<p>Drama, 9 mins, United Kingdom</p>

<p>Dir: Hayley Williams
Writ: Freddy Syborn
Prod: Tom Gardner
</p><div class="cf"></div><p>A displaced mother, her young daughter and her baby are hiding out in a school that's closed for the holidays. When they are discovered, the mother is faced with an impossible choice: does she abandon her little girl in order to try and keep her safe?</p>

<hr class="sessionBreak">

<h2>Pompeii</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/pompeii.jpg" />
<p>Drama, 8 mins, United Kingdom</p>

<p>Dir/Writ: Harry Lighton
Dir: Marco Alessi, Matthew Jacobs Morgan 
Prod: Sorcha Bacon
</p><div class="cf"></div><p>Tam gets on the first tube home, alone. In just a crop-top and high-waisted jeans, he feels conspicuous. So he plugs in his earphones and begins reliving his Halloween night out via his phone.</p>

<hr class="sessionBreak">

<h2>Hot and Tasty</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/hotandtasty.jpg" />
<p>Animation, 4 mins, United Kingdom</p>

<p>Two drunk friends accidentally walk into a crime scene.</p>
<p>Writ: Simona Mehandzhieva 
</p><div class="cf"></div><p>Dir/Writ: Laura Jayne Hodkin</p>

<hr class="sessionBreak">

<h2>The Plunge</h2><a href="https://vimeo.com/317270385" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2020/theplunge.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Comedy, 10 mins, United Kingdom</p>

<p>Dir: Simon Ryninks
Writ: Omar Khan
Prod: Tibo Travers
<a href="https://www.youtube.com/watch?v=zVFhfH86gGU" target="_blank">Special message from the filmmaker(s)</a>
</p><div class="cf"></div><p>After a night of passion, Emily tells Jay that she'd like to use a strap-on. Scared of losing a girl he has a connection with, Jay reluctantly agrees.</p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Rialto Cinemas Newmarket</h3></a>
<table id="table--pink-stripes" summary="uk-focus_auckland-central-2020">
<tbody>
<tr>
<td width="50"><p>Wed 7 Oct</p></td>
<td width="50"><p>6:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Uk-Focus#cinemas=751&date=2020-10-07">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sat 10 Oct</p></td>
<td width="50"><p>8:15 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Uk-Focus#cinemas=751&date=2020-10-10">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Tue 13 Oct</p></td>
<td width="50"><p>2:00 PM
<strong><a href="https://www.rialto.co.nz/Movie/Smsff20-Uk-Focus#cinemas=751&date=2020-10-13">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice</h3></a>
<table id="table--pink-stripes" summary="uk-focus_christchurch-2020">
<tbody>
<tr>
<td width="50"><p>Tue 20 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://alice.co.nz/movies/show-me-shorts-uk-focus/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Wellington"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington"><h3>Wellington - Light House Cuba</h3></a>
<table id="table--pink-stripes" summary="uk-focus_wellington-2020">
<tbody>
<tr>
<td width="50"><p>Wed 14 Oct</p></td>
<td width="50"><p>6:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-uk-focus#single-schedule">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sat 17 Oct</p></td>
<td width="50"><p>8:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-uk-focus#single-schedule">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Tue 20 Oct</p></td>
<td width="50"><p>4:30 PM
<strong><a href="https://www.lighthousecinema.co.nz/schedule/film/show-me-shorts-uk-focus#single-schedule">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="uk-focus_stewart-island-2020">
<tbody>
<tr>
<td width="50"><p>Wed 27 Jan</p></td>
<td width="50"><p>7:30 PM</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>