<h3 class="programme">19--23 Oct</h3>
<h2 class="programme"><a href="www.matakanacinemas.co.nz">Matakana Cinemas</a></h2>
<p>
2 Matakana Valley Rd<br>
Matakana<br>
Ph: 09 423 0218<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$17 General Admission<br>
$14 Students / Film Industry Guilds<br>
$13.50 Seniors<br>
$11.50 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="matakana-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Thu 15 Oct</p></td>
<td width="25"><p>6:45 PM
<strong><a href="https://www.matakanacinemas.co.nz/movie/show-me-shorts-2020-love-lines">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/love-lines"><strong>Love Lines</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 17 Oct</p></td>
<td width="25"><p>1:00 PM
<strong><a href="https://www.matakanacinemas.co.nz/movie/show-me-shorts-2020-whanau-friendly">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/whanau-friendly"><strong>Whānau Friendly</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 18 Oct</p></td>
<td width="25"><p>6:45 PM
<strong><a href="https://www.matakanacinemas.co.nz/movie/show-me-shorts-2020-the-sampler">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>