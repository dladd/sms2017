<h3 class="programme">18 Oct</h3>
<h2 class="programme"><a href="https://www.rubyscinema.co.nz/">Rubys Cinemas</a></h2>
<p>
50 Cardrona Valley Rd<br>
Wanaka 9381<br>
Ph: 03 443 6901<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$18.50 Adults<br>
$14.50 Students/Seniors<br>
$12.50 Children<br>
$14.50 Film Industry Guilds<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="wanaka-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 18 Oct</p></td>
<td width="25"><p>5:30 PM</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>