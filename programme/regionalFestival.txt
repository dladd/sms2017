

Show Me Shorts 
New Zealand International
Short Film Festival

Presented by Stuff [logo]


2019 





Kia ora and welcome 

Show Me Shorts is New Zealand's largest and most important international short film festival.

My team and I have curated a selection of seven of the best and most vibrant short films and one music video for screening in cinemas across Aotearoa. The films were selected from more than 2,000 submissions from filmmakers globally. 

The stories span a wide variety of genres, styles and topics. They come from as far flung as Norway, France and the Netherlands, and feature a lost camel, a stolen moon buggy, a history lesson, a hijacked car, and a reluctant gnome. Well known local actors Alison Bruce, Miriama McDowell and Jeffrey Thomas star in films that will screen alongside those with Hollywood names Jason Schwartzman and Jake Johnson.

We hope the stories we have brought together will inspire you, teach you something new, and make you feel all the feels!

Gina Dellabarca
Festival Director



It is my pleasure to welcome you to the 14th Show Me Shorts Film Festival.

A flourishing arts and cultural sector is an essential part of any society. Here in New Zealand, we have a long history of producing inspiring and innovative works that capture audiences not just here at home, but right around the world. The screen industry is a great example of this. Kiwi filmmakers are well-known for their creativity, and the Show Me Shorts festival plays an important role in nurturing our local talent. The festival is also committed to bringing cinema to communities across New Zealand, allowing more people to enjoy the artistry and imagination of short film.

To everyone attending the 2019 festival, I hope you enjoy the show! Whether you're left laughing, crying or wondering what exactly you just witnessed, I am sure these short films will stick with you long after the end credits are over.


Rt Hon Jacinda Ardern
Prime Minister of New Zealand
Minister for Arts, Culture and Heritage

The Sampler

Sit back and enjoy some of the best and most vibrant short films from New Zealand and around the world. This collection provides a great way to sample what Show Me Shorts is all about. The films feature a lost camel, a stolen moon buggy, a history lesson, a hijacked car, and a reluctant gnome. Well known local actors Alison Bruce, Miriama McDowell and Jeffrey Thomas feature in the films along with singer-songwriter Finn Andrews and Hollywood players Jason Schwartzman and Jake Johnson.

Estimated total run time: 109 minutes.
Rating TBC. Contact your cinema for details.

Amerigo and the New World        
Documentary, 14 mins, France        
America was not really discovered in 1492, yet soon after that date the name Amerigo started being used in relation to the American continent. This documentary explores the fake news around the fake discovery.        
Dir/Writ: Luis Briceno, Laurent Crouzeix
Prod: Jérémy Rochigneux, Manuel Labra

Nefta Football Club        
Drama, 17 mins, France        
In the south of Tunisia, two football fan brothers bump into a donkey lost in the middle of the desert on the border of Algeria. Strangely, the animal wears headphones over its ears.        
Dir/Writ: Yves Piat
Prod: Damien Megherbi, Justin Pechberty

Walk a Mile        
Drama, 16 mins, New Zealand        
A grumpy old man lives next door to a noisy family, whom he hates. All he wants is peace and quiet, but when something happens to his neighbours he realises how much he cares about them.        
Dir/Writ: Judith Cowley
Prod: Sarah Anne Dudley

Rū        
Thriller, 16 mins, New Zealand        
A pregnant woman must fight for her life when she inadvertently becomes the victim of a violent initiation.        
Dir/Writ/Prod: Awanui Simich-Pene        
Writ: Sebastian Hurrell        
Prod: Lindsay Gough

Memory Foam         
Drama, 15 mins, New Zealand        
As a middle aged married couple shop for a new bed, they begin to realise their bed might not be the only thing they've outgrown.        
Dir/Writ: Paloma Schneideman
Prod: Elspeth Grant

Finn Andrews - One By The Venom        
Music video, 3 mins, New Zealand        
A vignette-style music video for Finn Andrews' powerful ballad 'One By The Venom', all about the various ways one might end up meeting one's maker.        
Dir/Writ/Prod: Alexander Gandar        
Writ: Finn Andrews        
Prod: Tom Augustine, Amanda Robinson

Korte Kuitspier (Short Calf Muscle)         
Comedy, 13 mins, Netherlands        
An absurdist black comedy about Anders, who is different. But he doesn't know that. Yes, he's also gay, but that's not the point here. There is also this thing that others see but he doesn't.        Dir/Writ: Victoria Warmerdam        
Prod: Trent

To Plant a Flag        
Comedy, 15 mins, Norway        
In preparation for the moon landing in 1969, NASA sent a team of astronauts to the lunar landscapes of Iceland. Their hi-tech training mission soon discovers what obstacles one can meet when facing... an Icelandic sheep farmer.        
Dir/Writ: Bobbie Peers        
Prod: Ruben Thorkildsen, Henrik Hofstad










Vote for your favourite short!

Share your thoughts on Show Me Shorts, and vote for your favourite film to win the SAE People's Choice Award. Visit www.showmeshorts.co.nz to complete the survey online, or pick up a copy from a participating Show Me Shorts cinema. Just like our films, it's short and sweet.

[Logo: SAE]




Show Me Shorts Screenwriting Lab

Show Me Shorts Screenwriting Lab is a hothouse mentoring scheme for screenwriters and aspiring screenwriters to workshop new ideas for short film. This Lab is an extension of the previous version we have been running since 2010. It includes a full-day workshop with intensive feedback sessions and mentoring, plus follow-up sessions with a mentor who will guide each participant through the development of their script for up to one year.
Applications close Friday 29 November. The Lab is free to attend, but applications cost $20. It will take place in Auckland on a weekend in February 2020. We offer places for six local and two international short film makers to develop their short film concepts and scripts with the guidance and advice of experienced industry mentors.
Apply at www.showmeshorts.co.nz.

[Logos: University of Auckland, Pub Charity Ltd, The Wallace Foundation, NZFC]




Cinema venues and ticket information


ANTARCTICA

Date TBC
Scott Base
Ross Island
Antarctica
www.antarcticanz.govt.nz/scott-base

Ticket Prices
Free

TBD



ARROWTOWN

20 Oct
Dorothy Browns
18 Buckingham Street
Arrowtown
Ph: 03 442 1964
www.dorothybrowns.com

Ticket prices
$18.50 General Admission 
$15 Seniors/Students/Film Industry Guilds
$12 Children

Sun 20 Oct 
5:30 pm
http://www.dorothybrowns.com/film/show-me-shorts-film-festival-the-sampler-2019



AUCKLAND REGION -- DEVONPORT

18 Oct
The Vic Devonport
48-56 Victoria Road 
Devonport, Auckland
Ph: 09 446 0100
www.thevic.co.nz

Ticket prices
$16 General Admission
$12.50 Student / Film Industry Guilds
$11.50 Senior
$10.50 Child

Fri 18 Oct
8:00 pm*
https://ticketing.oz.veezi.com/purchase/39654?siteToken=Vk9gVvV%2bF0OF1pOaftJzvA%3d%3d
*Festival team will introduce this session



AUCKLAND REGION -- GREAT BARRIER ISLAND

21 Oct
Barrier Social Club
21 Medland Rd 
Tryphena, Great Barrier Island
Ph: 09 4290 421

Ticket price
$10 General Admission

Mon 21 Oct
7:15 pm*
*Festival team will introduce this session


AUCKLAND REGION -- WAIHEKE ISLAND

6 Oct
Waiheke Island Community Cinema
Artworks, Oneroa
Waiheke Island
Ph: 09 372 4240 
www.waihekecinema.co.nz
 
Ticket Prices
$16 General Admission
$12 Students/Seniors/Film Industry Guilds
$9 Children
 
Sun 6 Oct
7:30 pm*
Fri 18 Oct
4:30 pm
* Festival team will introduce this session



DUNEDIN

20 Oct
Rialto Cinemas Dunedin
11 Moray Pl 
Dunedin
Ph: 03 474 2200
https://www.rialto.co.nz/Cinema/Dunedin
 
Ticket Prices
$16 General Admission
$13 Student / Film Industry Guilds
$10 Senior/Child
 
Sun 20 Oct
6:30 pm
https://www.rialto.co.nz/Movie/Smsff19---The-Sampler#cinemas=750,751&date=2019-10-20



GISBORNE

19 Oct
Dome Cinema
Poverty Bay Club, Childers Rd and Customhouse St
Gisborne
Ph: 027 5902117 
http://www.domecinema.co.nz/
 
Ticket Prices
All tickets $14
 
Sat 19 Oct
7:00 pm



HAMILTON

18 Nov
Lido
Level 1, Centre Place, 501 Victoria St
Hamilton
Ph: 07 838 9010
www.lidocinema.co.nz


Ticket Prices
$16 General Admission
$13 Students/Film Industry Guilds
$10 Seniors/Hamilton Film Society Members

Mon 18 Nov
6:00 pm
https://ticketing.oz.veezi.com/purchase/25563?siteToken=3N0T%2F9VuMkGLknAhXvwHfg%3D%3D


NAPIER

6 Oct
MTG Century Theatre
1 Tennyson Street
Napier
Ph: 06 835 7781
www.mtghawkesbay.com

Ticket Prices
$17 General Admission
$13 Seniors/Students/Children/Film Industry Guilds/Friends of the Museum
$12 Children

Sun 6 Oct
6:00 pm


NELSON

12-13 Oct
Suter Theatre, Suter Art Gallery
208 Bridge St
Nelson
Ph: 03 548 3885

Ticket Prices
$17 General Admission
$14 Students/Film Industry Guilds
$12 Children/Seniors 

Sat 12 Oct
7:30 pm
https://suter.statecinemas.co.nz/movie/show-me-shorts-2019-the-sampler 
Sun 13 Oct
7:30 pm
https://suter.statecinemas.co.nz/movie/show-me-shorts-2019-the-sampler 



NEW PLYMOUTH

24 Oct
4th Wall Theatre
11 Baring Terrace
New Plymouth
Ph: 0800 484 925
www.4thwalltheatre.co.nz

Ticket Prices
$15 General Admission
$12 Seniors/Students/Children/Film Industry Guilds

Thu 24 Oct
7:30 pm
http://www.4thwalltheatre.co.nz/onat4thwall/coming-soon/show-me-shorts-film-festival.html.





RUSSELL

3 Nov
Cinema Kororareka
Town Hall, The Strand
Russell 
Ph: 022 079 2289
www.cinemakororareka.co.nz/
 
Ticket Prices
$10 General Admission


Sun 3 Nov
7:00 pm


TAURANGA

20 Oct
Rialto Cinemas Tauranga
21 Devonport Rd
Tauranga
Ph: 07 577 0445
www.rialtotauranga.co.nz
 
Ticket Prices
$15.50 General Admission
$15 Students/Film Industry Guilds
$10.50 Seniors/Children 

Sun 20 Oct
5:30 pm
https://www.rialtotauranga.co.nz/movie/show-me-shorts-2019




THAMES

22 Oct
Embassy Cinemas
708 Pollen Street
Thames
Ph: 07 868 6602
www.cinemathames.co.nz

Ticket Prices
$15 General Admission
$12 Seniors/Students/Film Industry Guilds
$10 Children

Tue 22 Oct
7:30 pm


WAIKAIA

16 Oct
The Lodge 223
27 Blaydon Street 
Waikaia
www.thelodge223.co.nz/

Ticket Prices
$15 General Admission 


Wed 16 Oct
7:30 pm


WAIKANAE

13 Oct
Shoreline Cinema        
10 Mahara Place
Waikanae
Ph: 04 902 8070
www.shorelinecinema.co.nz

Ticket Prices
$17.00 General Admission
$13.50 Seniors/Students/Film Industry Guilds
$10.00 Children

Sun 13 Oct
12:30 pm


WANAKA

20 Oct
Rubys Cinema
50 Cardrona Valley Rd
Wanaka
Ph: 03 443 6901
www.rubyscinema.co.nz

Ticket Prices
$18.50 General Admission
$14.50 Students/Seniors/Film Industry Guilds
$12.50 Children

Sun 20 Oct
5:30 pm
https://www.rubyscinema.co.nz/movie/show-me-shorts-the-sampler
 


WHANGAREI

17 Oct
Whangarei Film Society
Te Kohitanga Exhibition Hall, Forum North, 7 Rust Ave
Whangarei
Door sales only
www.whangareifilmsociety.org

Ticket Prices
$10 General Admission

Thu 17 Oct
6:00 pm


Other screenings

A more extensive programme of short films is showing in the following places: 

Auckland Opening Night & Award Ceremony ASB Waterfront Theatre: Sat 5 Oct
Auckland Rialto Cinemas Newmarket: Sun 6 -- Wed 16 Oct
Wellington Light House Cuba: Sat 13 -- Mon 22 Oct
Christchurch Alice: Thu 18 -- Wed 24 Oct
Titirangi Lopdell House: Sat 19 Oct
Matakana Matakana Cinemas: Sat 19 -- Wed 23 Oct
Picton Endeavour Park: Sat 12 Oct
Colville Colville Hall: Fri 1 - Sat 2 Nov


Please see our website www.showmeshorts.co.nz for full session times and ticket information.
Acknowledgements
 
Show Me Shorts is proudly New Zealand's leading international short film festival. The support of local businesses and goodwill of the filmmaking community are vital to our continued success. Huge thanks to our volunteers, sponsors and partners, plus our audience and wider community.

Ambassadors
Sir Richard Taylor
Miranda Harcourt ONZM
Te Radar
Kate Rodger
Nick Ward
Francesca Rudkin

Staff and volunteers
Festival director: Gina Dellabarca MNZM
Festival coordinator: Mark Prebble
Events coordinator: Harley Neville
Festival interns: Eva Ashmore, Jessica Lee
Programmers: Alisa Moore, Bonyta Watson, Clayton Barnett, Georgina Bloomfield, Gina Dellabarca, Kanna Yamasaki, Mandy Herrick, Mohammad Fahimi, Raymond Suen, Rebekah Ngatae, Renée Burgers
Opening night and awards: Laura Goldsmith, Grace Maguire
Web design & development: David Ladd
Communications support: Mary de Ruyter

Brand and creative direction 
Akin / studioakin.com

Publicity 
Siobhan Waterhouse, Miryam Jacobi - Mr Fahrenheit

Awards jury - national competition
Francesca Rudkin
Eiji Shimada
Michael Hurst
Nick Ward

Awards jury - international competition
Betsy Bauer
Dale Corlett
Peter Murdmaa

Board members
Andrew Cornwell (chair), Catherine Holland, Gina Dellabarca, Matthew Mudford, Paddy Buckley, Renée Burgers, Sally-Anne Kerr, Tamara Liebman 

Larger logos:
Stuff
Auckland Council
Foundation North
Department of Post
Akin
NZFC

Smaller logos:
Flicks.co.nz
The Lion Foundation
Wellington Community Trust
Pub Charity
Japan Foundation
Asia NZ Foundation
Japan Embassy
Rata Foundation
Waitematā Local Board
One Foundation
Wellington City Council
Southern Trust
Community Trust South
The Trusts Community Foundation
Four Winds Foundation
Devonport-Takapuna Local Board
Waitākere Ranges Local Board
Otago Community Trust
Blue Sky Community Trust
New Zealand Estonian Culture Group (Uus Meremaa Eesti Selts)
Antarctica NZ

