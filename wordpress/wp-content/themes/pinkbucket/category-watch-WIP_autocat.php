<?php 
/**
 * The template for displaying Category pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>

<div id="main" class="container container--main">

    <div class="grid">

		<div class="grid__item  two-thirds  palm-one-whole">
			<?php if (wpgrade::option('blog_archive_show_cat_billboard')) get_template_part('theme-partials/post-templates/header-category'); ?>

	if(wpgrade::option('blog_layout') == 'masonry') {
		$grid_class= 'class="grid  masonry" data-columns';
	} else {
		$grid_class = 'class="classic"';
	} ?>

        <?php if ( $paged < 2 ) : ?>

          <?php if (is_category( )) {
            $parent_category = get_query_var('cat');
            }
          ?>

          <h1><?php echo single_cat_title('', false); ?></h1>
          <?php if ( category_description() ) : ?>
            <div class="archive-meta"><?php echo category_description(); ?></div>
          <?php endif; ?>
          &nbsp;

	  <?php
	  $args = array(
	    'orderby' => 'name',
	    'order' => 'ASC',
	    'parent' => $parent_category
	    );
	  $subcategories = get_categories($args);
            echo '<div <?php echo $grid_class;?>>'
	      foreach($subcategories as $category) { 
		echo '<div class="<?php echo wpgrade::option('blog_layout')?>__item"><?php get_template_part('theme-partials/category-templates/content-'. wpgrade::option('blog_layout', 'masonry') ); ?></div>'

		echo '<p>Category: <a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </p> ';
		echo '<p> Description:'. $category->description . '</p>';
		echo '<p> Post Count: '. $category->count . '</p>';  } 
  	      ?>
            </div>

	  <div class="classic">
            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" style="padding-top: 76.417910447761%" href="http://showmeshorts.co.nz/?cat=screening-room">
		    <img alt="Screening Room" src="http://showmeshorts.co.nz/images/featured/sms2009_programme.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://localhost/sms2014/?cat=screening-room">
		      <h2 class="hN">Screening Room</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>The <a href="http://www.showmeshorts.co.nz/screeningroom/" title="Screening Room">Screening Room</a> is a monthly feature in which a team member or special guest discusses a selection of shorts freely available online. These typically follow a theme, with past examples including filmmakers who have transitioned to features, surrealist shorts, and holiday films. Check back in at the beginning of each month for new editions or <a href="http://visitor.constantcontact.com/manage/optin/ea?v=001GfcdKfB8aosy9CdH-IB4hw%3D%3D">subscribe to our monthly e-newsletter</a> to receive updates.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" style="padding-top: 76.417910447761%" href="http://showmeshorts.co.nz/watch/buy-the-dvd">
		    <img alt="DVD" src="http://www.showmeshorts.co.nz/wp-content/uploads/DVD.jpeg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/watch/buy-the-dvd">
		      <h2 class="hN">DVD</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>Searching for the perfect gift for the short-film enthusiast in your life? Or maybe just looking for some classic shorts to watch yourself? <a href="http://www.showmeshorts.co.nz/buy-the-dvd/" title="DVD">Grab a copy of our DVD</a> and bring the festival to the comfort of your own home.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" style="padding-top: 76.417910447761%" href="http://showmeshorts.co.nz/programme">
		    <img alt="Programme" src="http://www.showmeshorts.co.nz/images/2013/sms2013.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/programme">
		      <h2 class="hN">Programme</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>While the 2013 festival is now over, you can still <a href="http://www.showmeshorts.co.nz/programme/" title="Programme">browse the 2013 online programme</a> to peruse the selection, view trailers, and see last year's venues. You can find last year's winners and their prizes on our <a href="http://www.showmeshorts.co.nz/the-awards/" title="Awards page">Awards page</a>. You can also view last year's festival trailer, made by the talented <a href="http://www.jamesbrookman.net/" title="James Brookman Motion Picture Advertising">James Brookman</a> below. And of course, make sure to keep an eye out for this year's festival line-up announcement in October!</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
                  <img alt="Special Screenings" src="http://www.showmeshorts.co.nz/wp-content/uploads/SP_logo1-300x213.jpg">
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
                    <h2 class="hN">Special Screenings</h2>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>You can also find us bringing shorts to the NZ public throughout the year at other wonderful events like the <a href="http://http://artinthedark.co.nz/">Art in the Dark</a> festival in Ponsonby's Western Park in November. We also screen shorts before feature films at <a href="http://www.silopark.co.nz/Silo-Park/Cinema.aspx">Silo Park outdoor cinema</a> in Auckland's waterfront during the summer. For updates on impromptu screenings and upcoming events, <a href="http://visitor.constantcontact.com/manage/optin/ea?v=001GfcdKfB8aosy9CdH-IB4hw%3D%3D">sign up to our monthly e-newsletter</a>.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>
            &nbsp;

	  </div>
          <?php endif; ?>


            <?php if (have_posts()): ?>
                <div class="heading  heading--main">
                    <h2 class="hN"><?php echo single_cat_title( '', false ); ?></h2><span class="archive__side-title beta"><?php _e( 'Latest', 'bucket_txtd' ); ?></span>
                </div>
				
		<?php if ( category_description() ) : // Show an optional category description ?>
		    <div class="archive-meta"></div>
		<?php endif;

	            <div <?php echo $grid_class;?>>
                    <?php while (have_posts()): the_post(); ?><!--
                        --><div class="<?php echo wpgrade::option('blog_layout')?>__item"><?php get_template_part('theme-partials/post-templates/content-'. wpgrade::option('blog_layout', 'masonry') ); ?></div><!--
                 --><?php endwhile; ?>
                </div>
                <?php echo wpgrade::pagination();
	        else: get_template_part( 'no-results', 'index' ); endif; ?>
        </div><!--
     --><div class="grid__item  one-third  palm-one-whole  sidebar">
            <?php get_sidebar(); ?>
        </div>

    </div>
</div>
    
<?php get_footer(); ?>