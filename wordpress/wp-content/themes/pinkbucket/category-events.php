<?php 
/**
 * The template for displaying Category pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>

<div id="main" class="container container--main">

    <div class="grid">

		<div class="grid__item  two-thirds  palm-one-whole">
			<?php if (wpgrade::option('blog_archive_show_cat_billboard')) get_template_part('theme-partials/post-templates/header-category'); ?>

        <?php if ( $paged < 2 ) : ?>
          <h1>Events</h1>
	  <?php if ( category_description() ) : ?>
	    <div class="archive-meta"><?php echo category_description(); ?></div>
          <?php endif; ?>
         &nbsp;

	  <div class="classic">
            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap"  href="http://showmeshorts.co.nz/events/auckland-opening-night">
		    <img alt="Auckland Opening Night" src="http://www.showmeshorts.co.nz/images/webprogramme/events/aon.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/events/auckland-opening-night">
                    <h2 class="hN">Auckland Opening Night</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
		      <p>Join us for a very special night out, celebrating short films and the people who make them.<a href="http://www.showmeshorts.co.nz/events/auckland-opening-night" title="Auckland Opening Night">Click through</a> to find out more.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>


	  <div class="classic">
            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap"  href="http://showmeshorts.co.nz/events/awards-night">
		    <img alt="Awards Night" src="http://www.showmeshorts.co.nz/images/webprogramme/events/awardsnight.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/events/awards-night">
                    <h2 class="hN">Awards Night</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
		      <p>Be there to watch as the winners of the Show Me Shorts Awards are announced, prizes presented and the winning films played.<a href="http://www.showmeshorts.co.nz/events/awards-night" title="Awards Night">Click through</a> to find out more.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>


            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap"  href="http://showmeshorts.co.nz/events/wellington-opening-night">
		    <img alt="Wellington Opening Night" src="http://www.showmeshorts.co.nz/images/webprogramme/events/won.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/events/wellington-opening-night">
                    <h2 class="hN">Wellington Opening Night</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
		      <p>Celebrate local winners and watch all the award-winning films at the opening of the Wellington season. This event brings together short film makers and short film enthusiasts to toast our community’s best and brightest. <a href="http://www.showmeshorts.co.nz/events/wellington-opening-night" title="Wellington Opening Night">Click through</a> to find out more.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap"  href="http://showmeshorts.co.nz/events/short-film-talks/">
		    <img alt="Programme" src="http://www.showmeshorts.co.nz/images/webprogramme/events/asft.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/events/short-film-talks">
		      <h2 class="hN">Short Film Talks</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>Interested in learning more about short film making? Join us for the Short Film Talks during the festival. These two-hour sessions bring top short film makers together to each screen one of their works and discuss the joys and challenges of making that short film. <a href="http://www.showmeshorts.co.nz/events/short-film-talks" title="Short Film Talks">Click through</a> to find out more.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

	  <div class="classic">
            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap"  href="http://showmeshorts.co.nz/events/meet-the-festivals">
		    <img alt="Meet the festivals" src="http://www.showmeshorts.co.nz/images/webprogramme/events/2020/meetthefestivals.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/events/meet-the-festivals">
                    <h2 class="hN">Meet the Festivals</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
		      <p>A special online panel event with programmers and distributors from some of the best short film festivals from around the world <a href="http://www.showmeshorts.co.nz/events/meet-the-festivals" title="meet the festivals">Click through</a> to find out more.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            &nbsp;

	  </div>
          <?php endif; ?>


            <?php if (have_posts()): ?>
                <div class="heading  heading--main">
                    <h2 class="hN">Latest</h2>
                </div>
				
		<?php if ( category_description() ) : // Show an optional category description ?>
		    <div class="archive-meta"></div>
		<?php endif;

	            if(wpgrade::option('blog_layout') == 'masonry') {
		            $grid_class= 'class="grid  masonry" data-columns';
	            } else {
		            $grid_class = 'class="classic"';
	            } ?>

	            <div <?php echo $grid_class;?>>
                    <?php while (have_posts()): the_post(); ?><!--
                        --><div class="<?php echo wpgrade::option('blog_layout')?>__item"><?php get_template_part('theme-partials/post-templates/content-'. wpgrade::option('blog_layout', 'masonry') ); ?></div><!--
                 --><?php endwhile; ?>
                </div>
                <?php echo wpgrade::pagination();
	        endif; ?>
        </div><!--
     --><div class="grid__item  one-third  palm-one-whole  sidebar">
            <?php get_sidebar(); ?>
        </div>

    </div>
</div>
    
<?php get_footer(); ?>
