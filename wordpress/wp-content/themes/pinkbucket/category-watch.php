<?php 
/**
 * The template for displaying Category pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>

<div id="main" class="container container--main">

    <div class="grid">

		<div class="grid__item  two-thirds  palm-one-whole">
			<?php if (wpgrade::option('blog_archive_show_cat_billboard')) get_template_part('theme-partials/post-templates/header-category'); ?>

        <?php if ( $paged < 2 ) : ?>
          <h1>Watch</h1>
	  <?php if ( category_description() ) : ?>
	    <div class="archive-meta"><?php echo category_description(); ?></div>
          <?php endif; ?>
         &nbsp;

	  <div class="classic">
            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" style="padding-top: 76.417910447761%" href="http://showmeshorts.co.nz/category/watch/screening-room">
		    <img alt="Screening Room" src="http://showmeshorts.co.nz/images/featured/sms2009_programme.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://www.showmeshorts.co.nz/category/watch/screening-room/">
		      <h2 class="hN">Screening Room</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>The <a href="http://www.showmeshorts.co.nz/category/watch/screening-room/" title="Screening Room">Screening Room</a> is a monthly feature in which a team member or special guest discusses a selection of shorts freely available online. These typically follow a theme, with past examples including filmmakers who have transitioned to features, surrealist shorts, and holiday films. Check back in at the beginning of each month for new editions or <a href="http://eepurl.com/bnGAqf">subscribe to our monthly e-newsletter</a> to receive updates.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" style="padding-top: 76.417910447761%" href="http://showmeshorts.co.nz/watch/buy-the-dvd">
		    <img alt="DVD" src="http://www.showmeshorts.co.nz/images/featured/bestofdvd1_web.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/watch/buy-the-dvd">
		      <h2 class="hN">DVD</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>Searching for the perfect gift for the short-film enthusiast in your life? Or maybe just looking for some classic shorts to watch yourself? <a href="http://www.showmeshorts.co.nz/buy-the-dvd/" title="DVD">Grab a copy of our DVD</a> and bring the festival to the comfort of your own home.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" style="padding-top: 76.417910447761%" href="http://showmeshorts.co.nz/programme">
		    <img alt="Programme" src="http://www.showmeshorts.co.nz/images/misc/Programmes.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/programme">
		      <h2 class="hN">Programme</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>Each year Show Me Shorts curates a selection of the best short films from Aotearoa and around the world, plus several of the top local music videos. We divide these into themed sections so you see about eight grouped together around a common theme. <a href="http://www.showmeshorts.co.nz/programme">Click through</a> to find all the information about the films, screening dates and locations.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
                  <img alt="Special Screenings" src="http://www.showmeshorts.co.nz/images/misc/23SiloAudience.jpg">
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
                    <h2 class="hN">Special Screenings</h2>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>As part of our mission to connect New Zealanders with the best short films, we often curate special selections of our shorts for other festivals and events, such as <a href="http://www.splore.net/">Splore</a>, <a href="http://www.aucklandnz.com/lantern">Auckland Lantern Festival</a>, <a href="http://www.bigdayout.com">Big Day Out</a>, <a href="http://http://artinthedark.co.nz">Art in the Dark</a>, <a href="http://www.silopark.co.nz/Silo-Park/Cinema.aspx">Silo Cinema</a>, <a href="http://www.thebigidea.co.nz/survive-thrive">Survive &amp; Thrive</a> and <a href="<http://www.aucklandfilmsociety.org.nz/">Auckland Film Society</a>. We are also regularly invited to curate selections of New Zealand shorts for international festivals, such as <a href="<http://www.fedsquare.com/">Federation Square</a> in Melbourne, Australia; <a href="http://www.capalbiocinema.com/home.htm">Capalbio Cinema International Film Festival</a> in Rome, Italy; <a href="http://festivalregard.com/en/">Festival REGARD</a> in Sanguenay, Canada; and <a href="http://www.kff.tw/ehome01.aspx?ID=3D2">Kaohsiung Film Festival</a> in Taiwan. For updates on these screenings and events, sign up to our <a href="http://eepurl.com/bnGAqf">monthly e-newsletter</a>.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>
            &nbsp;

	  </div>
          <?php endif; ?>


            <?php if (have_posts()): ?>
                <div class="heading  heading--main">
                    <h2 class="hN">Latest</h2>
                </div>
				
		<?php if ( category_description() ) : // Show an optional category description ?>
		    <div class="archive-meta"></div>
		<?php endif;

	            if(wpgrade::option('blog_layout') == 'masonry') {
		            $grid_class= 'class="grid  masonry" data-columns';
	            } else {
		            $grid_class = 'class="classic"';
	            } ?>

	            <div <?php echo $grid_class;?>>
                    <?php while (have_posts()): the_post(); ?><!--
                        --><div class="<?php echo wpgrade::option('blog_layout')?>__item"><?php get_template_part('theme-partials/post-templates/content-'. wpgrade::option('blog_layout', 'masonry') ); ?></div><!--
                 --><?php endwhile; ?>
                </div>
                <?php echo wpgrade::pagination();
	        else: get_template_part( 'no-results', 'index' ); endif; ?>
        </div><!--
     --><div class="grid__item  one-third  palm-one-whole  sidebar">
            <?php get_sidebar(); ?>
        </div>

    </div>
</div>
    
<?php get_footer(); ?>