<?php 
/**
 * The template for displaying Category pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>

<div id="main" class="container container--main">

    <div class="grid">

		<div class="grid__item  two-thirds  palm-one-whole">
			<?php if (wpgrade::option('blog_archive_show_cat_billboard')) get_template_part('theme-partials/post-templates/header-category'); ?>

        <?php if ( $paged < 2 ) : ?>
          <h1>For Filmmakers</h1>
	  <?php if ( category_description() ) : ?>
	    <div class="archive-meta"><?php echo category_description(); ?></div>
          <?php endif; ?>
         &nbsp;

	  <div class="classic">
            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" href="http://showmeshorts.co.nz/for-filmmakers/short-film-lab">
		    <img alt="Short Film Lab" src="http://www.showmeshorts.co.nz/wp-content/uploads/lab-mb-hands-300x200.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/for-filmmakers/short-film-lab">
		      <h2 class="hN">Short Film Lab</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>The <a href="http://www.showmeshorts.co.nz/for-filmmakers/short-film-lab" title="Short Film Lab">Aotearoa Short Film Lab</a> is a prestigious hothouse mentoring scheme for screenwriters and aspiring screenwriters to workshop new ideas for short film. Each year, we connect a small group of promising screenwriters with industry veteran mentors to develop their short film ideas. <a href="http://www.showmeshorts.co.nz/short-film-lab">Click through</a> to find out more about the programme and how to apply.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap"  href="http://showmeshorts.co.nz/category/for-filmmakers/filmmaker-resources/">
		    <img alt="Filmmaker Resources" src="http://showmeshorts.co.nz/images/misc/filmingwellington.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/category/for-filmmakers/filmmaker-resources/">
		      <h2 class="hN">Filmmaker Resources</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>Running a short film festival over a decade and working closely with filmmakers has given us experience with some of the more common hurdles that are part of the short film making process. We are providing resources in the form of a series of articles in an effort to share some of this knowledge with developing filmmakers. Whether you are just starting out or looking to hone a particular part of your craft, <a href="http://www.showmeshorts.co.nz/category/for-filmmakers/filmmaker-resources/" title="Resources for Filmmakers">click through</a> and see if we can give you some useful ideas or tools.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap"  href="http://showmeshorts.co.nz/events/short-film/talks">
   		    <img alt="Programme" src="http://www.showmeshorts.co.nz/images/webprogramme/events/asft.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/events/short-film-talks">
		      <h2 class="hN">Short Film Talks</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>Interested in learning more about making short films? Our Short Film Talks provide a way to engage with the filmmakers and find out more about the craft. These two-hour sessions bring top short film makers together to each screen one of their works and discuss the joys and challenges of making that short film. <a href="http://www.showmeshorts.co.nz/events/short-film-talks" title="Short Film Talks">Click through</a> to find out more.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>
            &nbsp;

	  </div>
          <?php endif; ?>


            <?php if (have_posts()): ?>
                <div class="heading  heading--main">
                    <h2 class="hN">Latest</h2>
                </div>
				
		<?php if ( category_description() ) : // Show an optional category description ?>
		    <div class="archive-meta"></div>
		<?php endif;

	            if(wpgrade::option('blog_layout') == 'masonry') {
		            $grid_class= 'class="grid  masonry" data-columns';
	            } else {
		            $grid_class = 'class="classic"';
	            } ?>

	            <div <?php echo $grid_class;?>>
                    <?php while (have_posts()): the_post(); ?><!--
                        --><div class="<?php echo wpgrade::option('blog_layout')?>__item"><?php get_template_part('theme-partials/post-templates/content-'. wpgrade::option('blog_layout', 'masonry') ); ?></div><!--
                 --><?php endwhile; ?>
                </div>
                <?php echo wpgrade::pagination();
	        else: get_template_part( 'no-results', 'index' ); endif; ?>
        </div><!--
     --><div class="grid__item  one-third  palm-one-whole  sidebar">
            <?php get_sidebar(); ?>
        </div>

    </div>
</div>
    
<?php get_footer(); ?>