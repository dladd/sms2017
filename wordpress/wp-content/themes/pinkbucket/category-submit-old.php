<?php 
/**
 * The template for displaying Category pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>

<div id="main" class="container container--main">

    <div class="grid">

		<div class="grid__item  two-thirds  palm-one-whole">
			<?php if (wpgrade::option('blog_archive_show_cat_billboard')) get_template_part('theme-partials/post-templates/header-category'); ?>

        <?php if ( $paged < 2 ) : ?>
          <h1>Submit</h1>
	  <?php if ( category_description() ) : ?>
	    <div class="archive-meta"><?php echo category_description(); ?></div>
          <?php endif; ?>
         &nbsp;

	  <div class="classic">
            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" style="padding-top: 76.417910447761%" href="http://showmeshorts.co.nz/submit/enter-your-film">
		    <img alt="Enter Your Film" src="http://showmeshorts.co.nz/wp-content/uploads/2010/04/sms_official_selection_pink_RGB.png">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/submit/enter-your-film/">
		      <h2 class="hN">Enter Your Film</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>Entries for the 2014 festival are now closed but stay tuned for our call for entries in Februrary to enter for next year. <a href="http://showmeshorts.co.nz/submit/enter-your-film" title="Enter Your Film">Click through</a> to find out more or <a href="http://visitor.constantcontact.com/manage/optin/ea?v=001GfcdKfB8aosy9CdH-IB4hw%3D%3D">subscribe to our monthly e-newsletter</a> to receive updates about the festival (including the call for entries) and other great short film opportunities.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" style="padding-top: 76.417910447761%" href="http://showmeshorts.co.nz/submit/faq">
		    <img alt="FAQ" src="http://www.showmeshorts.co.nz/wp-content/uploads/lab-jm2-300x2001.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/submit/faq">
		      <h2 class="hN">Frequently Asked Questions</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>Looking for more information about entering? Check out our list of common questions to see if we can get you on your way. <a href="http://showmeshorts.co.nz/submit/faq" title="Enter Your Film">Click through</a> to find out more.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" style="padding-top: 76.417910447761%" href="http://showmeshorts.co.nz/the-awards/awards-criteria">
		    <img alt="Awards Criteria" src="http://www.showmeshorts.co.nz/images/featured/awardsEnvelopeSmall.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/the-awards/awards-criteria">
		      <h2 class="hN">Awards Criteria</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>To qualify as a New Zealand short film and be eligible for the awards, your film must not be available online, unless you are entering in the ‘Music video’ category and you must meet 2 of the 3 fundamental criteria, in addition to the criteria for specific awards.  <a href="http://www.showmeshorts.co.nz/the-awards/awards-criteria" title="Awards Criteria">Click through</a> for more information.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>
            &nbsp;

	  </div>
          <?php endif; ?>


            <?php if (have_posts()): ?>
                <div class="heading  heading--main">
                    <h2 class="hN"><?php echo single_cat_title( '', false ); ?></h2><span class="archive__side-title beta"><?php _e( 'Latest', 'bucket_txtd' ); ?></span>
                </div>
				
		<?php if ( category_description() ) : // Show an optional category description ?>
		    <div class="archive-meta"></div>
		<?php endif;

	            if(wpgrade::option('blog_layout') == 'masonry') {
		            $grid_class= 'class="grid  masonry" data-columns';
	            } else {
		            $grid_class = 'class="classic"';
	            } ?>

	            <div <?php echo $grid_class;?>>
                    <?php while (have_posts()): the_post(); ?><!--
                        --><div class="<?php echo wpgrade::option('blog_layout')?>__item"><?php get_template_part('theme-partials/post-templates/content-'. wpgrade::option('blog_layout', 'masonry') ); ?></div><!--
                 --><?php endwhile; ?>
                </div>
                <?php echo wpgrade::pagination();
	        else: get_template_part( 'no-results', 'index' ); endif; ?>
        </div><!--
     --><div class="grid__item  one-third  palm-one-whole  sidebar">
            <?php get_sidebar(); ?>
        </div>

    </div>
</div>
    
<?php get_footer(); ?>