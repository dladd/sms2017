<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 */
?>
<article <?php post_class('article  article--grid'); ?>>
	<?php get_template_part('theme-partials/post-templates/header-blog', get_post_format()); ?>
    <div class="article__meta  article--grid__meta_sub">
        <div class="article__category">
            <?php the_time('j M') ?>
            <span>  /  </span>
            <?php
                $categories = get_the_category();
                if ($categories) {
                    $category = $categories[0];
                    echo '<a class="small-link" href="'. get_category_link($category->term_id) .'" title="'. esc_attr(sprintf(__("View all posts in %s", 'bucket'), $category->name)) .'">'. $category->cat_name.'</a>';
                }
            ?>
        </div>
    </div>
    <div class="article--grid__body">
        <div class="article__content">
            <?php echo wpgrade_better_excerpt(); ?>
        </div>
    </div>    

</article><!-- .article -->