<?php
/**
 * If you would like to overwrite the css of the theme you will need to uncomment this action
 */

/* function add_editor_styles() { */
/*     add_editor_style( 'editor-style.css' ); */
/* } */
/* add_action( 'after_setup_theme', 'add_editor_styles' ); */

add_action('wp_enqueue_scripts', 'load_child_theme_styles', PHP_INT_MAX);

function load_child_theme_styles(){

// If your css changes are minimal we recommend you to put them in the main style.css.
    // In this case uncomment bellow
    wp_enqueue_style( 'parent-theme-style', get_stylesheet_directory_uri() . '/../bucket/theme-content/css/style.css' );
    wp_enqueue_style( 'MyFontsWebfontsKit', '/MyFontsWebfontsKit.css' );    
    wp_enqueue_style( 'child-theme-style', get_stylesheet_directory_uri() . '/style.css', array('parent-theme-style') );

    // If you want to create your own file.css you will need to load it like this (Don't forget to uncomment bellow) :
    //** wp_enqueue_style( 'custom-child-theme-style', get_stylesheet_directory_uri() . '/file.css' );

}
    
function the_slug() {
	$title = get_the_title();
    $slug = sanitize_title_with_dashes($title);
    return $slug;
}


/*
 * Load the translations from the child theme if present
 */
add_action( 'after_setup_theme', 'bucket_child_theme_setup' );
function bucket_child_theme_setup() {
	load_child_theme_textdomain( 'bucket_txtd', get_stylesheet_directory() . '/languages' );
}

// Remove WP Version From Styles	
add_filter( 'style_loader_src', 'sdt_remove_ver_css_js', 9999 );
// Remove WP Version From Scripts
add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999 );

// Function to remove version numbers
function sdt_remove_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}