<?php 
/**
 * The template for displaying Category pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>

<div id="main" class="container container--main">

    <div class="grid">

		<div class="grid__item  two-thirds  palm-one-whole">
			<?php if (wpgrade::option('blog_archive_show_cat_billboard')) get_template_part('theme-partials/post-templates/header-category'); ?>

        <?php if ( $paged < 2 ) : ?>
          <h1>Awards</h1>
	  <?php if ( category_description() ) : ?>
	    <div class="archive-meta"><?php echo category_description(); ?></div>
          <?php endif; ?>
         &nbsp;

	  <div class="classic">
            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" href="http://showmeshorts.co.nz/the-awards/previous-winners">
		    <img alt="Auckland Opening Night" src="http://www.showmeshorts.co.nz/wp-content/uploads/joeLeonieSmall.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/the-awards/previous-winners">
                    <h2 class="hN">Previous Winners</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>We love all of the films that make it into our programme each year but we also want to recognize those that stand out. Whether it is excellence in a particular category like directing and acting or outstanding overall achievement as exemplified by our Best Film, these films all present something special. Since 2012, our Best Film winner has also been eligible for the Academy Awards &reg;, giving Kiwi short film makers a shot at an Oscar &reg. <a href="http://www.showmeshorts.co.nz/the-awards/previous-winners" title="Previous Winners">Click through</a> to see a list of our past winners.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" href="http://showmeshorts.co.nz/the-awards/prizes">
		    <img alt="Prizes" src="http://www.showmeshorts.co.nz/images/misc/IMG_5614.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/the-awards/prizes/">
                    <h2 class="hN">Prizes</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
		      <p>Short film making can be a labour of love that all too often goes unrecognized. We love rewarding the hard work of our winning filmmakers and we hope these prizes will also help them make more films to entertain us. <a href="http://showmeshorts.co.nz/the-awards/prizes" title="Prizes">Click through</a> to see the prizes on offer from the festival and our amazing partners.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" href="http://showmeshorts.co.nz/the-awards/judges">
		    <img alt="Judges" src="http://www.showmeshorts.co.nz/images/misc/IMG_1831.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/the-awards/judges">
		      <h2 class="hN">Judges</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
    <p>Our awards are decided by a panel of experienced film industry practitioners, which changes every year. The winners are announced and their prizes presented at our Opening & Awards Ceremony. <a href="http://www.showmeshorts.co.nz/the-awards/judges" title="Judges">Click through</a> to find out more about our Jury.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" href="http://showmeshorts.co.nz/the-awards/awards-criteria">
		    <img alt="Awards Criteria" src="http://www.showmeshorts.co.nz/images/featured/awardsEnvelopeSmall.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/the-awards/awards-criteria">
		      <h2 class="hN">Awards Criteria</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>To qualify as a New Zealand short film and be eligible for the awards, your film must not be available online, unless you are entering in the ‘Music video’ category and you must meet 2 of the 4 fundamental criteria, in addition to the criteria for specific awards.  <a href="http://www.showmeshorts.co.nz/the-awards/awards-criteria" title="Awards Criteria">Click through</a> for more information.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>
            &nbsp;

	  </div>
          <?php endif; ?>


            <?php if (have_posts()): ?>
                <div class="heading  heading--main">
                    <h2 class="hN">Latest</h2>
                </div>
				
		<?php if ( category_description() ) : // Show an optional category description ?>
		    <div class="archive-meta"></div>
		<?php endif;

	            if(wpgrade::option('blog_layout') == 'masonry') {
		            $grid_class= 'class="grid  masonry" data-columns';
	            } else {
		            $grid_class = 'class="classic"';
	            } ?>

	            <div <?php echo $grid_class;?>>
                    <?php while (have_posts()): the_post(); ?><!--
                        --><div class="<?php echo wpgrade::option('blog_layout')?>__item"><?php get_template_part('theme-partials/post-templates/content-'. wpgrade::option('blog_layout', 'masonry') ); ?></div><!--
                 --><?php endwhile; ?>
                </div>
                <?php echo wpgrade::pagination();
	        endif; ?>
        </div><!--
     --><div class="grid__item  one-third  palm-one-whole  sidebar">
            <?php get_sidebar(); ?>
        </div>

    </div>
</div>
    
<?php get_footer(); ?>