<?php 
/**
 * The template for displaying Category pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>

<div id="main" class="container container--main">

    <div class="grid">

		<div class="grid__item  two-thirds  palm-one-whole">
			<?php if (wpgrade::option('blog_archive_show_cat_billboard')) get_template_part('theme-partials/post-templates/header-category'); ?>

        <?php if ( $paged < 2 ) : ?>
          <h1>Events</h1>
	  <?php if ( category_description() ) : ?>
	    <div class="archive-meta"><?php echo category_description(); ?></div>
          <?php endif; ?>
         &nbsp;

	  <div class="classic">
            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" style="padding-top: 76.417910447761%" href="http://showmeshorts.co.nz/events/auckland-opening-night">
		    <img alt="Auckland Opening Night" src="http://www.showmeshorts.co.nz/wp-content/uploads/openingNightAuckland4.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/events/auckland-opening-night">
                    <h2 class="hN">Auckland Opening Night &amp; Awards Ceremony</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
		      <p>This event kicks off our festival each year in style. Take a walk down the pink carpet, enjoy a glass of wine, rub shoulders with amazing short film makers, and see the winning films this November 6. <a href="http://www.showmeshorts.co.nz/events/auckland-opening-night" title="Auckland Opening Night">Click through</a> to find out more.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" style="padding-top: 76.417910447761%" href="http://showmeshorts.co.nz/events/wellington-opening-night">
		    <img alt="Wellington Opening Night" src="http://www.showmeshorts.co.nz/wp-content/uploads/wellingtonOpening.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/events/wellington-opening-night">
                    <h2 class="hN">Wellington Opening Night</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
		      <p>This is a great opportunity for Wellingtonians to see all of the winning films in one evening. Celebrate with short film makers and lovers alike and enjoy a complimentary glass of wine with us on November 13. <a href="http://www.showmeshorts.co.nz/events/wellington-opening-night" title="Wellington Opening Night">Click through</a> to find out more.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" style="padding-top: 76.417910447761%" href="http://showmeshorts.co.nz/events/short-film-talks/">
		    <img alt="Programme" src="http://www.showmeshorts.co.nz/images/2013/sms2013.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://showmeshorts.co.nz/events/short-film-talks">
		      <h2 class="hN">Short Film Talks</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>For those interested in the behind-the-scenes of some great short films, the Short Film Talks bring filmmakers and audiences together. These always popular and free-to-the-public sessions are presented during the Show Me Shorts Festival, in partnership with <a href="http://www.yoobee.ac.nz">Yoobee</a> and with support from the <a href="http://www.nzfilm.co.nz" target="_blank">New Zealand Film Commission</a>. <a href="http://www.showmeshorts.co.nz/events/short-film-talks" title="Short Film Talks">Click through</a> to find out more.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>
            &nbsp;

	  </div>
          <?php endif; ?>


            <?php if (have_posts()): ?>
                <div class="heading  heading--main">
                    <h2 class="hN"><?php echo single_cat_title( '', false ); ?></h2><span class="archive__side-title beta"><?php _e( 'Latest', 'bucket_txtd' ); ?></span>
                </div>
				
		<?php if ( category_description() ) : // Show an optional category description ?>
		    <div class="archive-meta"></div>
		<?php endif;

	            if(wpgrade::option('blog_layout') == 'masonry') {
		            $grid_class= 'class="grid  masonry" data-columns';
	            } else {
		            $grid_class = 'class="classic"';
	            } ?>

	            <div <?php echo $grid_class;?>>
                    <?php while (have_posts()): the_post(); ?><!--
                        --><div class="<?php echo wpgrade::option('blog_layout')?>__item"><?php get_template_part('theme-partials/post-templates/content-'. wpgrade::option('blog_layout', 'masonry') ); ?></div><!--
                 --><?php endwhile; ?>
                </div>
                <?php echo wpgrade::pagination();
	        endif; ?>
        </div><!--
     --><div class="grid__item  one-third  palm-one-whole  sidebar">
            <?php get_sidebar(); ?>
        </div>

    </div>
</div>
    
<?php get_footer(); ?>