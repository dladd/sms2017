<?php 
/**
 * The template for displaying Category pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>

<div id="main" class="container container--main">

    <div class="grid">

		<div class="grid__item  two-thirds  palm-one-whole">
			<?php if (wpgrade::option('blog_archive_show_cat_billboard')) get_template_part('theme-partials/post-templates/header-category'); ?>

        <?php if ( $paged < 2 ) : ?>
          <h1>For Educators</h1>
	  <?php if ( category_description() ) : ?>
	    <div class="archive-meta"><?php echo category_description(); ?></div>
          <?php endif; ?>
         &nbsp;

	  <div class="classic">

        <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" style="padding-top: 76.417910447761%" href="http://showmeshorts.co.nz/for-educators/shorts-in-schools-kit">
		    <img alt="Show Me Shorts in Schools kit" src="http://www.showmeshorts.co.nz/wp-content/uploads/print.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://www.showmeshorts.co.nz/for-educators/shorts-in-schools-kit/">
                    <h2 class="hN">Show Me Shorts in Schools kit</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>The Show Me Shorts in Schools kit provides students and teachers with an educational framework through which to explore New Zealand short films and the wider themes they address. The kit covers nine kiwi shorts and provides background information and four lesson plans for each film designed based on NZ curriculum requirements. The kit has now been made freely available in an effort to support remote teachers and home-schooling parents by providing quality educational materials during the coronavirus pandemic. You can find <a href="http://www.showmeshorts.co.nz/teaching-shorts-in-schools" title="Show Me Shorts in Schools kit info">more info on how to use these resources here</a> or <a href="http://www.showmeshorts.co.nz/for-educators/shorts-in-schools-kit" title="Show Me Shorts in Schools kit">go straight through to the films and resources</a>.</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>

            <div class="classic__item">
  	      <article class="format-standard has-post-thumbnail hentry category-watch media flush--bottom grid">
		<div class="media__img--rev grid__item five-twelfths palm-one-whole">
		  <a class="image-wrap" style="padding-top: 76.417910447761%" href="http://showmeshorts.co.nz/for-educators/school-screenings">
		    <img alt="Screenings For School Groups" src="http://www.showmeshorts.co.nz/wp-content/uploads/schoolScreening.jpg">
		  </a>
		</div>
		<div class="media__body grid__item seven-twelfths palm-one-whole">
		  <div class="article__title article--thumb__title">
		    <a href="http://www.showmeshorts.co.nz/for-educators/school-screenings/">
                    <h2 class="hN">Screenings for School Groups</h2>
		    </a>
		  </div>
		  <div class="article--grid__body">
		    <div class="article__content">
                      <p>Show Me Shorts offers screenings for school groups in cinemas throughout NZ as part of the festival each November. This is a specially designed programme for students aged 8-16 to view high-quality, fun, and educational short films on the big screen. <a href="http://www.showmeshorts.co.nz/for-educators/school-screenings" title="Screenings for School Groups">Click through</a> to find out more</p>
		    </div>
		  </div>
		</div>
	      </article>
              <hr class="separator separator--subsection">
  	    </div>
            &nbsp;

	  </div>
          <?php endif; ?>


            <?php if (have_posts()): ?>
                <div class="heading  heading--main">
                    <h2 class="hN">Latest</h2>
                </div>
				
		<?php if ( category_description() ) : // Show an optional category description ?>
		    <div class="archive-meta"></div>
		<?php endif;

	            if(wpgrade::option('blog_layout') == 'masonry') {
		            $grid_class= 'class="grid  masonry" data-columns';
	            } else {
		            $grid_class = 'class="classic"';
	            } ?>

	            <div <?php echo $grid_class;?>>
                    <?php while (have_posts()): the_post(); ?><!--
                        --><div class="<?php echo wpgrade::option('blog_layout')?>__item"><?php get_template_part('theme-partials/post-templates/content-'. wpgrade::option('blog_layout', 'masonry') ); ?></div><!--
                 --><?php endwhile; ?>
                </div>
                <?php echo wpgrade::pagination();
	        endif; ?>
        </div><!--
     --><div class="grid__item  one-third  palm-one-whole  sidebar">
            <?php get_sidebar(); ?>
        </div>

    </div>
</div>
    
<?php get_footer(); ?>
