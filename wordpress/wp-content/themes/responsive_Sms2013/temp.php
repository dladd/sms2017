        <div class="image_carousel">
	    <div id="foo2">
		<div class="slide">
		     <img src="images/webprogramme/films/cockatoo.jpg" alt="Cockatoo" width="600" height="340" />
		     <div>
			   <h4>Cockatoo</h4>
			   <p>On the six month anniversary of his break-up with his ex-girlfriend, Michael orders a girl from an agency</p>
		     </div>
		</div>
		<div class="slide">
		     <img src="images/webprogramme/films/catcam.jpg" alt="CatCam" width="600" height="340" />
		     <div>
			   <h4>Catcam</h4>
			   <p>An engineer’s cat routinely disappeared from his North Carolina home for days on end, so his owner created a camera to fit around the feline’s neck</p>
		     </div>
		</div>
		<div class="slide">
		     <img src="images/webprogramme/films/ellenisleaving.jpg" alt="Ellen is Leaving" width="600" height="340" />
		     <div>
			   <h4>Ellen is Leaving</h4>
			   <p>On the eve of departing overseas, Ellen makes the fateful decision to gift her boyfriend to another girl</p>
		     </div>
		</div>
	    </div>
	    <div class="clearfix"></div>
    	    <a class="prev" id="foo2_prev" href="#"><span>prev</span></a>
	    <a class="next" id="foo2_next" href="#"><span>next</span></a>
	    <div class="pagination" id="foo2_pag"></div>
        </div>









		$('#foo4').carouFredSel({
			responsive: true,
			width: '100%',
			scroll: 1,
			items: {
				width: 600,
			//	height: '30%',	//	optionally resize item-height
				visible: {
					min: 1,
					max: 1
				}
			}
		});




		$("#foo2").carouFredSel({
			responsive : true,
			items		: {
				width		: 600,
 			        start		: "random",
				visible: {
					min: 1,
					max: 1
				}
			},
			scroll	: {	
 		                pauseOnHover	: true,
				duration	: 500
			},
		        auto    : 5000,
			prev	: {	
				button	: "#foo2_prev",
				key		: "left"
			},
			next	: { 
				button	: "#foo2_next",
				key		: "right"
			},
			pagination	: "#foo2_pag"
		});