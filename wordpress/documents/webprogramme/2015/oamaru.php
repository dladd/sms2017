<h3 class="programme">22 November</h3>
<h2 class="programme"><a href="http://www.limelightcinema.co.nz">Limelight Cinema</a></h2>
<p>
239 Thames Street<br>
Oamaru<br>
Ph: 03 434 1077<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
$10 Seniors<br>
$12 Students/Film Industry Guilds<br>
$10 Children<br>
$7.50 Schools Screenings<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="oamaru-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 22 Nov</p></td>
<td width="25"><p> 2.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=LIME&sid=23811&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>