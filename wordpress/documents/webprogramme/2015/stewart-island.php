<h3 class="programme">21 November–2 December, 6–9 January</h3>
<h2 class="programme">Bunkhouse Theatre</h2>
<p>
10 Main Road<br>
Stewart Island<br>
Ph: 03 219 1113<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
$13 Seniors/Students/Film Industry Guilds<br>
$10 Children<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="stewart-island-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 21 Nov</p></td>
<td width="25"><p> 7.30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/chivalry-isnt-dead"><strong>Chivalry Isn’t Dead</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 25 Nov</p></td>
<td width="25"><p> 7.30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/double-take"><strong>Double Take</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 28 Nov</p></td>
<td width="25"><p> 7.30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/plunge-into-other-worlds"><strong>Plunge Into Other Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 2 Dec</p></td>
<td width="25"><p> 7.30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 6 Jan</p></td>
<td width="25"><p> 7.30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/aroha-aotearoa"><strong>Aroha Aotearoa</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 9 Jan</p></td>
<td width="25"><p> 7.30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/trailblazers"><strong>Trailblazers</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>