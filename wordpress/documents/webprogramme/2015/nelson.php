<h3 class="programme">21–22 November</h3>
<h2 class="programme"><a href="http://www.thefreehouse.co.nz">The Free House</a></h2>
<p>
95 Collingwood Street<br>
Nelson<br>
Ph: 03 548 9391 (opening hours) / 03 548 3887 (office hours)<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$12 General Admission<br>
$10 Seniors/Students/Children/Film Industry Guilds<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="nelson-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 21 Nov</p></td>
<td width="25"><p> 5.30pm
<br><strong><a href="https://oncue.eventsair.com/show-me-shorts-film-festival/freehouseeventnelson">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 22 Nov</p></td>
<td width="25"><p> 5.30pm
<br><strong><a href="https://oncue.eventsair.com/show-me-shorts-film-festival/freehouseeventnelson">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>