<h3 class="programme">21 November</h3>
<h2 class="programme"><a href="http://www.rialto.co.nz/cinema/dunedin">Rialto Cinemas</a></h2>
<p>
11 Moray Place<br>
Dunedin<br>
Ph: 03 474 2200<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$16 General Admission<br>
$13 Students/Film Industry Guilds<br>
$10 Seniors<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="dunedin-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 21 Nov</p></td>
<td width="25"><p> 6.30pm
<br><strong><a href="https://www.rialto.co.nz/Ticketing/Order#sessionId=61261&bookingSource=www|sessions">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>