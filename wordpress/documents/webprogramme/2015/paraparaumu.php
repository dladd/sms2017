<h3 class="programme">22 November</h3>
<h2 class="programme"><a href="http://www.dtcoastlands.co.nz">Downtown Cinemas Coastlands</a></h2>
<p>
150 Rimu Road<br>
Paraparaumu<br>
Ph: 04 298 6175<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$14 General Admission<br>
$12.50 Seniors/Students/Film Industry Guilds<br>
$10 Children<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="paraparaumu-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 22 Nov</p></td>
<td width="25"><p> 1.30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights*</strong></a> (See *1)</p></td>
</tr>
</div>
</tbody>
</table>
<p>[1] (*Festival Director Gina Dellabarca will introduce this session)</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>