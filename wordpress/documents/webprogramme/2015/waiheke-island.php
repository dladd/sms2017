<h3 class="programme">15 November</h3>
<h2 class="programme"><a href="http://www.waihekecinema.co.nz">Waiheke Island Community Cinema</a></h2>
<p>
Artworks, Oneroa<br>
Waiheke Island<br>
Ph: 09 372 4240<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
$11 Concession (Student/Senior/Film Industry Guilds)<br>
$8 Children<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="waiheke-island-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 15 Nov</p></td>
<td width="25"><p> 5.00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights*</strong></a> (See *1)</p></td>
</tr>
</div>
</tbody>
</table>
<p>[1] (*Festival Director Gina Dellabarca will introduce this session)</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>