<h3 class="programme">21 November</h3>
<h2 class="programme"><a href="http://pn.cinemagold.co.nz">Cinema Gold</a></h2>
<p>
70 Broadway Avenue<br>
Palmerston North<br>
Ph: 06 353 1902<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$17 General Admission<br>
$15 Students/Film Industry Guilds<br>
$14 Seniors<br>
$12 Children<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="palmerston-north-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 21 Nov</p></td>
<td width="25"><p> 8.30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>