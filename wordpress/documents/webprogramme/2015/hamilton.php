<h3 class="programme">28 November–2 December</h3>
<h2 class="programme"><a href="http://www.lidocinema.co.nz">Lido Cinema</a></h2>
<p>
The Balcony- Centre Place, 501 Victoria Street<br>
Hamilton<br>
Ph: 07 838 9010<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$16 General Admission<br>
$12.50 Students/Film Industry Guilds<br>
$10 Seniors<br>
$9 Children<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="hamilton-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 28 Nov</p></td>
<td width="25"><p> 3.30pm
<br><strong><a href="https://ticketing.us.veezi.com/purchase/5286?siteToken=3N0T%2f9VuMkGLknAhXvwHfg%3d%3d">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/chivalry-isnt-dead"><strong>Chivalry Isn’t Dead*</strong></a> (See *1)</p></td>
</tr>
<tr>
<td width="25"><p>Sun 29 Nov</p></td>
<td width="25"><p> 7.00pm
<br><strong><a href="https://ticketing.us.veezi.com/purchase/5287?siteToken=3N0T%2f9VuMkGLknAhXvwHfg%3d%3d">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/plunge-into-other-worlds"><strong>Plunge Into Other Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 30 Nov</p></td>
<td width="25"><p> 6.00pm
<br><strong><a href="https://ticketing.us.veezi.com/purchase/5288?siteToken=3N0T%2f9VuMkGLknAhXvwHfg%3d%3d">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/aroha-aotearoa"><strong>Aroha Aotearoa</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 2 Dec</p></td>
<td width="25"><p> 3.45pm
<br><strong><a href="https://ticketing.us.veezi.com/purchase/5289?siteToken=3N0T%2f9VuMkGLknAhXvwHfg%3d%3d">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<p>[1] (*Festival Director Gina Dellabarca will introduce this session)</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>