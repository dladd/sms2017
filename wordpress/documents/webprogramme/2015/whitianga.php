<h3 class="programme">4–10 January</h3>
<h2 class="programme">The Monkey House</h2>
<p>
18 Coghill Street<br>
Whitianga<br>
Ph: 07 866 5115<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$10 General Admission<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="whitianga-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Mon 4 Jan</p></td>
<td width="25"><p> 7.00 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 5 Jan</p></td>
<td width="25"><p> 7.00 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/trailblazers"><strong>Trailblazers</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 6 Jan</p></td>
<td width="25"><p> 7.00 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/double-take"><strong>Double Take</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 7 Jan</p></td>
<td width="25"><p> 7.00 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/plunge-into-other-worlds"><strong>Plunge Into Other Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 8 Jan</p></td>
<td width="25"><p> 7.00 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/aroha-aotearoa"><strong>Aroha Aotearoa</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 9 Jan</p></td>
<td width="25"><p> 7.00 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/chivalry-isnt-dead"><strong>Chivalry Isn’t Dead</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 10 Jan</p></td>
<td width="25"><p> 7.00 pm</p></td>
<td width="25"><p><strong>Audience Choice*</strong> (See *1)</p></td>
</tr>
</div>
</tbody>
</table>
<p>[1] (*Audience Choice sessions play the most popular films as voted on by audience members in each location. Complete the survey at your cinema or on the Show Me Shorts website to vote.)</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>