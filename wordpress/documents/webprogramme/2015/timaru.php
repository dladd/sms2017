<h3 class="programme">21 November</h3>
<h2 class="programme"><a href="http://www.moviemaxdigital.co.nz">Movie Max Digital</a></h2>
<p>
Corner Sophia & Canon Streets<br>
Timaru Central<br>
Ph: 03 684 6975<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$16 General Admission<br>
$13 Students/Film Industry Guilds<br>
$11 Seniors<br>
$10 Children<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="timaru-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 21 Nov</p></td>
<td width="25"><p> 8.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=MMAX&sid=32069&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>