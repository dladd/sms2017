<h3 class="programme">19–25 November</h3>
<h2 class="programme"><a href="http://http://cinematheque.aliceinvideoland.co.nz">Alice Cinematheque</a></h2>
<p>
Old High Street Post Office, 209 Tuam Street<br>
Christchurch<br>
Ph: 03 365 0615<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$17 General Admission<br>
$15 Students/Film Industry Guilds<br>
$12 Seniors/Children<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="christchurch-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Thu 19 Nov</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://cinematheque.aliceinvideoland.co.nz/Movie+show_me_shorts_2">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/chivalry-isnt-dead"><strong>Chivalry Isn’t Dead</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 20 Nov</p></td>
<td width="25"><p> 6.00pm
<br><strong><a href="http://cinematheque.aliceinvideoland.co.nz/Movie+show_me_shorts_5">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/plunge-into-other-worlds"><strong>Plunge Into Other Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 21 Nov</p></td>
<td width="25"><p> 1.00pm
<br><strong><a href="http://cinematheque.aliceinvideoland.co.nz/Movie+show_me_shorts_4">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 22 Nov</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://cinematheque.aliceinvideoland.co.nz/Movie+show_me_shorts_1">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/aroha-aotearoa"><strong>Aroha Aotearoa</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 23 Nov</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://cinematheque.aliceinvideoland.co.nz/Movie+show_me_shorts_3">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/double-take"><strong>Double Take</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 24 Nov</p></td>
<td width="25"><p> 6.00pm
<br><strong><a href="http://cinematheque.aliceinvideoland.co.nz/Movie+show_me_shorts_6">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/trailblazers"><strong>Trailblazers</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 25 Nov</p></td>
<td width="25"><p> 6.00pm</p></td>
<td width="25"><p><strong>Audience Choice*</strong> (See *1)</p></td>
</tr>
</div>
</tbody>
</table>
<p>[1] (*Audience Choice sessions play the most popular films as voted on by audience members in each location. Complete the survey at your cinema or on the Show Me Shorts website to vote.)</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>