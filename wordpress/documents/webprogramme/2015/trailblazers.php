<p>We New Zealanders have a strong oratory tradition and have always been inspired by tales of adventure and free spirits who defy convention. In this delightful collection of short films, you will meet some truly unique characters, both real and imagined.</p>

<p>Estimated total run time: 80 minutes. <br>
Rating: M - contains offensive language, sexual references and drug use.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Academy Cinemas</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#HavelockNorth">Havelock North - Cinema Gold</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#Wellington">Wellington - Paramount</option>
<option value="#Whitianga">Whitianga - The Monkey House</option>
</select>
<h2><em>Great North – Up In Smoke</em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/greatnorth-upinsmoke.jpg" />
<p>Music Video, 4 mins, NZ</p>

<p>Dir: Damian Golfinopoulos. Prod: Anna Duckworth.         </p>

<p>This imaginative, cinematic music video reveals an underwear-clad man searching through piles of junk at a dump.         </p>

<hr class="sessionBreak">

<h2><em>Alex the Magnificent         </em></h2><a href="https://vimeo.com/139635313 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/alexthemagnificent.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Documentary, 14 mins, USA 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Prod: Robert Monk Davis.</p>

<p>Alex Martin did something we have all wondered about: he gave up his worldly possessions to hitchhike across the country. A documentary about living your dreams and the kindness of strangers.</p>

<hr class="sessionBreak">

<h2><em>The Lawnmower Bandit         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/thelawnmowerbandit.jpg" />
<p>Documentary, 11 mins, NZ/Australia</p>

<p>Dir/Prod: Jackie van Beek. Prod: Aaron Watson, Francesca Fogarty.</p>

<p>Paul used to steal lawnmowers for a living, but when his father died he promised himself a better life.</p>

<hr class="sessionBreak">

<h2><em>Roberta SF15-942</em></h2><a href="https://vimeo.com/109724350 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/robertasf15-942.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 9 mins, Canada 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Caroline Monnet. Prod: Catherine Chagnon.</p>

<p>Eccentric housewife and grandmother Roberta struggles to fit the conformist society she lives in, and turns to amphetamines to cure her boredom.         </p>

<hr class="sessionBreak">

<h2><em>Big Time – My Doodled Diary</em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/bigtime-mydoodleddiary.jpg" />
<p>Coming of Age, 13 mins, India 
<p><strong>NZ Premiere</strong></p>        </p>

<p>Dir/Prod/Writ: Sonali Gulati.</p>

<p>As Maya writes in her diary about everything that rocks her world, a delightful and intimate exploration of what it means to be a teenager appears – and all too often, it "sucks big time". </p>

<hr class="sessionBreak">

<h2><em>Pink Boy</em></h2><a href="https://vimeo.com/134578861 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/pinkboy.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Documentary, 15 mins, USA 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Prod: Eric Rockey. </p>

<p>Butch lesbian BJ successfully avoided dresses her entire life – until she adopted Jeffrey. To her shock, her son wants to dress in pink and dance in public.         </p>

<hr class="sessionBreak">

<h2><em>Tits on a Bull         </em></h2><a href="https://vimeo.com/133937904 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/titsonabull.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 16 mins, NZ</p>

<p>Dir/Writ: Tim Worrall. Prod: Piripi Curtis, Lara Northcroft.</p>

<p>Star player Phoenix struggles to choose between her longtime friendship with ageing rugby coach Rusty, and the new relationship blossoming with her team captain Melanie.         </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Academy Cinemas</h3></a>
<table id="table--pink-stripes" summary="Trailblazersauckland-central-2015">
<tbody>
<tr>
<td width="50"><p>Sat 14 Nov</p></td>
<td width="50"><p> 6.30pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006197&cid=&did=20151114&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Mon 16 Nov</p></td>
<td width="50"><p> 6.30pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006197&cid=&did=20151116&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Thu 19 Nov</p></td>
<td width="50"><p> 12.00pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006197&cid=&did=20151119&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Mon 23 Nov</p></td>
<td width="50"><p> 12.00pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006197&cid=&did=20151123&ety=MT">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="Trailblazerschristchurch-2015">
<tbody>
<tr>
<td width="50"><p>Tue 24 Nov</p></td>
<td width="50"><p> 6.00pm
<strong><a href="http://cinematheque.aliceinvideoland.co.nz/Movie+show_me_shorts_6">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="HavelockNorth"></a>

<a href="http://www.showmeshorts.co.nz/programme/havelock-north"><h3>Havelock North - Cinema Gold</h3></a>
<table id="table--pink-stripes" summary="Trailblazershavelock-north-2015">
<tbody>
<tr>
<td width="50"><p>Tue 24 Nov</p></td>
<td width="50"><p> 6.00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="Trailblazersstewart-island-2015">
<tbody>
<tr>
<td width="50"><p>Sat 9 Jan</p></td>
<td width="50"><p> 7.30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Wellington"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington"><h3>Wellington - Paramount</h3></a>
<table id="table--pink-stripes" summary="Trailblazerswellington-2015">
<tbody>
<tr>
<td width="50"><p>Sat 21 Nov</p></td>
<td width="50"><p> 8.30pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21637&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Mon 23 Nov</p></td>
<td width="50"><p> 8.30pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21642&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Thu 26 Nov</p></td>
<td width="50"><p> 1.00pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21649&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Mon 30 Nov</p></td>
<td width="50"><p> 1.00pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21657&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Whitianga"></a>

<a href="http://www.showmeshorts.co.nz/programme/whitianga"><h3>Whitianga - The Monkey House</h3></a>
<table id="table--pink-stripes" summary="Trailblazerswhitianga-2015">
<tbody>
<tr>
<td width="50"><p>Tue 5 Jan</p></td>
<td width="50"><p> 7.00 pm</p></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>