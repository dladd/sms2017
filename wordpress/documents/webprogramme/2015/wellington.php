<h3 class="programme">19 November–2 December</h3>
<h2 class="programme"><a href="http://www.paramount.co.nz">Paramount</a></h2>
<p>
25 Courtenay Place<br>
Wellington<br>
Ph: 04 384 4080<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$25 Opening Night<br>
$15.90 General Admission<br>
$13.90 Students/Film Society<br>
$11.50 Seniors<br>
$10.50 Children<br>
$10.50 Schools Screenings (accompanying adults $15.90 unless booking more than 80 children)<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="wellington-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Thu 19 Nov</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21634&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/events/wellington-opening-night"><strong>WELLINGTON OPENING NIGHT*</strong></a> (See *1)</p></td>
</tr>
<tr>
<td width="25"><p>Fri 20 Nov</p></td>
<td width="25"><p> 1.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21636&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/aroha-aotearoa"><strong>Aroha Aotearoa</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 20 Nov</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21635&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/plunge-into-other-worlds"><strong>Plunge Into Other Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 21 Nov</p></td>
<td width="25"><p> 1.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21638&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 21 Nov</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21637&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/trailblazers"><strong>Trailblazers</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 22 Nov</p></td>
<td width="25"><p> 1.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21639&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/chivalry-isnt-dead"><strong>Chivalry Isn’t Dead</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 22 Nov</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21640&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/double-take"><strong>Double Take</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 23 Nov</p></td>
<td width="25"><p> 1.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21641&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 23 Nov</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21642&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/trailblazers"><strong>Trailblazers</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 24 Nov</p></td>
<td width="25"><p> 1.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21645&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/double-take"><strong>Double Take</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 24 Nov</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21646&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/aroha-aotearoa"><strong>Aroha Aotearoa</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 25 Nov</p></td>
<td width="25"><p> 1.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21647&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/plunge-into-other-worlds"><strong>Plunge Into Other Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 25 Nov</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21648&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 26 Nov</p></td>
<td width="25"><p> 1.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21649&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/trailblazers"><strong>Trailblazers</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 26 Nov</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21650&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/chivalry-isnt-dead"><strong>Chivalry Isn’t Dead</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 27 Nov</p></td>
<td width="25"><p> 1.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21651&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/double-take"><strong>Double Take</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 27 Nov</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21652&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/aroha-aotearoa"><strong>Aroha Aotearoa</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 28 Nov</p></td>
<td width="25"><p> 1.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21783&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/plunge-into-other-worlds"><strong>Plunge Into Other Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 28 Nov</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21784&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/chivalry-isnt-dead"><strong>Chivalry Isn't Dead</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 29 Nov</p></td>
<td width="25"><p> 1.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21655&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 29 Nov</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21656&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/double-take"><strong>Double Take</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 30 Nov</p></td>
<td width="25"><p> 1.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21657&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/trailblazers"><strong>Trailblazers</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 30 Nov</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21658&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/aroha-aotearoa"><strong>Aroha Aotearoa</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 1 Dec</p></td>
<td width="25"><p> 1.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21659&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/chivalry-isnt-dead"><strong>Chivalry Isn’t Dead</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 1 Dec</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21660&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/plunge-into-other-worlds"><strong>Plunge Into Other Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 2 Dec</p></td>
<td width="25"><p> 1.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21661&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 2 Dec</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21662&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><strong>Audience Choice**</strong> (See *2)</p></td>
</tr>
</div>
</tbody>
</table>
<p>[1] (*Watch all the award-winning films, meet the filmmakers and celebrate short film.)</p>

<p>[2] (**Audience Choice sessions play the most popular films as voted on by audience members in each location. Complete the survey at your cinema or on the Show Me Shorts website to vote.)</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>