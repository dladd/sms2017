<p>These short films offer entertainment for the whole family, young and old alike. Featuring a stilt walker, a ravenous granny, an eco-warrior, lost pet dogs and an A-Z of dangerous activities for possums.</p>

<p>Estimated total run time: 68 minutes. Most appropriate for children 8+ years. This session is also available for school-group bookings.<br>
Rating: PG - contains violence and coarse language.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Academy Cinemas</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#Hamilton">Hamilton - Lido Cinema</option>
<option value="#HavelockNorth">Havelock North - Cinema Gold</option>
<option value="#Pukekohe">Pukekohe - Cinema 3 Pukekohe</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#Wellington">Wellington - Paramount</option>
<option value="#Whitianga">Whitianga - The Monkey House</option>
</select>
<h2><em>Solstate – Atheum's Way</em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/solstate-atheumsway.jpg" />
<p>Music Video, 5 mins, NZ</p>

<p>Dir/Writ/Prod: Aleks Sakowski.</p>

<p>This handsomely animated music video tells the story of a father battling an epic storm in order to reunite with his daughter.         </p>

<hr class="sessionBreak">

<h2><em>Accidents, Blunders and Calamities         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/accidentsblundersandcalamities.jpg" />
<p>Animation, 5 mins, NZ 
<p><strong>World Premiere</strong></p>        </p>

<p>Dir/Writ/Prod: James Cunningham. Prod: Oliver Hilbert.</p>

<p>A father possum reads his kids a bedtime story that's an alphabet of the most dangerous animal of all – HUMANS!         </p>

<hr class="sessionBreak">

<h2><em>Bunny New Girl         </em></h2><a href="https://vimeo.com/113460134 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/bunnynewgirl.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 6 mins, Australia 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ/Prod: Natalie van den Dungen. Prod: Peter Fenton.</p>

<p>Nervous on her first day at a new primary school, a self-conscious girl learns that friendship can overcome difference.         </p>

<hr class="sessionBreak">

<h2><em>Erik and the Mystery Pack</em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/erikandthemysterypack.jpg" />
<p>Adventure, 7 mins, Canada 
<p><strong>World Premiere</strong></p></p>

<p>Dir/Writ: Erik Papatie. Prod: Wapikoni Mobile.</p>

<p>A whimsical Canadian adventure following Erik, who finds some missing dogs and is charged with returning them to the owners.         </p>

<hr class="sessionBreak">

<h2><em>Turtlebank Hustler         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/turtlebankhustler.jpg" />
<p>Drama, 9 mins, NZ 
<p><strong>World Premiere</strong></p></p>

<p>Dir: Steve McGillen. Writ/Prod: Lyse Beck.</p>

<p>Abby is a precocious eight-year-old who hustles people for money outside a supermarket for the unofficial Save the Turtles organisation.         </p>

<hr class="sessionBreak">

<h2><em>Claire and the Keys</em></h2><a href="https://vimeo.com/135482112 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/claireandthekeys.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 20 mins, USA 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: John Ludwick.         </p>

<p>The animated story of Claire, whose greatest desire is to learn to play music – but this is against her mother's wishes.         </p>

<hr class="sessionBreak">

<h2><em>My Stuffed Granny         </em></h2><a href="https://vimeo.com/95953874" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/mystuffedgranny.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 10 mins, United Kingdom 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir: Effie Pappa. Writ: Effie Pappa, Miranda Ballesteros, Melissa Iqbal. Prod: Miranda Ballesteros. Based on a story by Nina Couletakis.</p>

<p>Times are hard for little Sofia, as the Greek recession takes its toll. With her father out of work, they rely solely on Greedy Grumpy Granny's pension.</p>

<hr class="sessionBreak">

<h2><em>The Story of Percival Pilts</em></h2><a href="https://vimeo.com/116135030 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/thestoryofpercivalpilts.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 8 mins, Australia         </p>

<p>Dir/Writ: Janette Goodey, John Lewis. Prod: John Lewis.</p>

<p>Young Percival likes his stilts so much he vows he will never again let his feet touch the ground. Mark Hadlow narrates this whimsical tale of the challenges and charms in choosing to live an impractical life. </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Academy Cinemas</h3></a>
<table id="table--pink-stripes" summary="My Generationauckland-central-2015">
<tbody>
<tr>
<td width="50"><p>Sat 14 Nov</p></td>
<td width="50"><p> 12.00pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006186&cid=&did=20151114&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Mon 16 Nov</p></td>
<td width="50"><p> 12.00pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006186&cid=&did=20151116&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Wed 18 Nov</p></td>
<td width="50"><p> 6.30pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006186&cid=&did=20151118&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Sun 22 Nov</p></td>
<td width="50"><p> 10.00am
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006186&cid=&did=20151122&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Wed 25 Nov</p></td>
<td width="50"><p> 4.00pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006186&cid=&did=20151125&ety=MT">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Pukekohe"></a>

<a href="http://www.showmeshorts.co.nz/programme/pukekohe"><h3>Pukekohe - Cinema 3 Pukekohe</h3></a>
<table id="table--pink-stripes" summary="My Generationpukekohe-2015">
<tbody>
<tr>
<td width="50"><p>Mon 23 Nov</p></td>
<td width="50"><p> 11.00am
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PUKE&sid=43168&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="My Generationchristchurch-2015">
<tbody>
<tr>
<td width="50"><p>Sat 21 Nov</p></td>
<td width="50"><p> 1.00pm
<strong><a href="http://cinematheque.aliceinvideoland.co.nz/Movie+show_me_shorts_4">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Hamilton"></a>

<a href="http://www.showmeshorts.co.nz/programme/hamilton"><h3>Hamilton - Lido Cinema</h3></a>
<table id="table--pink-stripes" summary="My Generationhamilton-2015">
<tbody>
<tr>
<td width="50"><p>Wed 2 Dec</p></td>
<td width="50"><p> 3.45pm</p></td>
</tr>
</div>
</tbody>
</table><a name="HavelockNorth"></a>

<a href="http://www.showmeshorts.co.nz/programme/havelock-north"><h3>Havelock North - Cinema Gold</h3></a>
<table id="table--pink-stripes" summary="My Generationhavelock-north-2015">
<tbody>
<tr>
<td width="50"><p>Sun 22 Nov</p></td>
<td width="50"><p> 6.00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="My Generationstewart-island-2015">
<tbody>
<tr>
<td width="50"><p>Wed 2 Dec</p></td>
<td width="50"><p> 7.30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Wellington"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington"><h3>Wellington - Paramount</h3></a>
<table id="table--pink-stripes" summary="My Generationwellington-2015">
<tbody>
<tr>
<td width="50"><p>Sat 21 Nov</p></td>
<td width="50"><p> 1.00pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21638&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Mon 23 Nov</p></td>
<td width="50"><p> 1.00pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21641&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Wed 25 Nov</p></td>
<td width="50"><p> 8.30pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21648&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Sun 29 Nov</p></td>
<td width="50"><p> 1.00pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21655&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Wed 2 Dec</p></td>
<td width="50"><p> 1.00pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21661&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Whitianga"></a>

<a href="http://www.showmeshorts.co.nz/programme/whitianga"><h3>Whitianga - The Monkey House</h3></a>
<table id="table--pink-stripes" summary="My Generationwhitianga-2015">
<tbody>
<tr>
<td width="50"><p>Mon 4 Jan</p></td>
<td width="50"><p> 7.00 pm</p></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>