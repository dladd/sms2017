<p>Take a deep breath, and find yourself immersed in rich worlds crafted from dreams. Travel to strange lands without leaving the comfort of your local cinema. This is a collection of our most imaginative and striking short films.</p>

<p>Estimated total run time: 85 minutes. <br>
Rating: M - contains offensive language and content that may disturb.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Academy Cinemas</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#Hamilton">Hamilton - Lido Cinema</option>
<option value="#HavelockNorth">Havelock North - Cinema Gold</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#Wellington">Wellington - Paramount</option>
<option value="#Whitianga">Whitianga - The Monkey House</option>
</select>
<h2><em>Sweet Mix Kids ft. Pikachunes – The Whole World         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/sweetmixkidsftpikachunes-thewholeworld.jpg" />
<p>Music Video, 5 mins, NZ</p>

<p>Dir/Prod: Alexander Hoyles, Sandon Ihaia.</p>

<p>Pikachunes explores the world and his own mind, in search of a woman.         </p>

<hr class="sessionBreak">

<h2><em>The Tide Keeper         </em></h2><a href="https://vimeo.com/98574592 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/thetidekeeper.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Avant-garde, 9 mins, NZ        </p>

<p>Dir/Writ/Prod: Alyx Duncan.</p>

<p>One night an old seaman dreams a storm into his bed. This award-winning film is an artwork in motion. </p>

<hr class="sessionBreak">

<h2><em>Balsa Wood         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/balsawood.jpg" />
<p>Drama, 9 mins, United Kingdom 
<p><strong>World Premiere</strong></p></p>

<p>Dir/Writ: Dominique Lecchi. Prod: Agnes Meath Baker.</p>

<p>Two mixed-race siblings visit their extended Filipino family for lunch. This intense bubble of family and dysfunction explores how it is to feel like an outsider within your own family.         </p>

<hr class="sessionBreak">

<h2><em>Thicker Than Water         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/thickerthanwater.jpg" />
<p>Thriller, 14 mins, South Korea 
<p><strong>NZ Premiere</strong></p>        </p>

<p>Dir/Writ/Prod: Seung Yeob Lee.</p>

<p>Sungyong is a vampire. His mum tries her best to get him fresh blood every day: she gets the blood delivered or sometimes at a blood auction. His dad is less supportive.         </p>

<hr class="sessionBreak">

<h2><em>The Island         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/theisland.jpg" />
<p>Drama, 18 mins, Poland</p>

<p>Dir/Writ: Natalia Krasilnikova.</p>

<p>Meet Mykola Golowan, an old sculptor from the Ukrainian town of Lutsk, where he has been building a unique home for more than 30 years. This documentary illustrates the everyday life of an artist who does what he loves, without paying attention to the popularity and tourism his work attracts.         </p>

<hr class="sessionBreak">

<h2><em>Space Trash Men         </em></h2><a href="https://www.youtube.com/watch?v=SWqLvl2mclE " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/spacetrashmen.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Science Fiction, 14 mins, NZ 
<p><strong>World Premiere</strong></p></p>

<p>Dir/Writ/Prod: Ben Childs. Writ: Jack Barrowman.        </p>

<p>Two blue-collar astronauts learn to get along on their voyage to dump Earth's trash deep in space.</p>

<hr class="sessionBreak">

<h2><em>On Flying Water         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/onflyingwater.jpg" />
<p>Animation, 2 mins, France</p>

<p>Dir/Writ: Dominique Monfery. Prod: Julia Pajot.</p>

<p>In a little park at dawn, the sun heats a lake. The dew awakens and life is breathed into water droplets, which gather together at the heart of a spider web. A Disney-style animation about birth, death and the wonder we can find in-between.         </p>

<hr class="sessionBreak">

<h2><em>Last Door South         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/lastdoorsouth.jpg" />
<p>Animation, 15 mins, Belgium 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Sacha Feiner. Prod: Alon Knoll.</p>

<p>An exquisite animation about conjoined twin brothers. Locked in an expansive mansion since birth, they explore 'the world' of floors, stairs, corridors and rooms. Between endless exploration and private schooling, the brothers never question the limits of their world. Until one day...         </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Academy Cinemas</h3></a>
<table id="table--pink-stripes" summary="Plunge Into Other Worldsauckland-central-2015">
<tbody>
<tr>
<td width="50"><p>Fri 13 Nov</p></td>
<td width="50"><p> 6.30pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006196&cid=&did=20151113&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Tue 17 Nov</p></td>
<td width="50"><p> 6.30pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006196&cid=&did=20151117&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Wed 18 Nov</p></td>
<td width="50"><p> 4.00pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006196&cid=&did=20151118&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Sat 21 Nov</p></td>
<td width="50"><p> 12.00pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006196&cid=&did=20151121&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Tue 24 Nov</p></td>
<td width="50"><p> 6.30pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006196&cid=&did=20151124&ety=MT">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="Plunge Into Other Worldschristchurch-2015">
<tbody>
<tr>
<td width="50"><p>Fri 20 Nov</p></td>
<td width="50"><p> 6.00pm
<strong><a href="http://cinematheque.aliceinvideoland.co.nz/Movie+show_me_shorts_5">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Hamilton"></a>

<a href="http://www.showmeshorts.co.nz/programme/hamilton"><h3>Hamilton - Lido Cinema</h3></a>
<table id="table--pink-stripes" summary="Plunge Into Other Worldshamilton-2015">
<tbody>
<tr>
<td width="50"><p>Sun 29 Nov</p></td>
<td width="50"><p> 7.00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="HavelockNorth"></a>

<a href="http://www.showmeshorts.co.nz/programme/havelock-north"><h3>Havelock North - Cinema Gold</h3></a>
<table id="table--pink-stripes" summary="Plunge Into Other Worldshavelock-north-2015">
<tbody>
<tr>
<td width="50"><p>Sat 21 Nov</p></td>
<td width="50"><p> 6.00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="Plunge Into Other Worldsstewart-island-2015">
<tbody>
<tr>
<td width="50"><p>Sat 28 Nov</p></td>
<td width="50"><p> 7.30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Wellington"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington"><h3>Wellington - Paramount</h3></a>
<table id="table--pink-stripes" summary="Plunge Into Other Worldswellington-2015">
<tbody>
<tr>
<td width="50"><p>Fri 20 Nov</p></td>
<td width="50"><p> 8.30pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21635&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Wed 25 Nov</p></td>
<td width="50"><p> 1.00pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21647&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Sat 28 Nov</p></td>
<td width="50"><p> 1.00pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21783&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Tue 1 Dec</p></td>
<td width="50"><p> 8.30pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21660&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Whitianga"></a>

<a href="http://www.showmeshorts.co.nz/programme/whitianga"><h3>Whitianga - The Monkey House</h3></a>
<table id="table--pink-stripes" summary="Plunge Into Other Worldswhitianga-2015">
<tbody>
<tr>
<td width="50"><p>Thu 7 Jan</p></td>
<td width="50"><p> 7.00 pm</p></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>