<h3 class="programme">19–25 November</h3>
<h2 class="programme"><a href="http://hn.cinemagold.co.nz">Cinema Gold</a></h2>
<p>
11 Joll Road<br>
Havelock North<br>
Ph: 06 877 9016<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$16.50 General Admission<br>
$14.50 Students/Film Industry Guilds<br>
$12.50 Seniors<br>
$11.50 Children<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="havelock-north-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Thu 19 Nov</p></td>
<td width="25"><p> 6.00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/chivalry-isnt-dead"><strong>Chivalry Isn’t Dead</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 20 Nov</p></td>
<td width="25"><p> 6.00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/aroha-aotearoa"><strong>Aroha Aotearoa</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 21 Nov</p></td>
<td width="25"><p> 6.00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/plunge-into-other-worlds"><strong>Plunge Into Other Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 22 Nov</p></td>
<td width="25"><p> 6.00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 23 Nov</p></td>
<td width="25"><p> 6.00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/double-take"><strong>Double Take</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 24 Nov</p></td>
<td width="25"><p> 6.00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/trailblazers"><strong>Trailblazers</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 25 Nov</p></td>
<td width="25"><p> 6.00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/chivalry-isnt-dead"><strong>Chivalry Isn’t Dead</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>