<p>You’ll want to look twice at this vivid collection of short films about twosomes, self-expression and the way we view ourselves. Inventive production design and assured cinematography highlight strong storytelling that takes us outside the ordinary.</p>

<p>Estimated total run time: 91 minutes. <br>
Rating: M - contains offensive language and horror.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Academy Cinemas</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#HavelockNorth">Havelock North - Cinema Gold</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#Wellington">Wellington - Paramount</option>
<option value="#Whitianga">Whitianga - The Monkey House</option>
</select>
<h2><em>Chelsea Jade – Visions </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/chelseajade-visions.jpg" />
<p>Music Video, 4 mins, NZ</p>

<p>Dir: Alexander Gandar. Prod: Tom Townley.</p>

<p>Songstress Chelsea Jade's unique perspective as an artist is captured in this stylish music video for her song 'Visions'.         </p>

<hr class="sessionBreak">

<h2><em>The One and Only         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/theoneandonly.jpg" />
<p>Science Fiction, 5 mins, NZ 
<p><strong>World Premiere</strong></p></p>

<p>Dir/Writ: Arthur Gay, Peter Hansen. Writ/Prod: Sam Trafford.</p>

<p>In a desolate, post-apocalyptic world, a lonesome man takes solace in the little things – such as music.        </p>

<hr class="sessionBreak">

<h2><em>Not Like Her         </em></h2><a href="https://vimeo.com/134283221 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/notlikeher.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 14 mins, NZ</p>

<p>Dir/Writ: Hash Perambalam. Prod: Lucy Stonex.</p>

<p>After spending time with her plastic surgery-addicted mother – played with aplomb by Kate Elliott – a teenage girl's confidence is unnervingly strengthened.        </p>

<hr class="sessionBreak">

<h2><em>Restoration         </em></h2><a href="https://www.youtube.com/watch?v=7WCJa_JOTRE " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/restoration.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Horror, 15 mins, NZ 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Tim Tsiklauri. Writ: Luke Watkinson. Prod: Nicola Peeperkoorn.</p>

<p>A struggling painter takes on a commission that promises fame, but may cost him his soul.        </p>

<hr class="sessionBreak">

<h2><em>Harmony         </em></h2><a href="https://vimeo.com/139320376 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/harmony.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Thriller, 17 mins, Switzerland 
<p><strong>World Premiere</strong></p></p>

<p>Dir/Writ: Felix Schaffert. Prod: Rolf Schmid, Fabio Steineman.</p>

<p>A beautiful woman who has a symbiotic relationship with her nine-year-old daughter also has a dark secret. Does she really want her daughter to be just like her?         </p>

<hr class="sessionBreak">

<h2><em>Out of the Village         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/outofthevillage.jpg" />
<p>Drama, 16 mins, USA/Ghana 
<p><strong>World Premiere</strong></p>        </p>

<p>Dir/Writ/Prod: Jonathan Stein. Prod: Toby Wilkins.</p>

<p>Mebro and his little sister are the only remaining survivors after Ebola decimates their remote West African village. Their inspiring bond and resilience defy misguided attempts to help them.         </p>

<hr class="sessionBreak">

<h2><em>동心 (One-minded)         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/one-minded.jpg" />
<p>Drama, 20 mins, South Korea/France/USA 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ/Prod: Sébastien Simon, Forest Ian Etsler.</p>

<p>Shenanigans occur in an apartment shared by two Korean women, when one of them brings home a man she picked up in a club and two thieves invade the place. Meanwhile, a fan oscillates and observes.         </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Academy Cinemas</h3></a>
<table id="table--pink-stripes" summary="Double Takeauckland-central-2015">
<tbody>
<tr>
<td width="50"><p>Thu 12 Nov</p></td>
<td width="50"><p> 12.00pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006195&cid=&did=20151112&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Tue 17 Nov</p></td>
<td width="50"><p> 12.00pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006195&cid=&did=20151117&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Fri 20 Nov</p></td>
<td width="50"><p> 12.00pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006195&cid=&did=20151120&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Sun 22 Nov</p></td>
<td width="50"><p> 9.20pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006195&cid=&did=20151122&ety=MT">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="Double Takechristchurch-2015">
<tbody>
<tr>
<td width="50"><p>Mon 23 Nov</p></td>
<td width="50"><p> 8.30pm
<strong><a href="http://cinematheque.aliceinvideoland.co.nz/Movie+show_me_shorts_3">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="HavelockNorth"></a>

<a href="http://www.showmeshorts.co.nz/programme/havelock-north"><h3>Havelock North - Cinema Gold</h3></a>
<table id="table--pink-stripes" summary="Double Takehavelock-north-2015">
<tbody>
<tr>
<td width="50"><p>Mon 23 Nov</p></td>
<td width="50"><p> 6.00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="Double Takestewart-island-2015">
<tbody>
<tr>
<td width="50"><p>Wed 25 Nov</p></td>
<td width="50"><p> 7.30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Wellington"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington"><h3>Wellington - Paramount</h3></a>
<table id="table--pink-stripes" summary="Double Takewellington-2015">
<tbody>
<tr>
<td width="50"><p>Sun 22 Nov</p></td>
<td width="50"><p> 8.30pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21640&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Tue 24 Nov</p></td>
<td width="50"><p> 1.00pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21645&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Fri 27 Nov</p></td>
<td width="50"><p> 1.00pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21651&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Sun 29 Nov</p></td>
<td width="50"><p> 8.30pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21656&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Whitianga"></a>

<a href="http://www.showmeshorts.co.nz/programme/whitianga"><h3>Whitianga - The Monkey House</h3></a>
<table id="table--pink-stripes" summary="Double Takewhitianga-2015">
<tbody>
<tr>
<td width="50"><p>Wed 6 Jan</p></td>
<td width="50"><p> 7.00 pm</p></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>