<p>For those who love New Zealand cinema. This collection of local stories illustrates love and hope in many different guises. You’ll recognise some of these characters, and meet some new faces young and old. A touch of the ‘cinema of unease’ remains here, but aroha and whanau are very much alive in the land of the long white cloud.</p>

<p>Estimated total run time: 77 minutes. <br>
Rating: R18 - contains violence, offensive language, sexual references and content that may disturb.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Academy Cinemas</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#Hamilton">Hamilton - Lido Cinema</option>
<option value="#HavelockNorth">Havelock North - Cinema Gold</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#Wellington">Wellington - Paramount</option>
<option value="#Whitianga">Whitianga - The Monkey House</option>
</select>
<h2><em>The Eversons – Emily         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/theeversons-emily.jpg" />
<p>Music Video, 3 mins, NZ</p>

<p>Dir: THUNDERLIPS. Prod: Anna Duckworth, Rose Archer.</p>

<p>A glam-rock musical-comedy set in an infamous dumpling restaurant on Auckland's Dominion Road. Starring the band The Eversons, two dancing dumplings and JJ Fong as Emily.         </p>

<hr class="sessionBreak">

<h2><em>The Grind</em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/thegrind.jpg" />
<p>Documentary, 8 mins, NZ 
<p><strong>World Premiere</strong></p></p>

<p>Dir/Writ/Prod: Kyan Krumdieck. Writ/Prod: Annabelle Dick. </p>

<p>Welcome to the covert world of Grindr. New Zealand gay men reveal stories about their encounters of all kinds, set up through dating app Grindr, in this frank documentary. </p>

<hr class="sessionBreak">

<h2><em>Tihei</em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/tihei.jpg" />
<p>Documentary, 3 mins, NZ         </p>

<p>Dir: Hamish Bennett. Prod: Orlando Stewart.</p>

<p>Meet Tihei – a lyricist and a poet, improvising rap songs as the beat drops.        </p>

<hr class="sessionBreak">

<h2><em>Return </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/return.jpg" />
<p>Comedy, 15 mins, NZ</p>

<p>Dir/Writ/Prod: Ryan Heron. Writ: Guy Montgomery. Prod: Matiu Sadd.</p>

<p>Returning home to visit his parents for the weekend, a young man runs into complications juggling his friends, parents, magic mushrooms and several thousand chickens.         </p>

<hr class="sessionBreak">

<h2><em>Cub         </em></h2><a href="https://vimeo.com/89826048 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/cub.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 15 mins, NZ 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Jamie Lawrence. Prod: Kelly Kilgour.</p>

<p>A mother tries to end her teenage son's relationship with an older woman, but risks losing him altogether.         </p>

<hr class="sessionBreak">

<h2><em>U F O</em></h2><a href="https://vimeo.com/98698844 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/ufo.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Science Fiction, 17 mins, NZ</p>

<p>Dir/Writ: Gregory David King. Prod: Mhairead Connor.</p>

<p>Nine-year-old Brains needs to get back to his real family up in space before he is exterminated by the human family he lives with on planet Earth.         </p>

<hr class="sessionBreak">

<h2><em>Dirty Laundry</em></h2><a href="https://vimeo.com/139537542 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/dirtylaundry.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 4 mins, NZ 
<p><strong>World Premiere</strong></p></p>

<p>Dir/Writ: Olivia Walker. Prod: Unitec.</p>

<p>Glimpse the world as seen through the eyes of a disaffected teenager, during a wash cycle at her local laundromat.         </p>

<hr class="sessionBreak">

<h2><em>Whistle Blower</em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/whistleblower.jpg" />
<p>Drama, 12 mins, NZ 
<p><strong>World Premiere</strong></p></p>

<p>Dir: Nicole van Heerden. Writ: Catie Mulrennan. Prod: Arielle Sullivan.</p>

<p>A troubled teenage netball umpire finds solace through diligent study of the game.         </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Academy Cinemas</h3></a>
<table id="table--pink-stripes" summary="Aroha Aotearoaauckland-central-2015">
<tbody>
<tr>
<td width="50"><p>Fri 13 Nov</p></td>
<td width="50"><p> 12.00pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006193&cid=&did=20151113&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p><s>Sun 15 Nov<s></p></td>
<td width="50"><p><s> 5.30pm</s>  Cancelled</p></td>
</tr>
<tr>
<td width="50"><p>Fri 20 Nov</p></td>
<td width="50"><p> 6.30pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006193&cid=&did=20151120&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Mon 23 Nov</p></td>
<td width="50"><p> 6.30pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006193&cid=&did=20151123&ety=MT">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="Aroha Aotearoachristchurch-2015">
<tbody>
<tr>
<td width="50"><p>Sun 22 Nov</p></td>
<td width="50"><p> 8.30pm
<strong><a href="http://cinematheque.aliceinvideoland.co.nz/Movie+show_me_shorts_1">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Hamilton"></a>

<a href="http://www.showmeshorts.co.nz/programme/hamilton"><h3>Hamilton - Lido Cinema</h3></a>
<table id="table--pink-stripes" summary="Aroha Aotearoahamilton-2015">
<tbody>
<tr>
<td width="50"><p>Mon 30 Nov</p></td>
<td width="50"><p> 6.00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="HavelockNorth"></a>

<a href="http://www.showmeshorts.co.nz/programme/havelock-north"><h3>Havelock North - Cinema Gold</h3></a>
<table id="table--pink-stripes" summary="Aroha Aotearoahavelock-north-2015">
<tbody>
<tr>
<td width="50"><p>Fri 20 Nov</p></td>
<td width="50"><p> 6.00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="Aroha Aotearoastewart-island-2015">
<tbody>
<tr>
<td width="50"><p>Wed 6 Jan</p></td>
<td width="50"><p> 7.30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Wellington"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington"><h3>Wellington - Paramount</h3></a>
<table id="table--pink-stripes" summary="Aroha Aotearoawellington-2015">
<tbody>
<tr>
<td width="50"><p>Fri 20 Nov</p></td>
<td width="50"><p> 1.00pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21636&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Tue 24 Nov</p></td>
<td width="50"><p> 8.30pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21646&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Fri 27 Nov</p></td>
<td width="50"><p> 8.30pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21652&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Mon 30 Nov</p></td>
<td width="50"><p> 8.30pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21658&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Whitianga"></a>

<a href="http://www.showmeshorts.co.nz/programme/whitianga"><h3>Whitianga - The Monkey House</h3></a>
<table id="table--pink-stripes" summary="Aroha Aotearoawhitianga-2015">
<tbody>
<tr>
<td width="50"><p>Fri 8 Jan</p></td>
<td width="50"><p> 7.00 pm</p></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>
