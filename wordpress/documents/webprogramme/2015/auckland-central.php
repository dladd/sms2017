<h3 class="programme">12–25 November</h3>
<h2 class="programme"><a href="http://www.academycinemas.co.nz">Academy Cinemas</a></h2>
<p>
Auckland Library Building, 44 Lorne Street<br>
Auckland CBD<br>
Ph: 09 373 2761<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15.50 General Admission<br>
$12 Students/Film Industry Guilds<br>
$10 Seniors<br>
$9 Children<br>
</p>
<p><strong>NOTE</strong>: For our opening night at The Civic on November 11, please see the <a href="http://www.showmeshorts.co.nz/events/auckland-opening-night/" title="Auckland Opening Night & Awards Ceremony">Auckland Opening Night & Awards Ceremony</a> page or <a href="http://www.ticketmaster.co.nz/show-me-shorts-auckland-new-zealand-11-11-2015/event/24004F4DC9C31993" title="Book tickets to the Auckland Opening through Ticketmaster">click here to book tickets through Ticketmaster</a>.<p>

<h2>Screenings</h2><table id="table--pink-stripes" summary="auckland-central-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Thu 12 Nov</p></td>
<td width="25"><p> 12.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006195&cid=&did=20151112&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/double-take"><strong>Double Take</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 12 Nov</p></td>
<td width="25"><p> 6.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006194&cid=&did=20151112&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/chivalry-isnt-dead"><strong>Chivalry Isn’t Dead</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 13 Nov</p></td>
<td width="25"><p> 12.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006193&cid=&did=20151113&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/aroha-aotearoa"><strong>Aroha Aotearoa</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 13 Nov</p></td>
<td width="25"><p> 6.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006196&cid=&did=20151113&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/plunge-into-other-worlds"><strong>Plunge Into Other Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 14 Nov</p></td>
<td width="25"><p> 12.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006186&cid=&did=20151114&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 14 Nov</p></td>
<td width="25"><p> 6.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006197&cid=&did=20151114&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/trailblazers"><strong>Trailblazers</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 15 Nov</p></td>
<td width="25"><p> 10.00am
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006194&cid=&did=20151115&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/chivalry-isnt-dead"><strong>Chivalry Isn’t Dead</strong></a></p></td>
</tr>
<tr>
<td width="25"><p><s>Sun 15 Nov</s></p></td>
<td width="25"><p><s>5.30pm</s></p></td>
<td width="25"><p><s>Aroha Aotearoa</s>  <strong>Cancelled</strong> (see *1)</p></td>
</tr>
<tr>
<td width="25"><p>Mon 16 Nov</p></td>
<td width="25"><p> 12.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006186&cid=&did=20151116&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 16 Nov</p></td>
<td width="25"><p> 6.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006197&cid=&did=20151116&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/trailblazers"><strong>Trailblazers</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 17 Nov</p></td>
<td width="25"><p> 12.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006195&cid=&did=20151117&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/double-take"><strong>Double Take</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 17 Nov</p></td>
<td width="25"><p> 6.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006196&cid=&did=20151117&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/plunge-into-other-worlds"><strong>Plunge Into Other Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 18 Nov</p></td>
<td width="25"><p> 4.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006196&cid=&did=20151118&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/plunge-into-other-worlds"><strong>Plunge Into Other Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 18 Nov</p></td>
<td width="25"><p> 6.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006186&cid=&did=20151118&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 19 Nov</p></td>
<td width="25"><p> 12.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006197&cid=&did=20151119&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/trailblazers"><strong>Trailblazers</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 19 Nov</p></td>
<td width="25"><p> 6.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006194&cid=&did=20151119&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/chivalry-isnt-dead"><strong>Chivalry Isn’t Dead</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 20 Nov</p></td>
<td width="25"><p> 12.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006195&cid=&did=20151120&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/double-take"><strong>Double Take</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 20 Nov</p></td>
<td width="25"><p> 6.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006193&cid=&did=20151120&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/aroha-aotearoa"><strong>Aroha Aotearoa</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 21 Nov</p></td>
<td width="25"><p> 12.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006196&cid=&did=20151121&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/plunge-into-other-worlds"><strong>Plunge Into Other Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 21 Nov</p></td>
<td width="25"><p> 6.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006194&cid=&did=20151121&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/chivalry-isnt-dead"><strong>Chivalry Isn’t Dead</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 22 Nov</p></td>
<td width="25"><p> 10.00am
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006186&cid=&did=20151122&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 22 Nov</p></td>
<td width="25"><p> 9.20pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006195&cid=&did=20151122&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/double-take"><strong>Double Take</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 23 Nov</p></td>
<td width="25"><p> 12.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006197&cid=&did=20151123&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/trailblazers"><strong>Trailblazers</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 23 Nov</p></td>
<td width="25"><p> 6.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006193&cid=&did=20151123&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/aroha-aotearoa"><strong>Aroha Aotearoa</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 24 Nov</p></td>
<td width="25"><p> 12.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006194&cid=&did=20151124&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/chivalry-isnt-dead"><strong>Chivalry Isn’t Dead</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 24 Nov</p></td>
<td width="25"><p> 6.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006196&cid=&did=20151124&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/plunge-into-other-worlds"><strong>Plunge Into Other Worlds</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 25 Nov</p></td>
<td width="25"><p> 4.00pm
<br><strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006186&cid=&did=20151125&ety=MT">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 25 Nov</p></td>
<td width="25"><p> 6.30pm</p></td>
<td width="25"><p><strong>Audience Choice*</strong> (See *2)</p></td>
</tr>
</div>
</tbody>
</table>
<p>[1] (*Our apologies- this session has been cancelled due to a scheduling conflict.)</p>
<p>[2] (*Audience Choice sessions play the most popular films as voted on by audience members in each location. Complete the survey at your cinema or on the Show Me Shorts website to vote.)</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>
