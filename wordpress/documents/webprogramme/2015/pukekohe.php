<h3 class="programme">23–27 November</h3>
<h2 class="programme"><a href="http://www.pukekohecinemas.co.nz">Cinema 3 Pukekohe</a></h2>
<p>
85 Edinburgh Street<br>
Pukekohe<br>
Ph: 09 237 0216<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$17 General Admission<br>
$15 Students/Film Industry Guilds<br>
$11 Seniors/Children<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="pukekohe-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Mon 23 Nov</p></td>
<td width="25"><p> 11.00am
<br><strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PUKE&sid=43168&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 27 Nov</p></td>
<td width="25"><p> 6.30pm
<br><strong><a href="http://www.bookmyshow.co.nz/go.aspx?cid=PUKE&sid=43169">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights*</strong></a> (See *1)</p></td>
</tr>
</div>
</tbody>
</table>
<p>[1] (*Festival Director Gina Dellabarca will introduce this session)</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>