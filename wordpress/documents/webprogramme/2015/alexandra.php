<h3 class="programme">14 November</h3>
<h2 class="programme"><a href="http://www.centralcinema.co.nz">Central Cinema</a></h2>
<p>
21 Centennial Avenue<br>
Alexandra<br>
<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$13 General Admission<br>
$10 Children (15 and under)<br>
$11 Concession (Members/Students/Seniors/Film Industry Guilds)<br>
$8 School Groups<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="alexandra-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 14 Nov</p></td>
<td width="25"><p> 4.00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>