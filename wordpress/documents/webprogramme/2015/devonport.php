<h3 class="programme">13 November</h3>
<h2 class="programme"><a href="http://www.thevic.co.nz">The Vic</a></h2>
<p>
56 Victoria Road<br>
Devonport, Auckland<br>
Ph: 09 446 0100<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
$12 Students/Film Industry Guilds<br>
$11 Seniors<br>
$10 Children<br>
(Prices include $1 voluntary donation to The Vic Trust, which you can opt out of)<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="devonport-2015">
<tbody>
<div>
<tr>
<td width="25"><p>Fri 13 Nov</p></td>
<td width="25"><p> 8.30pm
<br><strong><a href="https://ticketing.us.veezi.com/purchase/15340?siteToken=Vk9gVvV%2bF0OF1pOaftJzvA%3d%3d">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights*</strong></a> (See *1)</p></td>
</tr>
</div>
</tbody>
</table>
<p>[1] (*Festival Director Gina Dellabarca will introduce this session)</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>