<p>This troupe of unlikely knights in shining armour are gentlemen, even in trying circumstances. When called upon they will rescue a maiden, offer a strong shoulder or even invent a secret life for your cat. In a world of casual affairs, Tinder and instant gratification, these men are a rare but welcome breed.</p>

<p>Estimated total run time: 72 minutes. <br>
Rating: M - contains offensive language and content that may disturb.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Academy Cinemas</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#Hamilton">Hamilton - Lido Cinema</option>
<option value="#HavelockNorth">Havelock North - Cinema Gold</option>
<option value="#Matakana">Matakana - Matakana Cinemas</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#Wellington">Wellington - Paramount</option>
<option value="#Whitianga">Whitianga - The Monkey House</option>
</select>
<h2><em>Tyra Hammond – So Good At Being In Trouble </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/tyrahammond-sogoodatbeingintrouble.jpg" />
<p>Music Video, 4 mins, NZ</p>

<p>Dir: Benj Brooking. Prod: Anna Duckworth.  </p>

<p>A woman falls helplessly in love with a gallant tow-truck driver on a stunning country road. She will go to extreme measures to see him again.         </p>

<hr class="sessionBreak">

<h2><em>Shabu-Shabu Spirit         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/shabu-shabuspirit.jpg" />
<p>Comedy, 11 mins, Japan         </p>

<p>Dir/Writ: Yuki Saito. Writ: Joji Okamoto. Prod: Chikara Ito, Yuka Koide.</p>

<p>When Keita goes to meet the parents of his fiancé, he is served a traditional shabu-shabu meal to gauge if he is worthy.         </p>

<hr class="sessionBreak">

<h2><em>Every Moment </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/everymoment.jpg" />
<p>Romance, 7 mins, NZ</p>

<p>Dir/Writ/Prod: Gabriel Reid. Writ/Prod: Maile Daugherty.</p>

<p>A heartbroken hotel maid is distracted from her sad break-up by a charming bellboy who weaves a tale of her bright future. Yes, life is full of ups and downs, but every moment counts.</p>

<hr class="sessionBreak">

<h2><em>The Orchestra </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/theorchestra.jpg" />
<p>Animation, 15 mins, Australia 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Mikey Hill. Prod: Melanie Brunt.</p>

<p>In a world filled with beautiful and harmonious music, elderly Vernon always seems to strike the wrong note.        </p>

<hr class="sessionBreak">

<h2><em>Digits        </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/digits.jpg" />
<p>Comedy, 8 mins, USA 
<p><strong>NZ Premiere</strong></p>        </p>

<p>Dir/Writ/Prod: Alexander Engel.</p>

<p>After losing the last two digits of a girl's number, a socially awkward fish enthusiast tries every combination to seek her out.         </p>

<hr class="sessionBreak">

<h2><em>Madam Black         </em></h2><a href="https://vimeo.com/128092320 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/madamblack.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Comedy, 11 mins, NZ         </p>

<p>Dir/Prod: Ivan Barge. Writ/Prod: Matthew Harris.</p>

<p>When a photographer runs over a child's beloved cat, he is forced to fabricate an increasingly elaborate story to explain its disappearance.         </p>

<hr class="sessionBreak">

<h2><em>A.D. 1363, the End of Chivalry        </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/ad1363theendofchivalry.jpg" />
<p>Comedy, 3 mins, USA/NZ 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ/Prod: Jake Mahaffy.</p>

<p>A historical epic depicting the little-known catastrophe that led to the demise of chivalry.         </p>

<hr class="sessionBreak">

<h2><em>Stutterer</em></h2><a href="https://vimeo.com/118746312" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/stutterer.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Romance, 13 mins, Ireland 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Benjamin Cleary. Prod: Serena Armitage, Shan Ogilvie.</p>

<p>A lonely typographer with a cruel speech impediment – but an eloquent inner voice – must face his ultimate fear.         </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Academy Cinemas</h3></a>
<table id="table--pink-stripes" summary="Chivalry Isn’t Deadauckland-central-2015">
<tbody>
<tr>
<td width="50"><p>Thu 12 Nov</p></td>
<td width="50"><p> 6.30pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006194&cid=&did=20151112&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Sun 15 Nov</p></td>
<td width="50"><p> 10.00am
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006194&cid=&did=20151115&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Thu 19 Nov</p></td>
<td width="50"><p> 6.30pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006194&cid=&did=20151119&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Sat 21 Nov</p></td>
<td width="50"><p> 6.30pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006194&cid=&did=20151121&ety=MT">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Tue 24 Nov</p></td>
<td width="50"><p> 12.00pm
<strong><a href="http://www.bookmyshow.co.nz/BuyTickets.aspx?srid=AKLC&eid=ET00006194&cid=&did=20151124&ety=MT">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Matakana"></a>

<a href="http://www.showmeshorts.co.nz/programme/matakana"><h3>Matakana - Matakana Cinemas</h3></a>
<table id="table--pink-stripes" summary="Chivalry Isn’t Deadmatakana-2015">
<tbody>
<tr>
<td width="50"><p>Fri 13 Nov</p></td>
<td width="50"><p> 8.30pm
<strong><a href="https://ticketing.us.veezi.com/purchase/1786?siteToken=NeQbpcRv90OQr%2fEjNRpl1g%3d%3d">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="Chivalry Isn’t Deadchristchurch-2015">
<tbody>
<tr>
<td width="50"><p>Thu 19 Nov</p></td>
<td width="50"><p> 8.30pm
<strong><a href="http://cinematheque.aliceinvideoland.co.nz/Movie+show_me_shorts_2">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Hamilton"></a>

<a href="http://www.showmeshorts.co.nz/programme/hamilton"><h3>Hamilton - Lido Cinema</h3></a>
<table id="table--pink-stripes" summary="Chivalry Isn’t Deadhamilton-2015">
<tbody>
<tr>
<td width="50"><p>Sat 28 Nov</p></td>
<td width="50"><p> 3.30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="HavelockNorth"></a>

<a href="http://www.showmeshorts.co.nz/programme/havelock-north"><h3>Havelock North - Cinema Gold</h3></a>
<table id="table--pink-stripes" summary="Chivalry Isn’t Deadhavelock-north-2015">
<tbody>
<tr>
<td width="50"><p>Thu 19 Nov</p></td>
<td width="50"><p> 6.00pm</p></td>
</tr>
<tr>
<td width="50"><p>Wed 25 Nov</p></td>
<td width="50"><p> 6.00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="Chivalry Isn’t Deadstewart-island-2015">
<tbody>
<tr>
<td width="50"><p>Sat 21 Nov</p></td>
<td width="50"><p> 7.30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Wellington"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington"><h3>Wellington - Paramount</h3></a>
<table id="table--pink-stripes" summary="Chivalry Isn’t Deadwellington-2015">
<tbody>
<tr>
<td width="50"><p>Sun 22 Nov</p></td>
<td width="50"><p> 1.00pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21639&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Thu 26 Nov</p></td>
<td width="50"><p> 8.30pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21650&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Tue 1 Dec</p></td>
<td width="50"><p> 1.00pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=PARA&sid=21659&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Whitianga"></a>

<a href="http://www.showmeshorts.co.nz/programme/whitianga"><h3>Whitianga - The Monkey House</h3></a>
<table id="table--pink-stripes" summary="Chivalry Isn’t Deadwhitianga-2015">
<tbody>
<tr>
<td width="50"><p>Sat 9 Jan</p></td>
<td width="50"><p> 7.00 pm</p></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>