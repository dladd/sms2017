<p>Gather your loved ones for a fun night out. Some of our most crowd-pleasing short films have been collected here for special screenings in some of our smaller locations. Guaranteed to be highly entertaining, and great fun! </p>

<p>Estimated total run time: 79 minutes. <br>
Rating: M - contains violence and offensive language.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#Alexandra">Alexandra - Central Cinema</option>
<option value="#Dargaville">Dargaville - Anzac Theatre</option>
<option value="#Devonport">Devonport - The Vic</option>
<option value="#Dunedin">Dunedin - Rialto Cinemas</option>
<option value="#Matakana">Matakana - Matakana Cinemas</option>
<option value="#Nelson">Nelson - The Free House</option>
<option value="#NewPlymouth">New Plymouth - 4th Wall Theatre</option>
<option value="#Oamaru">Oamaru - Limelight Cinema</option>
<option value="#PalmerstonNorth">Palmerston North - Cinema Gold</option>
<option value="#Paraparaumu">Paraparaumu - Downtown Cinemas Coastlands</option>
<option value="#Pukekohe">Pukekohe - Cinema 3 Pukekohe</option>
<option value="#Tauranga">Tauranga - Rialto Tauranga</option>
<option value="#Timaru">Timaru - Movie Max Digital</option>
<option value="#WaihekeIsland">Waiheke Island - Waiheke Island Community Cinema</option>
</select>
<h2><em>The Eversons – Emily         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/theeversons-emily.jpg" />
<p>Music Video, 3 mins, NZ</p>

<p>Dir: THUNDERLIPS. Prod: Anna Duckworth, Rose Archer.</p>

<p>A glam-rock musical-comedy set in an infamous dumpling restaurant on Auckland's Dominion Road. Starring the band The Eversons, two dancing dumplings and JJ Fong as Emily.         </p>

<hr class="sessionBreak">

<h2><em>Accidents, Blunders and Calamities         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/accidentsblundersandcalamities.jpg" />
<p>Animation, 5 mins, NZ 
<p><strong>World Premiere</strong></p></p>

<p>Dir/Writ/Prod: James Cunningham. Prod: Oliver Hilbert.</p>

<p>A father possum reads his kids a bedtime story that's an alphabet of the most dangerous animal of all – HUMANS!         </p>

<hr class="sessionBreak">

<h2><em>A.D. 1363, the End of Chivalry        </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/ad1363theendofchivalry.jpg" />
<p>Comedy, 3 mins, USA/NZ 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ/Prod: Jake Mahaffy.</p>

<p>A historical epic depicting the little-known catastrophe that led to the demise of chivalry.         </p>

<hr class="sessionBreak">

<h2><em>동心 (One-minded)         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/one-minded.jpg" />
<p>Drama, 20 mins, South Korea/France/USA 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ/Prod: Sébastien Simon, Forest Ian Etsler.</p>

<p>Shenanigans occur in an apartment shared by two Korean women, when one of them brings home a man she picked up in a club and two thieves invade the place. Meanwhile, a fan oscillates and observes.         </p>

<hr class="sessionBreak">

<h2><em>The Story of Percival Pilts</em></h2><a href="https://vimeo.com/116135030" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/thestoryofpercivalpilts.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 8 mins, Australia         </p>

<p>Dir/Writ: Janette Goodey, John Lewis. Prod: John Lewis.</p>

<p>Young Percival likes his stilts so much he vows he will never again let his feet touch the ground. Mark Hadlow narrates this whimsical tale of the challenges and charms in choosing to live an impractical life. </p>

<hr class="sessionBreak">

<h2><em>Whistle Blower</em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/whistleblower.jpg" />
<p>Drama, 12 mins, NZ</p>

<p>Dir: Nicole van Heerden. Writ: Catie Mulrennan. Prod: Arielle Sullivan.</p>

<p>A troubled teenage netball umpire finds solace through diligent study of the game.         </p>

<hr class="sessionBreak">

<h2><em>Stutterer </em></h2><a href="https://vimeo.com/118746312 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/stutterer.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Romance, 13 mins, Ireland 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Benjamin Cleary. Prod: Serena Armitage, Shan Ogilvie.</p>

<p>A lonely typographer with a cruel speech impediment – but an eloquent inner voice – must face his ultimate fear.         </p>

<hr class="sessionBreak">

<h2><em>Last Door South         </em></h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2015/lastdoorsouth.jpg" />
<p>Animation, 15 mins, Belgium</p>

<p>Dir/Writ: Sacha Feiner. Prod: Alon Knoll.</p>

<p>An exquisite animation about conjoined twin brothers. Locked in an expansive mansion since birth, they explore 'the world' of floors, stairs, corridors and rooms. Between endless exploration and private schooling, the brothers never question the limits of their world. Until one day...         </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="Alexandra"></a>

<a href="http://www.showmeshorts.co.nz/programme/alexandra"><h3>Alexandra - Central Cinema</h3></a>
<table id="table--pink-stripes" summary="Highlightsalexandra-2015">
<tbody>
<tr>
<td width="50"><p>Sat 14 Nov</p></td>
<td width="50"><p> 4.00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Devonport"></a>

<a href="http://www.showmeshorts.co.nz/programme/devonport"><h3>Devonport - The Vic</h3></a>
<table id="table--pink-stripes" summary="Highlightsdevonport-2015">
<tbody>
<tr>
<td width="50"><p>Fri 13 Nov</p></td>
<td width="50"><p> 8.30pm
<strong><a href="https://ticketing.us.veezi.com/purchase/15340?siteToken=Vk9gVvV%2bF0OF1pOaftJzvA%3d%3d">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Matakana"></a>

<a href="http://www.showmeshorts.co.nz/programme/matakana"><h3>Matakana - Matakana Cinemas</h3></a>
<table id="table--pink-stripes" summary="Highlightsmatakana-2015">
<tbody>
<tr>
<td width="50"><p>Sun 15 Nov</p></td>
<td width="50"><p> 1.00pm
<strong><a href="https://ticketing.us.veezi.com/purchase/1785?siteToken=NeQbpcRv90OQr%2fEjNRpl1g%3d%3d">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Pukekohe"></a>

<a href="http://www.showmeshorts.co.nz/programme/pukekohe"><h3>Pukekohe - Cinema 3 Pukekohe</h3></a>
<table id="table--pink-stripes" summary="Highlightspukekohe-2015">
<tbody>
<tr>
<td width="50"><p>Fri 27 Nov</p></td>
<td width="50"><p> 6.30pm
<strong><a href="http://www.bookmyshow.co.nz/go.aspx?cid=PUKE&sid=43169">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="WaihekeIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/waiheke-island"><h3>Waiheke Island - Waiheke Island Community Cinema</h3></a>
<table id="table--pink-stripes" summary="Highlightswaiheke-island-2015">
<tbody>
<tr>
<td width="50"><p>Sun 15 Nov</p></td>
<td width="50"><p> 5.00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Dargaville"></a>

<a href="http://www.showmeshorts.co.nz/programme/dargaville"><h3>Dargaville - Anzac Theatre</h3></a>
<table id="table--pink-stripes" summary="Highlightsdargaville-2015">
<tbody>
<tr>
<td width="50"><p>Sun 22 Nov</p></td>
<td width="50"><p> 5.30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Dunedin"></a>

<a href="http://www.showmeshorts.co.nz/programme/dunedin"><h3>Dunedin - Rialto Cinemas</h3></a>
<table id="table--pink-stripes" summary="Highlightsdunedin-2015">
<tbody>
<tr>
<td width="50"><p>Sat 21 Nov</p></td>
<td width="50"><p> 6.30pm
<strong><a href="https://www.rialto.co.nz/Ticketing/Order#sessionId=61261&bookingSource=www|sessions">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Nelson"></a>

<a href="http://www.showmeshorts.co.nz/programme/nelson"><h3>Nelson - The Free House</h3></a>
<table id="table--pink-stripes" summary="Highlightsnelson-2015">
<tbody>
<tr>
<td width="50"><p>Sat 21 Nov</p></td>
<td width="50"><p> 5.30pm
<strong><a href="https://oncue.eventsair.com/show-me-shorts-film-festival/freehouseeventnelson">Book tickets</a></strong></p></td>
</tr>
<tr>
<td width="50"><p>Sun 22 Nov</p></td>
<td width="50"><p> 5.30pm
<strong><a href="https://oncue.eventsair.com/show-me-shorts-film-festival/freehouseeventnelson">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="NewPlymouth"></a>

<a href="http://www.showmeshorts.co.nz/programme/new-plymouth"><h3>New Plymouth - 4th Wall Theatre</h3></a>
<table id="table--pink-stripes" summary="Highlightsnew-plymouth-2015">
<tbody>
<tr>
<td width="50"><p>Thur 19 Nov</p></td>
<td width="50"><p> 7.00pm
<strong><a href="https://www.patronbase.com/_4thWallTheatre/Productions/SMS1/Performances">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Oamaru"></a>

<a href="http://www.showmeshorts.co.nz/programme/oamaru"><h3>Oamaru - Limelight Cinema</h3></a>
<table id="table--pink-stripes" summary="Highlightsoamaru-2015">
<tbody>
<tr>
<td width="50"><p>Sun 22 Nov</p></td>
<td width="50"><p> 2.00pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=LIME&sid=23811&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="PalmerstonNorth"></a>

<a href="http://www.showmeshorts.co.nz/programme/palmerston-north"><h3>Palmerston North - Cinema Gold</h3></a>
<table id="table--pink-stripes" summary="Highlightspalmerston-north-2015">
<tbody>
<tr>
<td width="50"><p>Sat 21 Nov</p></td>
<td width="50"><p> 8.30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Paraparaumu"></a>

<a href="http://www.showmeshorts.co.nz/programme/paraparaumu"><h3>Paraparaumu - Downtown Cinemas Coastlands</h3></a>
<table id="table--pink-stripes" summary="Highlightsparaparaumu-2015">
<tbody>
<tr>
<td width="50"><p>Sun 22 Nov</p></td>
<td width="50"><p> 1.30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Tauranga"></a>

<a href="http://www.showmeshorts.co.nz/programme/tauranga"><h3>Tauranga - Rialto Tauranga</h3></a>
<table id="table--pink-stripes" summary="Highlightstauranga-2015">
<tbody>
<tr>
<td width="50"><p>Sun 29 Nov</p></td>
<td width="50"><p> 5.30pm
<strong><a href="http://www.rialtotauranga.co.nz/movies/10475.php">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><a name="Timaru"></a>

<a href="http://www.showmeshorts.co.nz/programme/timaru"><h3>Timaru - Movie Max Digital</h3></a>
<table id="table--pink-stripes" summary="Highlightstimaru-2015">
<tbody>
<tr>
<td width="50"><p>Sat 21 Nov</p></td>
<td width="50"><p> 8.00pm
<strong><a href="http://www.bookmyshow.co.nz/BookTickets.aspx?cid=MMAX&sid=32069&utm_source=CINEMA&utm_c">Book tickets</a></strong></p></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>