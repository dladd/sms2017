<h3 class="programme">13–26 October</h3>
<h2 class="programme"><a href="http://www.embassytheatre.co.nz">Embassy Theatre</a></h2>
<p>
10 Kent Terrace<br>
Wellington<br>
Ph: 04 384 7657<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$25 Opening Night<br>
$20 Opening Night - Seniors/Students/Film Industry Guilds<br>
$17.50 General Admission<br>
$14.50 Students, Film Society & Film Industry Guilds<br>
$12.50 Seniors & Children<br>
$60 All Shorts Multi-pass (valid for up to six sessions - see page xx for more details)<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="wellington-cbd-2016">
<tbody>
<div>
<tr>
<td width="25"><p>Thu 13 Oct</p></td>
<td width="25"><p> 8:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/wellington-opening-night"><strong>WELLINGTON OPENING NIGHT*</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 14 Oct</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/he-tangata"><strong>He Tangata</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 14 Oct</p></td>
<td width="25"><p> 8:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/secret-and-lies"><strong>Secret & Lies</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 15 Oct</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 15 Oct</p></td>
<td width="25"><p> 8:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/bump-in-the-night"><strong>Bump in the Night</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 16 Oct</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/heartstrings"><strong>Heartstrings</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 16 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/homeland"><strong>Homeland</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 17 Oct</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/secrets-and-lies"><strong>Secrets and Lies</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 17 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/he-tangata"><strong>He Tangata</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 18 Oct</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/bump-in-the-night"><strong>Bump in the Night</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 18 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/homeland"><strong>Homeland</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 19 Oct</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/he-tangata"><strong>He Tangata</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 19 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/heartstrings"><strong>Heartstrings</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 20 Oct</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 20 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/secrets-and-lies"><strong>Secrets and Lies</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 21 Oct</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/homeland"><strong>Homeland</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 21 Oct</p></td>
<td width="25"><p> 8:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/bump-in-the-night"><strong>Bump in the Night</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 22 Oct</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/secrets-and-lies"><strong>Secrets and Lies</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 22 Oct</p></td>
<td width="25"><p> 8:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/heartstrings"><strong>Heartstrings</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 23 Oct</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 23 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/he-tangata"><strong>He Tangata</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 24 Oct</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/heartstrings"><strong>Heartstrings</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 24 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/bump-in-the-night"><strong>Bump in the Night</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 25 Oct</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 25 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/homeland"><strong>Homeland</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 26 Oct</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/secrets-and-lies"><strong>Secrets and Lies</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 26 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><strong>Audience Choice</strong></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>