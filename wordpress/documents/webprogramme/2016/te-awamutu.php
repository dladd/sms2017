<h3 class="programme">22–28 November</h3>
<h2 class="programme">Regent Theatre</h2>
<p>
235 Alexandra Street<br>
Te Awamutu<br>
Ph: 07 871 5288<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$16.50 General Admission<br>
$13.50 Concession<br>
$10.50 Child<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="te-awamutu-2016">
<tbody>
<div>
<tr>
<td width="25"><p>Tue 22 Nov</p></td>
<td width="25"><p> 5:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 28 Nov</p></td>
<td width="25"><p> 5:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>