<h3 class="programme">14–16 October</h3>
<h2 class="programme"><a href="http://www.matakanacinemas.co.nz">Matakana Cinemas</a></h2>
<p>
Matakana Village, 2 Matakana Valley Road<br>
Matakana, Auckland<br>
Ph: 09 423 0218<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$16 General Admission<br>
$14 Students/Film Industry Guilds<br>
$13 Seniors<br>
$11 Children<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="matakana-2016">
<tbody>
<div>
<tr>
<td width="25"><p>Fri 14 Oct</p></td>
<td width="25"><p> 8:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/secrets-and-lies"><strong>Secrets and Lies</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 16 Oct</p></td>
<td width="25"><p> 1:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>