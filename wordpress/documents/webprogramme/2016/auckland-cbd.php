<h3 class="programme">2–16 October</h3>
<h2 class="programme"><a href="http://www.academycinemas.co.nz">Academy Cinemas</a></h2>
<p>
Auckland Library Building, 44 Lorne Street<br>
Auckland CBD<br>
Ph: 09 373 2761<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15.50 General Admission<br>
$12 Students/Film Industry Guilds<br>
$10 Seniors<br>
$9 Children<br>
$60 All Shorts Multi-pass (valid for up to six sessions - see page xx for more details)<br>
</p>
<p><strong>NOTE</strong>: For our opening night at The Civic on October 1, please see the <a href="http://www.showmeshorts.co.nz/events/auckland-opening-night/" title="Auckland Opening Night & Awards Ceremony">Auckland Opening Night & Awards Ceremony</a> page or <a href="http://www.ticketmaster.co.nz/Show-Me-Shorts-tickets/artist/2173479?tm_link=edp_Artist_Name" title="Book tickets to the Auckland Opening through Ticketmaster">click here to book tickets through Ticketmaster</a>.<p>

<h2>Screenings</h2><table id="table--pink-stripes" summary="auckland-cbd-2016">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 2 Oct</p></td>
<td width="25"><p> 2:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 2 Oct</p></td>
<td width="25"><p> 4:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/homeland"><strong>Homeland</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 2 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/secrets-and-lies"><strong>Secrets and Lies</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 3 Oct</p></td>
<td width="25"><p> 4:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 3 Oct</p></td>
<td width="25"><p> 8:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/he-tangata"><strong>He Tangata</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 4 Oct</p></td>
<td width="25"><p> 4:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/secrets-and-lies"><strong>Secrets and Lies</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 4 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/heartstrings"><strong>Heartstrings</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 4 Oct</p></td>
<td width="25"><p> 8:15pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/bump-in-the-night"><strong>Bump in the Night</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 5 Oct</p></td>
<td width="25"><p> 4:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/he-tangata"><strong>He Tangata</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 5 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 5 Oct</p></td>
<td width="25"><p> 8:15pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/homeland"><strong>Homeland</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 6 Oct</p></td>
<td width="25"><p> 4:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/bump-in-the-night"><strong>Bump in the Night</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 6 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/heartstrings"><strong>Heartstrings</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 6 Oct</p></td>
<td width="25"><p> 8:15pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/he-tangata"><strong>He Tangata</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 7 Oct</p></td>
<td width="25"><p> 4:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/bump-in-the-night"><strong>Bump in the Night</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 7 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/homeland"><strong>Homeland</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 7 Oct</p></td>
<td width="25"><p> 8:15pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/secrets-and-lies"><strong>Secrets and Lies</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 8 Oct</p></td>
<td width="25"><p> 2:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 8 Oct</p></td>
<td width="25"><p> 4:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/he-tangata"><strong>He Tangata</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 8 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/heartstrings"><strong>Heartstrings</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 8 Oct</p></td>
<td width="25"><p> 8:15pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/bump-in-the-night"><strong>Bump in the Night</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 9 Oct</p></td>
<td width="25"><p> 2:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/heartstrings"><strong>Heartstrings</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 9 Oct</p></td>
<td width="25"><p> 4:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/secrets-and-lies"><strong>Secrets and Lies</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 9 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/homeland"><strong>Homeland</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 10 Oct</p></td>
<td width="25"><p> 4:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/heartstrings"><strong>Heartstrings</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 11 Oct</p></td>
<td width="25"><p> 4:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/he-tangata"><strong>He Tangata</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 12 Oct</p></td>
<td width="25"><p> 4:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/secrets-and-lies"><strong>Secrets and Lies</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 12 Oct</p></td>
<td width="25"><p> 8:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/bump-in-the-night"><strong>Bump in the Night</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 13 Oct</p></td>
<td width="25"><p> 4:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/homeland"><strong>Homeland</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 14 Oct</p></td>
<td width="25"><p> 4:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 15 Oct</p></td>
<td width="25"><p> 2:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/homeland"><strong>Homeland</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 15 Oct</p></td>
<td width="25"><p> 4:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/secrets-and-lies"><strong>Secrets and Lies</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 16 Oct</p></td>
<td width="25"><p> 1:30pm</p></td>
<td width="25"><p><strong>Audience Choice</strong></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>