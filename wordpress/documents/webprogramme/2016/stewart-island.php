<h3 class="programme">19–30 November and 4–11 January</h3>
<h2 class="programme">Bunkhouse Theatre</h2>
<p>
10 Main Road<br>
Stewart Island<br>
Ph: 03 219 1113<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
$13 Seniors/Students/Film Industry Guilds<br>
$10 Children<br>
$60 All Shorts Multi-pass (valid for up to six sessions - see page xx for more details)<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="stewart-island-2016">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 19 Nov</p></td>
<td width="25"><p> 7:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/secrets-and-lies"><strong>Secrets and Lies</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 23 Nov</p></td>
<td width="25"><p> 7:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/he-tangata"><strong>He Tangata</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 26 Nov</p></td>
<td width="25"><p> 7:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/bump-in-the-night"><strong>Bump in the Night</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 30 Nov</p></td>
<td width="25"><p> 7:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/heartstrings"><strong>Heartstrings</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 4 Jan</p></td>
<td width="25"><p> 7:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 7 Jan</p></td>
<td width="25"><p> 7:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 11 Jan</p></td>
<td width="25"><p> 7:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/homeland"><strong>Homeland</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>