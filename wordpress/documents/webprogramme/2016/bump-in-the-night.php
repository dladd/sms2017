<p>It’s not easy to scare or shock with a short film. We don’t get creeped out very easily. If you’re up for a fright or enjoy being sunk into a sinister world created from the imagination of top filmmakers, this is the place. These short films are darkly impressive, tapping into our fears both real and imagined.</p>

<p>Estimated total run time: 88 minutes. <br>
Censorship rating to be advised. Please contact your cinema for details.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCBD">Auckland CBD - Academy Cinemas</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#WellingtonCBD">Wellington CBD - Embassy Theatre</option>
</select>
<h2>No Caller I.D. </h2><a href="https://vimeo.com/169492571" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/nocallerid.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Horror, 9 mins, NZ (NZ premiere)</p>

<p>Dir/Writ/Prod: Guy Pigden</p>
<p>Prod: Harley Neville</p>
<div class="cf"></div><p>A young woman receives a mysterious call in the night. After several strange events, she realises that she is not alone and that she may be in grave danger. </p>

<hr class="sessionBreak">

<h2>Wandering Soul</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/wanderingsoul.jpg" />
<p>Horror, 13 mins, Australia (NZ premiere)</p>

<p>Dir/Writ: Josh Tanner        </p>
<p>Writ/Prod: Jade van der Lei</p>
<div class="cf"></div><p>A Viet Cong soldier stationed in the claustrophobic tunnels of Cu Chi during the Vietnam War finds himself haunted by the ghost of a fallen comrade after the burial ceremony is compromised. </p>

<hr class="sessionBreak">

<h2>Sheep, Dog & Wolf - Breathe </h2><a href="https://vimeo.com/133407387" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/sheepdogandwolf-breathe.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Music video, 6 mins, NZ </p>

<p>Dir: THUNDERLIPS</p>
<p>Prod: Anna Duckworth, Meredith Rehburg, Mickey Finis, Alix Whittaker</p>
<div class="cf"></div><p>The oppressive feeling of not being able to breathe is captured in this visually engaging music video. </p>

<hr class="sessionBreak">

<h2>Unfaithful </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/unfaithful.jpg" />
<p>Thriller, 7 mins, NZ (world premiere)</p>

<p>Dir/Writ/Prod: Lucy Timmins</p>
<div class="cf"></div><p>All he wants is forgiveness from his girlfriend, but there are some things you can never come back from. </p>

<hr class="sessionBreak">

<h2>Knot </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/knot.jpg" />
<p>Drama, 12 mins, NZ/Australia (NZ premiere)</p>

<p>Dir/Writ/Prod: Niamh Peren</p>
<div class="cf"></div><p>In the sweeping rural landscape of Central Otago, a group of teenagers on a hunting trip turn violent. </p>

<hr class="sessionBreak">

<h2>Fabricated </h2><a href="https://vimeo.com/11938626" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/fabricated.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Sci-fi , 20 mins, USA (World premiere)</p>

<p>Dir: Brett Foxwell</p>
<div class="cf"></div><p>This ambitious stop-motion animated short film, ten years in the making, is a journey through a darkly fantastic alien world which was once our own. </p>

<hr class="sessionBreak">

<h2>Feeder </h2><a href="https://vimeo.com/131278692" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/feeder.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Horror, 17 mins, NZ</p>

<p>Dir: Christain Rivers        </p>
<p>Writ: Guy McDouall        </p>
<p>Prod: Mhairead Connor</p>
<div class="cf"></div><p>A songwriter must decide what lines he's willing to cross when a dark entity starts providing him with inspiration in exchange for an ever-increasing price. </p>

<hr class="sessionBreak">

<h2>Shmeat </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/shmeat.jpg" />
<p>Animation, 6 mins, NZ</p>

<p>Dir/Writ: Matasila Freshwater</p>
<p>Prod: Thomas Coppell</p>
<div class="cf"></div><p>In a dystopic New Zealand future, the struggle for food and resources inspires a peculiar scientist to venture into the night and procure a new food source. </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-cbd"><h3>Auckland CBD - Academy Cinemas</h3></a>
<table id="table--pink-stripes" summary="Bump in the Nightauckland-cbd-2016">
<tbody>
<tr>
<td width="50"><p>Tue 4 Oct</p></td>
<td width="50"><p> 8:15pm</p></td>
</tr>
<tr>
<td width="50"><p>Thu 6 Oct</p></td>
<td width="50"><p> 4:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Fri 7 Oct</p></td>
<td width="50"><p> 4:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Sat 8 Oct</p></td>
<td width="50"><p> 8:15pm</p></td>
</tr>
<tr>
<td width="50"><p>Wed 12 Oct</p></td>
<td width="50"><p> 8:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="Bump in the Nightchristchurch-2016">
<tbody>
<tr>
<td width="50"><p>Mon 31 Oct</p></td>
<td width="50"><p> 8:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="Bump in the Nightstewart-island-2016">
<tbody>
<tr>
<td width="50"><p>Sat 26 Nov</p></td>
<td width="50"><p> 7:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="WellingtonCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington-cbd"><h3>Wellington CBD - Embassy Theatre</h3></a>
<table id="table--pink-stripes" summary="Bump in the Nightwellington-cbd-2016">
<tbody>
<tr>
<td width="50"><p>Sat 15 Oct</p></td>
<td width="50"><p> 8:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Tue 18 Oct</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Fri 21 Oct</p></td>
<td width="50"><p> 8:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Mon 24 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>