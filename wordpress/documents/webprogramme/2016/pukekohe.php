<h3 class="programme">21–24 October</h3>
<h2 class="programme"><a href="http://www.pukekohecinemas.co.nz">Cinema 3 Pukekohe</a></h2>
<p>
85 Edinburgh Street<br>
Pukekohe<br>
Ph: 09 237 0216<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$17 General Admission<br>
$15 Students/Film Industry Guilds<br>
$11 Seniors/Children<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="pukekohe-2016">
<tbody>
<div>
<tr>
<td width="25"><p>Fri 21 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights*</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 24 Oct</p></td>
<td width="25"><p> 1:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>