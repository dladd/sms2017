<h3 class="programme">16 October</h3>
<h2 class="programme"><a href="http://www.anzactheatre.co.nz">Anzac Theatre</a></h2>
<p>
41 Hokianga Road<br>
Dargaville<br>
Ph: 09 439 8997<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$14 General Admission<br>
$12 Students/Seniors/Film Industry Guilds<br>
$8.50 Children<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="dargaville-2016">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 16 Oct</p></td>
<td width="25"><p> 5:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>