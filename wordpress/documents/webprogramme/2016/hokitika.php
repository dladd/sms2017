<h3 class="programme">2-6 November</h3>
<h2 class="programme">Regent Theatre</h2>
<p>
23 Weld Street<br>
Hokitika<br>
Ph: 03 755 8101<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$13.50 General Admission<br>
$10.50 Student<br>
$8 Child<br>
$9 Seniors<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="hokitika-2016">
<tbody>
<div>
<tr>
<td width="25"><p>Tue 2 Nov</p></td>
<td width="25"><p> 7:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 6 Nov</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>