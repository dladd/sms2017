<p>Embrace your inner child with this family-friendly selection of short films, featuring some of the top animation from Aotearoa and around the world. Meet a pint-sized inventor, a fastidious reindeer, a submarine captain left high and dry, and an entrepreneurial boy starting his own business making bow ties.</p>

<p>Estimated total run time: 74 minutes. Most appropriate for children 8+ years. This session is also available for school-group bookings.<br>
Censorship rating to be advised. Please contact your cinema for details.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCBD">Auckland CBD - Academy Cinemas</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#Devonport">Devonport - The Vic</option>
<option value="#Kaitaia">Kaitaia - Te Ahu</option>
<option value="#Pahiatua">Pahiatua - Regent Upstairs</option>
<option value="#Pukekohe">Pukekohe - Cinema 3 Pukekohe</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#WellingtonCBD">Wellington CBD - Embassy Theatre</option>
</select>
<h2>Kitten Witch </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/kittenwitch.jpg" />
<p>Fantasy, 9 mins, NZ (world premiere)</p>

<p>Dir/Writ/Prod: James Cunningham</p>
<p>Prod: Oliver Hilbert</p>
<div class="cf"></div><p>A young kitten wants to be a witch’s familiar and must pass a test or she will forever be just a simple cat. But the witch sees something in the kitten she does not expect. </p>

<hr class="sessionBreak">

<h2>Hey Deer!</h2><a href="https://www.youtube.com/watch?v=-XeIhfTVu8g" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/heydeer!.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 7 mins, Hungary (NZ premiere)</p>

<p>Dir/Writ: Örs Bárczy</p>
<div class="cf"></div><p>Each day an adorable deer eagerly tidies his house and shovels the snow from in front of his door. Each night there is a suspicious earthquake which causes the mess and snow to fall once more. </p>

<hr class="sessionBreak">

<h2>Litterbugs </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/litterbugs.jpg" />
<p>Adventure, 15 mins, UK (NZ premiere)</p>

<p>Dir/Writ: Peter Stanley-Ward        </p>
<p>Writ: Natalie Conway</p>
<p>Prod: Nicole Carmen-Davis, Chris Musselwhite</p>
<div class="cf"></div><p>Helped by her self-made flying mechanical creatures, a young inventor and a pint-sized superhero defeat the town bullies and find an unexpected friendship. </p>

<hr class="sessionBreak">

<h2>Perched </h2><a href="https://vimeo.com/159799307" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/perched.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 10 mins, UK (NZ premiere)</p>

<p>Dir: Liam Harris        </p>
<p>Writ: Nathaniel Price, Eoin Doran        </p>
<p>Prod: Elina Litvinova</p>
<div class="cf"></div><p>Hamish Fint is a crotchety old man used to a life of seclusion inside his submarine balanced precariously atop a mountain. He struggles to maintain equilibrium when an unwelcome visiting seagull rocks his world. </p>

<hr class="sessionBreak">

<h2>Ladi6 - Beffy </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/ladi6-beffy.jpg" />
<p>Music Video , 4 mins, NZ </p>

<p>Dir: Parallel Teeth</p>
<div class="cf"></div><p> A colourful adventure following a rhythmic tale of friendship.</p>

<hr class="sessionBreak">

<h2>Mo Can Tie A Bow </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/mocantieabow.jpg" />
<p>Documentary, 15 mins, Germany (NZ premiere)</p>

<p>Dir/Writ: André Hörmann        </p>
<p>Prod: Max Milhahn, Heike Kunze</p>
<div class="cf"></div><p>Mo is an 11yr-old boy with big dreams. He has set up a successful small business making and selling bow-ties. He learned this craft from his grandmother Arlene. </p>

<hr class="sessionBreak">

<h2>Fulfilament </h2><a href="https://vimeo.com/120627330" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/fulfilament.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 8 mins, UK (NZ premiere)</p>

<p>Dir/Writ: Rhiannon Evans</p>
<p>Writ: Joe Murtagh</p>
<p>Prod: Alexandra Breede</p>
<div class="cf"></div><p>This delightful animation follows a little lightbulb on a journey to find his place and his purpose in the world. </p>

<hr class="sessionBreak">

<h2>Spring Jam </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/springjam.jpg" />
<p>Animation, 6 mins, NZ</p>

<p>Dir/Writ: Ned Wenlock        </p>
<p>Prod: Georgiana Plaister</p>
<div class="cf"></div><p>A young stag, lacking impressive antlers, knows he needs to improvise sweet music if he's to have any chance during the mating season. </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-cbd"><h3>Auckland CBD - Academy Cinemas</h3></a>
<table id="table--pink-stripes" summary="My Generationauckland-cbd-2016">
<tbody>
<tr>
<td width="50"><p>Sun 2 Oct</p></td>
<td width="50"><p> 2:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Mon 3 Oct</p></td>
<td width="50"><p> 4:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Wed 5 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Sat 8 Oct</p></td>
<td width="50"><p> 2:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Fri 14 Oct</p></td>
<td width="50"><p> 4:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Devonport"></a>

<a href="http://www.showmeshorts.co.nz/programme/devonport"><h3>Devonport - The Vic</h3></a>
<table id="table--pink-stripes" summary="My Generationdevonport-2016">
<tbody>
<tr>
<td width="50"><p>Sun 9 Oct</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Pukekohe"></a>

<a href="http://www.showmeshorts.co.nz/programme/pukekohe"><h3>Pukekohe - Cinema 3 Pukekohe</h3></a>
<table id="table--pink-stripes" summary="My Generationpukekohe-2016">
<tbody>
<tr>
<td width="50"><p>Mon 24 Oct</p></td>
<td width="50"><p> 1:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="My Generationchristchurch-2016">
<tbody>
<tr>
<td width="50"><p>Sat 29 Oct</p></td>
<td width="50"><p> 1:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Kaitaia"></a>

<a href="http://www.showmeshorts.co.nz/programme/kaitaia"><h3>Kaitaia - Te Ahu</h3></a>
<table id="table--pink-stripes" summary="My Generationkaitaia-2016">
<tbody>
<tr>
<td width="50"><p>Sat 29 Oct</p></td>
<td width="50"><p> 2:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Pahiatua"></a>

<a href="http://www.showmeshorts.co.nz/programme/pahiatua"><h3>Pahiatua - Regent Upstairs</h3></a>
<table id="table--pink-stripes" summary="My Generationpahiatua-2016">
<tbody>
<tr>
<td width="50"><p>Sat 22 Oct</p></td>
<td width="50"><p> 2:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Mon 24 Oct</p></td>
<td width="50"><p> 2:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="My Generationstewart-island-2016">
<tbody>
<tr>
<td width="50"><p>Sat 7 Jan</p></td>
<td width="50"><p> 7:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="WellingtonCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington-cbd"><h3>Wellington CBD - Embassy Theatre</h3></a>
<table id="table--pink-stripes" summary="My Generationwellington-cbd-2016">
<tbody>
<tr>
<td width="50"><p>Sat 15 Oct</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Thu 20 Oct</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Sun 23 Oct</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Tue 25 Oct</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>