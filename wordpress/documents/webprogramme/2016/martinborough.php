<h3 class="programme">16–22 October</h3>
<h2 class="programme">Circus Cinema</h2>
<p>
34 Jellicoe Street<br>
Martinborough<br>
Ph: 06 306 9442<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$16 General Admission<br>
$14 Student/Senior<br>
$11 Child<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="martinborough-2016">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 16 Oct</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 19 Oct</p></td>
<td width="25"><p> 6:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 22 Oct</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>