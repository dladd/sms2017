<h3 class="programme">22–24 October</h3>
<h2 class="programme">Regent Upstairs</h2>
<p>
64 Main Street<br>
Pahiatua<br>
Ph: 06 376 8607, 06 376 8441 (after hours)<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$13 General Admission<br>
$10 Student/Senior/Film Industry Guilds<br>
$8 Child<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="pahiatua-2016">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 22 Oct</p></td>
<td width="25"><p> 2:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 22 Oct</p></td>
<td width="25"><p> 7:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 23 Oct</p></td>
<td width="25"><p> 7:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 24 Oct</p></td>
<td width="25"><p> 2:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 24 Oct</p></td>
<td width="25"><p> 5:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>