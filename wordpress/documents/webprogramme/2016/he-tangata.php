<p><p>An old Maori proverb asks: </p>
<p>He aha te mea nui o te ao? (What is the most important thing in the world?) </p>
<p>He tangata, he tangata, he tangata. (It is the people, it is the people, it is the people.)</p>
<p>This selection of short films brings together interesting people from Aotearoa and abroad. We meet Kiwis Anna, Stevo, Chelsea Jade and a group of flatmates re-shuffling their living arrangements. Journey from the snowy mountains of Kyrgyzstan, via an animated train, through war-torn landscapes, to a bathtub in Germany where three brothers are comically re-united.</p>
</p>

<p>Estimated total run time: 96 minutes. <br>
Censorship rating to be advised. Please contact your cinema for details.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCBD">Auckland CBD - Academy Cinemas</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#WellingtonCBD">Wellington CBD - Embassy Theatre</option>
</select>
<h2>Chelsea Jade - Low Brow </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/chelseajade-lowbrow.jpg" />
<p>Music video, 4 mins, NZ </p>

<p>Dir: Alexander Gandar</p>
<p>Prod: Billie Ruck</p>
<div class="cf"></div><p>A bold rendition of Chelsea Jade's pop banger 'Low Brow' performed by the artist with her unique visual style and presence. </p>

<hr class="sessionBreak">

<h2>Stevo </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/stevo.jpg" />
<p>Documentary, 15 mins, NZ </p>

<p>Dir/Prod: Heather Hayward        </p>
<p>Prod: Lesley Parker, Michelle Savill</p>
<div class="cf"></div><p>Stevo lives his life between Wellington city where he works as a security guard, and his home in the Urewera forests. He bridges the two worlds with the help of traditional Māori trade and barter. </p>

<hr class="sessionBreak">

<h2>Seide </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/seide.jpg" />
<p>Drama, 14 mins, Kyrgyzstan (NZ premiere)</p>

<p>Dir/Writ/Prod:         Elnura Osmonalieva</p>
<p>Prod: Charlotte Rabate</p>
<div class="cf"></div><p>The breathtaking beauty of Kyrgyzstan's snowy mountains are only just eclipsed by the powerful performances in this gentle tale of a girl forced into an arranged marriage. </p>

<hr class="sessionBreak">

<h2>Home </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/home.jpg" />
<p>Drama, 20 mins, Kosovo (NZ premiere)</p>

<p>Dir/Writ: Daniel Mulloy        </p>
<p>Prod: Chris Watling, Scott O'Donnell, Tim Nash, Shpat Deda, Afolabi Kuti</p>
<div class="cf"></div><p>As thousands of refugees attempt to get into Europe, a comfortable English family sets out on what appears to be a holiday... </p>

<hr class="sessionBreak">

<h2>Moving </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/moving.jpg" />
<p>Drama, 12 mins, NZ</p>

<p>Dir/Writ: Leon Wadham, Eli Kent        </p>
<p>Prod: Molly O'Shea, Ruby Reihana-Wilson</p>
<div class="cf"></div><p>In the wake of a painful break-up a group of friends band together to help the former couple move out of their old room. </p>

<hr class="sessionBreak">

<h2>ANNA</h2><a href="https://vimeo.com/144035119" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/anna.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 9 mins, NZ (NZ premiere)</p>

<p>Dir/Writ: Harriett Maire</p>
<p>Prod: Phoebe Jeurissen</p>
<div class="cf"></div><p>Anna is a young woman on the Autism spectrum. When someone takes her seat on the bus, she must cope with this unexpected change in her routine. </p>

<hr class="sessionBreak">

<h2>Une Tete Disparait (The Head Vanishes)</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/unetetedisparait(theheadvanishes).jpg" />
<p>Animation, 10 mins, France (NZ premiere)</p>

<p>Dir/Writ/Prod: Franck Dion</p>
<p>Prod: Richard Van Den Boom, Julie Roy</p>
<div class="cf"></div><p>Jacqueline is an elderly woman who has lost her mind a bit, but whatever, she has decided to take a train trip to the seaside by herself.</p>

<hr class="sessionBreak">

<h2>Die Badewanne (The Bathtub)</h2><a href="https://vimeo.com/167876801" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/diebadewanne.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Comedy, 13 mins, Germany (NZ premiere)</p>

<p>Dir/Writ/Prod: Tim Ellrich</p>
<p>Writ: Dominik Huber</p>
<div class="cf"></div><p>Three brothers try to dive back into their idealised past through an old family picture of them in a bathtub. </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-cbd"><h3>Auckland CBD - Academy Cinemas</h3></a>
<table id="table--pink-stripes" summary="He Tangataauckland-cbd-2016">
<tbody>
<tr>
<td width="50"><p>Mon 3 Oct</p></td>
<td width="50"><p> 8:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Wed 5 Oct</p></td>
<td width="50"><p> 4:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Thu 6 Oct</p></td>
<td width="50"><p> 8:15pm</p></td>
</tr>
<tr>
<td width="50"><p>Sat 8 Oct</p></td>
<td width="50"><p> 4:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Tue 11 Oct</p></td>
<td width="50"><p> 4:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="He Tangatachristchurch-2016">
<tbody>
<tr>
<td width="50"><p>Tue 1 Nov</p></td>
<td width="50"><p> 6:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="He Tangatastewart-island-2016">
<tbody>
<tr>
<td width="50"><p>Wed 23 Nov</p></td>
<td width="50"><p> 7:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="WellingtonCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington-cbd"><h3>Wellington CBD - Embassy Theatre</h3></a>
<table id="table--pink-stripes" summary="He Tangatawellington-cbd-2016">
<tbody>
<tr>
<td width="50"><p>Fri 14 Oct</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Mon 17 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Wed 19 Oct</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Sun 23 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>