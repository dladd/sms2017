<h3 class="programme">16 October</h3>
<h2 class="programme">Finns Paekakariki</h2>
<p>
2 Beach Road<br>
Paekakariki<br>
Ph: 04 292 8081<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$13.50 General Admission<br>
$10.50 Students/Film Industry Guilds<br>
$9 Seniors<br>
$8 Children<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="paekakariki-2016">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 16 Oct</p></td>
<td width="25"><p> 6:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights*</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>