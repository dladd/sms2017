<p>This carefully curated selection includes some of our most interesting and fun short films. It’s a great way to sample what Show Me Shorts is all about. The films on offer include both local and international stories: an animated deer, a wedding doco, a coffee table in love with it’s owner, a shrinking apartment, a trip to the seaside, a dystopian future, a band on tour, and perhaps most shockingly - a German comedy.</p>

<p>Estimated total run time: 91 minutes. <br>
Censorship rating to be advised. Please contact your cinema for details.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#Alexandra">Alexandra - Central Cinema</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#Dargaville">Dargaville - Anzac Theatre</option>
<option value="#Devonport">Devonport - The Vic</option>
<option value="#Dunedin">Dunedin - Rialto Cinemas</option>
<option value="#Gisborne">Gisborne - Dome Cinema</option>
<option value="#GreatBarrierIsland">Great Barrier Island - Barrier Social Club</option>
<option value="#Hokitika">Hokitika - Regent Theatre</option>
<option value="#Kaitaia">Kaitaia - Te Ahu</option>
<option value="#Martinborough">Martinborough - Circus Cinema</option>
<option value="#Matakana">Matakana - Matakana Cinemas</option>
<option value="#NewPlymouth">New Plymouth - 4th Wall Theatre</option>
<option value="#Paekakariki">Paekakariki - Finns Paekakariki</option>
<option value="#Pahiatua">Pahiatua - Regent Upstairs</option>
<option value="#Pukekohe">Pukekohe - Cinema 3 Pukekohe</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#Tauranga">Tauranga - Rialto Tauranga</option>
<option value="#TeAwamutu">Te Awamutu - Regent Theatre</option>
<option value="#WaihekeIsland">Waiheke Island - Waiheke Island Community Cinema</option>
</select>
<h2>Spring Jam </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/springjam.jpg" />
<p>Animation, 6 mins, NZ</p>

<p>Dir/Writ: Ned Wenlock        </p>
<p>Prod: Georgiana Plaister</p>
<div class="cf"></div><p>A young stag, lacking impressive antlers, knows he needs to improvise sweet music if he's to have any chance during the mating season. </p>

<hr class="sessionBreak">

<h2>Zero M2 </h2><a href="https://vimeo.com/102540424" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/zerom2.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 19 mins, France</p>

<p>Dir/Writ: Matthieu Landour        </p>
<p>Writ: Nicolas Bovorasmy        </p>
<p>Prod: Pierre-Emmanuel Fleurantin, Laurent Baujard</p>
<div class="cf"></div><p>After a series of unsuccessful flat-hunts in Paris, Paul finally moves into a nice affordable studio. He soon notices something amiss with his neighbours´strange behaviours and dubious noises during the night. </p>

<hr class="sessionBreak">

<h2>Topsy and Dave </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/topsyanddave.jpg" />
<p>Documentary, 9 mins, NZ </p>

<p>Dir: Kirsty Griffin, Vivienne Kernick        </p>
<p>Prod: Bella Pacific Media</p>
<div class="cf"></div><p>Love, patience, understanding and support are the cornerstone of any successful marriage. That’s never more obvious than in this documentary where we meet Topsy and Dave, residents of 'The Supported Life Style Hauraki Trust' who are preparing for their big day. </p>

<hr class="sessionBreak">

<h2>Une Tete Disparait (The Head Vanishes)</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/unetetedisparait(theheadvanishes).jpg" />
<p>Animation, 10 mins, France</p>

<p>Dir/Writ/Prod: Franck Dion</p>
<p>Prod: Richard Van Den Boom, Julie Roy</p>
<div class="cf"></div><p>Jacqueline is an elderly woman who has lost her mind a bit, but whatever, she has decided to take a train trip to the seaside by herself. </p>

<hr class="sessionBreak">

<h2>Fabricated </h2><a href="https://vimeo.com/11938626" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/fabricated.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Sci-fi , 20 mins, USA</p>

<p>Dir: Brett Foxwell</p>
<div class="cf"></div><p>This ambitious stop-motion animated short film, ten years in the making, is a journey through a darkly fantastic alien world which was once our own. </p>

<hr class="sessionBreak">

<h2>Ha the Unclear - Secret Lives of Furniture </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/hatheunclear-secretlivesoffurniture.jpg" />
<p>Music video, 4 mins, NZ </p>

<p>Dir: Simon Oliver</p>
<p>Prod: Sadie Wilson</p>
<div class="cf"></div><p>An entire lounge of furniture comes to life in this inventive music video about a coffee table infatuated with its owner. </p>

<hr class="sessionBreak">

<h2>Die Badewanne (The Bathtub)</h2><a href="https://vimeo.com/167876801" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/diebadewanne.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Comedy, 13 mins, Germany</p>

<p>Dir/Writ/Prod: Tim Ellrich</p>
<p>Writ: Dominik Huber</p>
<div class="cf"></div><p>Three brothers try to dive back into their idealised past through an old family picture of them in a bathtub.</p>

<hr class="sessionBreak">

<h2>Shout at the Ground </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/shoutattheground.jpg" />
<p>Comedy, 11 mins, NZ </p>

<p>Dir/Writ: Joe Lonie</p>
<p>Prod: Leela Menon, Robin Scholes</p>
<div class="cf"></div><p>Trapped in a speeding van, a Kiwi rock band succumb to travel sickness while deconstructing the heist that robbed them of an entire weekend's door take. </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="Alexandra"></a>

<a href="http://www.showmeshorts.co.nz/programme/alexandra"><h3>Alexandra - Central Cinema</h3></a>
<table id="table--pink-stripes" summary="Highlightsalexandra-2016">
<tbody>
<tr>
<td width="50"><p>Sat 5 Nov</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Devonport"></a>

<a href="http://www.showmeshorts.co.nz/programme/devonport"><h3>Devonport - The Vic</h3></a>
<table id="table--pink-stripes" summary="Highlightsdevonport-2016">
<tbody>
<tr>
<td width="50"><p>Fri 7 Oct</p></td>
<td width="50"><p> 8:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="GreatBarrierIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/great-barrier-island"><h3>Great Barrier Island - Barrier Social Club</h3></a>
<table id="table--pink-stripes" summary="Highlightsgreat-barrier-island-2016">
<tbody>
<tr>
<td width="50"><p>Mon 17 Oct</p></td>
<td width="50"><p> 7:15pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Matakana"></a>

<a href="http://www.showmeshorts.co.nz/programme/matakana"><h3>Matakana - Matakana Cinemas</h3></a>
<table id="table--pink-stripes" summary="Highlightsmatakana-2016">
<tbody>
<tr>
<td width="50"><p>Sun 16 Oct</p></td>
<td width="50"><p> 1:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Pukekohe"></a>

<a href="http://www.showmeshorts.co.nz/programme/pukekohe"><h3>Pukekohe - Cinema 3 Pukekohe</h3></a>
<table id="table--pink-stripes" summary="Highlightspukekohe-2016">
<tbody>
<tr>
<td width="50"><p>Fri 21 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="WaihekeIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/waiheke-island"><h3>Waiheke Island - Waiheke Island Community Cinema</h3></a>
<table id="table--pink-stripes" summary="Highlightswaiheke-island-2016">
<tbody>
<tr>
<td width="50"><p>Sun 9 Oct</p></td>
<td width="50"><p> 5:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="Highlightschristchurch-2016">
<tbody>
<tr>
<td width="50"><p>Wed 2 Nov</p></td>
<td width="50"><p> 6:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Dargaville"></a>

<a href="http://www.showmeshorts.co.nz/programme/dargaville"><h3>Dargaville - Anzac Theatre</h3></a>
<table id="table--pink-stripes" summary="Highlightsdargaville-2016">
<tbody>
<tr>
<td width="50"><p>Sun 16 Oct</p></td>
<td width="50"><p> 5:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Dunedin"></a>

<a href="http://www.showmeshorts.co.nz/programme/dunedin"><h3>Dunedin - Rialto Cinemas</h3></a>
<table id="table--pink-stripes" summary="Highlightsdunedin-2016">
<tbody>
<tr>
<td width="50"><p>Sun 16 Oct</p></td>
<td width="50"><p> 6:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Gisborne"></a>

<a href="http://www.showmeshorts.co.nz/programme/gisborne"><h3>Gisborne - Dome Cinema</h3></a>
<table id="table--pink-stripes" summary="Highlightsgisborne-2016">
<tbody>
<tr>
<td width="50"><p>Sat 12 Nov</p></td>
<td width="50"><p> 7:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Hokitika"></a>

<a href="http://www.showmeshorts.co.nz/programme/hokitika"><h3>Hokitika - Regent Theatre</h3></a>
<table id="table--pink-stripes" summary="Highlightshokitika-2016">
<tbody>
<tr>
<td width="50"><p>Tue 2 Nov</p></td>
<td width="50"><p> 7:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Sun 6 Nov</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Kaitaia"></a>

<a href="http://www.showmeshorts.co.nz/programme/kaitaia"><h3>Kaitaia - Te Ahu</h3></a>
<table id="table--pink-stripes" summary="Highlightskaitaia-2016">
<tbody>
<tr>
<td width="50"><p>Thu 27 Oct</p></td>
<td width="50"><p> 5:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Fri 28 Oct</p></td>
<td width="50"><p> 5:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Sun 30 Oct</p></td>
<td width="50"><p> 5:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Martinborough"></a>

<a href="http://www.showmeshorts.co.nz/programme/martinborough"><h3>Martinborough - Circus Cinema</h3></a>
<table id="table--pink-stripes" summary="Highlightsmartinborough-2016">
<tbody>
<tr>
<td width="50"><p>Sun 16 Oct</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Wed 19 Oct</p></td>
<td width="50"><p> 6:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Sat 22 Oct</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="NewPlymouth"></a>

<a href="http://www.showmeshorts.co.nz/programme/new-plymouth"><h3>New Plymouth - 4th Wall Theatre</h3></a>
<table id="table--pink-stripes" summary="Highlightsnew-plymouth-2016">
<tbody>
<tr>
<td width="50"><p>Thu 20 Oct</p></td>
<td width="50"><p> 7:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Pahiatua"></a>

<a href="http://www.showmeshorts.co.nz/programme/pahiatua"><h3>Pahiatua - Regent Upstairs</h3></a>
<table id="table--pink-stripes" summary="Highlightspahiatua-2016">
<tbody>
<tr>
<td width="50"><p>Sat 22 Oct</p></td>
<td width="50"><p> 7:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Sun 23 Oct</p></td>
<td width="50"><p> 7:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Mon 24 Oct</p></td>
<td width="50"><p> 5:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="Highlightsstewart-island-2016">
<tbody>
<tr>
<td width="50"><p>Wed 4 Jan</p></td>
<td width="50"><p> 7:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Tauranga"></a>

<a href="http://www.showmeshorts.co.nz/programme/tauranga"><h3>Tauranga - Rialto Tauranga</h3></a>
<table id="table--pink-stripes" summary="Highlightstauranga-2016">
<tbody>
<tr>
<td width="50"><p>Sun 30 Oct</p></td>
<td width="50"><p> 5:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="TeAwamutu"></a>

<a href="http://www.showmeshorts.co.nz/programme/te-awamutu"><h3>Te Awamutu - Regent Theatre</h3></a>
<table id="table--pink-stripes" summary="Highlightste-awamutu-2016">
<tbody>
<tr>
<td width="50"><p>Tue 22 Nov</p></td>
<td width="50"><p> 5:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Mon 28 Nov</p></td>
<td width="50"><p> 5:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Paekakariki"></a>

<a href="http://www.showmeshorts.co.nz/programme/paekakariki"><h3>Paekakariki - Finns Paekakariki</h3></a>
<table id="table--pink-stripes" summary="Highlightspaekakariki-2016">
<tbody>
<tr>
<td width="50"><p>Sun 16 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>