<h3 class="programme">7–9  October</h3>
<h2 class="programme"><a href="http://www.thevic.co.nz">The Vic</a></h2>
<p>
56 Victoria Road<br>
Devonport, Auckland<br>
Ph: 09 446 0100<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
$12 Students/Film Industry Guilds<br>
$11 Seniors<br>
$10 Children<br>
(Prices include $1 voluntary donation to The Vic Trust, which you can opt out of)<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="devonport-2016">
<tbody>
<div>
<tr>
<td width="25"><p>Fri 7 Oct</p></td>
<td width="25"><p> 8:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights*</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 8 Oct</p></td>
<td width="25"><p> 6:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/homeland"><strong>Homeland</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 9 Oct</p></td>
<td width="25"><p> 4:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>