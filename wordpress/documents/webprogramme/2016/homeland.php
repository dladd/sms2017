<p>Our feelings about the word homeland have a special charge. It’s more than where we live, but a part of us we carry around, shaping who we are. So when we’re homesick, we can feel a physical pain or emptiness. The characters in these short films are all very different: astronauts, explorers, a shepherd, a mother, and a lovesick coffee table. Some of them are far from home, others are trying to create or maintain one, or get back to theirs. The thing they have in common is a deep yearning for the warm hearth of their homeland.</p>

<p>Estimated total run time: 95 minutes. <br>
Censorship rating to be advised. Please contact your cinema for details.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCBD">Auckland CBD - Academy Cinemas</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#Devonport">Devonport - The Vic</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#WellingtonCBD">Wellington CBD - Embassy Theatre</option>
</select>
<h2>Cradle </h2><a href="https://vimeo.com/149335152" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/cradle.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Sci-fi, 16 mins, NZ </p>

<p>Dir: Damon Duncan        </p>
<p>Writ: Hugh Calveley        </p>
<p>Prod: Luke Robinson</p>
<div class="cf"></div><p>Eade is a 14yr-old girl returning to Earth on the spaceship Cradle when an explosion cripples the spacecraft and injures her dad. She must try to outwit the computer that refuses to perform the operation needed to save his life. </p>

<hr class="sessionBreak">

<h2>Zero M2 </h2><a href="https://vimeo.com/102540424" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/zerom2.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 19 mins, France (NZ premiere)</p>

<p>Dir/Writ: Matthieu Landour        </p>
<p>Writ: Nicolas Bovorasmy        </p>
<p>Prod: Pierre-Emmanuel Fleurantin, Laurent Baujard</p>
<div class="cf"></div><p>After a series of unsuccessful flat-hunts in Paris, Paul finally moves into a nice affordable studio. He soon notices something amiss with his neighbours´strange behaviours and dubious noises during the night. </p>

<hr class="sessionBreak">

<h2>Ha the Unclear - Secret Lives of Furniture </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/hatheunclear-secretlivesoffurniture.jpg" />
<p>Music video, 4 mins, NZ </p>

<p>Dir: Simon Oliver</p>
<p>Prod: Sadie Wilson</p>
<div class="cf"></div><p>An entire lounge of furniture comes to life in this inventive music video about a coffee table infatuated with its owner. </p>

<hr class="sessionBreak">

<h2>In The Distance </h2><a href="https://vimeo.com/128271607" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/inthedistance.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 8 mins, Germany (NZ premiere)</p>

<p>Dir/Writ: Florian Grolig</p>
<div class="cf"></div><p>It's calm and peaceful above the clouds in this serene animation observing a man living isolated at the top of a tall building. But in the distance is war, and night by night the chaos advances. </p>

<hr class="sessionBreak">

<h2>Parvaneha (Butterflies) </h2><a href="https://vimeo.com/102540424" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/butterflies.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 3 mins, Iran (world premiere)</p>

<p>Dir/Writ/Prod: Adnan Zandi</p>
<div class="cf"></div><p>Any mother can relate to this short story of an Iranian woman searching for somewhere private to feed her hungry baby. </p>

<hr class="sessionBreak">

<h2>9 Chemin des Gauchoirs </h2><a href="https://www.youtube.com/watch?v=UPUzhfAPd2Y" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/9chemindesgauchoirs.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 13 mins, France (NZ premiere)</p>

<p>Dir/Writ: Lyonel Charmette        </p>
<p>Prod: Jean-François Sarazin</p>
<div class="cf"></div><p>A shepherd follows a wayward goat onto a mysterious cable car, where each carriage has been converted into an elaborate room from a house. Where are the owners? </p>

<hr class="sessionBreak">

<h2>Icarus </h2><a href="https://vimeo.com/169169039" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/icarus.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Sci-fi, 20 mins, USA (world premiere)</p>

<p>Dir/Writ: Tom Teller        </p>
<p>Writ: Andrew Guastaferro        </p>
<p>Prod: Michelle Evans</p>
<div class="cf"></div><p>When a Mars colony comms satellite is damaged, Emilia Riley embarks on a seemingly harmless repair excursion. A shuttle malfunction cuts connectivity to the ground and Chris, her son, makes the knee-jerk decision to go after her. </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-cbd"><h3>Auckland CBD - Academy Cinemas</h3></a>
<table id="table--pink-stripes" summary="Homelandauckland-cbd-2016">
<tbody>
<tr>
<td width="50"><p>Sun 2 Oct</p></td>
<td width="50"><p> 4:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Wed 5 Oct</p></td>
<td width="50"><p> 8:15pm</p></td>
</tr>
<tr>
<td width="50"><p>Fri 7 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Sun 9 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Thu 13 Oct</p></td>
<td width="50"><p> 4:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Sat 15 Oct</p></td>
<td width="50"><p> 2:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Devonport"></a>

<a href="http://www.showmeshorts.co.nz/programme/devonport"><h3>Devonport - The Vic</h3></a>
<table id="table--pink-stripes" summary="Homelanddevonport-2016">
<tbody>
<tr>
<td width="50"><p>Sat 8 Oct</p></td>
<td width="50"><p> 6:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="Homelandchristchurch-2016">
<tbody>
<tr>
<td width="50"><p>Fri 28 Oct</p></td>
<td width="50"><p> 6:00pm</p></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="Homelandstewart-island-2016">
<tbody>
<tr>
<td width="50"><p>Wed 11 Jan</p></td>
<td width="50"><p> 7:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="WellingtonCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington-cbd"><h3>Wellington CBD - Embassy Theatre</h3></a>
<table id="table--pink-stripes" summary="Homelandwellington-cbd-2016">
<tbody>
<tr>
<td width="50"><p>Sun 16 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Tue 18 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Fri 21 Oct</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Tue 25 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>