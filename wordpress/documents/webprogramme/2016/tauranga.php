<h3 class="programme">30 October</h3>
<h2 class="programme"><a href="http://www.rialtotauranga.co.nz">Rialto Tauranga</a></h2>
<p>
21 Devonport Rd<br>
Tauranga<br>
Ph: 07 577 0445<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$17 General Admission<br>
$15 Students/Film Industry Guilds<br>
$10.50 Seniors/Children<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="tauranga-2016">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 30 Oct</p></td>
<td width="25"><p> 5:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights*</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>