<h3 class="programme">27–30 October</h3>
<h2 class="programme">Te Ahu</h2>
<p>
Corner South Road and Mathews Avenue<br>
Kaitaia<br>
Ph: 09 408 0519<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
$8 Children<br>
$11 Students<br>
$11 Seniors<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="kaitaia-2016">
<tbody>
<div>
<tr>
<td width="25"><p>Thu 27 Oct</p></td>
<td width="25"><p> 5:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 28 Oct</p></td>
<td width="25"><p> 5:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 29 Oct</p></td>
<td width="25"><p> 2:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 30 Oct</p></td>
<td width="25"><p> 5:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>