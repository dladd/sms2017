<p>The characters in these short films are all hiding something. Featuring a sordid night out, a thirst for vengeance, guilt, loneliness, sexual indiscretion, rock ‘n’ roll and a heist gone wrong. The suppression or revelation of their secrets will play out to deliver high drama, action, comedy and surprises. To what lengths will they go to hide or uncover the truth? Are the rumours true?</p>

<p>Estimated total run time: 98 minutes. <br>
Censorship rating to be advised. Please contact your cinema for details.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCBD">Auckland CBD - Academy Cinemas</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#Matakana">Matakana - Matakana Cinemas</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#WellingtonCBD">Wellington CBD - Embassy Theatre</option>
</select>
<h2>The Event </h2><a href="https://www.youtube.com/watch?v=B9gWvuHfyyU" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/theevent.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Comedy, 10 mins, NZ </p>

<p>Dir: Eddy Fifield        </p>
<p>Writ: Dominic Hoey        </p>
<p>Prod: Hazel Gibson</p>
<div class="cf"></div><p>Set in Auckland's infamous K R'd, the morning after. Seff is trying to find his keys while battling a vicious hangover and a sordid rumour. </p>

<hr class="sessionBreak">

<h2>Long Time Coming </h2><a href="https://vimeo.com/118099255" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/longtimecoming.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 12 mins, NZ (world premiere)</p>

<p>Dir/Writ: Tom Augustine        </p>
<p>Prod: Puteri Raja Ariff, Rebekah Ngatae, Sarah Hall</p>
<div class="cf"></div><p>When Alice's friend is the victim of a disturbing crime, she seeks help getting revenge as she grapples with grief and anger. </p>

<hr class="sessionBreak">

<h2>Break In The Weather </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/breakintheweather.jpg" />
<p>Drama, 16 mins, NZ (world premiere)</p>

<p>Jamie unexpect</p>
<p>Dir/Writ: Aidee Walker        </p>
<p>Prod: Alexander Gandar</p>
<div class="cf"></div><p>edly finds herself being the live-in nurse for the person she despises most – her father. Can the pop hits of the 90s help her overcome the betrayal from her past? </p>

<hr class="sessionBreak">

<h2>Fract </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/fract.jpg" />
<p>Drama, 14 mins, NZ (world premiere)</p>

<p>Dir/Writ: Georgina Bloomfield        </p>
<p>Prod: Dave Mark</p>
<div class="cf"></div><p>A teenage outcast discovers the cast on her broken arm can be a way to make new friends. </p>

<hr class="sessionBreak">

<h2>Wait </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/wait.jpg" />
<p>Drama, 14 mins, NZ </p>

<p>Dir/Writ: Yamin Tun</p>
<p>Prod: Vicky Pope, Daniel Higgins</p>
<div class="cf"></div><p>This award winning short film tells the story of a Chinese migrant family in 1980s New Zealand that is at the point of breaking apart. </p>

<hr class="sessionBreak">

<h2>Love is Blind </h2><a href="https://vimeo.com/160734282" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/loveisblind.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Comedy, 7 mins, UK (NZ premiere)</p>

<p>Dir/Writ: Dan Hodgson</p>
<p>Prod: Elizabeth Brown</p>
<div class="cf"></div><p>When Alice's husband James returns home early to try and fix their marriage, Alice is in the arms of a her lover. A comedic quest begins as she attempts the reconciliation while simultaneously negotiating the lover's escape. </p>

<hr class="sessionBreak">

<h2>Homebodies </h2><a href="https://vimeo.com/161414063" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/homebodies.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Comedy, 13 mins, Australia (NZ premiere)</p>

<p>Dir/Writ/Prod: Yianni Warnock        </p>
<p>Prod: Charles Williams</p>
<div class="cf"></div><p>Shannon cooks dinner whilst speaking to a perverted stranger online. Meanwhile her husband Andrew takes a bath wrapped in Christmas lights. A hilarious and shocking tale of two lost souls looking for solace in the wrong places. </p>

<hr class="sessionBreak">

<h2>Shout at the Ground </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/shoutattheground.jpg" />
<p>Comedy, 11 mins, NZ (world premiere)</p>

<p>Dir/Writ: Joe Lonie</p>
<p>Prod: Leela Menon, Robin Scholes</p>
<div class="cf"></div><p>Trapped in a speeding van, a Kiwi rock band succumb to travel sickness while deconstructing the heist that robbed them of an entire weekend's door take. </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-cbd"><h3>Auckland CBD - Academy Cinemas</h3></a>
<table id="table--pink-stripes" summary="Secrets and Liesauckland-cbd-2016">
<tbody>
<tr>
<td width="50"><p>Sun 2 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Tue 4 Oct</p></td>
<td width="50"><p> 4:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Fri 7 Oct</p></td>
<td width="50"><p> 8:15pm</p></td>
</tr>
<tr>
<td width="50"><p>Sun 9 Oct</p></td>
<td width="50"><p> 4:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Wed 12 Oct</p></td>
<td width="50"><p> 4:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Sat 15 Oct</p></td>
<td width="50"><p> 4:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Matakana"></a>

<a href="http://www.showmeshorts.co.nz/programme/matakana"><h3>Matakana - Matakana Cinemas</h3></a>
<table id="table--pink-stripes" summary="Secrets and Liesmatakana-2016">
<tbody>
<tr>
<td width="50"><p>Fri 14 Oct</p></td>
<td width="50"><p> 8:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="Secrets and Lieschristchurch-2016">
<tbody>
<tr>
<td width="50"><p>Thu 27 Oct</p></td>
<td width="50"><p> 8:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="Secrets and Liesstewart-island-2016">
<tbody>
<tr>
<td width="50"><p>Sat 19 Nov</p></td>
<td width="50"><p> 7:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="WellingtonCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington-cbd"><h3>Wellington CBD - Embassy Theatre</h3></a>
<table id="table--pink-stripes" summary="Secrets and Lieswellington-cbd-2016">
<tbody>
<tr>
<td width="50"><p>Mon 17 Oct</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Thu 20 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Sat 22 Oct</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Wed 26 Oct</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>