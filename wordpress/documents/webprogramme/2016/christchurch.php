<h3 class="programme">27 October–2 November</h3>
<h2 class="programme"><a href="http://http://cinematheque.aliceinvideoland.co.nz">Alice Cinematheque</a></h2>
<p>
Old High Street Post Office, 209 Tuam Street<br>
Christchurch<br>
Ph: 03 365 0615<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$17 General Admission<br>
$15 Students/Film Industry Guilds<br>
$12 Seniors/Children<br>
</p>


<h2>Screenings</h2><table id="table--pink-stripes" summary="christchurch-2016">
<tbody>
<div>
<tr>
<td width="25"><p>Thu 27 Oct</p></td>
<td width="25"><p> 8:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/secrets-and-lies"><strong>Secrets and Lies</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 28 Oct</p></td>
<td width="25"><p> 6:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/homeland"><strong>Homeland</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 29 Oct</p></td>
<td width="25"><p> 1:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 30 Oct</p></td>
<td width="25"><p> 8:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/heartstrings"><strong>Heartstrings</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 31 Oct</p></td>
<td width="25"><p> 8:30pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/bump-in-the-night"><strong>Bump in the Night</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 1 Nov</p></td>
<td width="25"><p> 6:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/he-tangata"><strong>He Tangata</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 2 Nov</p></td>
<td width="25"><p> 6:00pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/highlights"><strong>Highlights</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>