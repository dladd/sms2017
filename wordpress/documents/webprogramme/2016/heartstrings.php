<p>Romantic love breeds rich and complex emotions, perfect subject matter for short film makers to mine. This compilation of tiny but powerful stories, touching on love in many forms, will tug your heartstrings. There’s young love, unrequited love, searching for love, heartbreak, love making, and the kind of love that lasts a lifetime. </p>

<p>Estimated total run time: 96 minutes. <br>
Censorship rating to be advised. Please contact your cinema for details.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCBD">Auckland CBD - Academy Cinemas</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#WellingtonCBD">Wellington CBD - Embassy Theatre</option>
</select>
<h2>Big Bird </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/bigbird.jpg" />
<p>Rom-com, 10 mins, Ireland (NZ premiere)</p>

<p>Dir: Jan Boon        </p>
<p>Writ: Derek O'Connor        </p>
<p>Prod: Declan Lynch</p>
<div class="cf"></div><p>Two underdogs are looking for love and they might just have found it. </p>

<hr class="sessionBreak">

<h2>Topsy and Dave </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/topsyanddave.jpg" />
<p>Documentary, 9 mins, NZ </p>

<p>Dir: Kirsty Griffin, Vivienne Kernick        </p>
<p>Prod: Bella Pacific Media</p>
<div class="cf"></div><p>Love, patience, understanding and support are the cornerstone of any successful marriage. That’s never more obvious than in this documentary where we meet Topsy and Dave, residents of 'The Supported Life Style Hauraki Trust' who are preparing for their big day. </p>

<hr class="sessionBreak">

<h2>Nkosi Coiffure </h2><a href="https://filmfr...sions/3443482" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/nkosicoiffure.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 14 mins, Belgium (NZ premiere)</p>

<p>Dir/Writ: Frederike Migom        </p>
<p>Prod: Jules Debrock</p>
<div class="cf"></div><p>During a fight with her boyfriend on the street in Brussels’ Congolese neighbourhood, Eva escapes into a hair salon. The African women in the salon initially support her, seeing a woman in distress. But when they find out what the fight is about, opinions differ. </p>

<hr class="sessionBreak">

<h2>Eight Years Later </h2><a href="https://vimeo.com/161092729" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/eightyearslater.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 15 mins, NZ (world premiere)</p>

<p>Dir/Writ/Prod: Andrew R. Blackman</p>
<div class="cf"></div><p>Eight years after Marc rejected the birth of his daughter resulting from a one-night stand, he still hasn't found what he's looking for in a relationship. A dramatisation of anecdotes by the real-life Marc. </p>

<hr class="sessionBreak">

<h2>Love on the Top </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/loveonthetop.jpg" />
<p>Drama, 18 mins, Slovenia (NZ premiere)</p>

<p>Dir/Writ: Jan Cvitkovič        </p>
<p>Prod: Miha Černec</p>
<div class="cf"></div><p>A vivacious 80-year-old couple have been together for what has virtually been their whole life. They live in a modest little house with an idyllic old-fashioned rural atmosphere. Or do they? </p>

<hr class="sessionBreak">

<h2>LarzRanda - Lifeguard</h2><a href="http://vimeo.com/136484845" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/larzranda-lifeguard.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Music video, 4 mins, NZ </p>

<p>Dir: THUNDERLIPS</p>
<p>Prod: Anna Duckworth, Tammy Brenstrum</p>
<div class="cf"></div><p>A lifeguard at a country club for teens executes a daring plan to win the heart of a patron he admires in this energetic and fun music video. </p>

<hr class="sessionBreak">

<h2>Victor XX </h2><a href="https://vimeo.com/126639798" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/victorxx.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 20 mins, Spain (NZ premiere)</p>

<p>Dir/Writ: Ian Garrido</p>
<div class="cf"></div><p>A young woman is experimenting with her gender by picking up a woman as a man, while her girlfriend waits at home. </p>

<hr class="sessionBreak">

<h2>Mixteip - The Greatest Tape Ever Told </h2><a href="https://www.youtube.com/watch?v=m2N6p8I1vBM" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2016/mixteip-thegreatesttapeevertold.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Music, 7 mins, Finland (NZ premiere)</p>

<p>Dir: Teemu Åke        </p>
<p>Writ: Susanna Åke        </p>
<p>Prod: Meri-Ellen Pystynen</p>
<div class="cf"></div><p>Nothing stimulates memories like music. This is a story of heartbreak and nostalgia told through the eyes of a cassette tape trying to reunite two ex-lovers. </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-cbd"><h3>Auckland CBD - Academy Cinemas</h3></a>
<table id="table--pink-stripes" summary="Heartstringsauckland-cbd-2016">
<tbody>
<tr>
<td width="50"><p>Tue 4 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Thu 6 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Sat 8 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Sun 9 Oct</p></td>
<td width="50"><p> 2:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Mon 10 Oct</p></td>
<td width="50"><p> 4:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="Heartstringschristchurch-2016">
<tbody>
<tr>
<td width="50"><p>Sun 30 Oct</p></td>
<td width="50"><p> 8:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="Heartstringsstewart-island-2016">
<tbody>
<tr>
<td width="50"><p>Wed 30 Nov</p></td>
<td width="50"><p> 7:30pm</p></td>
</tr>
</div>
</tbody>
</table><a name="WellingtonCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington-cbd"><h3>Wellington CBD - Embassy Theatre</h3></a>
<table id="table--pink-stripes" summary="Heartstringswellington-cbd-2016">
<tbody>
<tr>
<td width="50"><p>Sun 16 Oct</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
<tr>
<td width="50"><p>Wed 19 Oct</p></td>
<td width="50"><p> 6:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Sat 22 Oct</p></td>
<td width="50"><p> 8:30pm</p></td>
</tr>
<tr>
<td width="50"><p>Mon 24 Oct</p></td>
<td width="50"><p> 4:00pm</p></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>