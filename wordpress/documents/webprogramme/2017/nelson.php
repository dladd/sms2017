<h3 class="programme">18-19 Nov</h3>
<h2 class="programme">Suter Theatre</h2>
<p>
208 Bridge St<br>
Nelson<br>
Ph: 03 548 3885<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$16.50 General Admission<br>
$13.50 Students/Film Industry Guilds<br>
$11.50 Children/Seniors<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="nelson-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 18 Nov</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 19 Nov</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>