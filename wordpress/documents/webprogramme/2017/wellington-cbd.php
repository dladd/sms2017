<h3 class="programme">9-22 Nov</h3>
<h2 class="programme"><a href="http://www.embassytheatre.co.nz">Embassy Theatre</a></h2>
<p>
10 Kent Terrace<br>
Wellington<br>
Ph: 04 384 7657<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$25 Opening Night<br>
$20 Opening Night -- Seniors/Students/Film Industry Guilds<br>
$17.50 General Admission<br>
$14.50 Students/Film Society/Film Industry Guilds<br>
$12.50 Seniors/Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="wellington-cbd-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Thu 9 Nov</p></td>
<td width="25"><p>8:00 pm
<strong><a href="https://www.eventcinemas.co.nz/Sessions#cinemas=512&movies=10020&date=2017-11-09">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/events/wellington-opening-night"><strong>Wellington Opening Night**</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 10 Nov</p></td>
<td width="25"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Daring-To-Be-Different#cinemas=512&date=2017-11-10">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/daring-to-be-different"><strong>Daring to be Different</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 10 Nov</p></td>
<td width="25"><p>8:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-Quebec-Focus#cinemas=512&date=2017-11-10">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/québec-focus"><strong>Québec Focus</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 11 Nov</p></td>
<td width="25"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Sessions#cinemas=512&date=2017-11-11&movies=11792">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 11 Nov</p></td>
<td width="25"><p>8:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Light-In-The-Dark#cinemas=512&date=2017-11-11">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/light-in-the-dark"><strong>Light in the Dark</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 12 Nov</p></td>
<td width="25"><p>1:00 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/wellington-short-film-talk"><strong>Wellington Short Film Talk*</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 12 Nov</p></td>
<td width="25"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Worlds-Collide#cinemas=512&date=2017-11-12">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/worlds-collide"><strong>Worlds Collide</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 12 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-The-Sampler#cinemas=512&date=2017-11-12">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 13 Nov</p></td>
<td width="25"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Light-In-The-Dark#cinemas=512&date=2017-11-13">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/light-in-the-dark"><strong>Light in the Dark</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 13 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Made-To-Move#cinemas=512&date=2017-11-13">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/made-to-move"><strong>Made to Move</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 14 Nov</p></td>
<td width="25"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Daring-To-Be-Different#cinemas=512&date=2017-11-14">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/daring-to-be-different"><strong>Daring to be Different</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 14 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-Quebec-Focus#cinemas=512&date=2017-11-14">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/québec-focus"><strong>Québec Focus</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 15 Nov</p></td>
<td width="25"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Sessions#cinemas=512&date=2017-11-15&movies=11792">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 15 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Light-In-The-Dark#cinemas=512&date=2017-11-15">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/light-in-the-dark"><strong>Light in the Dark</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 16 Nov</p></td>
<td width="25"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Made-To-Move#cinemas=512&date=2017-11-16">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/made-to-move"><strong>Made to Move</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 16 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Worlds-Collide#cinemas=512&date=2017-11-16">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/worlds-collide"><strong>Worlds Collide</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 17 Nov</p></td>
<td width="25"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-The-Sampler#cinemas=512&date=2017-11-17">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 17 Nov</p></td>
<td width="25"><p>8:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Daring-To-Be-Different#cinemas=512&date=2017-11-17">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/daring-to-be-different"><strong>Daring to be Different</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 18 Nov</p></td>
<td width="25"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-Quebec-Focus#cinemas=512&date=2017-11-18">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/québec-focus"><strong>Québec Focus</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 18 Nov</p></td>
<td width="25"><p>8:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Made-To-Move#cinemas=512&date=2017-11-18">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/made-to-move"><strong>Made to Move</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 19 Nov</p></td>
<td width="25"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Sessions#cinemas=512&date=2017-11-19&movies=11792">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 19 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Worlds-Collide#cinemas=512&date=2017-11-19">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/worlds-collide"><strong>Worlds Collide</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 20 Nov</p></td>
<td width="25"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Daring-To-Be-Different#cinemas=512&date=2017-11-20">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/daring-to-be-different"><strong>Daring to be Different</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 20 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-The-Sampler#cinemas=512&date=2017-11-20">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 21 Nov</p></td>
<td width="25"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Made-To-Move#cinemas=512&date=2017-11-21">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/made-to-move"><strong>Made to Move</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 21 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Light-In-The-Dark#cinemas=512&date=2017-11-21">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/light-in-the-dark"><strong>Light in the Dark</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 22 Nov</p></td>
<td width="25"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-The-Sampler#cinemas=512&date=2017-11-22">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 22 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Sessions#cinemas=512&date=2017-11-22">Book tickets</a></strong></p></td>
<td width="25"><p><strong>Audience Picks***</strong></p></td>
</tr>
</div>
</tbody>
</table>
<p>*See page xx for details</p>

<p>**Watch all the award-winning films and enjoy a glass of wine</p>

<p>***As voted by Wellington audience members</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>