<h3 class="programme">12 Nov</h3>
<h2 class="programme"><a href="http://www.rialto.co.nz">Rialto Cinemas</a></h2>
<p>
11 Moray Place<br>
Dunedin<br>
Ph: 03 474 2200<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$16 General Admission<br>
$13 Students/Film Industry Guilds<br>
$10 Seniors<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="dunedin-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 12 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-The-Sampler#cinemas=750&date=2017-11-12">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>