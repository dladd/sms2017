<h3 class="programme">16 Nov</h3>
<h2 class="programme"><a href="http://www.whangareifilmsociety.org">Whangarei Film Society</a></h2>
<p>
Capitaine Bougainville Theatre, Forum North, 7 Rust Ave<br>
Whangarei<br>
Door sales only<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$10 General Admission<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="whangarei-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Thu 16 Nov</p></td>
<td width="25"><p>6:00 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>