<h3 class="programme">16-22 Nov</h3>
<h2 class="programme"><a href="http://https://alice.co.nz/">Alice Cinematheque</a></h2>
<p>
Old High Street Post Office, 209 Tuam Street<br>
Christchurch<br>
Ph: 03 365 0615<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
$11 Seniors/Children/Students/Film Industry Guilds<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="christchurch-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Thu 16 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-light-in-the-dark/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/light-in-the-dark"><strong>Light in the Dark</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 17 Nov</p></td>
<td width="25"><p>8:30 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-made-to-move/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/made-to-move"><strong>Made to Move</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 18 Nov</p></td>
<td width="25"><p>1:00 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-my-generation/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 18 Nov</p></td>
<td width="25"><p>8:30 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-daring-to-be-different/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/daring-to-be-different"><strong>Daring to be Different</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 19 Nov</p></td>
<td width="25"><p>1:00 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-the-sampler/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 19 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-quebec-focus/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/québec-focus"><strong>Québec Focus</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 20 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-worlds-collide/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/worlds-collide"><strong>Worlds Collide</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 21 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-light-in-the-dark/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/light-in-the-dark"><strong>Light in the Dark</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 22 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-the-sampler/">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>