<h3 class="programme">4 Nov</h3>
<h2 class="programme">Te Ahu</h2>
<p>
Corner South Road and Mathews Avenue<br>
Kaitaia<br>
Ph: 09 408 0519<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
$11 Students/Seniors<br>
$8 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="kaitaia-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 4 Nov</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>