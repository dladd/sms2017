<h3 class="programme">12 Nov</h3>
<h2 class="programme">Finns Paekakariki</h2>
<p>
2 Beach Road<br>
Paekakariki<br>
Ph: 04 292 8081<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$13.50 General Admission<br>
$10.50 Students/Film Industry Guilds<br>
$9 Seniors<br>
$8 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="paekakariki-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 12 Nov</p></td>
<td width="25"><p>6:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler*</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<p>*Festival Director will introduce this session</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>