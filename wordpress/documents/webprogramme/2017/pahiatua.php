<h3 class="programme">11 Nov</h3>
<h2 class="programme">Regent Upstairs</h2>
<p>
64 Main Street<br>
Pahiatua<br>
Ph: 06 376 8607, 06 376 8441 (after hours)<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$13 General Admission<br>
$10 Students/Seniors/Film Industry Guilds<br>
$8 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="pahiatua-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 11 Nov</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>