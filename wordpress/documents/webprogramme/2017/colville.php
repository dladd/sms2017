<h3 class="programme">25 Nov</h3>
<h2 class="programme">Colville Town Hall, Colville Rd</h2>
<p>
Colville<br>
Ph: 07 866 6920<br>
<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$12 General Admission<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="colville-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 25 Nov</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>