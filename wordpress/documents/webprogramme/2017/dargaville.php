<h3 class="programme">5 Nov</h3>
<h2 class="programme"><a href="http://www.anzactheatre.co.nz">Anzac Theatre</a></h2>
<p>
41 Hokianga Road<br>
Dargaville<br>
Ph: 09 439 8997<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$14 General Admission<br>
$12 Students/Seniors/Film Industry Guilds<br>
$8.50 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="dargaville-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 5 Nov</p></td>
<td width="25"><p>5:30 pm
<strong><a href="https://ticketing.oz.veezi.com/purchase/5096?siteToken=i%2BbFM5FWv0y%2BmfcFft867A%3D%3D">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>