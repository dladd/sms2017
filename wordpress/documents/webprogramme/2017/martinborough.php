<h3 class="programme">11 Nov</h3>
<h2 class="programme">Circus Cinema</h2>
<p>
34 Jellicoe Street<br>
Martinborough<br>
Ph: 06 306 9442<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$16 General Admission<br>
$14 Students/Seniors<br>
$11 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="martinborough-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 11 Nov</p></td>
<td width="25"><p>6:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>