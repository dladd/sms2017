<h3 class="programme">29 Oct - 5 Nov</h3>
<h2 class="programme"><a href="http://www.rialto.co.nz">Rialto Cinemas Newmarket</a></h2>
<p>
Rialto Centre, 167-169 Broadway<br>
Newmarket, Auckland<br>
Ph: 09 369 2417<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$17.50 General Admission<br>
$14.50 Students<br>
$12 Film Industry Members<br>
$11 Seniors/Children<br>
</p>
<p><strong>NOTE</strong>: For our opening night at The Civic on October 28, please see the <a href="http://www.showmeshorts.co.nz/events/auckland-opening-night/" title="Auckland Opening Night & Awards Ceremony">Auckland Opening Night & Awards Ceremony</a> page or <a href="http://www.ticketmaster.co.nz/event/240052C17C520A35" title="Book tickets to the Auckland Opening through Ticketmaster">click here to book tickets through Ticketmaster</a>.<p>

<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="auckland-central-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 29 Oct</p></td>
<td width="25"><p>1:00 pm
<strong><a href="https://www.rialto.co.nz/Ticketing/Order#token=6ea01e4bee5d02ca39d953bd5d0ae26a&step=tickets">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/events/masterclass"><strong>9 The Movie: Screening & Masterclass by Stephane E. Roy</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 29 Oct</p></td>
<td width="25"><p>3:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Smsff-My-Generation#cinemas=750&date=2017-10-29">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation*</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 29 Oct</p></td>
<td width="25"><p>5:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Worlds-Collide#cinemas=750&date=2017-10-29">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/worlds-collide"><strong>Worlds Collide**</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 30 Oct</p></td>
<td width="25"><p>2:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Smsff-My-Generation#cinemas=751&date=2017-10-30">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Mon 30 Oct</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Light-In-The-Dark#cinemas=751&date=2017-10-30">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/light-in-the-dark"><strong>Light in the Dark**</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 31 Oct</p></td>
<td width="25"><p>2:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Worlds-Collide#cinemas=751&date=2017-10-31">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/worlds-collide"><strong>Worlds Collide</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Tue 31 Oct</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Daring-To-Be-Different#cinemas=751&date=2017-10-31">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/daring-to-be-different"><strong>Daring to be Different**</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 1 Nov</p></td>
<td width="25"><p>2:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-The-Sampler#cinemas=751&date=2017-11-01">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 1 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Made-To-Move#cinemas=751&date=2017-11-01">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/made-to-move"><strong>Made to Move**</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 2 Nov</p></td>
<td width="25"><p>2:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Daring-To-Be-Different#cinemas=751&date=2017-11-02">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/daring-to-be-different"><strong>Daring to be Different</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 2 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-Quebec-Focus#cinemas=751&date=2017-11-02">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/québec-focus"><strong>Québec Focus</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Thu 2 Nov</p></td>
<td width="25"><p>8:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Made-To-Move#cinemas=751&date=2017-11-02">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/made-to-move"><strong>Made to Move</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 3 Nov</p></td>
<td width="25"><p>2:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Light-In-The-Dark#cinemas=751&date=2017-11-03">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/light-in-the-dark"><strong>Light in the Dark</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 3 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Worlds-Collide#cinemas=751&date=2017-11-03">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/worlds-collide"><strong>Worlds Collide</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 3 Nov</p></td>
<td width="25"><p>8:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Daring-To-Be-Different#cinemas=751&date=2017-11-03">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/daring-to-be-different"><strong>Daring to be Different</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 4 Nov</p></td>
<td width="25"><p>2:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Smsff-My-Generation#cinemas=751&date=2017-11-04">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 4 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Light-In-The-Dark#cinemas=751&date=2017-11-04">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/light-in-the-dark"><strong>Light in the Dark</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 4 Nov</p></td>
<td width="25"><p>8:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Made-To-Move#cinemas=751&date=2017-11-04">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/made-to-move"><strong>Made to Move</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 5 Nov</p></td>
<td width="25"><p>3:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-Quebec-Focus#cinemas=751&date=2017-11-05">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/québec-focus"><strong>Québec Focus</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 5 Nov</p></td>
<td width="25"><p>5:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-The-Sampler#cinemas=751&date=2017-11-05">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 5 Nov</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><strong>Audience Picks***</strong></p></td>
</tr>
</div>
</tbody>
</table>
<p>*NZ premiere screening with filmmaker intro</p>

<p>**World premiere screening with filmmaker intro</p>

<p>***As voted by Auckland audience members</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>