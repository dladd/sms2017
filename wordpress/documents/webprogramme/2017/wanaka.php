<h3 class="programme">19 Nov</h3>
<h2 class="programme"><a href="http://www.rubyscinema.co.nz">Ruby's Cinemas</a></h2>
<p>
50 Cardrona Valley Rd<br>
Wanaka<br>
Ph: 03 443 6901<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$18.50 General Admission<br>
$14.50 Students/Seniors/Film Industry Members<br>
$12.50 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="wanaka-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 19 Nov</p></td>
<td width="25"><p>5:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>