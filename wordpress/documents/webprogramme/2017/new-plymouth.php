<h3 class="programme">10 Nov</h3>
<h2 class="programme"><a href="http://www.4thwalltheatre.co.nz">4th Wall Theatre</a></h2>
<p>
11 Baring Terrace<br>
New Plymouth<br>
Ph: 0800 484 925<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
$12 Seniors/Students/Children/Film Industry Guilds<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="new-plymouth-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Fri 10 Nov</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>