[caption align="alignright" width="150"]<a href="http://www.showmeshorts.co.nz/images/programmes/sms2017_programme.pdf"><img src="http://localhost/sms2017/images/webprogramme/events/2017/programmeCover2017.jpg" alt="Click to download the print programme" width="150" class="size-full wp-image-3116" /></a> Click to download the print programme[/caption]<p>Show Me Shorts Film Festival is proud to present our 11th annual programme. Browse by our 20 fantastic locations throughout New Zealand (+ Antartica!) and our seven themed sessions below.</p>

<p>Also be sure to check out our events- we kick off the festival in style with the <a href="http://www.showmeshorts.co.nz/events/auckland-opening-night/" title="Auckland Opening Night">Auckland Opening Night & Awards Ceremony</a> at The Civic in Auckland and the <a href="http://www.showmeshorts.co.nz/events/wellington-opening-night/" title="Wellington Opening Night">Wellington Opening Night</a> at The Embassy in Wellington. The <a href="http://www.showmeshorts.co.nz/events/short-film-talks/" title="Short Film Talks">Short Film Talks</a> in Auckland and Wellington are fantastic ways to learn more about shorts from some of NZ's top short film makers.</p>

<p>You can also find more about this year's festival from the <a href="http://www.showmeshorts.co.nz/directors-welcome-2017/">Director's Welcome</a> or download a <a href="http://www.showmeshorts.co.nz/images/programmes/sms2017_programme.pdf" title="PDF of the 2017 Show Me Shorts Film Festival">PDF of our print programme</a>.</p>
<br>

<h2>Trailer</h2>
<iframe width="853" height="480" src="https://www.youtube.com/embed/dgpMyj6D0Gk" frameborder="0" allowfullscreen></iframe>


[raw]

<a name="locations"></a>

<h2>Locations</h2>

<div id="navMapContainer">
<img id="smsMapImage" src="http://localhost/sms2017/images/webprogramme/smsMap2017.png" />

</div>
<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value=""><em>(Or select a location and cinema...)</em></option>
</select>
<br style="clear:both;" />


<h2>Events</h2>

<a href="http://localhost/sms2017/events/auckland-opening-night"><div id="imagelink" style="background-image: url(http://localhost/sms2017/images/webprogramme/events/2017/aucklandopening.jpg);"><overlayHead><span>Auckland Opening Night<span class='spacer'></span><br /><span class='spacer'></span>& Awards Ceremony</span></overlayHead></div></a><a href="http://localhost/sms2017/events/wellington-opening-night"><div id="imagelink" style="background-image: url(http://localhost/sms2017/images/webprogramme/events/2017/wellingtonopening.jpg);"><overlayHead><span>Wellington<span class='spacer'></span><br /><span class='spacer'></span>Opening Night</span></overlayHead></div></a><a href="http://localhost/sms2017/events/short-film-talks"><div id="imagelink" style="background-image: url(http://localhost/sms2017/images/webprogramme/events/2017/shortfilmtalks.jpg);"><overlayHead><span>Short Film Talks</span></overlayHead></div></a><br style="clear:both;" />
<a name="sessions"></a><h2>Sessions</h2>

<a href="http://localhost/sms2017/programme/what-binds-us"><div id="imagelink" style="background-image: url(http://localhost/sms2017/images/webprogramme/sessions/2017/whatbindsus.jpg);"><overlayHead><span>What Binds Us</span></overlayHead></div></a><hr class="sessionBreak"><br style="clear:both;" />

[/raw]