<p>And... action! The camera rolls for this fast-paced collection of short films. The stories centre around movement of people, a piano, and a stolen purse. High-energy action sequences are contrasted with the concentrated expression of sporting prowess. These films will keep you on your toes.</p>

<p>Estimated total run time: 85 minutes. <br>
Rated R16 for violence & offensive language.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Rialto Cinemas Newmarket</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#WellingtonCBD">Wellington CBD - Embassy Theatre</option>
</select>
<h2>铁手 (Iron Hands)</h2><a href="https://vimeo.com/194457798" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/ironhands.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 11 mins, USA/China 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Johnson Cheng</p>
<p>Prod: Tianqi Zhuo, Felicity Wang, Xixi Wang</p>
<div class="cf"></div><p>As a 12-year-old girl prepares to try out for the traditionally all-boys Chinese Youth Olympic weightlifting team, she makes an unlikely connection with the gym's reclusive groundskeeper.        </p>

<hr class="sessionBreak">

<h2>Zinzan</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/zinzan.jpg" />
<p>Drama, 11 mins, New Zealand 
<p><strong>World Premiere</strong></p></p>

<p>Dir/Writ: Ben Walton</p>
<p>Prod: Arielle Sullivan</p>
<div class="cf"></div><p>A rugby-mad father fears his son might not be recruited into the national Under 20s team when he insists on bringing his boyfriend to practice.         </p>

<hr class="sessionBreak">

<h2>Do No Harm        </h2><a href="https://vimeo.com/204983972" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/donoharm.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Action,        12 mins, New Zealand</p>

<p>Dir/Writ: Roseanne Liang</p>
<p>Prod: Hamish Mortland, Tim White</p>
<div class="cf"></div><p>3am. 1980s Hongjing. In an ageing private hospital, a single-minded surgeon is forced to break her physician's oath when violent gangsters storm in to stop a crucial operation.        </p>

<hr class="sessionBreak">

<h2>Girl at the Door</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/girlatthedoor.jpg" />
<p>Drama, 12 mins, South Korea 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ/Prod: Joo-sung Song</p>
<div class="cf"></div><p>Hye-ri is learning kick-boxing. She practises the armbar manoeuvre to fend off her drunk father's abusive behaviour.</p>

<hr class="sessionBreak">

<h2>Ostoja Will Move Your Piano</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/ostojawillmoveyourpiano.jpg" />
<p>Drama, 15 mins, Serbia/Montenegro 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir: Sandra Mitrovic</p>
<p>Writ: Sandra Mitrovic, Masa Clark</p>
<p>Prod: Sandra Mitrovic, Vesna Boskovic</p>
<div class="cf"></div><p>Ostoja is a legendary piano mover from Belgrade. Today his work becomes a series of increasingly complex and comical moves.</p>

<hr class="sessionBreak">

<h2>Through the Supermarket in Five Easy Pieces        </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/throughthesupermarketinfiveeasypieces.jpg" />
<p>Dance, 9 mins, Finland 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Anna Maria Joakimsdottir-Hutri</p>
<p>Prod: Petteri Lehtinen</p>
<div class="cf"></div><p>A delightful dance film about a family trying to navigate their weekly grocery shopping trip without disintegrating or losing anyone.</p>

<hr class="sessionBreak">

<h2>Renters -- Lake South</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/renters-lakesouth.jpg" />
<p>Music video, 4 mins, New Zealand</p>

<p>Dir: Linsell Richards</p>
<p>Dir/Writ: Lake McKenna</p>
<p>Prod: Poor Sailors Arts Collective</p>
<div class="cf"></div><p>The Lake South song 'Renters' is an introspective tune about the New Zealand housing crisis. This music video features the Straight Outta Roskill Crew rolling on pimped-out BMX bikes.</p>

<hr class="sessionBreak">

<h2>Just Go!</h2><a href="https://www.youtube.com/watch?v=LKM0tFHla18" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/justgo!.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Action,        11 mins, Latvia 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ/Prod: Pavel Gumennikov</p>
<p>Writ: Laura Lapina</p>
<div class="cf"></div><p>Just is a young man who lost his legs in an accident, but this doesn't stop him from giving chase when his crush Ieva has her bag stolen. A fast-paced action film inspired by a true story.        </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Rialto Cinemas Newmarket</h3></a>
<table id="table--pink-stripes" summary="Made to Moveauckland-central-2017">
<tbody>
<tr>
<td width="50"><p>Wed 1 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Made-To-Move#cinemas=751&date=2017-11-01">Book tickets</a></strong></p></td>
<td width="50"><p>*World premiere screening with filmmaker intro</p></td>
</tr>
<tr>
<td width="50"><p>Thu 2 Nov</p></td>
<td width="50"><p>8:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Made-To-Move#cinemas=751&date=2017-11-02">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sat 4 Nov</p></td>
<td width="50"><p>8:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Made-To-Move#cinemas=751&date=2017-11-04">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="Made to Movechristchurch-2017">
<tbody>
<tr>
<td width="50"><p>Fri 17 Nov</p></td>
<td width="50"><p>8:30 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-made-to-move/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="Made to Movestewart-island-2017">
<tbody>
<tr>
<td width="50"><p>Sat 13 Jan</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="WellingtonCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington-cbd"><h3>Wellington CBD - Embassy Theatre</h3></a>
<table id="table--pink-stripes" summary="Made to Movewellington-cbd-2017">
<tbody>
<tr>
<td width="50"><p>Mon 13 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Made-To-Move#cinemas=512&date=2017-11-13">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Thu 16 Nov</p></td>
<td width="50"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Made-To-Move#cinemas=512&date=2017-11-16">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sat 18 Nov</p></td>
<td width="50"><p>8:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Made-To-Move#cinemas=512&date=2017-11-18">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Tue 21 Nov</p></td>
<td width="50"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Made-To-Move#cinemas=512&date=2017-11-21">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>