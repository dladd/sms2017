<h3 class="programme">3 Nov</h3>
<h2 class="programme"><a href="http://www.thevic.co.nz">The Vic</a></h2>
<p>
56 Victoria Road<br>
Devonport, Auckland<br>
Ph: 09 446 0100<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15.50 General Admission<br>
$12 Students/Film Industry Guilds<br>
$11 Seniors<br>
$10 Children<br>
(Prices include $1 voluntary donation to The Vic Trust)<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="devonport-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Wed 1 Nov</p></td>
<td width="25"><p>7:30 pm
<strong><a href="https://www.thevic.co.nz/movie/show-me-shorts-qubec-focus">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/québec-focus"><strong>Québec Focus**</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Fri 3 Nov</p></td>
<td width="25"><p>8:00 pm
<strong><a href="http://www.thevic.co.nz/movie/show-me-shorts-the-sampler">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler*</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<p>*Festival Director will introduce this session</p>

<p>**Filmmaker Q&A to follow</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>