<p>Too hard to pick which films to see? We get it. This selection includes some of our most accessible, heart-warming, vibrant and fun shorts from New Zealand and around the world. It's a great way to sample what Show Me Shorts is all about. The films on offer feature family ties, a misread psychic prediction, a kosher butcher in disguise, a couple of Game of Thrones stars, and the unexpected thrill of eating avocado on toast.</p>

<p>Estimated total run time: 91 minutes. <br>
Rated M for sex scenes & offensive language.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#Antarctica">Antarctica - Scott Base</option>
<option value="#AucklandCentral">Auckland Central - Rialto Cinemas Newmarket</option>
<option value="#Blenheim">Blenheim - Top Town Cinema</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#Colville">Colville - Colville Town Hall, Colville Rd</option>
<option value="#Dargaville">Dargaville - Anzac Theatre</option>
<option value="#Devonport">Devonport - The Vic</option>
<option value="#Dunedin">Dunedin - Rialto Cinemas</option>
<option value="#Gisborne">Gisborne - Dome Cinema, Poverty Bay Club building</option>
<option value="#GreatBarrierIsland">Great Barrier Island - Barrier Social Club</option>
<option value="#Kaitaia">Kaitaia - Te Ahu</option>
<option value="#Martinborough">Martinborough - Circus Cinema</option>
<option value="#Matakana">Matakana - Matakana Cinemas</option>
<option value="#Nelson">Nelson - Suter Theatre</option>
<option value="#NewPlymouth">New Plymouth - 4th Wall Theatre</option>
<option value="#Paekakariki">Paekakariki - Finns Paekakariki</option>
<option value="#Pahiatua">Pahiatua - Regent Upstairs</option>
<option value="#Pukekohe">Pukekohe - Cinema 3 Pukekohe</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#Tauranga">Tauranga - Rialto Cinemas Tauranga</option>
<option value="#Thames">Thames - Embassy Cinemas</option>
<option value="#WaihekeIsland">Waiheke Island - Waiheke Island Community Cinema</option>
<option value="#Wanaka">Wanaka - Ruby's Cinemas</option>
<option value="#WellingtonCBD">Wellington CBD - Embassy Theatre</option>
<option value="#Whangarei">Whangarei - Whangarei Film Society</option>
</select>
<h2>Sybil's Psychic Hotline        </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/sybilspsychichotline.jpg" />
<p>Comedy, 8 mins, New Zealand 
<p><strong>World Premiere</strong></p></p>

<p>Dir/Writ/Prod: Grant Lahood</p>
<div class="cf"></div><p>In this dark comedy, Con decides the perfect way to cheer up her depressed and bedridden partner Bella is to call a clairvoyant phone line.        </p>

<hr class="sessionBreak">

<h2>The Chop        </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/thechop.jpg" />
<p>Comedy, 17 mins, UK 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Lewis Rose</p>
<p>Prod: Daphnée Hocquard</p>
<div class="cf"></div><p>A comedy about Yossi, a kosher butcher who loses his job, cannot find work at other kosher butcheries, and decides to pretend to be Muslim in order to get work at a halal butchery.        </p>

<hr class="sessionBreak">

<h2>Adnyat        </h2><a href="https://m.youtube.com/watch?v=232pKY4ZhfM" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/adnyat.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 15 mins, India 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Santosh Sopan Davakhar</p>
<p>Prod: Diksha Santosh Davakhar, Rajesh Sopan Davakhar</p>
<div class="cf"></div><p>Adnyat is an eight-year-old boy who sets out on a journey that explores and borrows from a variety of religious customs, worshipping rituals, and traditions. </p>

<hr class="sessionBreak">

<h2>The Wall</h2><a href="https://vimeo.com/207535195" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/thewall.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 7 mins, Australia 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ/Prod: Nick Baker</p>
<p>Dir/Prod: Tristan Klein</p>
<div class="cf"></div><p>This poetic animated film tells a tale of love and hope. A grandmother and her grandson flee their city, only to find a tall, endless wall blocking their path.        </p>

<hr class="sessionBreak">

<h2>The World In Your Window        </h2><a href="https://vimeo.com/205269969" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/theworldinyourwindow.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Coming of age, 15 mins, New Zealand</p>

<p>Dir/Writ: Zoe McIntosh</p>
<p>Writ: Costa Botes</p>
<p>Prod: Hamish Mortland</p>
<div class="cf"></div><p>Eight-year-old Jesse is stuck in a tiny caravan with his grief-stricken father, until his accidental friendship with a V8-driving transsexual unlocks the means for their liberation.        </p>

<hr class="sessionBreak">

<h2>Edith</h2><a href="https://youtu.be/yZG3XylR7wM" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/edith.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 15 mins, UK 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir: Christian Cooke        </p>
<p>Writ: Ray Robinson</p>
<p>Prod: April Kelley, Sara Huxley, Fiona Neilson</p>
<div class="cf"></div><p>Since his wife's death, Jake has fallen into a life of isolation. He meets Shelia, who helps him see the light again. A hopeful drama starring Peter Mullan, Michelle Fairley and Robyn Malcolm.        </p>

<hr class="sessionBreak">

<h2>Roger</h2><a href="https://vimeo.com/176919951" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/roger.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Comedy, 8 mins, UK 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Brendan Cleaves</p>
<p>Writ: Stuart Foreman</p>
<p>Prod: Alasdair Mitchell</p>
<div class="cf"></div><p>Stephen (Seann Walsh) arrives home after six months in Mongolia to discover his best friend Roy (John Bradley) has replaced him with a ventriloquist's dummy called Roger.        </p>

<hr class="sessionBreak">

<h2>Laundry</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/laundry.jpg" />
<p>Comedy, 10 mins, New Zealand</p>

<p>Dir/Writ: Becs Arahanga                 </p>
<p>Prod: Julian Arahanga, Kath Akuhata Brown</p>
<div class="cf"></div><p>A happily married woman (Aidee Walker), with a busy family life, struggles to find an intimate moment with her partner. She discovers orgasmic pleasure in an unlikely place.</p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Rialto Cinemas Newmarket</h3></a>
<table id="table--pink-stripes" summary="The Samplerauckland-central-2017">
<tbody>
<tr>
<td width="50"><p>Wed 1 Nov</p></td>
<td width="50"><p>2:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-The-Sampler#cinemas=751&date=2017-11-01">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sun 5 Nov</p></td>
<td width="50"><p>5:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-The-Sampler#cinemas=751&date=2017-11-05">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Devonport"></a>

<a href="http://www.showmeshorts.co.nz/programme/devonport"><h3>Devonport - The Vic</h3></a>
<table id="table--pink-stripes" summary="The Samplerdevonport-2017">
<tbody>
<tr>
<td width="50"><p>Fri 3 Nov</p></td>
<td width="50"><p>8:00 pm
<strong><a href="https://www.thevic.co.nz/movie/show-me-shorts-the-sampler">Book tickets</a></strong></p></td>
<td width="50"><p>*Festival Director will introduce this session</p></td>
</tr>
</div>
</tbody>
</table><a name="GreatBarrierIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/great-barrier-island"><h3>Great Barrier Island - Barrier Social Club</h3></a>
<table id="table--pink-stripes" summary="The Samplergreat-barrier-island-2017">
<tbody>
<tr>
<td width="50"><p>Mon 13 Nov</p></td>
<td width="50"><p>7:15 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Matakana"></a>

<a href="http://www.showmeshorts.co.nz/programme/matakana"><h3>Matakana - Matakana Cinemas</h3></a>
<table id="table--pink-stripes" summary="The Samplermatakana-2017">
<tbody>
<tr>
<td width="50"><p>Sat 18 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://ticketing.oz.veezi.com/purchase/13489?siteToken=NeQbpcRv90OQr%2fEjNRpl1g%3d%3d">Book tickets</a></strong></p></td>
<td width="50"><p>* Festival Director will introduce this session</p></td>
</tr>
</div>
</tbody>
</table><a name="Pukekohe"></a>

<a href="http://www.showmeshorts.co.nz/programme/pukekohe"><h3>Pukekohe - Cinema 3 Pukekohe</h3></a>
<table id="table--pink-stripes" summary="The Samplerpukekohe-2017">
<tbody>
<tr>
<td width="50"><p>Sat 4 Nov</p></td>
<td width="50"><p>6:30 pm</p></td>
<td width="50"><p>* Festival Director will introduce this session</p></td>
</tr>
</div>
</tbody>
</table><a name="WaihekeIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/waiheke-island"><h3>Waiheke Island - Waiheke Island Community Cinema</h3></a>
<table id="table--pink-stripes" summary="The Samplerwaiheke-island-2017">
<tbody>
<tr>
<td width="50"><p>Sun 29 Oct</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"><p>* World premiere screening with filmmaker intro</p></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="The Samplerchristchurch-2017">
<tbody>
<tr>
<td width="50"><p>Sun 19 Nov</p></td>
<td width="50"><p>1:00 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-the-sampler/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Wed 22 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-the-sampler/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="The Samplerstewart-island-2017">
<tbody>
<tr>
<td width="50"><p>Wed 24 Jan</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="WellingtonCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington-cbd"><h3>Wellington CBD - Embassy Theatre</h3></a>
<table id="table--pink-stripes" summary="The Samplerwellington-cbd-2017">
<tbody>
<tr>
<td width="50"><p>Sun 12 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-The-Sampler#cinemas=512&date=2017-11-12">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Fri 17 Nov</p></td>
<td width="50"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-The-Sampler#cinemas=512&date=2017-11-17">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Mon 20 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-The-Sampler#cinemas=512&date=2017-11-20">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Wed 22 Nov</p></td>
<td width="50"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-The-Sampler#cinemas=512&date=2017-11-22">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Paekakariki"></a>

<a href="http://www.showmeshorts.co.nz/programme/paekakariki"><h3>Paekakariki - Finns Paekakariki</h3></a>
<table id="table--pink-stripes" summary="The Samplerpaekakariki-2017">
<tbody>
<tr>
<td width="50"><p>Sun 12 Nov</p></td>
<td width="50"><p>6:30 pm</p></td>
<td width="50"><p>*Festival Director will introduce this session</p></td>
</tr>
</div>
</tbody>
</table><a name="Antarctica"></a>

<a href="http://www.showmeshorts.co.nz/programme/antarctica"><h3>Antarctica - Scott Base</h3></a>
<table id="table--pink-stripes" summary="The Samplerantarctica-2017">
<tbody>
<tr>
<td width="50"><p>Sun 19 Nov</p></td>
<td width="50"><p></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Devonport"></a>

<a href="http://www.showmeshorts.co.nz/programme/devonport"><h3>Devonport - The Vic</h3></a>
<table id="table--pink-stripes" summary="The Samplerdevonport-2017">
<tbody>
<tr>
<td width="50"><p>Fri 3 Nov</p></td>
<td width="50"><p>8:00 pm
<strong><a href="http://www.thevic.co.nz/movie/show-me-shorts-the-sampler">Book tickets</a></strong></p></td>
<td width="50"><p>*Festival Director will introduce this session</p></td>
</tr>
</div>
</tbody>
</table><a name="GreatBarrierIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/great-barrier-island"><h3>Great Barrier Island - Barrier Social Club</h3></a>
<table id="table--pink-stripes" summary="The Samplergreat-barrier-island-2017">
<tbody>
<tr>
<td width="50"><p>Mon 13 Nov</p></td>
<td width="50"><p>7:15 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Matakana"></a>

<a href="http://www.showmeshorts.co.nz/programme/matakana"><h3>Matakana - Matakana Cinemas</h3></a>
<table id="table--pink-stripes" summary="The Samplermatakana-2017">
<tbody>
<tr>
<td width="50"><p>Sat 18 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://ticketing.oz.veezi.com/purchase/13489?siteToken=NeQbpcRv90OQr%2fEjNRpl1g%3d%3d">Book tickets</a></strong></p></td>
<td width="50"><p>*Festival Director will introduce this session</p></td>
</tr>
</div>
</tbody>
</table><a name="Pukekohe"></a>

<a href="http://www.showmeshorts.co.nz/programme/pukekohe"><h3>Pukekohe - Cinema 3 Pukekohe</h3></a>
<table id="table--pink-stripes" summary="The Samplerpukekohe-2017">
<tbody>
<tr>
<td width="50"><p>Sat 4 Nov</p></td>
<td width="50"><p>6:30 pm</p></td>
<td width="50"><p>*Festival Director will introduce this session</p></td>
</tr>
</div>
</tbody>
</table><a name="WaihekeIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/waiheke-island"><h3>Waiheke Island - Waiheke Island Community Cinema</h3></a>
<table id="table--pink-stripes" summary="The Samplerwaiheke-island-2017">
<tbody>
<tr>
<td width="50"><p>Sun 29 Oct</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"><p>*World premiere screening with filmmaker intro</p></td>
</tr>
</div>
</tbody>
</table><a name="Blenheim"></a>

<a href="http://www.showmeshorts.co.nz/programme/blenheim"><h3>Blenheim - Top Town Cinema</h3></a>
<table id="table--pink-stripes" summary="The Samplerblenheim-2017">
<tbody>
<tr>
<td width="50"><p>Sun 19 Nov</p></td>
<td width="50"><p>6:30 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Colville"></a>

<a href="http://www.showmeshorts.co.nz/programme/colville"><h3>Colville - Colville Town Hall, Colville Rd</h3></a>
<table id="table--pink-stripes" summary="The Samplercolville-2017">
<tbody>
<tr>
<td width="50"><p>Sat 25 Nov</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Dargaville"></a>

<a href="http://www.showmeshorts.co.nz/programme/dargaville"><h3>Dargaville - Anzac Theatre</h3></a>
<table id="table--pink-stripes" summary="The Samplerdargaville-2017">
<tbody>
<tr>
<td width="50"><p>Sun 5 Nov</p></td>
<td width="50"><p>5:30 pm
<strong><a href="https://ticketing.oz.veezi.com/purchase/5096?siteToken=i%2BbFM5FWv0y%2BmfcFft867A%3D%3D">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Dunedin"></a>

<a href="http://www.showmeshorts.co.nz/programme/dunedin"><h3>Dunedin - Rialto Cinemas</h3></a>
<table id="table--pink-stripes" summary="The Samplerdunedin-2017">
<tbody>
<tr>
<td width="50"><p>Sun 12 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-The-Sampler#cinemas=750&date=2017-11-12">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Gisborne"></a>

<a href="http://www.showmeshorts.co.nz/programme/gisborne"><h3>Gisborne - Dome Cinema, Poverty Bay Club building</h3></a>
<table id="table--pink-stripes" summary="The Samplergisborne-2017">
<tbody>
<tr>
<td width="50"><p>Sat 11 Nov</p></td>
<td width="50"><p>7:00 pm</p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sun 12 Nov</p></td>
<td width="50"><p>7:00 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Kaitaia"></a>

<a href="http://www.showmeshorts.co.nz/programme/kaitaia"><h3>Kaitaia - Te Ahu</h3></a>
<table id="table--pink-stripes" summary="The Samplerkaitaia-2017">
<tbody>
<tr>
<td width="50"><p>Sat 4 Nov</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Martinborough"></a>

<a href="http://www.showmeshorts.co.nz/programme/martinborough"><h3>Martinborough - Circus Cinema</h3></a>
<table id="table--pink-stripes" summary="The Samplermartinborough-2017">
<tbody>
<tr>
<td width="50"><p>Sat 11 Nov</p></td>
<td width="50"><p>6:30 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Nelson"></a>

<a href="http://www.showmeshorts.co.nz/programme/nelson"><h3>Nelson - Suter Theatre</h3></a>
<table id="table--pink-stripes" summary="The Samplernelson-2017">
<tbody>
<tr>
<td width="50"><p>Sat 18 Nov</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sun 19 Nov</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="NewPlymouth"></a>

<a href="http://www.showmeshorts.co.nz/programme/new-plymouth"><h3>New Plymouth - 4th Wall Theatre</h3></a>
<table id="table--pink-stripes" summary="The Samplernew-plymouth-2017">
<tbody>
<tr>
<td width="50"><p>Fri 10 Nov</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Pahiatua"></a>

<a href="http://www.showmeshorts.co.nz/programme/pahiatua"><h3>Pahiatua - Regent Upstairs</h3></a>
<table id="table--pink-stripes" summary="The Samplerpahiatua-2017">
<tbody>
<tr>
<td width="50"><p>Sat 11 Nov</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Tauranga"></a>

<a href="http://www.showmeshorts.co.nz/programme/tauranga"><h3>Tauranga - Rialto Cinemas Tauranga</h3></a>
<table id="table--pink-stripes" summary="The Samplertauranga-2017">
<tbody>
<tr>
<td width="50"><p>Sun 19 Nov</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"><p>*Festival Director will introduce this session</p></td>
</tr>
</div>
</tbody>
</table><a name="Thames"></a>

<a href="http://www.showmeshorts.co.nz/programme/thames"><h3>Thames - Embassy Cinemas</h3></a>
<table id="table--pink-stripes" summary="The Samplerthames-2017">
<tbody>
<tr>
<td width="50"><p>Tue 28 Nov</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Wanaka"></a>

<a href="http://www.showmeshorts.co.nz/programme/wanaka"><h3>Wanaka - Ruby's Cinemas</h3></a>
<table id="table--pink-stripes" summary="The Samplerwanaka-2017">
<tbody>
<tr>
<td width="50"><p>Sun 19 Nov</p></td>
<td width="50"><p>5:30 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Paekakariki"></a>

<a href="http://www.showmeshorts.co.nz/programme/paekakariki"><h3>Paekakariki - Finns Paekakariki</h3></a>
<table id="table--pink-stripes" summary="The Samplerpaekakariki-2017">
<tbody>
<tr>
<td width="50"><p>Sun 12 Nov</p></td>
<td width="50"><p>6:30 pm</p></td>
<td width="50"><p>*Festival Director will introduce this session</p></td>
</tr>
</div>
</tbody>
</table><a name="Whangarei"></a>

<a href="http://www.showmeshorts.co.nz/programme/whangarei"><h3>Whangarei - Whangarei Film Society</h3></a>
<table id="table--pink-stripes" summary="The Samplerwhangarei-2017">
<tbody>
<tr>
<td width="50"><p>Thu 16 Nov</p></td>
<td width="50"><p>6:00 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>