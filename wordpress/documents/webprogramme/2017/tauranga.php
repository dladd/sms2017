<h3 class="programme">19 Nov</h3>
<h2 class="programme"><a href="http://www.rialtotauranga.co.nz">Rialto Cinemas Tauranga</a></h2>
<p>
21 Devonport Rd<br>
Tauranga<br>
Ph: 07 577 0445<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$17.00 General Admission<br>
$15 Students/Film Industry Guilds<br>
$10.50 Seniors/Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="tauranga-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 19 Nov</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler*</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<p>*Festival Director will introduce this session</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>