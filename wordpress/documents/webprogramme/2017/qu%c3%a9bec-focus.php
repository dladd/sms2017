<p>Discover what we have in common with people from Canada's French-speaking province of Québec. The films in this collection explore the lives of Québécois living in modern Montreal, taking up native dance, experiencing an endless suburban summer, languishing in a sawmill town with a vast snowy backdrop, and caught in imaginary dream-worlds.</p>

<p>Estimated total run time: 100 minutes. <br>
Rated R16 for sex scenes, offensive language, sexual abuse themes & suicide. </p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Rialto Cinemas Newmarket</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#Devonport">Devonport - The Vic</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#WellingtonCBD">Wellington CBD - Embassy Theatre</option>
</select>
<h2>La Peau Sauvage (Wild Skin)</h2><a href="https://vimeo.com/176497627 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/lapeausauvage(wildskin).jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Fantasy, 19 mins, Canada 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Ariane Louis-Seize</p>
<p>Prod: Jeanne-Marie Poulain, Hany Ouichou</p>
<div class="cf"></div><p>The quiet life of a solitary woman is disturbed when she discovers a baby python in her apartment. This unleashes her deepest urges and will let her express, for the first time, who she truly is.        </p>

<hr class="sessionBreak">

<h2>No Wave</h2><a href="https://vimeo.com/195529128 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/nowave.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Thriller, 12 mins, Canada 
<p><strong>NZ Premiere</strong></p></p>

<div class="cf"></div><p>Henry listens to the sounds of ocean waves on a relaxation radio station to soothe his anxiety and help him sleep. But did he just hear the screams of a drowning man in his calming sea?        Dir/Writ/Prod: Stephane Lapointe</p>

<hr class="sessionBreak">

<h2>Mutants</h2><a href="https://vimeo.com/167773142" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/mutants.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Coming of age, 17 mins, Canada 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Alexandre Dostie</p>
<p>Prod: Hany Ouichou, Gabrielle Tougas-Fréchette</p>
<div class="cf"></div><p>In the summer of 1996, life throws a curveball in the face of Keven Guénette... and it strikes. Guided by his paraplegic baseball coach, Keven discovers the mutation: sex and love.        </p>

<hr class="sessionBreak">

<h2>Bleu Tonnerre (Blue Thunder)</h2><a href="https://vimeo.com/121409931 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/bleutonnerre(bluethunder).jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Musical, 21 mins, Canada 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Philippe David Gagné, Jean Marc E. Roy</p>
<p>Prod: Gabrielle Tougas-Fréchette, Ménaic Raoul</p>
<div class="cf"></div><p>Bruno is a sawmill worker in his thirties who breaks up with his girlfriend. He breaks into song while trying to sort out his life, and rekindle his previous glory as a wrestler named Blue Thunder.</p>

<hr class="sessionBreak">

<h2>Mon Dernier Été (My Last Summer)</h2><a href="https://vimeo.com/158956619 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/mondernierete(mylastsummer).jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Coming of age, 15 mins, Canada 
<p><strong>NZ Premiere</strong></p></p>

<p>Prod: Étienne Hansez</p>
<div class="cf"></div><p>During a summer heat wave in Montreal, two 11-year-olds, Tom and Édith, meet and become close friends. When Tom discovers Édith's terrible secret, his innocence is shattered.        Dir/Writ: Paul-Claude Demers</p>

<hr class="sessionBreak">

<h2>Sonny Side Up        </h2><a href="https://vimeo.com/170622083 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/sonnysideup.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Documentary,        7 mins, Canada 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ/Prod: Sonny Papatie</p>
<div class="cf"></div><p>An inspiring documentary about how a young man changes his life when he gives up drugs and alcohol to become a traditional dancer.        </p>

<hr class="sessionBreak">

<h2>Vaysha, L'Aveugle (Blind Vaysha)</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/vayshalaveugle(blindvaysha).jpg" />
<p>Animation, 9 mins, Canada 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Théodore Ushev</p>
<p>Prod: Marc Bertrand</p>
<div class="cf"></div><p>This award-winning animation tells the story of little Vaysha, who has a left eye that sees the past and a right eye that sees the future, preventing her from living in the present.        </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Rialto Cinemas Newmarket</h3></a>
<table id="table--pink-stripes" summary="Québec Focusauckland-central-2017">
<tbody>
<tr>
<td width="50"><p>Thu 2 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-Quebec-Focus#cinemas=751&date=2017-11-02">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sun 5 Nov</p></td>
<td width="50"><p>3:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-Quebec-Focus#cinemas=751&date=2017-11-05">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Devonport"></a>

<a href="http://www.showmeshorts.co.nz/programme/devonport"><h3>Devonport - The Vic</h3></a>
<table id="table--pink-stripes" summary="Québec Focusdevonport-2017">
<tbody>
<tr>
<td width="50"><p>Wed 1 Nov</p></td>
<td width="50"><p>7:30 pm
<strong><a href="https://www.thevic.co.nz/movie/show-me-shorts-qubec-focus">Book tickets</a></strong></p></td>
<td width="50"><p>*Filmmaker Q&A to follow</p></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="Québec Focuschristchurch-2017">
<tbody>
<tr>
<td width="50"><p>Sun 19 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-quebec-focus/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="Québec Focusstewart-island-2017">
<tbody>
<tr>
<td width="50"><p>Wed 10 Jan</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="WellingtonCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington-cbd"><h3>Wellington CBD - Embassy Theatre</h3></a>
<table id="table--pink-stripes" summary="Québec Focuswellington-cbd-2017">
<tbody>
<tr>
<td width="50"><p>Fri 10 Nov</p></td>
<td width="50"><p>8:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-Quebec-Focus#cinemas=512&date=2017-11-10">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Tue 14 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-Quebec-Focus#cinemas=512&date=2017-11-14">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sat 18 Nov</p></td>
<td width="50"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-Quebec-Focus#cinemas=512&date=2017-11-18">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Devonport"></a>

<a href="http://www.showmeshorts.co.nz/programme/devonport"><h3>Devonport - The Vic</h3></a>
<table id="table--pink-stripes" summary="Québec Focusdevonport-2017">
<tbody>
<tr>
<td width="50"><p>Wed 1 Nov</p></td>
<td width="50"><p>7:30 pm
<strong><a href="https://www.thevic.co.nz/movie/show-me-shorts-qubec-focus">Book tickets</a></strong></p></td>
<td width="50"><p>*Filmmaker Q&A to follow</p></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>