<p>We live in a time of unprecedented mobility. Misunderstandings are bound to happen as we explore other lands, cultures and ideas. What happens when east meets west? When different world views clash? The moments explored in these short films are funny, scary, sad and sometimes all three at once.</p>

<p>Estimated total run time: 105 minutes. <br>
Rated M for violence, offensive language & drug use.</p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Rialto Cinemas Newmarket</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#WellingtonCBD">Wellington CBD - Embassy Theatre</option>
</select>
<h2>Big City</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/bigcity.jpg" />
<p>Drama, 9 mins, Australia 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Jordan Bond</p>
<p>Dir: Lachlan Ryan</p>
<p>Prod: Jarrod Theodore</p>
<div class="cf"></div><p>Lonely Vijay picks up a stray drunk, Chris, in a Melbourne taxi. During the ride they become friends as Chris experiences some of Vijay's nightly troubles, and Vijay learns to see the city in a new light.</p>

<hr class="sessionBreak">

<h2>A New Home</h2><a href="https://www.youtube.com/watch?v=Tx9JRE1EwBw" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/anewhome.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Thriller, 14 mins, Slovenia 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Žiga Virc</p>
<p>Writ/Prod: Boštjan Virc</p>
<div class="cf"></div><p>A woman moves into an empty new apartment block. Each day she passes a refugee tent city in the park. Her creeping fear and paranoia threaten to overwhelm her.</p>

<hr class="sessionBreak">

<h2>Not Yet</h2><a href="https://vimeo.com/192339576" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/notyet.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 15 mins, Islamic Republic of Iran 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Arian Vazirdaftari</p>
<p>Writ: Shadi Karamroudi</p>
<p>Prod: Majid Barzegar</p>
<div class="cf"></div><p>Two parents Skype their daughter, who is studying abroad, on her 21st birthday. Their unity is a sham, a fact the filmmaker exposes to us via clever use of split-screen cinematography.</p>

<hr class="sessionBreak">

<h2>Import        </h2><a href="https://vimeo.com/163084706" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/import.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Comedy, 17 mins, Netherlands</p>

<p>Dir/Writ: Ena Sendijarevic</p>
<p>Prod: Iris Otten, Sander van Meurs, Pieter Kuijpers</p>
<div class="cf"></div><p>A Bosnian refugee family ends up in a small village in the Netherlands after getting a residence permit in 1994. Absurd situations arise as they try to make this new world their home.        </p>

<hr class="sessionBreak">

<h2>A Drowning Man</h2><a href="https://vimeo.com/217990864/6af8450a85 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/adrowningman.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 15 mins, Denmark 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ/Prod: Mahdi Fleifel</p>
<p>Prod: Patrick Campbell, Signe Byrge Sørensen</p>
<div class="cf"></div><p>Alone and far from home, a Palestinian refugee in Greece looks for the means to get through his day. He is forced to make compromises to survive.        </p>

<hr class="sessionBreak">

<h2>Motel</h2><a href="https://www.facebook.com/226134091188155/videos/236191890182375/" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/motel.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 15 mins, New Zealand 
<p><strong>World Premiere</strong></p></p>

<p>Dir/Writ/Prod: Lauren Porteous</p>
<div class="cf"></div><p>A lonely rural motelier befriends one of her guests. For one night, the lives of someone who can't move, and another who can't stop, intersect.</p>

<hr class="sessionBreak">

<h2>La Madre Buena (The Good Mother) </h2><a href="https://vimeo.com/183095913" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/lamadrebuena(thegoodmother).jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Comedy, 6 mins, Mexico 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ/Prod: Sarah Clift</p>
<p>Prod: Madrefoca</p>
<div class="cf"></div><p>With her only child's birthday looming, a Mexican mother embarks upon an epic journey across land and through politics to find the right piñata for her son.</p>

<hr class="sessionBreak">

<h2>Wave</h2><a href="https://vimeo.com/214217381" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/wave.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 14 mins, Ireland 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Benjamin Cleary</p>
<p>Dir: TJ O'Grady Peyton</p>
<p>Prod: Rebecca Bourke</p>
<div class="cf"></div><p>A man wakes from a coma speaking a fully formed but unrecognisable language, baffling linguistic experts from around the globe.</p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Rialto Cinemas Newmarket</h3></a>
<table id="table--pink-stripes" summary="Worlds Collideauckland-central-2017">
<tbody>
<tr>
<td width="50"><p>Sun 29 Oct</p></td>
<td width="50"><p>5:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Worlds-Collide#cinemas=750&date=2017-10-29">Book tickets</a></strong></p></td>
<td width="50"><p>*World premiere screening with filmmaker intro</p></td>
</tr>
<tr>
<td width="50"><p>Tue 31 Oct</p></td>
<td width="50"><p>2:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Worlds-Collide#cinemas=751&date=2017-10-31">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Fri 3 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Worlds-Collide#cinemas=751&date=2017-11-03">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="Worlds Collidechristchurch-2017">
<tbody>
<tr>
<td width="50"><p>Mon 20 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-worlds-collide/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="Worlds Collidestewart-island-2017">
<tbody>
<tr>
<td width="50"><p>Wed 17 Jan</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="WellingtonCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington-cbd"><h3>Wellington CBD - Embassy Theatre</h3></a>
<table id="table--pink-stripes" summary="Worlds Collidewellington-cbd-2017">
<tbody>
<tr>
<td width="50"><p>Sun 12 Nov</p></td>
<td width="50"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Worlds-Collide#cinemas=512&date=2017-11-12">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Thu 16 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Worlds-Collide#cinemas=512&date=2017-11-16">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sun 19 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Worlds-Collide#cinemas=512&date=2017-11-19">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>