<p>It takes a lot of courage to be your own weird self. This goes double if you're facing extra challenges: like missing limbs, being deaf, or other people judging your sexual preferences. The characters we meet in this collection of short films are boldly taking on life, challenging us to question our tolerance and inspiring a generosity of spirit.</p>

<p>Estimated total run time: 102 minutes. <br>
Rated R16 for sex scenes, sexual content, offensive language & deals with suicide. </p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Rialto Cinemas Newmarket</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#WellingtonCBD">Wellington CBD - Embassy Theatre</option>
</select>
<h2>Turtles -- LarzRanda        </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/turtles-larzranda.jpg" />
<p>Music video, 4 mins, New Zealand</p>

<p>Dir: THUNDERLIPS</p>
<p>Prod: Al Wad, Alix Whittaker</p>
<div class="cf"></div><p>Transgender rapper LarzRanda lays his body bare in this simple but effective music video by THUNDERLIPS. A fitting accompaniment to the lyrics, which lay bare his soul and his story.</p>

<hr class="sessionBreak">

<h2>Tama        </h2><a href="https://www.youtube.com/watch?v=KDoJvmyH2II" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/tama.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Coming of age, 9 mins, New Zealand</p>

<p>Dir/Writ: Jared Flitcroft, Jack O'Donnell</p>
<p>Writ: David Hansen</p>
<p>Prod: Ashleigh Flynn</p>
<div class="cf"></div><p>During a fraught car trip, Tama, a deaf Maori boy, must rise up to confront his family. The story culminates in an electrifying silent performance of the haka.</p>

<hr class="sessionBreak">

<h2>The Promise of Piha</h2><a href="https://vimeo.com/129335895 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/thepromiseofpiha.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 16 mins, New Zealand</p>

<p>Dir/Writ: Hanelle Harris</p>
<p>Writ: Taofia Pelesasa</p>
<p>Prod: Jadin Leslie, Elspeth Grant</p>
<div class="cf"></div><p>A young Samoan man has been chosen as the next leader of his community, but first he must confess his true desire.</p>

<hr class="sessionBreak">

<h2>Boi        </h2><a href="https://vimeo.com/183643472" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/boi.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 15 mins, Belgium 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ/Prod: Anthony Nti</p>
<p>Writ: Chingiz Karibekov</p>
<p>Prod: RITCS (School of Arts)</p>
<div class="cf"></div><p>Two young Bulgarian brothers have to work for their uncle, who collects scrap metal in Belgium. Over the course of one day, their relationship is tested.</p>

<hr class="sessionBreak">

<h2>Each To Their Own </h2><a href="https://vimeo.com/174877453 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/eachtotheirown.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Coming of age, 20 mins, New Zealand</p>

<p>Dir/Writ: Maria Ines Manchego</p>
<p>Prod: Lani-rain Feltham</p>
<div class="cf"></div><p>A grieving teenage girl attempts to escape her suburban family by allowing herself to be drawn into a charismatic church.</p>

<hr class="sessionBreak">

<h2>Do You Like Me Like This?</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/doyoulikemelikethis.jpg" />
<p>Comedy, 7 mins, New Zealand 
<p><strong>World Premiere</strong></p></p>

<p>Dir/Writ: Miryam Jacobi </p>
<p>Prod: Jack Barry</p>
<div class="cf"></div><p>A girl is waiting for her bus in suburbia when a boy comes along. An unconventional romance unfolds as the girl tries to get his attention.</p>

<hr class="sessionBreak">

<h2>Saatanan Kanit (Fucking Bunnies)</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/saatanankanit(fuckingbunnies).jpg" />
<p>Comedy, 17 mins, Finland 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Teemu Niukkanen</p>
<p>Writ: Antti Toivonen</p>
<p>Prod: Kaisla Viitala, Daniel Kuitunen, Tero Tamminen</p>
<div class="cf"></div><p>Raimo's comfy middle-class bubble is burst when a Satan-worshipping sex cult moves in next door. This pushes us all to examine how tolerant we really are.        </p>

<hr class="sessionBreak">

<h2>Downside Up        </h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/downsideup.jpg" />
<p>Fantasy, 15 mins, Belgium 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Peter Ghesquiere</p>
<p>Prod: Hendrik Verthé, Kobe Van Steenberghe, Frank Van Passel</p>
<div class="cf"></div><p>Imagine a world where everybody has Down's Syndrome. One day, a boy is born who is different -- but 'normal' from our outside perspective. This is his story, a warm tale of love and diversity.</p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Rialto Cinemas Newmarket</h3></a>
<table id="table--pink-stripes" summary="Daring to be Differentauckland-central-2017">
<tbody>
<tr>
<td width="50"><p>Tue 31 Oct</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Daring-To-Be-Different#cinemas=751&date=2017-10-31">Book tickets</a></strong></p></td>
<td width="50"><p>*World premiere screening with filmmaker intro</p></td>
</tr>
<tr>
<td width="50"><p>Thu 2 Nov</p></td>
<td width="50"><p>2:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Daring-To-Be-Different#cinemas=751&date=2017-11-02">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Fri 3 Nov</p></td>
<td width="50"><p>8:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Daring-To-Be-Different#cinemas=751&date=2017-11-03">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="Daring to be Differentchristchurch-2017">
<tbody>
<tr>
<td width="50"><p>Sat 18 Nov</p></td>
<td width="50"><p>8:30 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-daring-to-be-different/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="Daring to be Differentstewart-island-2017">
<tbody>
<tr>
<td width="50"><p>Sat 20 Jan</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="WellingtonCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington-cbd"><h3>Wellington CBD - Embassy Theatre</h3></a>
<table id="table--pink-stripes" summary="Daring to be Differentwellington-cbd-2017">
<tbody>
<tr>
<td width="50"><p>Fri 10 Nov</p></td>
<td width="50"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Daring-To-Be-Different#cinemas=512&date=2017-11-10">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Tue 14 Nov</p></td>
<td width="50"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Daring-To-Be-Different#cinemas=512&date=2017-11-14">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Fri 17 Nov</p></td>
<td width="50"><p>8:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Daring-To-Be-Different#cinemas=512&date=2017-11-17">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Mon 20 Nov</p></td>
<td width="50"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Daring-To-Be-Different#cinemas=512&date=2017-11-20">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>