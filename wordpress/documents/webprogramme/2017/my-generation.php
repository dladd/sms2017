<p>Embrace your inner child with this family-friendly selection of short films. Let us take you on an adventure into space, across a Jurassic world, through the New Zealand bush, and inside a 2D poster. We'll attend a children's birthday party gone feral, and escape from a city made of cardboard. Suitable for adults and children of all ages.</p>

<p>Estimated total run time: 64 minutes. <br>
Rated PG for violence & scary scenes. </p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Rialto Cinemas Newmarket</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#WellingtonCBD">Wellington CBD - Embassy Theatre</option>
</select>
<h2>Fire in Cardboard City</h2><a href="https://www.youtube.com/watch?v=O4TKkGk2y-I " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/fireincardboardcity.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 9 mins, New Zealand</p>

<p>Dir/Writ: Phil Brough</p>
<p>Writ/Prod: Matt Heath</p>
<p>Prod: Orlando Stewart</p>
<div class="cf"></div><p>When a city made entirely from cardboard catches fire, it is up to the local fire chief and his brave deputies to save Cardboard City and its citizens from impending doom.</p>

<hr class="sessionBreak">

<h2>All the Marbles</h2><a href="https://vimeo.com/200286382" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/allthemarbles.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Fantasy, 18 mins, USA 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Michael Swingler</p>
<p>Writ/Prod: Carl Petersen</p>
<div class="cf"></div><p>A little boy challenges a villainous bully to a game of marbles in a bizarre and fantastical world where marbles are as precious as gold.</p>

<hr class="sessionBreak">

<h2>In a Heartbeat</h2><a href="https://www.youtube.com/watch?v=0pauY5cgRU4" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/inaheartbeat.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 4 mins, USA 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ/Prod: Beth David</p>
<p>Dir/Writ/Prod: Esteban Bravo</p>
<div class="cf"></div><p>A young gay boy runs the risk of being outed by his own heart after it pops out of his chest to chase down the boy of his dreams.        </p>

<hr class="sessionBreak">

<h2>Little Things -- Miller Yule</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/littlethings-milleryule.jpg" />
<p>Music video, 4 mins, New Zealand</p>

<p>Writ: Lee Stapleton</p>
<p>Prod: Lauren Watling, Jess Rose</p>
<div class="cf"></div><p>The music video for Miller Yule's song 'Little Things' is as charming and full of warmth as the song. A couple of children's party entertainers are mocked and ignored by their audience.        Dir/Writ: Craig Murray</p>

<hr class="sessionBreak">

<h2>Citipati</h2><a href="https://vimeo.com/169523362" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/citipati.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Science fiction, 7 mins, Germany 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Andreas Feix</p>
<p>Prod: Francesco Faranna</p>
<div class="cf"></div><p>A dinosaur tries to survive a cataclysmic meteorite strike on his planet. A beautifully realised and emotional story about the fragility of life in our universe.</p>

<hr class="sessionBreak">

<h2>Little Big Bang</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/littlebigbang.jpg" />
<p>Animation, 3 mins, New Zealand 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Jack Niederer</p>
<p>Prod: Aaron Wright</p>
<div class="cf"></div><p>A scared young alien crash-lands on a desolate and unfamiliar planet. It has to survive the night and overcome its fear to make a new friend in a most unexpected way.</p>

<hr class="sessionBreak">

<h2>Post No Bills</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/postnobills.jpg" />
<p>Animation, 5 mins, Canada 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ/Prod: Robin Hays</p>
<p>Dir: Andy Poon</p>
<div class="cf"></div><p>On a wall plastered with posters, Noodle Boy must make his way through a series of obstacles and challenges in order to save his crush, Miss Fortune, from the city's clean-up crew.</p>

<hr class="sessionBreak">

<h2>Possum</h2><a href="https://vimeo.com/203547316 " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/possum.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Adventure, 16 mins, New Zealand</p>

<p>Dir/Writ: Dave Whitehead</p>
<p>Prod: Paul Murphy, Sadie Wilson</p>
<div class="cf"></div><p>Two young brothers accompany their lumberjack father to a forest campsite. Together they hatch a plan to go hunting for a notorious possum named Scar.</p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Rialto Cinemas Newmarket</h3></a>
<table id="table--pink-stripes" summary="My Generationauckland-central-2017">
<tbody>
<tr>
<td width="50"><p>Sun 29 Oct</p></td>
<td width="50"><p>3:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Smsff-My-Generation#cinemas=750&date=2017-10-29">Book tickets</a></strong></p></td>
<td width="50"><p>*NZ premiere screening with filmmaker intro</p></td>
</tr>
<tr>
<td width="50"><p>Mon 30 Oct</p></td>
<td width="50"><p>2:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Smsff-My-Generation#cinemas=751&date=2017-10-30">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sat 4 Nov</p></td>
<td width="50"><p>2:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Smsff-My-Generation#cinemas=751&date=2017-11-04">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="My Generationchristchurch-2017">
<tbody>
<tr>
<td width="50"><p>Sat 18 Nov</p></td>
<td width="50"><p>1:00 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-my-generation/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="My Generationstewart-island-2017">
<tbody>
<tr>
<td width="50"><p>Wed 3 Jan</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="WellingtonCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington-cbd"><h3>Wellington CBD - Embassy Theatre</h3></a>
<table id="table--pink-stripes" summary="My Generationwellington-cbd-2017">
<tbody>
<tr>
<td width="50"><p>Sat 11 Nov</p></td>
<td width="50"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Sessions#cinemas=512&date=2017-11-11&movies=11792">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Wed 15 Nov</p></td>
<td width="50"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Sessions#cinemas=512&date=2017-11-15&movies=11792">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sun 19 Nov</p></td>
<td width="50"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Sessions#cinemas=512&date=2017-11-19&movies=11792">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>