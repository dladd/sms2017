<h3 class="programme">3-24 Jan</h3>
<h2 class="programme">Bunkhouse Theatre</h2>
<p>
10 Main Road<br>
Stewart Island<br>
Ph: 027 867 9381<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
$13 Seniors/Students/Film Industry Guilds<br>
$10 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="stewart-island-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Wed 3 Jan</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/my-generation"><strong>My Generation</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 6 Jan</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/light-in-the-dark"><strong>Light in the Dark</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 10 Jan</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/québec-focus"><strong>Québec Focus</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 13 Jan</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/made-to-move"><strong>Made to Move</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 17 Jan</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/worlds-collide"><strong>Worlds Collide</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sat 20 Jan</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/daring-to-be-different"><strong>Daring to be Different</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Wed 24 Jan</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>