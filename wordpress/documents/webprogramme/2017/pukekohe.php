<h3 class="programme">4 Nov</h3>
<h2 class="programme"><a href="http://www.pukekohecinemas.co.nz">Cinema 3 Pukekohe</a></h2>
<p>
85 Edinburgh Street<br>
Pukekohe<br>
Ph: 09 237 0216<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$17 General Admission<br>
$15 Students/Film Industry Guilds<br>
$11 Seniors/Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="pukekohe-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 4 Nov</p></td>
<td width="25"><p>6:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler*</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<p>*Festival Director will introduce this session</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>