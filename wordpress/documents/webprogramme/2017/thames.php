<h3 class="programme">28 Nov</h3>
<h2 class="programme"><a href="http://www.cinemathames.co.nz">Embassy Cinemas</a></h2>
<p>
708 Pollen Street<br>
Thames<br>
Ph: 07 868 6602<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
$12 Seniors/Students/Film Industry Members<br>
$10 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="thames-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Tue 28 Nov</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>