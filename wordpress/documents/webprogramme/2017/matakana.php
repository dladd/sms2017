<h3 class="programme">18 Nov</h3>
<h2 class="programme"><a href="http://www.matakanacinemas.co.nz">Matakana Cinemas</a></h2>
<p>
Matakana Village, 2 Matakana Valley Road<br>
Matakana<br>
Ph: 09 423 0218<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$16 General Admission<br>
$14 Students/Film Industry Guilds<br>
$13 Seniors<br>
$11 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="matakana-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 18 Nov</p></td>
<td width="25"><p>6:30 pm
<strong><a href="https://ticketing.oz.veezi.com/purchase/13489?siteToken=NeQbpcRv90OQr%2fEjNRpl1g%3d%3d">Book tickets</a></strong></p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler*</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<p>*Festival Director will introduce this session</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>