<p>Stories convey coded messages and knowledge that can help us to navigate our path. The short films in this section of the programme tell of new bonds forged within fractured whanau, and the healing balm of friendship prescribed for loss and loneliness. They give survival tips for the impending apocalypse. They offer a glimpse of hope, whether by torchlight, lamplight or exploding firework.</p>

<p>Estimated total run time: 100 minutes. <br>
Rated M for offensive language. </p>

<select onchange="location = this.options[this.selectedIndex].value;" name="location-dropdown">
<option value="">(Select cinema for times and tickets...)</option>
<option value="#AucklandCentral">Auckland Central - Rialto Cinemas Newmarket</option>
<option value="#Christchurch">Christchurch - Alice Cinematheque</option>
<option value="#StewartIsland">Stewart Island - Bunkhouse Theatre</option>
<option value="#WellingtonCBD">Wellington CBD - Embassy Theatre</option>
</select>
<h2>Tree</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/tree.jpg" />
<p>Drama, 16 mins, New Zealand</p>

<p>Dir/Writ: Lauren Jackson</p>
<p>Prod: Jeremy Macey, Andrew Cochrane</p>
<div class="cf"></div><p>One evening Alisi climbs a huge tree and won't come down. As night falls and we discover her secret, Alisi must choose whether to follow tradition or find a new path -- with or without her family.</p>

<hr class="sessionBreak">

<h2>Ambience</h2><a href="https://vimeo.com/229096485" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/ambience.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Science fiction, 13 mins, South Korea/USA 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Ji Hyun Kim        </p>
<p>Writ: Julia Yu        </p>
<p>Prod: Nikolai Metin</p>
<div class="cf"></div><p>Junkyu works for HereForYou, an app-based service company. He travels around the city at night on a scooter to help people with a variety of tasks.        </p>

<hr class="sessionBreak">

<h2>Baby?</h2><a href="https://vimeo.com/221048246" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/baby.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Drama, 12 mins, New Zealand 
<p><strong>World Premiere</strong></p></p>

<p>Dir/Writ/Prod: Mark Prebble</p>
<p>Writ/Prod: Marion Prebble        </p>
<p>Prod: Ruby Reihana-Wilson</p>
<div class="cf"></div><p>Isabelle is determined not to spend her 65th birthday alone. She invites herself into the home of her new neighbour, a struggling mum in the middle of a nervous breakdown.</p>

<hr class="sessionBreak">

<h2>Waiting</h2><a href="https://www.youtube.com/watch?v=JHFC9VrzC0c " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/waiting.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Coming of age, 12 mins, New Zealand</p>

<p>Dir/Prod: Amberley Jo Aumua</p>
<p>Writ: Samuel Kamu</p>
<div class="cf"></div><p>Two best friends become brothers as they wait for a life-changing phone call. This film won two awards at NZIFF 2017.</p>

<hr class="sessionBreak">

<h2>Maria</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/maria.jpg" />
<p>Drama, 14 mins, New Zealand        </p>

<p>Dir: Jeremiah Tauamiti</p>
<p>Writ: Taofia Pelesasa </p>
<p>Prod: Karin Williams</p>
<div class="cf"></div><p>An ailing Polynesian matriarch must find the strength to lead her family one last time.</p>

<hr class="sessionBreak">

<h2>The Last One        </h2><a href="https://www.youtube.com/watch?v=qKaEHTIu7qM&feature=youtu.be " target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/thelastone.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Science fiction, 15 mins, UK 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir: Andrea Banjanin        </p>
<p>Writ/Prod: Paul Bailey </p>
<p>Prod: Justine Priestley</p>
<div class="cf"></div><p>Every living thing on the planet has disappeared. No warning. No apparent reason. Every human and animal, gone. Except Matthew.</p>

<hr class="sessionBreak">

<h2>Happy Birthday</h2><img class="overlaybase alignright " src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/happybirthday.jpg" />
<p>Comedy, 6 mins, New Zealand 
<p><strong>NZ Premiere</strong></p></p>

<p>Writ/Prod: Dominic Hoey</p>
<div class="cf"></div><p>A trio of flatmates inadvertently kickstart the apocalypse by abusing a new Facebook feature.        Dir/Prod: Eddy Fifield        </p>

<hr class="sessionBreak">

<h2>Catherine</h2><a href="https://vimeo.com/169825442" target="_blank">
<div id="overlaybase" class="alignright">
  <img title="watch trailer" src="http://www.showmeshorts.co.nz/images/webprogramme/films/2017/catherine.jpg" target="_blank"/>
  <img src="http://www.showmeshorts.co.nz/images/webprogramme/playButtonSm.png" class="overlaytop"/>
</div>
</a>
<p>Animation, 12 mins, Belgium 
<p><strong>NZ Premiere</strong></p></p>

<p>Dir/Writ: Britt Raes </p>
<p>Prod: Karim Rhellam</p>
<div class="cf"></div><p>Catherine is a girl who loves pets! Maybe a little too much? Most of all, she loves her cat. As Catherine grows up, her cat becomes her whole life, leaving little room for other humans.        </p>
<hr class="sessionBreak">

<h2>Screenings</h2>
<a name="AucklandCentral"></a>

<a href="http://www.showmeshorts.co.nz/programme/auckland-central"><h3>Auckland Central - Rialto Cinemas Newmarket</h3></a>
<table id="table--pink-stripes" summary="Light in the Darkauckland-central-2017">
<tbody>
<tr>
<td width="50"><p>Mon 30 Oct</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Light-In-The-Dark#cinemas=751&date=2017-10-30">Book tickets</a></strong></p></td>
<td width="50"><p>*World premiere screening with filmmaker intro</p></td>
</tr>
<tr>
<td width="50"><p>Fri 3 Nov</p></td>
<td width="50"><p>2:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Light-In-The-Dark#cinemas=751&date=2017-11-03">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Sat 4 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://www.rialto.co.nz/Movie/Show-Me-Shorts-2017-Light-In-The-Dark#cinemas=751&date=2017-11-04">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="Christchurch"></a>

<a href="http://www.showmeshorts.co.nz/programme/christchurch"><h3>Christchurch - Alice Cinematheque</h3></a>
<table id="table--pink-stripes" summary="Light in the Darkchristchurch-2017">
<tbody>
<tr>
<td width="50"><p>Thu 16 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-light-in-the-dark/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Tue 21 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://alice.co.nz/movies/show-me-shorts-light-in-the-dark/">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="StewartIsland"></a>

<a href="http://www.showmeshorts.co.nz/programme/stewart-island"><h3>Stewart Island - Bunkhouse Theatre</h3></a>
<table id="table--pink-stripes" summary="Light in the Darkstewart-island-2017">
<tbody>
<tr>
<td width="50"><p>Sat 6 Jan</p></td>
<td width="50"><p>7:30 pm</p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><a name="WellingtonCBD"></a>

<a href="http://www.showmeshorts.co.nz/programme/wellington-cbd"><h3>Wellington CBD - Embassy Theatre</h3></a>
<table id="table--pink-stripes" summary="Light in the Darkwellington-cbd-2017">
<tbody>
<tr>
<td width="50"><p>Sat 11 Nov</p></td>
<td width="50"><p>8:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Light-In-The-Dark#cinemas=512&date=2017-11-11">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Mon 13 Nov</p></td>
<td width="50"><p>4:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Light-In-The-Dark#cinemas=512&date=2017-11-13">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Wed 15 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Light-In-The-Dark#cinemas=512&date=2017-11-15">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
<tr>
<td width="50"><p>Tue 21 Nov</p></td>
<td width="50"><p>6:30 pm
<strong><a href="https://www.eventcinemas.co.nz/Movie/Show-Me-Shorts-2017-Light-In-The-Dark#cinemas=512&date=2017-11-21">Book tickets</a></strong></p></td>
<td width="50"></td>
</tr>
</div>
</tbody>
</table><div class="sms button" style="text-align: center; padding-top: 50px;"><h3><a class="sms button" href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>