<h3 class="programme">11-12 Nov</h3>
<h2 class="programme"><a href="http://www.domecinema.co.nz">Dome Cinema, Poverty Bay Club building</a></h2>
<p>
Corner Childers Road and Customhouse Street<br>
Gisborne<br>
<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$14 General Admission<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="gisborne-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sat 11 Nov</p></td>
<td width="25"><p>7:00 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
<tr>
<td width="25"><p>Sun 12 Nov</p></td>
<td width="25"><p>7:00 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>