<h3 class="programme">29 Oct</h3>
<h2 class="programme"><a href="http://www.waihekecinema.co.nz">Waiheke Island Community Cinema</a></h2>
<p>
Artworks, Oneroa<br>
Waiheke Island<br>
Ph: 09 372 4240<br>
</p>
<h3 class="programme">Ticket Prices:</h3>
<p>
$15 General Admission<br>
$11 Students/Seniors/Film Industry Guilds<br>
$8 Children<br>
</p>


<h3 class="programme">Screenings</h3><table id="table--pink-stripes" summary="waiheke-island-2017">
<tbody>
<div>
<tr>
<td width="25"><p>Sun 29 Oct</p></td>
<td width="25"><p>7:30 pm</p></td>
<td width="25"><p><a href="http://www.showmeshorts.co.nz/programme/the-sampler"><strong>The Sampler*</strong></a></p></td>
</tr>
</div>
</tbody>
</table>
<p>*World premiere screening with filmmaker intro</p>

<div style="text-align: center; padding-top: 50px;"><h3><a href="http://www.showmeshorts.co.nz/programme/">Back to Programme</a></h3></div>